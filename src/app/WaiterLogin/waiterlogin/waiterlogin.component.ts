import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-waiterlogin',
  templateUrl: './waiterlogin.component.html',
  styleUrls: ['./waiterlogin.component.css'],
})
export class WaiterloginComponent implements OnInit {
  constructor(
    private router: Router,
    private sharedserviceService: SharedserviceService
  ) {}

  userName: Storage = sessionStorage;
  uName: any;
  password: any;

  ngOnInit(): void {}

  login() {
    this.sharedserviceService
      .loginChecking(this.uName, this.password)
      .subscribe((data: any) => {
        console.log(data);
        if (data == null) {
          console.log('Invalid username and password!!');
          alert('Invalid username or password!!');
        } else {
          console.log('Login Successful!!');
          this.userName.setItem('username', JSON.stringify(this.uName));
          this.router.navigate(['KotComponent']);
        }
      });
  }
}
