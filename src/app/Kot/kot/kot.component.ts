import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { AddKotTable } from 'src/app/modelclass/add-kot-table';
import { AddKotWaiter } from 'src/app/modelclass/add-kot-waiter';
import { CategoryMst } from 'src/app/modelclass/category-mst';
import { ItemMst } from 'src/app/modelclass/item-mst';
import { KotCategoryInventoryMst } from 'src/app/modelclass/kot-category-inventory-mst';
import { KotTableDtl } from 'src/app/modelclass/kot-table-dtl';
import { KotTableHdr } from 'src/app/modelclass/kot-table-hdr';
import { datas } from 'src/app/Pop-Up-Windows/search-pop-up/search-pop-up.component';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-kot',
  templateUrl: './kot.component.html',
  styleUrls: ['./kot.component.css'],
})
export class KotComponent implements OnInit {
  @ViewChild('quantity')
  quantity!: ElementRef;
  categoryMst: CategoryMst[] = [];
  itemMstDtl: ItemMst = new ItemMst();
  addKotTable: AddKotTable[] = [];
  addKotWaiter: AddKotWaiter[] = [];
  kotCategoryInventoryMst: KotCategoryInventoryMst[] = [];
  catId: any;
  itmName: any;
  kotCategory: KotCategoryInventoryMst[] = [];
  kotItem: KotCategoryInventoryMst[] = [];
  selectedCategory: any;
  itemId: any;
  kotTablehdr: KotTableHdr = new KotTableHdr();
  kotTableDtl: KotTableDtl = new KotTableDtl();
  tableName: any;
  date: Date = new Date();
  time: any;
  itemRate: any;
  productQty: number = 0;
  customizationRate: number = 0;
  customization: any = null;
  waiterName: any;
  kotTableDtlList: KotTableDtl[] = [];
  totalItems: any;
  waiterId: any;
  kotPreviousCategory: KotCategoryInventoryMst[] = [];
  lastPush: boolean = false;
  showTable: any[] = [];
  itemName: any;
  isDisable: boolean = false;
  userName: Storage = sessionStorage;
  uname: any;
  constructor(
    private sharedserviceService: SharedserviceService,
    public fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.userName.getItem('username');
    if (this.userName == null) {
      return;
    }
    this.uname = JSON.parse(this.userName.getItem('username'));
    console.log(this.uname);
    console.log('Initialized');
    this.sharedserviceService.getKotCategory().subscribe((data) => {
      this.kotCategoryInventoryMst = data;
      // for (var z of this.kotCategoryInventoryMst) {
      for (let i = 0; i <= this.kotCategoryInventoryMst.length - 1; i++) {
        if (this.kotCategoryInventoryMst[i].leaf === 'N') {
          this.kotCategory.push(this.kotCategoryInventoryMst[i]);
          // this.kotPreviousCategory.push(this.kotCategoryInventoryMst[i]);
        } else if (this.kotCategoryInventoryMst[i].leaf === 'Y') {
          this.kotItem.push(this.kotCategoryInventoryMst[i]);
        }
      }
    });

    this.sharedserviceService.getAddKotTable().subscribe((data) => {
      this.addKotTable = data;
    });

    this.sharedserviceService.getAddKotWaiter().subscribe((data) => {
      this.addKotWaiter = data;
    });
  }

  getItemMst(categoryId: any) {
    this.catId = categoryId;
    this.sharedserviceService
      .getItemMstByCategoryId(this.catId)
      .subscribe((data) => {
        this.itemMstDtl = data;
      });
  }

  getTableDetails(tableName: any) {
    this.tableName = tableName;
    this.kotTableDtlList.length = 0;
    this.isDisable = false;
    this.kotTablehdr = new KotTableHdr();

    this.sharedserviceService
      .getKotTableDtlByTableName(tableName)
      .subscribe((data) => {
        if (data.length > 0) {
          alert('Selected Table is already in use!!!');
        }
        for (let i = 0; i <= data.length - 1; i++) {
          this.kotTableDtlList.push(data[i]);
          if (data[i].kotNumber != null) {
            this.isDisable = true;
          }
          this.kotTablehdr = data[0].kotTableHdr;
          this.kotTablehdr.id = data[0].kotTableHdr.id;
          console.log(this.kotTablehdr);
          console.log(this.kotTablehdr.id);
        }
      });
  }

  getKotItemByCategory(childItem: any) {
    if (this.tableName == null) {
      alert('Please select the table name first!!!');
      return;
    }
    console.log('CATEGORYCLICKED');
    this.kotCategoryInventoryMst.length = 0;
    this.kotItem.length = 0;
    // for (let tem of this.kotCategory) {
    //   this.kotPreviousCategory.push(tem);
    // }
    this.kotCategory.length = 0;
    this.selectedCategory = childItem;
    this.sharedserviceService
      .getKotItemByCategory(this.selectedCategory)
      .subscribe((data) => {
        this.kotCategoryInventoryMst = data;
        console.log(this.kotCategoryInventoryMst);

        // if (typeof this.kotCategoryInventoryMst[0].leaf != 'undefined') {
        if (this.kotCategoryInventoryMst.length > 0) {
          console.log('array size grter thn 0');
          for (let i = 0; i <= this.kotCategoryInventoryMst.length - 1; i++) {
            if (i == this.kotCategoryInventoryMst.length - 1) {
              this.kotPreviousCategory.push(
                this.kotCategoryInventoryMst[i].parentItem
              );
            }
            if (this.kotCategoryInventoryMst[i].leaf === 'N') {
              this.kotCategory.push(this.kotCategoryInventoryMst[i]);
            } else if (this.kotCategoryInventoryMst[i].leaf === 'Y') {
              // this.sharedserviceService
              //   .getItemMstByCategoryId(
              //     this.kotCategoryInventoryMst[i].childItem
              //   )
              //   .subscribe((data) => {
              //     this.itemMstDtl = data;
              this.kotItem.push(this.kotCategoryInventoryMst[i]);
              // });
            }
          }
          console.log(this.kotPreviousCategory);
        } else {
          console.log('array size les thn 0');
          return;
        }
      });
  }

  setItemNameForSave(childItem: any) {
    if (this.tableName == null) {
      alert('Please select the table name first!!!');
      return;
    }
    this.itmName = childItem;
    this.sharedserviceService
      .getItemMstByName(this.itmName)
      .subscribe((data) => {
        this.itemMstDtl = data;
        this.itemName = this.itemMstDtl.itemName;
        this.itemId = this.itemMstDtl.id;
        this.itemRate = this.itemMstDtl.standardPrice;
      });

    setTimeout(() => this.quantity.nativeElement.focus());
  }
  setWaiterName(waiterid: any) {
    this.waiterId = waiterid;
  }
  saveKotDetails() {
    this.kotTablehdr.tableName = this.tableName;
    this.kotTablehdr.tableStatus = 'RUNNING';
    this.kotTablehdr.billedStatus = 'NO';
    this.kotTablehdr.waiterName = this.uname;

    this.sharedserviceService
      .saveKotTableHdr(this.kotTablehdr)
      .subscribe((data) => {
        this.kotTablehdr = data;
        if (null != this.kotTablehdr.id) {
          this.kotTableDtl.itemId = this.itemId;

          this.kotTableDtl.kotTableHdr = this.kotTablehdr;
          this.kotTableDtl.customization = this.customization;
          this.kotTableDtl.customizationRate = this.customizationRate;
          if (this.productQty == null || this.productQty == 0) {
            alert('Please enter the quantity!!!');
            return;
          }
          this.kotTableDtl.qty = this.productQty;
          this.kotTableDtl.rate = this.itemRate;
          this.sharedserviceService
            .saveKotTableDtl(this.kotTableDtl)
            .subscribe((data) => {
              console.log(data.id);
              this.kotTableDtl = new KotTableDtl();
              this.sharedserviceService
                .getKotTableDtlByHdr(this.kotTablehdr)
                .subscribe((data) => {
                  console.log(data[0].id);
                  this.kotTableDtlList = data;

                  this.totalItems = this.kotTableDtlList.length;
                });
            });
        }
      });
  }

  PrintKOT() {
    this.sharedserviceService
      .printKot(this.kotTablehdr.id, this.kotTablehdr)
      .subscribe((data) => {
        console.log(data);
        if (null != data) {
          alert('Saved Succesfully!!!');
        }
      });
    this.kotTablehdr = new KotTableHdr();
    this.kotTableDtlList.length = 0;
    this.tableName = '';
    this.productQty = 0;
    this.customization = '';
    this.customizationRate = 0;
  }

  PrintBill() {
    if (null == this.kotTablehdr) {
      alert('Please select the running table for billing!!!');
      return;
    }
    if (this.tableName == '') {
      alert('Please select the running table for billing!!!');
      return;
    }
    this.sharedserviceService
      .printBill(this.kotTablehdr.id, this.kotTablehdr)
      .subscribe((data) => {
        console.log(data);
        if (null != data) {
          alert('Billing Process Started!!!');
        }
      });

    this.kotTablehdr = new KotTableHdr();
    this.kotTableDtlList.length = 0;
    this.tableName = '';
    this.productQty = 0;
    this.customization = '';
    this.customizationRate = 0;
  }

  backToPrevious() {
    console.log('backToPrevious');
    this.kotCategory.length = 0;
    this.kotItem.length = 0;

    let category_value = this.kotPreviousCategory.pop();
    // if (this.kotPreviousCategory.length >= 0) {
    if (null != category_value) {
      this.sharedserviceService
        .getKotItemByCategory(category_value)
        .subscribe((data) => {
          console.log(data);
          for (let i = 0; i <= data.length - 1; i++) {
            if (data[i].leaf === 'N') {
              this.kotCategory.push(data[i]);
            } else if (data[i].leaf === 'Y') {
              this.kotItem.push(data[i]);
            }
          }
        });
    } else {
      this.sharedserviceService.getKotCategory().subscribe((data) => {
        // this.kotCategoryInventoryMst = data;
        // this.kotItem.length = 0;
        for (let i = 0; i <= data.length - 1; i++) {
          if (data[i].leaf === 'N') {
            this.kotCategory.push(data[i]);
          } else if (data[i].leaf === 'Y') {
            this.kotItem.push(data[i]);
          }
        }
      });
    }
  }

  deleteItem(kotTableDtl: KotTableDtl) {
    console.log(kotTableDtl.id);
    this.sharedserviceService.deleteItem(kotTableDtl).subscribe((data) => {
      console.log(data);
      this.kotTableDtlList.length = 0;
      this.totalItems = 0;
      this.sharedserviceService
        .getKotTableDtlByHdr(this.kotTablehdr)
        .subscribe((data) => {
          console.log(data);
          this.kotTableDtlList = data;

          this.totalItems = this.kotTableDtlList.length;
        });
    });
  }
}
