import { DatePipe } from '@angular/common';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import {
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment.prod';
import { PurchaseDtl } from '../modelclass/purchase-dtl';
import { PurchaseHdr } from '../modelclass/purchase-hdr';
import { Summary } from '../modelclass/summary';
import { Supplier } from '../modelclass/supplier';
import {
  datas,
  SearchPopUpComponent,
} from '../Pop-Up-Windows/search-pop-up/search-pop-up.component';
import { SupplierPopUpComponent } from '../Pop-Up-Windows/supplier-pop-up/supplier-pop-up.component';
import { ProductSearchServiceService } from '../services/product-search-service.service';
import { SaleOrderPopupService } from '../services/sale-order-popup.service';
import { SharedserviceService } from '../services/sharedservice.service';

@Component({
  selector: 'app-purchase-window',
  templateUrl: './purchase-window.component.html',
  styleUrls: ['./purchase-window.component.css'],
})
export class PurchaseWindowComponent implements OnInit {
  supp: Supplier = new Supplier();
  itm: datas = new datas();
  itemByName: datas[] = [];
  itemByReqParam:datas=new datas;
  // supplier input variable
  branchCode: any = environment.myBranch;
  rowseletion = false;
  receiveSupp: any;
  supName: any;
  suppGST: any;
  supID: any;
  supInvDate: any;
  ourVoucherDate: any;
  narrate: any;
  supInvNo: any;
  vouchNo: any;
  invTotal: any;
  invTot: number = 0;
  ponumber: any;
  selectSuppName: any;
  // item input variable
  receiveItm: any;
  itmName: any;
  itmBarCode: any;
  itmCess: number = 0.0;
  itmDiscount: any;
  itmTaxRate: number = 0.0;
  itmMrp: any;
  batch: any;
  itmId: any;
  purchaseDtlArray: PurchaseDtl[] = [];
  newAmount: number = 0.0;
  cessAmt: number = 0.0;
  taxAmt: number = 0.0;
  qty: number = 0.0;
  rate: number = 0.0;
  manDate: any;
  expDate: any;
  expiry: Boolean = true;
  store: any;
  discountAmount: any;
  totalAmount: any;
  qtyTotal: any;
  amtTotal: any;
  discTotal: any;
  expTotal: any;
  taxTotal: any;
  cessTotal: any;
  totalBfrTax: any;
  grandTotal: any;
  expeTotal: any;

  itmserial: number = 1;

  purchaseDtl: PurchaseDtl = new PurchaseDtl();
  purchaseHdr: PurchaseHdr = new PurchaseHdr();
  purchaseSummary:Summary=new Summary;
  // getSummary:Summary[]=[];
  constructor(
    public addDialog1: MatDialog,
    public addDialog: MatDialog,
    private sharedserviceService: SharedserviceService,
    public datepipe: DatePipe,
    private router: Router,
    public productSearchServiceService: ProductSearchServiceService,
    private saleOrderPopupService: SaleOrderPopupService
  ) {}

  ngOnInit(): void {
    this.receiveSupplierDtls();
    this.receiveItemDtls();
  }

  //popup for supplier in purchase
  // Supplier popup open .............................................
  onKeypressEventSupplier() {
    const dialogConfigAdd1 = new MatDialogConfig();
    dialogConfigAdd1.disableClose = false;
    dialogConfigAdd1.autoFocus = true;
    // dialogConfigAdd1.position = { right: `10px`, top: `20px` }
    dialogConfigAdd1.width = '80%';
    dialogConfigAdd1.height = '70%';

    this.addDialog1.open(SupplierPopUpComponent, dialogConfigAdd1);

    this.addDialog1.afterAllClosed.subscribe((result: any) => {
      this.supp = result;
      console.log('supplier popup window' + this.supp);
    });
  }
  // Supplier popup get .............................................
  receiveSupplierDtls() {
    console.log('receiveSupplierDtls');
    this.saleOrderPopupService.getSupplierDtls().subscribe((data: any) => {
      this.receiveSupp = data;
      this.supName = this.receiveSupp.accountName;
      this.suppGST = this.receiveSupp.partyGst;
      this.supID = this.receiveSupp.id;
      console.log(this.supName + 'supplier name');
    });
  }
  // Item popup open .............................................
  onKeypressEventItem(event: any) {
    const dialogConfigAdd = new MatDialogConfig();
    dialogConfigAdd.disableClose = false;
    dialogConfigAdd.autoFocus = true;
    dialogConfigAdd.width = '80%';
    dialogConfigAdd.height = '70%';
    this.addDialog.open(SearchPopUpComponent, dialogConfigAdd);
  }
  // Item popup get .............................................
  receiveItemDtls() {
    console.log('receiveItemDtls');
    this.saleOrderPopupService.getItemDtls().subscribe((data: any) => {
      this.receiveItm = data;

      this.itmName = this.receiveItm.itemName;
      this.itmBarCode = this.receiveItm.barCode;
      this.itmCess = this.receiveItm.cess;
      this.itmDiscount = this.receiveItm.itemDiscount;
      this.itmTaxRate = this.receiveItm.taxRate;
      this.itmMrp = this.receiveItm.standardPrice;
      this.itmId = this.receiveItm[6];
      console.log(this.itmName + 'Selected item name');
      console.log(this.itmId + 'Item id');
      console.log(this.receiveItm[6]);
    });
  }
  //function to calculate Amount, Cess and Tax Amounts..................................
  calculateAmount() {
    this.newAmount = this.rate * this.qty;
    this.cessAmt = (this.rate * this.qty * this.itmCess) / 100;
    this.taxAmt = (this.rate * this.qty * this.itmTaxRate) / 100;
    console.log(this.newAmount + 'Amount calculated');
    console.log(this.cessAmt + 'cess calculated');
    console.log(this.taxAmt + 'tax calculated');
  }
  //function to add item to table......................................................
  addItem() {
    console.log(this.purchaseHdr);
    if (this.purchaseHdr.id == null) {
      if(this.supID==null){
        alert('Please select supplier...!!!');
        return;
      }
      if(this.supInvDate==null){
        alert('Please select supplier invoice date...!!!');
        return;
      }
      if(this.ourVoucherDate==null){
        alert('Please select voucher date');
        return;
      }
      if(this.supInvNo==null){
        alert('Please enter Suppier Invoice Number...!!!');
        return;
      }
      console.log('New Purchase');
      this.purchaseHdr = new PurchaseHdr();
      this.purchaseHdr.supplierId = this.supID;
      console.log(this.supID + 'supplier ID');
      this.purchaseHdr.finalSavedStatus = 'N';
      if (this.invTotal != null) {
        this.invTot = Number(this.invTotal);
        console.log(this.invTot + 'Invoice Total in number');
      }
      this.purchaseHdr.invoiceTotal = this.invTot;
      this.purchaseHdr.pONum = this.ponumber;
      this.purchaseHdr.narration = this.narrate;
      let ourVoucherDates = new Date();
      console.log(ourVoucherDates + 'daaaatttteeedaaaatttteee');
      this.purchaseHdr.voucherDate = this.datepipe.transform(
        ourVoucherDates,
        'yyyy-MM-dd'
      );
      console.log(this.purchaseHdr.voucherDate + 'daaaatttteee');
      this.purchaseHdr.supplierInvNo = this.supInvNo;
      this.purchaseHdr.branchCode = this.branchCode;
      this.purchaseHdr.voucherType = 'PURCHASETYPE';
      this.purchaseHdr.purchaseType = 'Local Purchase';
      console.log('purchaseHdr : ' + this.purchaseHdr);

      this.sharedserviceService.savePurchaseHdr(this.purchaseHdr).subscribe(
        (data) => {
          this.purchaseHdr = data;

          this.purchaseHdr.id = data.id;
          console.log(data);
          console.log(data.id);
          this.addPurchaseDtl(this.purchaseHdr);
          this.ShowPurchaseDetails(this.purchaseHdr);

          this.receiveItm = '';
          console.log('Add item--purchaseHdr data Send');
        },
        (error: any) => {
          console.log('Data not send');
          // alert("Unable to save purchaseHdr!!")
        }
      );
    } else {
      this.addPurchaseDtl(this.purchaseHdr);
    }
    // if(this.purchaseDtl!=null)
    // {
    // //
    // }

    // if(this.purchaseHdr!=null){
    //   console.log("Item "+this.itmserial+" adding");
    //   this.purchaseDtl=new PurchaseDtl;
    //   if(this.itmName==null){
    //   alert("Select an item!!");
    //   return;
    //   }
    // this.sharedserviceService.showItemByname(this.itmName).subscribe((data: any)=>{
    // this.itemByName=data;
    // console.log(this.itemByName);})

    // if(this.itemByName==null){
    // alert("Select an item!!")
    // }
    //Batch , Manufacturing and Expiry date
    // if(this.batch!=null){
    // if(this.batch.length>0){
    // //Set batch
    // this.purchaseDtl.batch=this.batch;
    // //expiry date
    // if(this.expDate!=null){
    // this.purchaseDtl.expiryDate=this.expDate;
    // // this.expiry=true;
    // }
    // else{
    // alert("Select Expiry Date!!");
    // }
    // //manufacturing date
    // if(this.manDate!=null){
    // this.purchaseDtl.manufactureDate=this.manDate;
    // }
    // }
    // else{
    // this.purchaseDtl.batch="NOBATCH";
    // //automatically generate batch if item has property
    // this.purchaseDtl.expiryDate=null;
    // }
    // }
    // }
  }

  // add item button - Post (save)
  addPurchaseDtl(purchasehdr: PurchaseHdr) {
    if (purchasehdr != null) {
      if(this.itmName==null){
        alert("Please select Item...!!!");
        return;
      }
      if(this.batch==null){
        // this.batch="NOBATCH"
        this.purchaseDtl.batch = "NOBATCH";
      }
      else{
        this.purchaseDtl.batch = this.batch;
        if(this.expDate==null){
          alert("Please select Expirydate...!!!")
          return;
        }
      }
      if(this.qty==null||this.qty==0){
        alert("Please enter quantity...!!!")
          return;
      }
      if(this.rate==null||this.rate==0){
        alert("Please enter purchase rate...!!!")
          return;
      }
      if(this.itmMrp==null||this.itmMrp==0){
        alert("Please enter mrp...!!!")
          return;
      }
      // if(this.cessAmt==null||this.cessAmt==0){
      //   alert("Please enter Cess Rate...!!!")
      //     return;
      // }
      // if(this.taxAmt==null||this.taxAmt==0){
      //   alert("Please enter Tax Rate...!!!")
      //     return;
      // }
      if(this.store==null){
        alert("Please select store...!!!")
        return;
      }
      // this.sharedserviceService.getitemByNameReqParamforCloud(this.itmName).subscribe((data:any)=>{
      //   this.itemByReqParam=data;
      //   console.log(data)
      //   console.log('Item name checking')
     
      // if(this.itemByReqParam==null){
      //   alert("Please select Item...!!!");
      //   return;
      // }
      
      this.purchaseDtl = new PurchaseDtl();
      this.purchaseDtl.purchaseHdrId = this.purchaseHdr.id;
      console.log(this.purchaseDtl.purchaseHdrId + 'Header id');
      this.purchaseDtl.itemName = this.itmName;
      this.purchaseDtl.barcode = this.itmBarCode;
      this.purchaseDtl.taxRate = this.itmTaxRate;
      this.purchaseDtl.discount = 0;
      this.purchaseDtl.itemId = this.itmId;
      this.purchaseDtl.taxAmt = this.taxAmt;
      this.purchaseDtl.purchseRate = this.rate;
      this.purchaseDtl.cessAmt = this.cessAmt;
      this.purchaseDtl.cessRate = this.itmCess;
      this.purchaseDtl.mrp = this.itmMrp;
      this.purchaseDtl.qty = this.qty;
      this.purchaseDtl.taxRate = this.itmTaxRate;
      this.purchaseDtl.amount = this.newAmount;
      this.purchaseDtl.netCost = this.rate;
      this.purchaseDtl.itemSerial = this.itmserial;
      this.purchaseDtl.store = this.store;
      this.purchaseDtl.batch = this.batch;
      this.purchaseDtl.unitId = this.receiveItm[7];

      this.sharedserviceService
        .savePurchaseDtl(this.purchaseDtl, this.purchaseHdr.id)
        .subscribe(
          (data) => {
            this.purchaseDtl = data;
            this.ShowPurchaseDetails(purchasehdr);
            console.log(data);
            console.log('Add item--purchaseDtl data Send');
            alert('Item Added');
            this.showTotal(this.purchaseHdr.id);
            // Alert message item added in table.................................................
            // if (!this.rowseletion) {
            //   alert('Item Added');
            //   return;
            // }
            // this.clearAddItem();
          },
          (error: any) => {
            console.log('Data not send');
            // this.clearAddItem();
            // alert("Unable to save purchaseHdr!!")
          }
        );
      this.itmserial++;
      // this.setTotal();
      this.clearAddItem();
      // this.rate = 0;
      // this.qty = 0;
      // this.manDate = 0;
      // this.taxAmt = 0;
      // this.cessAmt = 0;
      // this.newAmount = 0;
      // this.expDate = 0;
    // });
    }
    
  }
  // Show calculation purchase details .............................................
  ShowPurchaseDetails(purchasehdr: PurchaseHdr) {
    this.totalAmount = 0;
    this.discountAmount = 0;
    this.sharedserviceService.getPurchaseDtl(purchasehdr).subscribe((data) => {
      this.purchaseDtlArray = data;

      for (let i = 0; i <= this.purchaseDtlArray.length; i++) {
        if (typeof this.purchaseDtlArray[i].id != 'undefined') {
          this.totalAmount = this.totalAmount + this.purchaseDtlArray[i].amount;
          this.discountAmount =
            this.discountAmount + this.purchaseDtlArray[i].discount;
        }
      }
    });
  }
  // Delete item (button) in table...........................................................
  deleteItem() {
    if (!this.rowseletion) {
      alert('Please sected Item');
      return;
    }
    this.sharedserviceService
      .deleteSelectedItems(this.purchaseDtl.id)
      .subscribe((data) => {
        this.ShowPurchaseDetails(this.purchaseHdr);
        this.rowseletion = false;
        alert('deleted');
        return;
      });
  }
  // highligth row in table.................................................................
  highlightRow(selectedItem: PurchaseDtl) {
    this.rowseletion = true;
    this.purchaseDtl = selectedItem;
  }
  // Final save button........................................................
  finalSave() {
    this.purchaseHdr.supplierInvDate = this.supInvDate;
    this.purchaseHdr.supplierInvNo = this.supInvNo;
    this.purchaseHdr.discount = this.discountAmount;
    this.purchaseHdr.supplierId = this.receiveSupp.id;
    this.purchaseHdr.finalSavedStatus = 'Y';

    console.log('final save iddddddddddddddddddddddddd' + this.purchaseHdr.id);
    console.log('supplier invoice date' + this.supInvDate);
    this.sharedserviceService
      .showItemCodeBarCodeItemIdForm(environment.myBranch)
      .subscribe((data: any) => {
        this.purchaseHdr.voucherNumber = data + 'PVN';
        this.purchaseHdr.ourVoucherDate = this.ourVoucherDate;
        this.purchaseHdr.invoiceTotal = this.grandTotal;
        this.purchaseHdr.purchaseType = 'Local Purchase';

        this.sharedserviceService
          .updatePurchaseHdr(this.purchaseHdr, this.purchaseHdr.id)
          .subscribe((data) => {
            console.log('final result of purchase hdr after updation' + data),
              (this.purchaseHdr = data);

            this.saleOrderPopupService.savePurchaseHdr(this.purchaseHdr);

            // this.router.navigate(['purchaseFinalBill']);
            this.router.navigate(['PurchaseBill']);
            console.log('purchase saved---------------');
            console.log('purchase print out---------------');
            //----------purchase print out-----------------
            this.sharedserviceService
              .purchasePdfForWeb(data.id)
              .subscribe((data: any) => {
                console.log(data);
                console.log('purchase print out --------------------');
              });
          });
        this.clearAll();
      });

    if (!this.rowseletion) {
      alert('Purchase Saved');
      return;
    }
  }

  clearAll() {
    this.clearAddItem();
    this.supp = new Supplier();
    this.itm = new datas();
    this.itemByName = [];
    this.itmName=null;
    this.receiveSupp = 0;
    this.supName = 0;
    this.suppGST = 0;
    this.supID = 0;
    this.supInvDate = 0;
    this.ourVoucherDate = 0;
    this.narrate = 0;
    this.supInvNo = 0;
    this.vouchNo = 0;
    this.invTotal = 0;
    this.invTot = 0;
    this.ponumber = 0;
    this.selectSuppName = 0;

    this.receiveItm = 0;
    this.itmName = 0;
    this.itmBarCode = 0;
    this.itmCess = 0;
    this.itmDiscount = 0;
    this.itmTaxRate = 0;
    this.itmMrp = 0;
    this.batch = 0;
    this.itmId = 0;
    this.purchaseDtlArray = [];
    this.newAmount = 0;
    this.cessAmt = 0;
    this.taxAmt = 0;
    this.qty = 0;
    this.rate = 0;
    this.manDate = 0;
    this.expDate = 0;
    this.expiry = true;
    this.store = 0;
    this.discountAmount = 0;
    this.totalAmount = 0;
    this.qtyTotal = 0;
    this.amtTotal = 0;
    this.discTotal = 0;
    this.expTotal = 0;
    this.taxTotal = 0;
    this.cessTotal = 0;
    this.totalBfrTax = 0;
    this.grandTotal = 0;
    this.expeTotal = 0;

    this.itmserial = 1;

    this.purchaseDtl = new PurchaseDtl();
    this.purchaseHdr = new PurchaseHdr();
  }

  clearAddItem(){
    console.log("In Add item clear function")
    this.itmName=null;
    this.batch=null;
    this.itmBarCode=null;
    this.itmTaxRate=0;
    this.itmCess=0;
    this.itmMrp=0;
    this.itmDiscount=0;
    this.manDate=0;
    this.expDate=0;
    this.store="";
    this.rate = 0;
      this.qty = 0;
      this.manDate = 0;
      this.taxAmt = 0;
      this.cessAmt = 0;
      this.newAmount = 0;
      this.expDate = 0;
  }

  showTotal(purchaseHdrId:any){
    this.sharedserviceService.showPurchaseSummary(purchaseHdrId).subscribe((data:any)=>{
      this.purchaseSummary=data;
      console.log("Purchase Summary");
      console.log(this.purchaseSummary);
      this.qtyTotal=this.purchaseSummary.totalQty;
      this.totalAmount=this.purchaseSummary.totalAmount;
      this.taxTotal=this.purchaseSummary.totalTax;
      this.cessTotal=this.purchaseSummary.totalCessAmt;
      this.grandTotal=this.purchaseSummary.totalAmount+this.purchaseSummary.totalTax;
  })
  }

  deleteRowItem(purchaseDtl:PurchaseDtl){
      this.sharedserviceService.purchaseDetailDelete(purchaseDtl.id).subscribe((data:any)=>{
        this.ShowPurchaseDetails(this.purchaseHdr);
        this.showTotal(this.purchaseHdr.id);
      })     
  }

  //hold this purchase button
  holdPurchase(){
    this.clearAll();
  }

}
