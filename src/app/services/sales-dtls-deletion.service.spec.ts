import { TestBed } from '@angular/core/testing';

import { SalesDtlsDeletionService } from './sales-dtls-deletion.service';

describe('SalesDtlsDeletionService', () => {
  let service: SalesDtlsDeletionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalesDtlsDeletionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
