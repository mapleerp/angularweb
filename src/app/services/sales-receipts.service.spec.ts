import { TestBed } from '@angular/core/testing';

import { SalesReceiptsService } from './sales-receipts.service';

describe('SalesReceiptsService', () => {
  let service: SalesReceiptsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalesReceiptsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
