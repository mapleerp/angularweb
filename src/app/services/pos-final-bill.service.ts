import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { BranchMst } from '../modelclass/branch-mst';
import { CompanyMst } from '../modelclass/company-mst';
import { SalesBillDetails } from '../modelclass/sales-bill-details';
import { SalesDtl } from '../modelclass/sales-dtl';

@Injectable({
  providedIn: 'root'
})
export class PosFinalBillService {
 

  constructor() { }
  finalBillStatus:SalesBillDetails= new SalesBillDetails;
  companyMst:CompanyMst=new CompanyMst;
  branchMst: BranchMst=new BranchMst;
 salesDtls:SalesDtl[]=[];
//  public salesDtls= new BehaviorSubject<SalesDtl[]>([]);
 
  billInvoiceNumber:any;
  invoiceBillDate:any;

  totalAmount=0;


  setPosFinalBill(SalesBillDetails: import("../modelclass/sales-bill-details").SalesBillDetails) {

    console.log("setPosFinalBill function called")

   this.companyMst=SalesBillDetails.companyMst;
   this.branchMst=SalesBillDetails.branchMst;
   console.log(SalesBillDetails.arrayOfSalesDtl.length)
  // this.salesDtls.next(SalesBillDetails.salesDtls);
  this.salesDtls=SalesBillDetails.arrayOfSalesDtl;

   console.log(this.salesDtls)
   this.totalAmount=SalesBillDetails.totalAmount;
   this.billInvoiceNumber=SalesBillDetails.salesTransHdr.voucherNumber;
   this.invoiceBillDate=SalesBillDetails.salesTransHdr.voucherDate;


   
  }

  getPosFinalBillCompanyMSt()
  {
    return this.companyMst;
  }
  getPosFinalBillbranchMst()
  {
    return this.branchMst;
  }
  getPosFinalBillsalesDtls()
  {
    // return this.salesDtls.asObservable;
    return this.salesDtls;
  }
  getPosFinalBilltotalAmount()
  {
    return this.totalAmount;
  }
  getPosFinalBillbillInvoiceNumber()
  {
    return this.billInvoiceNumber;
  }
  getPosFinalBillinvoiceBillDate()
  {
    return this.invoiceBillDate;
  }

}
