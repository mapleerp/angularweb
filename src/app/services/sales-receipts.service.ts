import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { SalesReceipts } from '../modelclass/sales-receipts';
import { SalesTransHdr } from '../modelclass/sales-trans-hdr';
import { CardSales } from '../point-of-sales-window/point-of-sales-window.component';
import { SharedserviceService } from './sharedservice.service';

@Injectable({
  providedIn: 'root'
})
export class SalesReceiptsService {

  salesReceipts= new SalesReceipts;
  dateCov:any;
  date= new Date();

  constructor( private sharedserviceService: SharedserviceService,public datepipe: DatePipe) { }


  addingSalesReceipts(salesReceiptVoucherNo:any,totalAmount:any,cardSalesList:CardSales,salesTransHdr:SalesTransHdr)
  {
    
this.salesReceipts= new SalesReceipts;
if(null==salesReceiptVoucherNo)
{
  this.sharedserviceService.showItemCodeBarCodeItemIdForm(environment.myBranch).
  subscribe((data: null)=>{salesReceiptVoucherNo=data;
  this.salesReceipts.voucherNumber=salesReceiptVoucherNo})
}
else {
  this.salesReceipts.voucherNumber=salesReceiptVoucherNo;
}
this.sharedserviceService.getAccountHeadByName(cardSalesList.card).subscribe(
 data=>{this.salesReceipts.accountId=data.id} 
);
this.salesReceipts.receiptMode=cardSalesList.card;
this.salesReceipts.receiptAmount=cardSalesList.cardamount;
this.salesReceipts.userId=this.sharedserviceService.getUserName();
this.salesReceipts.branchCode=environment.myBranch;
this.dateCov= this.datepipe.transform(this.date, 'dd-MM-yyyy');
this.salesReceipts.rereceiptDate=this.dateCov;
this.salesReceipts.salesTransHdr=salesTransHdr;

this.sharedserviceService.saveSalesReceipts(this.salesReceipts).subscribe(
  data=>{this.salesReceipts=data});


  }

  addingSalesReceiptsWithOutCards(salesReceiptVoucherNo:any,totalAmount:any,
    cardSales:any,salesTransHdr:SalesTransHdr)
  {

    this.salesReceipts= new SalesReceipts;
if(null==salesReceiptVoucherNo)
{
  this.sharedserviceService.showItemCodeBarCodeItemIdForm(environment.myBranch).
  subscribe((data: null)=>{salesReceiptVoucherNo=data;
  this.salesReceipts.voucherNumber=salesReceiptVoucherNo})
}
else {
  this.salesReceipts.voucherNumber=salesReceiptVoucherNo;
}
this.sharedserviceService.getAccountHeadByName(environment.myBranch+"-CASH").subscribe(
 data=>{this.salesReceipts.accountId=data.id} 
);

this.salesReceipts.receiptMode=environment.myBranch+"-CASH";
this.salesReceipts.receiptAmount=totalAmount;
this.salesReceipts.userId=this.sharedserviceService.getUserName();
this.salesReceipts.branchCode=environment.myBranch;
this.dateCov= this.datepipe.transform(this.date, 'dd-MM-yyyy');
this.salesReceipts.rereceiptDate=this.dateCov;
this.salesReceipts.salesTransHdr=salesTransHdr;

this.sharedserviceService.saveSalesReceipts(this.salesReceipts).subscribe(
  data=>{this.salesReceipts=data});


  }


 getAllCardAmount(salesTransHdr:SalesTransHdr) 
 {
   this.sharedserviceService.getAllSalesReceiptsBySalesTransHdr(salesTransHdr.id).subscribe(
     data=>{this.salesReceipts=data}
   )
 }

 getSumofSalesReceiptbySalesTransHdr(id:any)
 {
   let cardAmount = this.sharedserviceService.getSumofSalesReceiptbySalesTransHdr(id).subscribe();
   return cardAmount;
 }
}
