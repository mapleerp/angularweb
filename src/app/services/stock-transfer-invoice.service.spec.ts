import { TestBed } from '@angular/core/testing';

import { StockTransferInvoiceService } from './stock-transfer-invoice.service';

describe('StockTransferInvoiceService', () => {
  let service: StockTransferInvoiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StockTransferInvoiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
