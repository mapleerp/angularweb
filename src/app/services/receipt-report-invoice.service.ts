import { Injectable } from '@angular/core';
import { ReceiptHdr } from '../modelclass/receipt-hdr';
import { SharedserviceService } from './sharedservice.service';
import { DatePipe } from '@angular/common';
@Injectable({
  providedIn: 'root'
})
export class ReceiptReportInvoiceService {
  receiptHdr: ReceiptHdr = new ReceiptHdr(); 

  voucherDate: any;
  voucherNumber: any;
  toBranch: any;

  unit:any;
  qty:any;
  hsnCode:any;
  rate:any;
  amount:any;

  // showAllStockRequestDtl: setReceiptReportRequest [] = [];
  dateConv: any;
  stockRequestDtls: any;
  itemId: any;
  totalAmount=0;
  companyName: any;
  constructor( private sharedserviceService: SharedserviceService,public datepipe: DatePipe) { }

  setReceiptReportRequest(receiptHdr: ReceiptHdr) {
    
    this.voucherNumber=receiptHdr.voucherNumber;
    this.voucherDate=receiptHdr.voucherDate;
    this.companyName=receiptHdr.companyMst.companyName;
        this.getStockCompanyMSt( this.companyName);
        this.getStockVoucherNumber(this.voucherNumber);
        this.getVoucherDate(this.voucherDate);    
  }
  gettotalAmount(totalAmount: number) {
    return this.totalAmount;
  }
  getVoucherDate(voucherDate: any) {
    return this.voucherDate;
  }
  getStockVoucherNumber(voucherNumber: any) {
    return this.voucherNumber;
  }
  getStockCompanyMSt(companyName: any) {
    return this.companyName;
  }

}