import { TestBed } from '@angular/core/testing';

import { StockRequestInvoiceService } from './stock-request-invoice.service';

describe('StockRequestInvoiceService', () => {
  let service: StockRequestInvoiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StockRequestInvoiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
