


import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { GSTInputDtlAndSmryReport } from '../modelclass/gstinput-dtl-and-smry-report';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, observable, Observable, of } from 'rxjs';
import { StockMovementView } from '../modelclass/stock-movement-view';
import { DayEndWebReport } from 'src/app/modelclass/day-end-web-report';
import { SalesSummaryReport } from '../modelclass/sales-summary-report';
import { SalesDetailsReport } from '../modelclass/sales-details-report';
import { SalesBillDetails } from '../modelclass/sales-bill-details';
import { HsnCodewieseSalesReport } from '../modelclass/hsn-codewiese-sales-report';
import { DailySalesReport } from '../modelclass/daily-sales-report';
import { TrailBalanceReport } from '../modelclass/trail-balance-report';
import { StockTransferOutReport } from '../modelclass/stock-transfer-out-report';
import { CategoryMst } from '../modelclass/category-mst';
import { StockReport } from '../modelclass/stock-report';
import { BranchMst } from '../modelclass/branch-mst';
import { PurchaseDetailsReport } from '../modelclass/purchase-details-report';
import { environment } from 'src/environments/environment.prod';
import { UserMst } from '../modelclass/user-mst';
import { JarVersionMst } from '../modelclass/jar-version-mst';
import { datas } from 'src/app/Pop-Up-Windows/search-pop-up/search-pop-up.component';

import { CompanyMst } from '../modelclass/company-mst';

import { VoucherReprintDtl } from '../modelclass/voucher-reprint-dtl';
import { CustomerMst } from '../modelclass/customer-mst';

import { Editedqtydetails } from '../dialogWindows/qty-edit-window/qty-edit-window.component';

import { WholeSaleBillDtls } from '../modelclass/whole-sale-bill-dtls';

import { analyzeAndValidateNgModules } from '@angular/compiler';
import { UnitMst } from '../modelclass/unit-mst';
import { ShippingAddress } from '../modelclass/shipping-address';

import { FinancialYearMst } from '../modelclass/financial-year-mst';
import { DayEndClosureHdr } from '../modelclass/day-end-closure-hdr';
import { BarcodeBatchMst } from '../modelclass/barcode-batch-mst';
import { MultiUnitMst } from '../modelclass/multi-unit-mst';
import { ParamValueConfig } from '../modelclass/param-value-config';
import { LocalCustomerMst } from '../modelclass/local-customer-mst';
import { SalesTransHdr } from '../modelclass/sales-trans-hdr';
import { TaxMst } from '../modelclass/tax-mst';
import { SalesDtl } from '../modelclass/sales-dtl';
import { Summary } from '../modelclass/summary';
import { AccountHeads } from '../modelclass/account-heads';
import { SalesReceipts } from '../modelclass/sales-receipts';
import { DashBoardConfigMst } from '../modelclass/dash-board-config-mst';
import { WebUser } from '../modelclass/web-user';

import { Dashboardreporttype } from '../modelclass/dashboardreporttype';
import { SalesOrderTransHdr } from '../modelclass/sales-order-trans-hdr';

import { Supplier } from '../modelclass/supplier';

import { ReceiptModeMst } from '../modelclass/receipt-mode-mst';

import { SalesTypeMst } from '../modelclass/sales-type-mst';

import { ReceiptModeReport } from '../modelclass/receipt-mode-report';
import { PurchaseHdr } from '../modelclass/purchase-hdr';
import { PurchaseDtl } from '../modelclass/purchase-dtl';

import { PurchaseReport } from '../modelclass/purchase-report';
import { KotTableHdr } from '../modelclass/kot-table-hdr';
import { KotTableDtl } from '../modelclass/kot-table-dtl';
import { ItemMst } from '../modelclass/item-mst';
import { itemDetails } from '../Pop-Up-Windows/item-list-pop-up/item-list-pop-up.component';
import { AccountBalanceReport } from '../modelclass/account-balance-report';
import { ItemStockPopUp } from '../modelclass/item-stock-pop-up';
import { WebsiteClass } from '../modelclass/website-class';
// import { ItemImageUrlDetail } from '../modelclass/item-image-url-detail';
import { StockTransferOutHdr } from '../modelclass/stock-transfer-out-hdr';
import { StockTransferOutDtl } from '../modelclass/stock-transfer-out-dtl';

import { ItemImageUrlDetail } from '../modelclass/item-image-url-detail';
import { UserSubsidiary } from '../modelclass/user-subsidiary';

import { SalesManMst } from '../modelclass/sales-man-mst';

import { StockRequestHdr } from '../modelclass/stock-request-hdr';
import { StockRequestDtl } from '../modelclass/stock-request-dtl';
import { StockRequestReport } from '../modelclass/stock-request-report';
import { ReceiptHdr } from '../modelclass/receipt-hdr';
import { ReceiptDtl } from '../modelclass/receipt-dtl';

// Payment
import { PaymentHdr } from '../modelclass/payment-hdr';
import { PaymentDtl } from '../modelclass/payment-dtl';
import { PaymentVoucher } from '../modelclass/payment-voucher';
import { JournalHdr } from '../modelclass/journal-hdr';
import { JournalDtl } from '../modelclass/journal-dtl';
import { Voucherstock } from '../modelclass/voucherstock';



@Injectable({
  providedIn: 'root',
})
export class SharedserviceService {
  public newCourse: any;
  public newUser = new BehaviorSubject<any>({});
  private etdQty = new BehaviorSubject<any>({});
  public result: datas[] = [];
  public InvoiceBillDetails: datas[] = [];
  edtqty: Editedqtydetails = new Editedqtydetails();
  custmerMst: CustomerMst = new CustomerMst();
  WholeSaleVoucherNumber: WholeSaleBillDtls = new WholeSaleBillDtls();
  glCUstomerDtls: CustomerMst[] = [];
  posInvoiceBillDetails: SalesDtl[] = [];

  //public temp:datas | undefined;

  //private content = new BehaviorSubject<boolean>(false)
  //public share=this.content.asObservable();
  loginStatus: any;
  companyname = environment.company;
  HOST: String = '';
  globalData = new BehaviorSubject<any>({});
  itemQty: any;
  edittedPosQty: any;
  userName: any;
  public shippingDtls = new BehaviorSubject<any>({});
  billSummary = new BehaviorSubject<any>({});
  public salesDtlsFrompopup = new BehaviorSubject<SalesDtl[]>([]);
  // public salesDtlArr = new BehaviorSubject<any>([]);
  SalesTransHdrStatus = 0;
  salesDtl: SalesDtl[] = [];
  salesTransHdrHistory: SalesTransHdr = new SalesTransHdr();
  // itemCodeGeneration:any

  salesDtlArr: SalesDtl[] = [];
  userMst:UserMst=new UserMst;

  private itemsDtl = new BehaviorSubject<any>({});
  itemDtl: ItemStockPopUp[] = [];

  private vouchDtl = new BehaviorSubject<any>({});

  private baseURL = '/gstinputdtlandsmryreportresource/getgstinputdtlandsmry';

  fromDate:any;
  toDate:any;
  voucherNumber:any;
  voucherDate:any;

  constructor(private http: HttpClient) {}

  getGstInputReport(fromdate: String, toDate: String) {
    return this.http.get<GSTInputDtlAndSmryReport[]>(
      'https://' +
        this.HOST +
        '/' +
        '/gstinputdtlandsmryreportresource/getgstinputdtlandsmry?fromdate=' +
        fromdate +
        '&&todate=' +
        toDate
    );
  }
  updateLoginStatus(value: any, companymstid: any, username: any, userMst:UserMst) {
    this.loginStatus = value;
    this.HOST = companymstid;
    environment.company=companymstid;
    this.userName = username;
    this.userMst=userMst;
    /**this.content.next(value);**/
    console.log('LoginStatusUpdated' + this.loginStatus);
  }
  getUserName() {
    return this.userName;
  }

  getUserMstInLogin(){
    //returns the userMst after login
    return this.userMst;
  }

  getloginStatus() {
    console.log('getLoginStatus' + this.loginStatus);
    return this.loginStatus;
  }
  logout() {
    this.loginStatus = false;
    this.HOST = '';
    this.userName='';
    environment.company='MAPLE';
    this.userMst=new UserMst;
    environment.myBranch=undefined;
    console.log('LogoutStatus' + this.loginStatus);
    localStorage.clear();
  }

  getBranchWiseStrockReport(
    fromdate: string,
    todate: string,
    branchname: string
  ) {
    return this.http.get<StockMovementView[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        branchname +
        '/' +
        'stockresource/getstockmovementallitem' +
        '?fromdate=' +
        fromdate +
        '&&todate=' +
        todate
    );
  }

  getDayEndReports(reportdate: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'dayendreportresource/dayendwebreportwithtotalsalesforhc' +
        '?reportdate=' +
        reportdate
    );

    return this.http.get<DayEndWebReport[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'dayendreportresource/dayendwebreportwithtotalsalesforhc' +
        '?reportdate=' +
        reportdate
    );
  }
  getSalesSummaryreport(
    dateConvFist: any,
    dateConvSec: any,
    branchname: string
  ) {
    return this.http.get<SalesSummaryReport[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'dailysalesreportdtl/' +
        branchname +
        '?rdate=' +
        dateConvFist +
        '&&tdate=' +
        dateConvSec
    );
  }

  getDetailsreport(dateConvFist: any, dateConvSec: any, branchname: string) {
    return this.http.get<SalesDetailsReport[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'salestranshdrreport/salesdtlreport/' +
        branchname +
        '?fdate=' +
        dateConvFist +
        '&&tdate=' +
        dateConvSec
    );
  }

  getSalesBillDtlReport(dateConv: any) {
    return this.http.get<SalesBillDetails[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/allsalesnumericbyvoucherdate?date=' +
        dateConv
    );
  }

  getHsnCodeWisesreport(
    dateConvFist: any,
    dateConvSec: any,
    branchname: string
  ) {
    return this.http.get<HsnCodewieseSalesReport[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'hsnwisereportresource/hsnwisereport/' +
        branchname +
        '?fdate=' +
        dateConvFist +
        '&&tdate=' +
        dateConvSec
    );
  }

  getDailySalesreport(dateConvFist: any, dateConvSec: any, branchname: string) {
    return this.http.get<DailySalesReport[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'dailysalesreportdtl/' +
        branchname +
        '?rdate=' +
        dateConvFist +
        '&&tdate=' +
        dateConvSec
    );
  }

  getTrailBalancereport(dateConvSec: any, branchname: string) {
    return this.http.get<TrailBalanceReport[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'trialbalance/' +
        branchname +
        '?startdate=' +
        dateConvSec
    );
  }

  getStatementofAccountReport(
    dateConvFist: any,
    dateConvSec: any,
    branchname: string,
    accountId: any
  ) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/accountingresource/accountstatement/' +
        accountId +
        '/' +
        branchname +
        '?startdate=' +
        dateConvFist +
        '&&enddate=' +
        dateConvSec
    );
    return this.http.get<AccountBalanceReport[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/accountingresource/accountstatement/' +
        accountId +
        '/' +
        branchname +
        '?startdate=' +
        dateConvFist +
        '&&enddate=' +
        dateConvSec
    );
  }

  getStockTransferOutDetailreport(
    dateConvFist: any,
    dateConvSec: any,
    branchname: string
  ) {
    return this.http.get<StockTransferOutReport[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        branchname +
        '/stocktransferreport/webstocktransferoutdtlreport' +
        '?fdate=' +
        dateConvFist +
        '&&tdate=' +
        dateConvSec
    );
  }
  getStockTransferOutreport(
    dateConvFist: any,
    dateConvSec: any,
    branchname: string
  ) {
    return this.http.get<StockTransferOutReport[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/stocktransferouthdr/stocktransferouthdrbetweendate/' +
        branchname +
        '?fdate=' +
        dateConvFist +
        '&&tdate=' +
        dateConvSec
    );
  }
  getCategoryMst() {
    console.log('getCategoryMst function Called');
    return this.http.get<CategoryMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/categorymsts'
    );
  }
  getUnitMst() {
    console.log('getUnitMst called');
    return this.http.get<UnitMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/unitmst'
    );
  }

  getStockreport(dateConv: any, category: any, branchname: any) {
    return this.http.get<StockReport[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/stockreport/web/' +
        branchname +
        '/' +
        category +
        '?fromdate=' +
        dateConv
    );
  }
  getBranchNames() {
    return this.http.get<BranchMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/allbranches'
    );
  }

  getCommandByBranchPage(branchname: any, command: any) {
    this.http.get(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/executecommand?data=' +
        command +
        '&&branch=' +
        branchname
    );
  }
  getCommandForAllPage(branchCode: any, command: any) {
    this.http.get(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/executecommand?data=' +
        command +
        '&&branch=' +
        branchCode
    );
  }

  getPurchaseDetailsreport(
    dateConvFist: any,
    dateConvSec: any,
    branchname: string
  ) {
    return this.http.get<PurchaseDetailsReport[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/purchasereportresource/' +
        branchname +
        '/purchasedtlreport?fromdate=' +
        dateConvFist +
        '&&todate=' +
        dateConvSec
    );
  }

  getPurchaseSummaryreport(
    dateConvFist: any,
    dateConvSec: any,
    branchname: string
  ) {
    return this.http.get<PurchaseDetailsReport[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/purchasereportresource/' +
        branchname +
        '/purchasesummarytreport?fromdate=' +
        dateConvFist +
        '&&tomdate=' +
        dateConvSec
    );
  }

  getHsnPuchasereport(dateConvFist: any, dateConvSec: any, branchname: string) {
    return this.http.get<HsnCodewieseSalesReport[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/purchasedtl/hsncodepuchasereport/' +
        branchname +
        '?fdate=' +
        dateConvFist +
        '&&tdate=' +
        dateConvSec
    );
  }

  saveBranchCreation(branchMst: any = BranchMst) {
    return this.http.get(
      'http127' +
        '/' +
        this.HOST +
        branchMst.accountNumber +
        branchMst.bankBranch +
        branchMst.branchEmail
    );
    // console.log('Data received'+branchMst);
  }

  getWebLogin(company: string, passwords: any, username: any) {
    return this.http.get<UserMst>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        company +
        '/usermst/' +
        username +
        '/' +
        passwords +
        '/web/weblogin'
    );
  }

  getAllBranchcodes(company: string) {
    //return this.http.get<JarVersionMst[]>('http://localhost:8080/getallbranchcodedetails');
    return this.http.get<JarVersionMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        company +
        '/jarversionmst/' +
        'getallbranchcodedetails'
    );
  }

  getSearch(search: String) {
    //return this.http.get<JarVersionMst []>('http://localhost:8080/search'+'/'+search);
    return this.http.get<JarVersionMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/jarversionmst/searchbranchcodedetails/' +
        search
    );
  }

  saveupdates(jarVersionMst: JarVersionMst[]): Observable<JarVersionMst[]> {
    //return this.http.put<JarVersionMst[]>("http://localhost:8080/saveupdatesbranchcode",jarVersionMst);
    return this.http.put<JarVersionMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/jarversionmst/' +
        'saveupdatesbranchcode',
      jarVersionMst
    );
  }
  saveupDeploymentDetails(
    jarVersionMstDialog: JarVersionMst
  ): Observable<Object> {
    console.log(jarVersionMstDialog);
    //return this.http.post<JarVersionMst>("http://localhost:8080/savedata",jarVersionMstDialog);
    return this.http.post<JarVersionMst>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/jarversionmst/' +
        'savenewdetails',
      jarVersionMstDialog
    );
  }

  deleteDetails(selectbranch: String[]) {
    console.log(selectbranch);
    console.log('http://localhost:8080/deletedetails' + selectbranch);
    //return this.http.delete<any>("http://localhost:8080/deletedetails/"+selectbranch);
    return this.http.delete<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/jarversionmst/' +
        'deletedetails/' +
        selectbranch
    );
  }

  upDateSeletedcolum(selectedobjList: JarVersionMst[]) {
    //return this.http.put<JarVersionMst[]>("http://localhost:8080/updateselectedbranchcode",selectedobjList);
    return this.http.put<JarVersionMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/jarversionmst/' +
        'getallbranchcodedetails',
      selectedobjList
    );
  }

  searchPopup(mymodel: any) {
    // return this.http.get<datas[]>(
    //   environment.hostURL + '/searchs' + '/' + mymodel
    // );
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itempopupsearchwithcomp?data=' +
        mymodel
    );
    return this.http.get<datas[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itempopupsearchwithcomp?data=' +
        mymodel
    );
  }

  setInfo(si: any) {
    console.log(si.itemName + '"data received to service name');
    this.newUser.next(si);
    console.log(this.newUser + 'behaviour obj');
    // this.newUser=JSON.parse(JSON.stringify(si))
    // this.result=JSON.parse(JSON.stringify(si))
    // console.log(this.result[0].name+"data received to service")
  }
  tabledatatesting() {
    return of(this.result);
  }

  setInfoo() {
    console.log('setInfoo');
    return this.newCourse;
  }

  getNewUserInfo() {
    return this.newUser.asObservable();
  }

  AllDataPopup() {
    console.log('alldatapopupcalled');

    return this.http.get<datas[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/allitemmsts'
    );
    //return this.http.get<datas[]>('http://localhost:8080/getallproducts');

    // return this.http.get<datas[]>(
    //   environment.hostURL +
    //     ':' +
    //     environment.port +
    //     '/' +
    //     environment.company +
    //     '/allitemmsts'
    // );
    // return this.http.get<datas[]>('http://localhost:8080/getallproducts');
  }

  globaldata(tdatas: any) {
    console.log('Global data received at service');

    this.globalData.next(tdatas);
  }

  getGlobalData() {
    return this.globalData.asObservable();
  }

  custRegToServer(newCustReg: CustomerMst, companyid: string) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        companyid +
        '/customerregistration'
    );

    return this.http.post<CustomerMst>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        companyid +
        '/customerregistration',
      newCustReg
    );
  }

  saveCompanyCreationCreation(company: CompanyMst): Observable<Object> {
    console.log('http://localhost:8182/companymstforwebapp');
    console.log(company);
    return this.http.post<CompanyMst>(
      environment.hostURL + ':' + environment.port + '/companymstforwebapp',
      company
    );
    //return this.http.get<CompanyMst[]>('http://localhost:8182/companymstforwebapp');
  }

  GetshowDaliysaleshdr(
    dateConvFist: any,
    dateConvSec: any,
    branchname: string
  ) {
    return this.http.get<SalesSummaryReport[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/dailysalesreportdtl/' +
        branchname +
        '?rdate=' +
        dateConvFist +
        '&&tdate=' +
        dateConvSec
    );
  }

  getDailyttotalsales(dateConvFist: any, dateConvSec: any) {}

  getVoucherDetails(voucherNumber: any, branchname: string, voucherDate: any) {
    console.log('voucherdetacalled');
    return this.http.get<VoucherReprintDtl[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/voucherreprintbydate/' +
        voucherNumber +
        '/' +
        branchname +
        '?date=' +
        voucherDate
    );
  }
  qtySharing(qty: any) {
    this.itemQty = qty;
  }
  sendItemQty() {
    return this.itemQty;
  }

  sendBilldetailsToInvoicePage(generateBill: any) {
    this.InvoiceBillDetails = generateBill;
    console.log('sendBilldetailsToInvoicePage at sharedservices');
  }

  receiveInvoiceBillDetails() {
    console.log('receiveInvoiceBillDetails');
    return this.InvoiceBillDetails;
  }

  edittedQtyTosent(qty: Editedqtydetails) {
    console.log(qty + 'edittedqty recevied at shared services');
    this.etdQty.next(qty);
  }

  receiveEdittedQty() {
    console.log('edittedQTYsent to POSORSale window');
    return this.etdQty.asObservable();
  }

  saveuserRegistrationForm(userMst: UserMst, branch: any): Observable<Object> {
    console.log(userMst);
    return this.http.post<UserMst>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        branch +
        '/user',
      userMst
    );
  }

  saveWebUser(webUser: WebUser) {
    return this.http.post<WebUser>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company+
        '/webuserresource/createwebuser',
      webUser
    );
  }

  showUserDetails() {
    console.log('showUserDetails called');
    // return this.http.get<UserMst[]>('http://localhost:8182/'+this.companyname +'/allusers');
    return this.http.get<UserMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/allusers'
    );
  }

  getAllCustomerDetailsfromserver(): Observable<AccountHeads[]> {
    console.log(' function called');

    return this.http.get<AccountHeads[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/accountheads'
    );
  }

  saveItemCreationForm(itemdata: datas, imageurl: string): Observable<datas> {
    // return this.http.post<datas>('http://localhost:8182/'+this.companyname +'/itemmst',itemdata);
    console.log( environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/web/itemmst?imageurl=' +
      imageurl,
    itemdata)
    
    return this.http.post<datas>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/web/itemmst?imageurl=' +
        imageurl,
      itemdata
    );
  }

  // getAllCustomerDetailsfromserver(): Observable<CustomerMst[]> {

  //   console.log(" getAllCustomerDetailsfromserver called")

  //   return this.http.get<CustomerMst[]>("http://localhost:8080/HOTCAKES/customerregistrations")

  // }

  sendSaleOrdreBilldetailsToInvoicePage(generateBill: any, receivecust: any) {
    this.InvoiceBillDetails = generateBill;

    this.custmerMst = receivecust;
    console.log('sendBilldetailsToInvoicePage at sharedservices');
  }

  getCustomerDtlsforInvoice() {
    return this.custmerMst;
  }

  postwholeSaleBillDtls(
    wholeSaleBillDtls: WholeSaleBillDtls
  ): Observable<Object> {
    console.log('billdtls sent to server');

    return this.http.post<WholeSaleBillDtls>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company+
        '/' +
        'salestranshdrresource/savesalesinweb',
      wholeSaleBillDtls
    );

    // return this.http.post<WholeSaleBillDtls>('http://192.168.2.17'+':'+8182+'/'+this.companyname +'/salestranshdrresource/savesalesinweb',wholeSaleBillDtls);
  }

  showItemCodeBarCodeItemIdForm(branch: any): any {
    // return this.http.get<string>('http://localhost:8182/'+this.companyname +'/vouchernumber?id='+branch,{ responseType: 'text' as 'json'});
    return this.http.get<string>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/vouchernumber?id=' +
        branch,
      { responseType: 'text' as 'json' }
    );
  }

  sentVoucherNumberToInvoiceBill(VoucherNumberFromServer: WholeSaleBillDtls) {
    this.WholeSaleVoucherNumber = VoucherNumberFromServer;
    console.log(
      this.WholeSaleVoucherNumber.voucherNumber +
        'sentVoucherNumberToInvoiceBill'
    );
  }
  getWholeSaleVoucherNumber() {
    return this.WholeSaleVoucherNumber;
  }

  sentShippingDtltoService(
    ShippingArees: ShippingAddress
  ): Observable<ShippingAddress> {
    this.shippingDtls.next(ShippingArees);
    // console.log(
    //   environment.hostURL +
    //     ':' +
    //     environment.port +
    //     '/' +
    //     environment.company +
    //     '/sitemst',
    //   ShippingArees
    // );
    // return this.http.post<ShippingAddress>(
    //   environment.hostURL +
    //     ':' +
    //     environment.port +
    //     '/' +
    //     environment.company +
    //     '/sitemst',
    //   ShippingArees
    // );
    return this.http.post<ShippingAddress>(environment.hostURL +':' +environment.port +'/' +environment.company +'/shippingaddressresource/'+ShippingArees.accountHeads.id+'/shippingaddress',ShippingArees);
  }
  getShippingDtls() {
    return this.shippingDtls.asObservable();
  }
  saveCategoryCreationForm(categoryMst1: CategoryMst) {
    console.log(categoryMst1);
    return this.http.post<CategoryMst>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/categorymst',
      categoryMst1
    );
  }
  showCategoryDetails() {
    return this.http.get<CategoryMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/categorymsts'
    );
  }

  deletItemById(id: String) {
    let itemid = id;
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itemmstresource/' +
        'deleteitemnewurl/' +
        id
    );
    return this.http.delete<String>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itemmstresource/deleteitemnewurl/' +
        itemid
    );
  }
  getUserMst() {
    console.log('getUserMst function Called');
    return this.http.get<UserMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/allusers'
    );
  }
  getReportNameByDashBoardName(dashBoardName: any) {
    console.log('getReportNameByDashBoardName function Called');
    return this.http.get<Dashboardreporttype[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        dashBoardName +
        '/dashboardreporttyperesource/getreportnames'
    );
  }
  saveDashBoardConfiguration(dashBoardConfigMst: DashBoardConfigMst) {
    console.log(dashBoardConfigMst);
    return this.http.post<DashBoardConfigMst>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/dashboardconfigmstresource/savedashboardconfiguration',
      dashBoardConfigMst
    );
  }

  getAllShippingDtlsfromserver(customerid: any) {
    // return this.http.get<ShippingAddress[]>(
    //   environment.hostURL +
    //     ':' +
    //     environment.port +
    //     '/' +
    //     environment.company +
    //     '/sitemstbycustomerid/' +
    //     Customerid
    // );
    return this.http.get<ShippingAddress[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/shippingaddressresource/' +
        customerid
        +'/shipping'
    );
  }
  showDashBoardDetails() {
    return this.http.get<DashBoardConfigMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/dashboardconfigmstresource/getalldashboardconfiguration'
    );
  }
  showDashBoardDetailsByUserId(userId: any) {
    return this.http.get<DashBoardConfigMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/dashboardconfigmstresource/getdashboardconfigurationbyuserid/' +
        userId
    );
  }
  deleteDadhboardDetailsById(id: any) {
    return this.http.delete<String>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/dashboardconfigmstresource/deletedashboarddetail/' +
        id
    );
  }
  showAllDashBoardDetails() {
    return this.http.get<DashBoardConfigMst>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/dashboardconfigmstresource/getalldashboardconfiguration'
    );
  }
  getCountries() {
    return this.http.get<String>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/companymst/getcountry'
    );
  }
  getStateByCountry(country: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/companymst/getstatebycountry?' +
        country
    );
    return this.http.get<String>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/companymst/getstatebycountry?country=' +
        country
    );
  }
  getCurrencyMst() {
    return this.http.get<DashBoardConfigMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/currencymst/getallcurrency'
    );
  }

  getStockItemPopupSearchwithBatch(dateConversion: any, arg1: string) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/popupresource/stockitempopupsearchwithbatch?date=' +
        dateConversion +
        '&&data=' +
        arg1
    );
    return this.http.get<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/popupresource/stockitempopupsearchwithbatch?date=' +
        dateConversion +
        '&&data=' +
        arg1
    );
  }
  getFinancialYear() {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/financialyearmst'
    );
    return this.http.get<FinancialYearMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/financialyearmst'
    );
  }
  getMaxDayEndClousre() {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/dayendclosuremst/maxofdayend/' +
        environment.myBranch
    );
    return this.http.get<DayEndClosureHdr>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/dayendclosuremst/maxofdayend/' +
        environment.myBranch
    );
  }

  getFinancialYearCount() {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/financialyearmst/getfinanicialyearcount'
    );
    return this.http.get<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/financialyearmst/getfinanicialyearcount'
    );
  }

  getBarCodeBatchMst(barcode: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/barcodebatchmstresource/getbarcodebatchmstbyid/' +
        barcode
    );

    return this.http.get<BarcodeBatchMst>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/barcodebatchmstresource/getbarcodebatchmstbyid/' +
        barcode
    );
  }

  getitemByNameReqParam(itemName: String) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itemmst/itemname?itemdata=' +
        itemName
    );

    return this.http.get<datas>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itemmst/itemname?itemdata=' +
        itemName
    );
  }

  getMultiUnitbyPrimaryUnit(id: any, uniId: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/multiunitmst/' +
        id +
        '/' +
        uniId +
        '/getbyprimaryunit'
    );
    return this.http.get<MultiUnitMst>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/multiunitmst/' +
        id +
        '/' +
        uniId +
        '/getbyprimaryunit'
    );
  }
  getUnitByItem(unitname: object) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/unitmst/' +
        unitname
    );
    return this.http.get<UnitMst>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/unitmst/' +
        unitname
    );
  }

  getSingleStockItemByName(itemName: String, batch: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/stockitemsearch/' +
        itemName +
        '/' +
        batch +
        '/' +
        environment.myBranch
    );
    return this.http.get<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/stockitemsearch/' +
        itemName +
        '/' +
        batch +
        '/' +
        environment.myBranch
    );
  }

  getQtyFromItemBatchMstByItemAndQty(
    itemCode: any,
    batch: any,
    storeName: any
  ) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itembatchmstresource/itembatchdtlqty/' +
        batch +
        '/' +
        itemCode +
        '/' +
        storeName
    );
    return this.http.get<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itembatchmstresource/itembatchdtlqty/' +
        batch +
        '/' +
        itemCode +
        '/' +
        storeName
    );
  }

  getcustomerByNameAndBranchCode(arg0: string, myBranch: string) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/customermstbynameandbrachcode/' +
        arg0 +
        '/' +
        myBranch
    );
    return this.http.get<CustomerMst>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/customermstbynameandbrachcode/' +
        arg0 +
        '/' +
        myBranch
    );
  }

  getParamValueConfig(arg0: string) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/paramvalueconfig/' +
        arg0 +
        '/' +
        environment.myBranch
    );
    return this.http.get<ParamValueConfig>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/paramvalueconfig/' +
        arg0 +
        '/' +
        environment.myBranch
    );
  }

  getlocalCustomerMst(localCustomer: string) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/getlocalcustomerbyname/' +
        localCustomer
    );

    return this.http.get<LocalCustomerMst>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/getlocalcustomerbyname/' +
        localCustomer
    );
  }

  saveSalesHdr(salesTransHdr: SalesTransHdr) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdr' +
        salesTransHdr
    );

    console.log('salesTransHdrHistory');

    return this.http.post<SalesTransHdr>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdr',
      salesTransHdr
    );
  }

  getSalesTransHdrStatus() {
    return this.SalesTransHdrStatus;
  }

  getTaxByItemId(id: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/taxmst/taxmstbyitemidandenddate/' +
        id
    );
    return this.http.get<TaxMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/taxmst/taxmstbyitemidandenddate/' +
        id
    );
  }

  TaxCalculator(taxRate: any, rateBeforeTax: number) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/taxmst/taxcalculator/' +
        taxRate +
        '/' +
        rateBeforeTax
    );

    return this.http.get<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/taxmst/taxcalculator/' +
        taxRate +
        '/' +
        rateBeforeTax
    );
  }

  saveSalesDtl(
    salesDtl: SalesDtl,
    date: any,
    salesMode: any,
    salesType: any,
    CustId: any,
    localCustId: any,
    salesMan: any
  ) {
    // let salesMode = 'ZOMATO';

    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdrresource/saveonlinesales/' +
        environment.myBranch +
        '/' +
        environment.username,
      +'/' +
        salesMode +
        '/' +
        salesType +
        '/' +
        CustId +
        '/' +
        localCustId +
        '/' +
        salesMan,

      salesDtl
    );

    return this.http.post<SalesDtl[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdrresource/saveonlinesales/' +
        environment.myBranch +
        '/' +
        environment.username +
        '/' +
        salesMode +
        '/' +
        salesType +
        '/' +
        CustId +
        '/' +
        localCustId +
        '/' +
        salesMan,

      salesDtl
    );
  }

  //.....................POS Window...///

  getSalesBillDtl(salesTransHdrId: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdr/' +
        salesTransHdrId +
        '/salesdtl'
    );

    return this.http.get<SalesDtl[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdr/' +
        salesTransHdrId +
        '/salesdtl'
    );
  }

  PostSalesDtlToPosWindow(salesDtl: SalesDtl[]) {
    console.log(this.salesDtl + 'valee');
    this.salesDtlsFrompopup.next(salesDtl);

    console.log(this.salesDtlsFrompopup.value + 'valllleee');
  }

  salesDtlfromShardService() {
    return this.salesDtlsFrompopup.asObservable();
    // return this.salesDtl
  }

  getSalesWindowsSummary(id: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdr/' +
        id +
        '/saleswindowsummary'
    );

    return this.http.get<Summary>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdr/' +
        id +
        '/saleswindowsummary'
    );
  }

  postSalesSummaryToService(summary: Summary) {
    this.billSummary.next(summary);
  }

  getSalesSummary() {
    console.log('getSalesSummary');
    return this.billSummary.asObservable();
  }

  getAccountHeadByName(arg0: string) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/accountheadsbyname/' +
        arg0
    );

    return this.http.get<AccountHeads>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/accountheadsbyname/' +
        arg0
    );
  }

  saveSalesReceipts(salesReceipts: SalesReceipts) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salesreceipts',
      salesReceipts
    );
    return this.http.post<SalesReceipts>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salesreceipts',
      salesReceipts
    );
  }

  getSalesDtl(salesTransHdr: SalesTransHdr) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdr/' +
        salesTransHdr.id +
        '/salesdtl'
    );
    return this.http.get<SalesDtl[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdr/' +
        salesTransHdr.id +
        '/salesdtl'
    );
  }

  updateSalesTransHdr(salesTransHdr: SalesTransHdr, date: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdrresource/salesfinalsavereturningbillentity/' +
        salesTransHdr.id +
        '/' +
        'MAIN' +
        '?logdate=' +
        date,
      salesTransHdr
    );
    return this.http.put<SalesBillDetails>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdrresource/salesfinalsavereturningbillentity/' +
        salesTransHdr.id +
        '/' +
        'MAIN' +
        '?logdate=' +
        date,
      salesTransHdr
    );
  }
  getsalesTransHdrHistory() {
    console.log('salesTransHdrHistory');

    return this.salesTransHdrHistory;
  }

  savesalesTransHdrHistory(salesTransHdr: SalesTransHdr) {
    console.log('savesalesTransHdrHistory.........!!');
    console.log(salesTransHdr);
    this.salesTransHdrHistory = salesTransHdr;

    console.log(this.salesTransHdrHistory.id + 'i...............d');
  }

  getSalesDtlBydtlId(id: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salesdtl/' +
        id +
        '/getsalesdtlbyid'
    );
    return this.http.get<SalesDtl>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salesdtl/' +
        id +
        '/getsalesdtlbyid'
    );
  }

  deleteSalesDtl(id: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salesdtlsandoffer/' +
        id
    );

    return this.http.delete<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salesdtlsandoffer/' +
        id
    );
  }

  generateReport(
    userid: string,
    branchName: string,
    dateConvFist: any,
    dateConvSec: any
  ) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/dailysalesreport/userwisesalesreportsummary/' +
        userid +
        '/' +
        branchName +
        '?fdate=' +
        dateConvFist +
        '&&tdate=' +
        dateConvSec
    );
    return this.http.get<DailySalesReport[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/dailysalesreport/userwisesalesreportsummary/' +
        userid +
        '/' +
        branchName +
        '?fdate=' +
        dateConvFist +
        '&&tdate=' +
        dateConvSec
    );
  }

  getSalesTransHdrForUserSale(
    voucherNumber: any,
    voucherDate: any,
    branchName: string
  ) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdrbyvoucheranddate/' +
        voucherNumber +
        '/' +
        branchName +
        '?rdate=' +
        voucherDate
    );
    return this.http.get<DailySalesReport[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdrbyvoucheranddate/' +
        voucherNumber +
        '/' +
        branchName +
        '?rdate=' +
        voucherDate
    );
  }

  getUserWiseSalesReceiptReport(
    userid: string,
    branchName: string,
    dateConvFist: any,
    dateConvSec: any
  ) {
    return this.http.get<ReceiptModeReport[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salesreceipts/userwisesalesreceiptsummaryreport/' +
        userid +
        '/' +
        branchName +
        '?fdate=' +
        dateConvFist +
        '&&tdate=' +
        dateConvSec
    );
  }

  getSalesTransHdr(id: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdr/' +
        id
    );
    return this.http.get<SalesTransHdr>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdr/' +
        id
    );
  }

  getHoldedPossSalesByUser(username: string) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        environment.myBranch +
        '/salestranshdr/getholdedpossalesbyuser/' +
        username
    );

    return this.http.get<SalesTransHdr[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        environment.myBranch +
        '/salestranshdr/getholdedpossalesbyuser/' +
        username
    );
  }

  getHoldedPossDtls(myBranch: string) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdrresource/getposdetail/' +
        myBranch
    );

    return this.http.get<any[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdrresource/getposdetail/' +
        myBranch
    );
  }

  getCategoryById(categoryid: String) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/categorymstbyid/' +
        categoryid
    );

    return this.http.get<any[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/categorymstbyid/' +
        categoryid
    );
  }

  getGroupWiseSalesReport(
    fromdate: any,
    todate: any,
    branchname: any,
    temp: string
  ) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdrreport/getpharmacygroupwisesalesdtlreport/' +
        branchname +
        '?fdate=' +
        fromdate +
        '&&tdate=' +
        todate +
        '&&category=' +
        temp
    );

    return this.http.get<any[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestranshdrreport/getpharmacygroupwisesalesdtlreport/' +
        branchname +
        '?fdate=' +
        fromdate +
        '&&tdate=' +
        todate +
        '&&category=' +
        temp
    );
  }

  getAllReceiptMode() {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'findallreceipt'
    );

    return this.http.get<ReceiptModeMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'findallreceipt'
    );
  }

  getSumofSalesReceiptBySalestransHdr(id: any) {
    return this.http.get<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salesreceiptamount/' +
        id
    );
  }

  getSalesTypes() {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestypemstrepository/salestypemsts/' +
        environment.myBranch
    );

    return this.http.get<SalesTypeMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salestypemstrepository/salestypemsts/' +
        environment.myBranch
    );
  }
  saveSupplierCreationForm(supplier: Supplier) {
    console.log(supplier);
    return this.http.post<Supplier>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/supplier',
      supplier
    );
  }
  generateVoucherNumber(branch: any): any {
    return this.http.get<string>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/vouchernumber?id=' +
        branch,
      { responseType: 'text' as 'json' }
    );
  }
  showSupplierDetails() {
    return this.http.get<Supplier[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/suppliers'
    );
  }
  // getAccountHead(supname:any) {
  //   return this.http.get<AccountHeads[]>(environment.hostURL +':' +environment.port +'/' +environment.company +'/accountheadsbyname/'+supname);
  // }
  updateSupplier(supplier: Supplier, id: any) {
    console.log(
      (environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/supplierresource/' +
        supplier.id +
        '/updatesupplier',
      supplier)
    );
    return this.http.put<Supplier>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/supplierresource/' +
        id +
        '/updatesupplier',
      supplier
    );
  }

  showItemByname(item: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itemmst/itemname?itemdata=' +
        item
    );
    return this.http.get<datas[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itemmst/itemname?itemdata=' +
        item
    );
  }

  savePurchaseHdr(purchaseHdr: PurchaseHdr) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/purchasehdr',
      purchaseHdr
    );
    return this.http.post<PurchaseHdr>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/purchasehdr',
      purchaseHdr
    );
  }
  savePurchaseDtl(purchaseDtl: PurchaseDtl, id: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/purchasehdr/' +
        id +
        '/purchasedtl',
      purchaseDtl
    );
    return this.http.post<PurchaseDtl>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/purchasehdr/' +
        id +
        '/purchasedtl',
      purchaseDtl
    );
  }
  showSummary(id: any) {
    return this.http.get<Summary[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/purchasehdr/' +
        id +
        '/purchasedtlsummary'
    );
  }

  getAllSalesReceiptsBySalesTransHdr(id: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/retrivesalesreceipts/' +
        id
    );
    return this.http.get<SalesReceipts>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/retrivesalesreceipts/' +
        id
    );
  }

  getSumofSalesReceiptbySalesTransHdr(id: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salesreceiptamount/' +
        id
    );
    return this.http.get<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salesreceiptamount/' +
        id
    );
  }

  getTaxSummaryDetails(voucherNumber: any, branchCode: any, voucherDate: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salesinvoicetax/' +
        voucherNumber +
        '/' +
        branchCode +
        '?rdate=' +
        voucherDate
    );
    return this.http.get<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/salesinvoicetax/' +
        voucherNumber +
        '/' +
        branchCode +
        '?rdate=' +
        voucherDate
    );
  }

  getItemMstByCategoryId(catId: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itemmst/' +
        catId +
        '/categorymst'
    );
    return this.http.get<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itemmst/' +
        catId +
        '/categorymst'
    );
  }
  getPartyDetails(searchfield: any, sup: any, both: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        'accountheads/searchaccountbysupplierandboth' +
        '?data=' +
        searchfield
    );

    return this.http.get<AccountHeads[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'accountheads/searchaccountbysupplierandboth' +
        '?data=' +
        searchfield
    );
  }

  getPurchaseDtl(purchasehdr: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'purchasehdr/' +
        purchasehdr.id +
        '/purchasedtl'
    );

    return this.http.get<PurchaseDtl[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'purchasehdr/' +
        purchasehdr.id +
        '/purchasedtl'
    );
  }

  GetSearchItemByNameWithComp(searchfiled: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'itempopupsearchwithcomp?data=' +
        searchfiled
    );

    return this.http.get<any[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'itempopupsearchwithcomp?data=' +
        searchfiled
    );
  }

  deleteSelectedItems(id: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'purchasedtl/' +
        id
    );

    return this.http.delete<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'purchasedtl/' +
        id
    );
  }

  updatePurchaseHdr(purchaseHdr: PurchaseHdr, id: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'webpurchasehdr/' +
        id,
      purchaseHdr
    );

    return this.http.put<PurchaseHdr>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'webpurchasehdr/' +
        id,
      purchaseHdr
    );
  }

  getAddKotWaiter() {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/addkotwaiters'
    );
    return this.http.get<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/addkotwaiters'
    );
  }
  getAddKotTable() {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/addkottables'
    );
    return this.http.get<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/addkottables'
    );
  }

  getKotCategory() {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/kotcategoryinventorymstresource/allcategories'
    );
    return this.http.get<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/kotcategoryinventorymstresource/allcategories'
    );
  }

  getKotItemByCategory(selectedCategory: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/kotcategoryinventorymstresource/' +
        selectedCategory +
        '/allitemsbycategory'
    );
    return this.http.get<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/kotcategoryinventorymstresource/' +
        selectedCategory +
        '/allitemsbycategory'
    );
  }

  getItemMstByName(itmName: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itemmst/' +
        itmName +
        '/itemname'
    );
    return this.http.get<ItemMst>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itemmst/' +
        itmName +
        '/itemname'
    );
  }

  saveKotTableHdr(kotTablehdr: KotTableHdr) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/kotresource/savekothdr',
      kotTablehdr
    );

    return this.http.post<KotTableHdr>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/kotresource/savekothdr',
      kotTablehdr
    );
  }

  saveKotTableDtl(kotTableDtl: KotTableDtl) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/kotresource/savekotdtl',
      kotTableDtl
    );

    return this.http.post<KotTableDtl>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/kotresource/savekotdtl',
      kotTableDtl
    );
  }

  getKotTableDtlByHdr(kotTablehdr: KotTableHdr) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/kotresource/getkottabledtlbyhdr/' +
        kotTablehdr.id
    );

    return this.http.get<KotTableDtl[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/kotresource/getkottabledtlbyhdr/' +
        kotTablehdr.id
    );
  }

  printKot(id: any, kotTablehdr: KotTableHdr) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/kotresource/printkot/' +
        id,
      kotTablehdr
    );

    return this.http.put<KotTableHdr>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/kotresource/printkot/' +
        id,
      kotTablehdr
    );
  }

  getPruchaseTaxDtl(voucherNumber: any, ourVoucherDate: any, branchCode: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'retrivetaxrate/' +
        voucherNumber +
        '/' +
        branchCode +
        '?rdate=' +
        ourVoucherDate
    );

    return this.http.get<PurchaseReport[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'retrivetaxrate/' +
        voucherNumber +
        '/' +
        branchCode +
        '?rdate=' +
        ourVoucherDate
    );
  }

  getAccountHeads(supplierId: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'accountheadsbyid/' +
        supplierId
    );

    return this.http.get<AccountHeads>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'accountheadsbyid/' +
        supplierId
    );
  }

  printBill(id: any, kotTablehdr: KotTableHdr) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/kotresource/kotfinalsave/' +
        id,
      kotTablehdr
    );

    return this.http.put<KotTableHdr>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/kotresource/kotfinalsave/' +
        id,
      kotTablehdr
    );
  }

  getKotTableDtlByTableName(tableName: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/kotresource/getkottabledtlbytablename/' +
        tableName
    );

    return this.http.get<KotTableDtl[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/kotresource/getkottabledtlbytablename/' +
        tableName
    );
  }

  deleteItem(kotTableDtl: KotTableDtl) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/kotresource/deleteitem/' +
        kotTableDtl.id
    );

    return this.http.delete<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/kotresource/deleteitem/' +
        kotTableDtl.id
    );
  }

  loginChecking(uName: any, password: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/usermst/' +
        uName +
        '/' +
        password +
        '/web/weblogin'
    );

    return this.http.get<UserMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/usermst/' +
        uName +
        '/' +
        password +
        '/web/weblogin'
    );
  }

  getItemMstBySearchableString(searchfiled: string) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itempopup/' +
        searchfiled
    );

    return this.http.get<itemDetails[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itempopup/' +
        searchfiled
    );
  }

  getAllItems() {
    return this.http.get<ItemMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itemmsts'
    );
  }

  getItemWiseSalesReport(fromdate: any, todate: any, branchname: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itembatchmstresource/getitemwisesalesreportbyitemnamewithbranch/' +
        branchname +
        '?fromdate=' +
        fromdate +
        '&&todate=' +
        todate
    );

    return this.http.get<itemDetails[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itembatchmstresource/getitemwisesalesreportbyitemnamewithbranch/' +
        branchname +
        '?fromdate=' +
        fromdate +
        '&&todate=' +
        todate
    );
  }

  getPurchaseConsolidatedReport(fromdate: any, todate: any, branchname: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/purchasehdrresource/getpurchaseconsolidatedreport/' +
        branchname +
        '?fromdate=' +
        fromdate +
        '&&todate=' +
        todate
    );

    return this.http.get<PurchaseReport[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/purchasehdrresource/getpurchaseconsolidatedreport/' +
        branchname +
        '?fromdate=' +
        fromdate +
        '&&todate=' +
        todate
    );
  }

  getProfitAndLossReport(selecteddate: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/trialbalance' +
        '?startdate=' +
        selecteddate
    );

    return this.http.get<any[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/trialbalance' +
        '?startdate=' +
        selecteddate
    );
  }
  getSalesAndStockConsolidatedReport(
    fromdate: any,
    todate: any,
    frombranchname: any,
    temp: string
  ) {
    return null;
  }

  getWebsiteConstants() {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
      
        'websiteresource/websitecontent'
    );

    return this.http.get<WebsiteClass>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
      
        'websiteresource/websitecontent'
    );
  }
  finalSaveforCloudPOS(finalSales: Map<string, Object[]>) {
    console.log(finalSales);
    return this.http.post<SalesBillDetails>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/cloud/sales',
      Object.fromEntries(finalSales)
    );
  }

  PostSalesDtlToCloudPosWindow(salesDtl: SalesDtl) {
    if(this.salesDtlArr.length!=0){
      for(let i=0;i<this.salesDtlArr.length;i++){
          if(this.salesDtlArr[i].itemId==salesDtl.itemId){
            this.salesDtlArr[i].qty+=salesDtl.qty;
            this.salesDtlArr[i].amount=this.salesDtlArr[i].amount*this.salesDtlArr[i].qty;
            // alert("Item exists in cart");
            this.salesDtlsFrompopup.next(this.salesDtlArr);
            return;     
          }
      }
    }
    this.salesDtlArr.push(salesDtl);
    console.log("In Post SalesDtl to Cloud POS or Cloud Wholesale")
    console.log(this.salesDtlArr);
    this.salesDtlsFrompopup.next(this.salesDtlArr);
    // console.log(this.salesDtlsFrompopup.value + 'valllleee');
  }

  // getSalesDtlFromCloudPOSWindow(){
  //   return this.salesDtlArr.asObservable();
  // }

  // salesPdfForWeb(id:any){
  //   return this.http.get<any[]>(
  //     environment.hostURL +
  //       ':' +
  //       environment.port +
  //       '/' +
  //       environment.company +
  //       '/websalespdf?hdrid='+id
  //   );
  // }

  //register company
  registerCompany(company: CompanyMst) {
     console.log(company.id+"rlflllllllllllllllll");
    return this.http.post<CompanyMst>(
      environment.hostURL + ':' + environment.port + '/companymstforcloudweb',
      company
    );
  }

  //get country
  getCountryList() {
    // return this.country;
    return this.http.get<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/companymst/getcountry'
    );
  }

  imageUpload(selectedFile: FormData) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itemimageupload?file=' +
        selectedFile
    );

    return this.http.post<string>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itemimageupload',
      selectedFile,
      { responseType: 'text' as 'json' }
    );
  }

  getItemImage(itemId: any) {
    return this.http.get<ItemImageUrlDetail[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itemmstresource/getimageurlbyitemid/' +
        itemId
    );
  }

  saveBranch(branch: BranchMst) {
    return this.http.post<BranchMst>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/branch',
      branch
    );
  }


// getItemImage(itemId:any){
//   return this.http.get<ItemImageUrlDetail>(environment.hostURL + ':' + environment.port + '/' +  environment.company + '/itemmstresource/getimageurlbyitemid/'+itemId);
// }

saveCompanyUserRegistration(userMst: UserMst, companyId: any) {
  console.log(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      companyId +
      '/usermstresource/webusercompany',
    userMst
  );
  return this.http.post<UserMst>(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      companyId +
      '/usermstresource/webusercompany',
    userMst
  );
}

  saveAccounts(accountheads: AccountHeads) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/accountheadsresource/partyregistration',
      accountheads
    );
    return this.http.post<AccountHeads>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/accountheadsresource/partyregistration',
      accountheads
    );
    // return this.http.post<Accounts>('http://192.168.2.13:8185/accountheadsresource/partyregistration',accounts);
  }

  showAllAccounts() {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/accountheads'
    );
    return this.http.get<AccountHeads[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/accountheads'
    );
  }
  getAssetAccountsforGroup() {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/accountheads/fetchaccountheadsbyassetaccount'
    );
    return this.http.get<AccountHeads[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/accountheads/fetchaccountheadsbyassetaccount'
    );
  }
  getCompanyName() {
    console.log(this.HOST + ' company name from service');
    return this.HOST;
  }

  getAllReceiptModeWithoutCash() {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/receiptmodemstresource/findallreceiptwithoutcash'
    );

    return this.http.get<ReceiptModeMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/receiptmodemstresource/findallreceiptwithoutcash'
    );
  }

  purchasePdfForWeb(id: any) {
    return this.http.get<any[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/webpurchasepdf?hdrid=' +
        id
    );
  }

   getInvoiceFormat() {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/getalljasperbypricetype'
    );
    return this.http.get<any[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/getalljasperbypricetype'
    );
  }

  getSalesMan(){
    console.log(environment.hostURL+':'+environment.port+'/'+environment.company+'/salesmanmstresource/findallsalesman')
    return this.http.get<any>(environment.hostURL+':'+environment.port+'/'+environment.company+'/salesmanmstresource/findallsalesman');
  }

  getPriceType() {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/findallpricedefinitionmst'
    );
    return this.http.get<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/findallpricedefinitionmst'
    );
  }


getBranches(){
  // return this.branch;
  return this.http.get<BranchMst[]>(environment.hostURL + ':' + environment.port + '/' + environment.company + '/allbranches');
  }

  saveStockTransferHdrInService(stockHdr: StockTransferOutHdr) {
  //   console.log(stockHdr)

  // console.log( environment.hostURL +
  //   ':' +
  //   environment.port +
  //   '/' +
  //   environment.company +
  //   '/stocktransferouthdr',
  //   stockHdr
  //   )

  // return this.http.post<StockTransferOutHdr>(
  //   environment.hostURL +
  //   ':' +
  //   environment.port +
  //   '/' +
  //   environment.company +
  //   '/stocktransferouthdr',
  //   stockHdr
  // );
  console.log(stockHdr)

  console.log( environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/stocktransferouthdrresource/savestocktransferouthdr',
    stockHdr
    )
  return this.http.post<StockTransferOutHdr>(
    environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/stocktransferouthdrresource/savestocktransferouthdr',
    stockHdr
  );

  }

  getStockTransferHdrById(stocktranshdrid : any) {
    console.log( environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/stocktransferouthdr/stocktransferouthdrbyid/'+stocktranshdrid
     )

    return this.http.get<StockTransferOutHdr>(
      environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/stocktransferouthdr/stocktransferouthdrbyid/'+stocktranshdrid
       
    );
  }

  // getItemMstByItemName(itemName:any){

  //   console.log( environment.hostURL +
  //     ':' +
  //     environment.port +
  //     '/' +
  //     environment.company +
  //     '/itemmst/itemname?itemdata=',itemName
  //    )
     
  //   return this.http.get<any>(
  //     environment.hostURL +
  //     ':' +
  //     environment.port +
  //     '/' +
  //     environment.company +
  //     '/itemmst/itemname?itemdata=',itemName
  //   );
  // }
  saveStockTransferDtl(stockDtl: StockTransferOutDtl) {
    console.log("in service - saving stock detail")
    console.log(stockDtl)
    console.log( environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/stocktransferouthdrforweb/'+
      stockDtl.stockTransferOutHdr.id+
      '/stocktransferoutdtl',
      stockDtl
      )
  
    return this.http.post<StockTransferOutDtl>(
      environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/stocktransferouthdrforweb/'+
      stockDtl.stockTransferOutHdr.id+
      '/stocktransferoutdtl',
      stockDtl
    );
   }
  getAllStockDetails(stockhdrid: any) {
    console.log( environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/stocktransferouthdr/'+stockhdrid +'/stocktransferoutdtl'
     )

    return this.http.get<StockTransferOutDtl[]>(

      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +

        '/stocktransferouthdr/'+stockhdrid +'/stocktransferoutdtl' 
       
    );
  }
  getItemDetailsfromserver(dateConversion: any,searchfiled: any) {
    
    console.log( environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/stockitempopupsearchwithbatch?date=' +dateConversion+
      '&&data=' +
      searchfiled)
       
    return this.http.get<ItemStockPopUp[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/stockitempopupsearchwithbatch?date=' +dateConversion+
      '&&data=' +
      searchfiled
    );
   
    
    
  }

  additemDtl(itmDtls: ItemStockPopUp) {
    console.log('itmDtl');
    this.itemsDtl.next(itmDtls);
  }
  getitemsDtl() {
    return this.itemsDtl.asObservable();
  }
  
  sumOfTotalAmount(headerid : any) {
    console.log( environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/stockrequestdtlresource/' + headerid +'/sumTotalStockTranOut'
     )

    return this.http.get<any>(
        environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/stocktransferoutdtl/' + headerid +'/sumTotalStockTranOut'  
         
      );
  }
  // stocktranferheader
  finalSaveStockTranfer(stocktransferouthdrid:any,stocktranferheader : StockTransferOutHdr) {
 

    console.log( environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/stocktransferouthdrresource/finalsave/' + stocktransferouthdrid,stocktranferheader
       )

    return this.http.put<StockTransferOutHdr>(
      environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/stocktransferouthdrresource/finalsave/' +
      stocktransferouthdrid,stocktranferheader
    );
    }

  saveUserSubsidiary(userSub: UserSubsidiary) {
    console.log(environment.hostURL +':' +environment.port +'/' +environment.company +'/usersubsidiaryresource/userbranch',userSub)
    return this.http.post<UserSubsidiary>(environment.hostURL +':' +environment.port +'/' +environment.company +'/usersubsidiaryresource/userbranch',userSub);
  }

  showAllUserBranchAssociation(){
    console.log(environment.hostURL +':' +environment.port +'/' +environment.company +'/usersubsidiaryresource/usersubsidiary')
    return this.http.get<UserSubsidiary[]>(environment.hostURL +':' +environment.port +'/' +environment.company +'/usersubsidiaryresource/usersubsidiary');
  }

  deleteUserBranchAssociationById(userSubId: String) {
    console.log(environment.hostURL +':' +environment.port +'/' +this.companyname +'/usersubsidiary/' +userSubId);
    return this.http.delete<String>(environment.hostURL +':' +environment.port +'/' +this.companyname +'/usersubsidiary/' +userSubId);
  }

  getAllBranchesByUserId(user:any){
    console.log(environment.hostURL +':' +environment.port +'/' +environment.company +'/usersubsidiaryresource/usersubsidiary/'+user)
    return this.http.get<UserSubsidiary[]>(environment.hostURL +':' +environment.port +'/' +environment.company +'/usersubsidiaryresource/usersubsidiary/'+user);
  }

  setmyBranch(branch:any){
    environment.myBranch=branch;
    console.log(environment.myBranch+' myBranch updated');
  }


  getAllUsers(){
    return this.http.get<UserMst[]>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/getallusers/'+environment.myBranch
    );
  }
  getMaxDayEndClosure( branchname: string) {
    return this.http.get<DayEndClosureHdr>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'dayendclosuremst/maxofdayend/' +
        branchname 
        
    );
  }

  saveStockRequestHdr(stockRequestHdr: StockRequestHdr) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/stockrequesthdrresource/savestockrequesthdr',
        stockRequestHdr
    );
    return this.http.post<StockRequestHdr>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/stockrequesthdrresource/savestockrequesthdr',
        stockRequestHdr
    );
  }


  saveStockRequestDtl(stockDtl: StockRequestDtl) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/stockrequestdtlresource/' +
        stockDtl.stockRequestHdr.id +
        '/savestockrequestdtl',
        stockDtl
    );
    return this.http.post<StockRequestDtl>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/stockrequestdtlresource/' +
        stockDtl.stockRequestHdr.id +
        '/savestockrequestdtl',
        stockDtl
    );
  }

  getShowAllStockRequestDtl(stockrequesthdrid: any) {
    console.log( environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/stockrequestdtlresource/'+stockrequesthdrid +'/stockrequestdtl'
     )

    return this.http.get<StockRequestDtl[]>(

      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +

        '/stockrequestdtlresource/'+stockrequesthdrid +'/stockrequestdtl' 
       
    );
  }

  stockRequestsumOfTotalAmount(headerid : any) {
    console.log( environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/stockrequestdtlresource/' + headerid +'/sumtotalstockrequest'
     )

    return this.http.get<any>(
      environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/stockrequestdtlresource/' + headerid +'/sumtotalstockrequest'  
       
    );
  }


  deleteStockRequestSelectedItems(id: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/stockrequestdtlresource/' +
        id
    );

    return this.http.delete<any>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        
        '/stockrequestdtlresource/' +
        id
    );
  }



  updateStockRequestHdr(stockRequestHdr: StockRequestHdr, id: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'stockrequesthdrresource/' +
        id,
        stockRequestHdr
    );

    return this.http.put<StockRequestHdr>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/' +
        'stockrequesthdrresource/' +
        id,
        stockRequestHdr
    );
  }

  retrieveAllItemBatchDtlforStockTransfer(id: any) {
    console.log(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itembatchdtlresource/stockverificationforstocktransfer/' +
        id,{ responseType: 'text' as 'json' }
    );
    return this.http.get<String>(
      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +
        '/itembatchdtlresource/stockverificationforstocktransfer/' +
        id,{ responseType: 'text' as 'json' }
    );
  }





  getHoldedStockRequest() {
    console.log( environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/stockrequesthdrresource/holdedstockrequesthdrs'
     )

    return this.http.get<StockRequestHdr[]>(

      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +

        '/stockrequesthdrresource/holdedstockrequesthdrs' 
       
    );
  }

  getStockRequestReport(vocherNumber:any,date:any) {
    console.log( environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/stockrequesthdrresource/stockrequestreport/'+vocherNumber+
      '?rdate=' +
      date
     )

    return this.http.get<StockRequestReport[]>(

      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +

        '/stockrequesthdrresource/stockrequestreport/' +vocherNumber
        + '?rdate=' +
        date
       
    );
  }



// Final Save
// Saving in table 
fetchAccountHeadsByAccountName() {
  console.log(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/accountheads/fetchaccountheadsbybankaccount'
  );

  return this.http.get<AccountHeads[]>(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/accountheads/fetchaccountheadsbybankaccount'
  );
}

  
// Saving receiptHdr
saveReceiptHdr(receiptHdr: ReceiptHdr) {
  console.log(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/receipthdr',
      receiptHdr
  );
  return this.http.post<ReceiptHdr>(
    environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/receipthdr',
    receiptHdr
  );    
}

// Saving receiptDtl
saveReceiptDtl(receiptDtl: ReceiptDtl, id: any) {
  console.log(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/receipthdr/' +
      id +
      '/receiptdtl',
      receiptDtl
  );
  return this.http.post<ReceiptDtl>(
    environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/receipthdr/' +
    id +
    '/receiptdtl',
    receiptDtl
  );
}
// Updating receiptDtl
updateReceiptHdr(receiptHdr: ReceiptHdr, id: any) {
  console.log(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/' +
      'receipthdrresource/finalsavereceipthdr/' +
      id,
      receiptHdr
  );

  return this.http.put<ReceiptHdr>(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/' +
      'receipthdrresource/finalsavereceipthdr/' +
      id,
      receiptHdr
  );
}
// Receipt Total
// getSummaryReceiptTotal(id: any) {
//   console.log(
//     environment.hostURL +
//       ':' +
//       environment.port +
//       '/' +
//       environment.company +
//       '/receiptdtl/' +
//       id +
//       '/receipttotalamount'
//   );
//   return this.http.get<any>(
//     environment.hostURL +
//       ':' +
//       environment.port +
//       '/' +
//       environment.company +
//       '/receiptdtl/' +
//       id +
//       '/receipttotalamount'
//   );
// }

// Receipt Excel - Report


receiptWindowPdfForWeb(id: any) {
  return this.http.get<any[]>(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/webreceiptwindowreportpdf?hdrid=' +
      id
  );
}


// receiptWindowPdfForWeb(id: any) {
//   return this.http.get<any[]>(
//     environment.hostURL +
//       ':' +
//       environment.port +
//       '/' +
//       environment.company +
//       '/webreceiptwindowreportpdf' +
//       id
//   );
// }

receiptReportPdf(voucherNumber: any,voucherDate:any) {
  return this.http.get<any[]>(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/receipthdrresource/receiptreport/' + voucherNumber + '?rdate=' + voucherDate
      
  );
}

  getPurchaseReport(purchaseHdrId:any){
    console.log(environment.hostURL +':' +environment.port +'/' +environment.company +'/purchasereportresource/getimportpurchaseinvoice/'+purchaseHdrId)
    return this.http.get<PurchaseReport[]>(environment.hostURL +':' +environment.port +'/' +environment.company +'/purchasereportresource/getimportpurchaseinvoice/'+purchaseHdrId);
  }

  showPurchaseSummary(id: any) {
    console.log(environment.hostURL +':' +environment.port +'/' + environment.company +'/purchasehdr/' +id + '/purchasedtlsummary')
    return this.http.get<Summary>(environment.hostURL +':' +environment.port +'/' + environment.company +'/purchasehdr/' +id + '/purchasedtlsummary');
  }

  purchaseDetailDelete(purchaseDtlId: any) {
    console.log("purchaseDetailDelete in service ")
    return this.http.delete<any>(environment.hostURL +':' +environment.port +'/' + environment.company +'/purchasedtl/' +purchaseDtlId );
  }


  getStockTransferReport(vocherNumber:any,date:any) {
    console.log( environment.hostURL +

      ':' +
      environment.port +
      '/' +
      environment.company +

      '/stocktransferoutreport/'+vocherNumber+
      '?rdate=' +
      date
     )

    return this.http.get<StockTransferOutReport[]>(

      environment.hostURL +
        ':' +
        environment.port +
        '/' +
        environment.company +

        '/stocktransferoutreport/' +vocherNumber
        + '?rdate=' +
        date
       
    );
  }

// --------------------------------------PAYMENT WINDOW--------------------------------------------//
//..............SAVE - Hdr.........................
savePaymentHdr(paymentHdr: PaymentHdr){
  console.log(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/paymenthdr',
      paymentHdr
  );
  return this.http.post<PaymentHdr>(
    environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/paymenthdr',
    paymentHdr
  );    
}
//..............SAVE - Dtl.........................
savepaymentDtl(paymentDtl: PaymentDtl, id: any) {
  console.log(
    environment.hostURL +

      ':' +
      environment.port +
      '/' +
      environment.company +

      '/paymenthdr/' +
      id +
      '/paymentdtl',
      paymentDtl
  );
  return this.http.post<PaymentDtl>(
    environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/paymenthdr/' +
    id +
    '/paymentdtl',
    paymentDtl
  );
}

//................FINAL SAVE..............................
updatePaymentHdr(paymentHdr: PaymentHdr, id: any) {
  console.log(
    environment.hostURL +

      ':' +
      environment.port +
      '/' +
      environment.company +

      '/' +
      'paymenthdrresource/' +
      id,
      paymentHdr
  );

  return this.http.put<PaymentHdr>(
    environment.hostURL +

      ':' +
      environment.port +
      '/' +
      environment.company +
      '/' +
      'paymenthdrresource/' +
      id,
      paymentHdr
  );
}


getStockTransferBetweenDate(fromdate: any, todate: any, branchname: any) {
  console.log(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +

      '/stocktransferouthdr/stocktransferouthdrbetweendate/' +
      branchname +
      '?fdate=' +
      fromdate +
      '&&tdate=' +
      todate
  );

  return this.http.get<StockTransferOutHdr[]>(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/stocktransferouthdr/stocktransferouthdrbetweendate/' +
      branchname +
      '?fdate=' +
      fromdate +
      '&&tdate=' +
      todate
  );
}



  savesalesman(salesMan:SalesManMst){
    console.log(
      environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/salesmanmstresource/createsalesman',
    salesMan
  );
  return this.http.post<SalesManMst>(
    environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/salesmanmstresource/createsalesman',
    salesMan
  );
}


  resendStockTransfer(id: any) {
  console.log(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +

      '/itembatchdtlresource/stockverificationforstocktransfer/' +
      id,{ responseType: 'text' as 'json' }
  );
  return this.http.get<String>(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/itembatchdtlresource/stockverificationforstocktransfer/' +
      id,{ responseType: 'text' as 'json' }
  );
}

deleteSalesman(salesManId:any){
  
console.log( environment.hostURL +
  ':' +
  environment.port +
  '/' +
  environment.company +
  '/salesmanmstresource/'+salesManId+'/deletesalesman');

  return this.http.delete<any>(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/salesmanmstresource/'+salesManId+'/deletesalesman'
  );
}


getStockTransferOutDtlByVoucherNo(branchname:any,voucherNo:any){
  console.log(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +

      '/stocktransferoutdtl/stocktransferoutdtlbyvoucher/' +voucherNo+'/'+
      branchname 
      
  );
  return this.http.get<StockTransferOutDtl[]>(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/stocktransferoutdtl/stocktransferoutdtlbyvoucher/' +voucherNo+'/'
      +branchname );
}

getQtyFromItemBatchMstByItemIdAndQty(itemId:any,batch:any,storeName:any) {
  console.log( environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/itembatchmstresource/itembatchdtlqty/'  + batch +'/'+itemId+'/'+storeName 
   );
  
   return this.http.get<any>(
     environment.hostURL +
     ':' +
     environment.port +
     '/' +
     environment.company +

     '/itembatchmstresource/itembatchdtlqty/' + batch +'/'+itemId+'/'+storeName 
      
   );
   }

// .....................PAYMENT REPORT.........................
paymentReportPdf(vouchernumber: any, branchcode:any, date:any) {
      console.log(
      environment.hostURL +      
      ':' +
      environment.port +
      '/' +
      environment.company +      
      '/' +
      'paymentreport/' + vouchernumber +'/'+ branchcode + '?rdate=' + date      
      );  

        return this.http.get<PaymentVoucher>(
          environment.hostURL +
          
          ':' +
          environment.port +
          '/' +
          environment.company +
          '/' +
          'paymentreport/' + vouchernumber +'/'+ branchcode + '?rdate=' + date 
          );
    }


  
deleteStockTranferDtl(stocktransferoutdtlid){
  console.log( environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/stocktransferoutdtl/' +stocktransferoutdtlid
   );

  return this.http.delete<any>(
    environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/stocktransferoutdtl/' +stocktransferoutdtlid
   );
}

getHoldedStockTransfer(){

  console.log( environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/stocktransferouthdr/holdedstocktransferouthdrs'
   );


  return this.http.get<any>(
    environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/stocktransferouthdr/holdedstocktransferouthdrs'
  );

  
   

}


getAllItemBatchDtlforStockTransfer(stkhdrid:any){

  console.log( environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/itembatchdtlresource/stockverificationforstocktransfer/' + stkhdrid 
   );

  return this.http.get<string>(
    environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/itembatchdtlresource/stockverificationforstocktransfer/' + stkhdrid,{ responseType: 'text' as 'json' } 
   );


}




getStockTransferInvoice(voucherNo:any,date:any){
  console.log(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +

      '/stocktransferoutreport/' +voucherNo+'?rdate='+date       
  );

  return this.http.get<StockTransferOutReport[]>(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/stocktransferoutreport/' +voucherNo+'?rdate='+date );
}

stockOutReportSummaryBetweenDate(fromdate: any, todate: any){
  console.log(environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/stocktransferoutreport/stocktransferoutsummaryreportbetweendate?fromdate=' +
    fromdate +
    '&&todate=' +
    todate);

  return this.http.get<StockTransferOutReport[]>(
    environment.hostURL +
      ':' +
      environment.port +
      '/' +
      environment.company +
      '/stocktransferoutreport/stocktransferoutsummaryreportbetweendate?fromdate=' +
      fromdate +
      '&&todate=' +
      todate);
}

setFromDateForReport(fDate:any){
this.fromDate=fDate;
}

setToDateForReport(tDate:any){
this.toDate=tDate;
}

getFromDateForReport(){
return this.fromDate;
}

getToDateForReport(){
return this.toDate;
}

getVoucherValue(searchfiled: any) {
  console.log(environment.hostURL + ':' + environment.port + '/' + environment.company + '/' + 'vouchernopopupsearchwithcomp?data=' +
  searchfiled)
  return this.http.get<any[]>(environment.hostURL + ':' + environment.port + '/' + environment.company + '/' + 'vouchernopopupsearchwithcomp?data=' +
    searchfiled);
}

// setVoucherDateForReport(voucherDate:any){
//   this.voucherDate=voucherDate;
//   }
  
//   setVoucherNumberForReport(voucherNo:any){
//   this.voucherNumber=voucherNo;
//   }
  
//   getVoucherDateForReport(){
//   return this.voucherDate;
//   }
  
//   getVoucherNumberForReport(){
//   return this.voucherNumber;
//   }


saveJournalHdr(journalHdr:JournalHdr){

  console.log(environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/journalhdrresource',
    journalHdr);

  return this.http.post<JournalHdr>(
    environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/journalhdrresource',
    journalHdr
  );
}

saveJournalDtl(id:any,journalDtl:JournalDtl){
  console.log(environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/journaldtlresource/'+id+
    '/journaldtl',
    journalDtl)

  return this.http.post<JournalDtl>(
    environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/journaldtlresource/'+id+
    '/journaldtl',
    journalDtl
  );
}
getJournalDebitTotal(id:any){

console.log(environment.hostURL +
  ':' +
  environment.port +
  '/' +
  environment.company +
  '/journaldtl/'+id+
  '/sumdebitamount')

return this.http.get<any>(
    environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/journaldtl/'+id+
    '/sumdebitamount'
  );
}

getJournalCreditTotal(id:any){
 console.log(
    environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/journaldtl/'+id+
    '/sumcreditamount'
  );
  
  return this.http.get<any>(
    environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/journaldtl/'+id+
    '/sumcreditamount'
  );
}

getJournalDtl(journalHdr:JournalHdr){
  console.log(
    environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/journalhdr/'+journalHdr.id+
    '/journaldtls'
  );
  
  return this.http.get<JournalDtl[]>(
    environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/journalhdr/'+journalHdr.id+
    '/journaldtls'
  );
}

updateJournalFinalSave(journalHdr:JournalHdr){
  console.log('In update Journal-- final save')
  return this.http.put<JournalHdr>(
    environment.hostURL +
    ':' +
    environment.port +
    '/' +
    environment.company +
    '/journalhdrresource/'+journalHdr.id,journalHdr
  );
}

addVoucherDetail(voucherdtls: Voucherstock) {
  console.log('voucherDtl');
  this.vouchDtl.next(voucherdtls);
}
getVoucherDtl() {
  return this.vouchDtl.asObservable();
}

}


