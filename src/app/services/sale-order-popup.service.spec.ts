import { TestBed } from '@angular/core/testing';

import { SaleOrderPopupService } from './sale-order-popup.service';

describe('SaleOrderPopupService', () => {
  let service: SaleOrderPopupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SaleOrderPopupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
