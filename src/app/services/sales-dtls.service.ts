import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { ItemStockPopUp } from '../modelclass/item-stock-pop-up';
import { SalesBillDetails } from '../modelclass/sales-bill-details';
import { SalesDtl } from '../modelclass/sales-dtl';
import { SalesTransHdr } from '../modelclass/sales-trans-hdr';
import { ShippingAddress } from '../modelclass/shipping-address';
import { Summary } from '../modelclass/summary';
import { UnitMst } from '../modelclass/unit-mst';
import { datas } from '../Pop-Up-Windows/search-pop-up/search-pop-up.component';
import { SharedserviceService } from './sharedservice.service';

@Injectable({
  providedIn: 'root',
})
export class SalesDtlsService {
 
  salesDtl = new SalesDtl();
  getUnitByItem: UnitMst = new UnitMst();
  getItem: datas = new datas();
  dateCov: any;
  input: any;
  getSalesBillDtl: SalesDtl[] = [];
  date = new Date();
  summary: Summary = new Summary();
  salesMode: any;
  salesType: any;
  custId: any;
  localCustId: any;
  salesman: any;
  wholeSaleBillReport: SalesBillDetails = new SalesBillDetails();

  shippingDtl: ShippingAddress = new ShippingAddress();

  constructor(
    private sharedserviceService: SharedserviceService,
    public datepipe: DatePipe
  ) {}

  saveSalesDtsl(
    salesTransHdr: SalesTransHdr,
    itemSelected: ItemStockPopUp,
    productQty: any
  ) {
    this.dateCov = this.datepipe.transform(this.date, 'dd-MM-yyyy');

    console.log(salesTransHdr);
    this.salesDtl = new SalesDtl();
    this.salesDtl.salesTransHdr = salesTransHdr;
    console.log(this.salesDtl.salesTransHdr.id);
    this.salesDtl.itemName = itemSelected.itemName;
    this.salesDtl.barcode = itemSelected.barcode;
    this.salesDtl.batch = itemSelected.batch;
    this.salesDtl.qty = productQty;
    this.salesDtl.itemId = itemSelected.itemId;

    this.salesDtl.mrp = itemSelected.standardPrice;
    console.log(this.salesDtl.mrp);
    this.salesDtl.rate = this.salesDtl.mrp * productQty;
    this.sharedserviceService
      .getUnitByItem(itemSelected.unitName)
      .subscribe((subscribedData) => {
        this.getUnitByItem = subscribedData;
      });
    // this.salesDtl.unitName=this.getUnitByItem.unitName;
    this.salesDtl.unitName = itemSelected.unitName;

    console.log(this.dateCov);

    this.sharedserviceService
      .saveSalesDtl(
        this.salesDtl,
        this.dateCov,
        this.salesMode,
        this.salesType,
        this.custId,
        this.localCustId,
        this.salesman
      )
      .subscribe(
        (saveSalesDtl) => {
          this.getSalesBillDtl = saveSalesDtl;
          this.sharedserviceService.PostSalesDtlToPosWindow(
            this.getSalesBillDtl
          );
          this.sharedserviceService.savesalesTransHdrHistory(
            this.getSalesBillDtl[0].salesTransHdr
          );

          //   this.sharedserviceService.getSalesBillDtl(this.salesDtl.salesTransHdr.id).subscribe(
          //     getSalesBillDt=>{this.getSalesBillDtl=getSalesBillDt;
          //       console.log(this.getSalesBillDtl);
          //       this.sharedserviceService.PostSalesDtlToPosWindow(this.getSalesBillDtl);
          // })
        },
        (error: any) => {
          console.log(error), alert('Unable to save');
        }
      );

    console.log('saveSalesDtslService Called');
  }

  wholeSalesDetails(
    selectSalesType: string,
    selectcustName: any,
    selectSalesMan: string,
    id: String
  ) {
    this.salesType = selectSalesType;
    this.custId = id;
    this.salesman = selectSalesMan;
    this.salesMode = selectcustName;
    this.localCustId = null;
  }

  WholesaleBillReport(
    SalesBillDetails: SalesBillDetails,
    shippingDtl: ShippingAddress
  ) {
    this.wholeSaleBillReport = SalesBillDetails;

    this.shippingDtl = shippingDtl;
  }

  getWholeSaleReport() {
    return this.wholeSaleBillReport;
  }
  getShippingDtls() {
    return this.shippingDtl;
  }


}
