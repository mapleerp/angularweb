import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { StockRequestHdr } from '../modelclass/stock-request-hdr';
import { StockRequestReport } from '../modelclass/stock-request-report';
import { SharedserviceService } from './sharedservice.service';

@Injectable({
  providedIn: 'root'
})
export class StockRequestInvoiceService {
  voucherDate: any;
  voucherNumber: any;
  toBranch: any;

  unit:any;
  qty:any;
  hsnCode:any;
  rate:any;
  amount:any;


  showAllStockRequestDtl: StockRequestReport [] = [];
  dateConv: any;
  stockRequestDtls: any;
  itemId: any;
  totalAmount=0;
  companyName: any;
  constructor( private sharedserviceService: SharedserviceService,public datepipe: DatePipe) { }

  setStockRequestBill(stockRequestHdr: StockRequestHdr) {
    
    this.voucherNumber=stockRequestHdr.voucherNumber;
    this.dateConv=this.datepipe.transform(stockRequestHdr.voucherDate , 'yyyy-MM-dd');
    this.voucherDate=stockRequestHdr.voucherDate;
    this.toBranch=stockRequestHdr.toBranch;
    this.companyName=stockRequestHdr.companyMst.companyName;
    // this.sharedserviceService.getStockRequestReport(this.voucherNumber, this.dateConv).subscribe(
    //   data=>{
    //     this.showAllStockRequestDtl = data;
    //     console.log(data)
    //     console.log("report data"+this.showAllStockRequestDtl)
    //     for(let i=0;i<this.showAllStockRequestDtl.length;i++){
          // this.unit=this.showAllStockRequestDtl[i].unitId;
          // this.qty=this.showAllStockRequestDtl[i].qty;
          // this.hsnCode=this.showAllStockRequestDtl[i].hsnCode;
          // this.rate=this.showAllStockRequestDtl[i].rate;
          // this.amount=this.showAllStockRequestDtl[i].amount;
          // this.itemId=this.showAllStockRequestDtl[i].itemId;
          // this.totalAmount=this.totalAmount+this.showAllStockRequestDtl[i].amount;
        // }
        this.getStockCompanyMSt( this.companyName);
        this.getStockVoucherNumber(this.voucherNumber);
        this.getVoucherDate(this.voucherDate);
        this.getTobranch(this.toBranch);
        this.gettotalAmount(this.totalAmount);
      
      // });
    
  }
  gettotalAmount(totalAmount: number) {
    return this.totalAmount;
  }
  getTobranch(toBranch: any) {
    return this.toBranch;
  }
  getVoucherDate(voucherDate: any) {
    return this.voucherDate;
  }
  getStockVoucherNumber(voucherNumber: any) {
    return this.voucherNumber;
  }
  getStockCompanyMSt(companyName: any) {
    return this.companyName;
  }
  getStockRequestArray()
  {
   
    return this.showAllStockRequestDtl;
  }
  
}
