import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { AccountHeads } from '../modelclass/account-heads';
import { CustomerMst } from '../modelclass/customer-mst';
import { ItemMst } from '../modelclass/item-mst';
import { PurchaseHdr } from '../modelclass/purchase-hdr';
import { ShippingAddress } from '../modelclass/shipping-address';
import { Supplier } from '../modelclass/supplier';
import { itemDetails } from '../Pop-Up-Windows/item-list-pop-up/item-list-pop-up.component';
import { datas } from '../Pop-Up-Windows/search-pop-up/search-pop-up.component';

@Injectable({
  providedIn: 'root',
})
export class SaleOrderPopupService {
  purchasHdr: PurchaseHdr = new PurchaseHdr();

  productList: datas[] = [];

  invoiceBillClear: any;

  //customerDtl: CustomerMst = new CustomerMst;
  private customerDtl = new BehaviorSubject<any>({});
  customerDtls: AccountHeads[] = [];

  //supplier popup-purchase
  private supplierDtl = new BehaviorSubject<any>({});
  supplierDtls: AccountHeads[] = [];
  //item popup-purchase
  private itemDtl = new BehaviorSubject<any>({});
  itemDtls: datas[] = [];

  selectedShippingDtl = new BehaviorSubject<any>({});
  isPrinting = false;
  itemName = new BehaviorSubject<any>({});

  constructor(private router: Router) {}

  addItemName(dts: ItemMst) {
    console.log('addCustomerDtl');
    this.itemName.next(dts.itemName);
  }

  getItemName() {
    console.log('abcdddddddddddddddddddd' + this.itemName);
    return this.itemName.asObservable();
  }

  addCustomerDtl(customerDtls: AccountHeads) {
    console.log('addCustomerDtl');
    this.customerDtl.next(customerDtls);
  }

  getCustomerDtls() {
    return this.customerDtl.asObservable();
  }

  custDtlsentToSalesPopupService(customerDtls: AccountHeads[]) {
    console.log('custDtlsentToSalesPopupService called');
    this.customerDtls = customerDtls;
  }

  getCustmerDtls(): AccountHeads[] {
    console.log('getCustmerDtls serviecs called');

    return this.customerDtls;
  }
  resetInvoiceBillWindow(invoiceBillreset: any) {
    console.log(invoiceBillreset + 'resetInvoiceBillWindow');
    this.invoiceBillClear = invoiceBillreset;
  }

  clearInvoiceBillWindow() {
    if (typeof this.invoiceBillClear == 'undefined') {
      this.invoiceBillClear = 'NotReload';
    }
    return this.invoiceBillClear;
  }

  addShippingDtls(selectedShippingDtl: ShippingAddress) {
    console.log('addShippingDtls');

    this.selectedShippingDtl.next(selectedShippingDtl);
  }

  getShippingDtls() {
    console.log('getShippingDtls');

    return this.selectedShippingDtl.asObservable();
  }

  //...//

  printDocument(documentName: string, documentData: string[]) {
    this.isPrinting = true;
    this.router.navigate([
      '/',
      {
        outlets: {
          print: ['print', documentName, documentData.join()],
        },
      },
    ]);
  }

  onDataReady() {
    setTimeout(() => {
      window.print();
      this.isPrinting = false;
      this.router.navigate([{ outlets: { print: null } }]);
    });
  }
  //...///
  //----------Popups in Purchase
  //Supplier-purchase

  addSupplierDtl(supplierDtls: AccountHeads) {
    console.log('addSupplierDtl' + supplierDtls.accountName);
    this.supplierDtl.next(supplierDtls);
  }
  getSupplierDtls() {
    console.log(this.supplierDtl.asObservable());
    return this.supplierDtl.asObservable();
  }
  supDtlSentToPopupService(supplierDtls: AccountHeads[]) {
    console.log('supDtlSentToPopupService called');
    this.supplierDtls = supplierDtls;
  }
  getSupplirDtls(): AccountHeads[] {
    console.log('getSupplirDtls services called');

    return this.supplierDtls;
  }
  //item popup- purchase
  addItemDtl(itemDtls: datas) {
    console.log('addItemDtl');
    this.itemDtl.next(itemDtls);
  }
  getItemDtls() {
    return this.itemDtl.asObservable();
  }
  itemDtlSentToPopupService(itemDtls: datas[]) {
    console.log('itemDtlSentToPopupService called');
    this.itemDtls = itemDtls;
  }
  getItmDtls(): datas[] {
    console.log('getItmDtls services called');
    return this.itemDtls;
  }

  savePurchaseHdr(purchaseHdr: PurchaseHdr) {
    this.purchasHdr = purchaseHdr;
  }

  getPurchaseHdrHistory() {
    return this.purchasHdr;
  }
}
