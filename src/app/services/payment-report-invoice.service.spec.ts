import { TestBed } from '@angular/core/testing';

import { PaymentReportInvoiceService } from './payment-report-invoice.service';

describe('PaymentReportInvoiceService', () => {
  let service: PaymentReportInvoiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaymentReportInvoiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
