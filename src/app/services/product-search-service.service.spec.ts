import { TestBed } from '@angular/core/testing';

import { ProductSearchServiceService } from './product-search-service.service';

describe('ProductSearchServiceService', () => {
  let service: ProductSearchServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductSearchServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
