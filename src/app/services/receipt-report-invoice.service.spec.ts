import { TestBed } from '@angular/core/testing';

import { ReceiptReportInvoiceService } from './receipt-report-invoice.service';

describe('ReceiptReportInvoiceService', () => {
  let service: ReceiptReportInvoiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReceiptReportInvoiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
