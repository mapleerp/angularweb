import { TestBed } from '@angular/core/testing';

import { PosFinalBillService } from './pos-final-bill.service';

describe('PosFinalBillService', () => {
  let service: PosFinalBillService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PosFinalBillService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
