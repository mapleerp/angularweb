import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { StockTransferOutHdr } from '../modelclass/stock-transfer-out-hdr';
import { StockTransferOutReport } from '../modelclass/stock-transfer-out-report';
import { SharedserviceService } from './sharedservice.service';

@Injectable({
  providedIn: 'root'
})
export class StockTransferInvoiceService {
  voucherDate: any;
  voucherNumber: any;
  toBranch: any;

  unit:any;
  qty:any;
  hsnCode:any;
  rate:any;
  amount:any;


  showAllStockTransferDtl: StockTransferOutReport [] = [];
  dateConv: any;
  stockRequestDtls: any;
  itemId: any;
  totalAmount=0;
  companyName: any;
  constructor( private sharedserviceService: SharedserviceService,public datepipe: DatePipe) { }

  setStockRequestBill(stockTransferOutHdr: StockTransferOutHdr) {
    
    this.voucherNumber=stockTransferOutHdr.voucherNumber;
    this.dateConv=this.datepipe.transform(stockTransferOutHdr.voucherDate , 'yyyy-MM-dd');
    this.voucherDate=stockTransferOutHdr.voucherDate;
    this.toBranch=stockTransferOutHdr.toBranch;
    this.companyName=stockTransferOutHdr.companyMst.companyName;
    
        this.getStockCompanyMSt( this.companyName);
        this.getStockVoucherNumber(this.voucherNumber);
        this.getVoucherDate(this.voucherDate);
        this.getTobranch(this.toBranch);
        this.gettotalAmount(this.totalAmount);
      
     
    
  }
  gettotalAmount(totalAmount: number) {
    return this.totalAmount;
  }
  getTobranch(toBranch: any) {
    return this.toBranch;
  }
  getVoucherDate(voucherDate: any) {
    return this.voucherDate;
  }
  getStockVoucherNumber(voucherNumber: any) {
    return this.voucherNumber;
  }
  getStockCompanyMSt(companyName: any) {
    return this.companyName;
  }
  getStockRequestArray()
  {
   
    return this.showAllStockTransferDtl;
  }

}