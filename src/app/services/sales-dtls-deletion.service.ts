import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { QtyEditWindowComponent } from '../dialogWindows/qty-edit-window/qty-edit-window.component';
import { SalesDtl } from '../modelclass/sales-dtl';
import { SharedserviceService } from './sharedservice.service';

@Injectable({
  providedIn: 'root'
})
export class SalesDtlsDeletionService {
 

  getSalesBillDtl:SalesDtl[]=[];

  constructor( private sharedserviceService: SharedserviceService, public editQtyAdd: MatDialog,) { }

  salesDetailsDeletion(salesdtl:SalesDtl)
  {

    if(salesdtl.id==null)
    {
      return
    }
  
    this.sharedserviceService.deleteSalesDtl(salesdtl.id).subscribe(
      data=>{
        this.sharedserviceService.getSalesDtl(salesdtl.salesTransHdr).subscribe(
          data=>{this.getSalesBillDtl=data,
            this.sharedserviceService.PostSalesDtlToPosWindow(
              this.getSalesBillDtl
            );
         }
        )
      }
    );
  
  
  }

  ItemQtyEdit(salesdtl: SalesDtl) {

 
    const dialogConfigAdd = new MatDialogConfig();
    dialogConfigAdd.disableClose = false;
    dialogConfigAdd.autoFocus = true;
    dialogConfigAdd.width = "30%";
    dialogConfigAdd.height = "30%";
    dialogConfigAdd.data = {salesdtl }
    this.editQtyAdd.open(QtyEditWindowComponent, dialogConfigAdd);
  
  }
}
