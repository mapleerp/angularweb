import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { CustomerMst } from '../modelclass/customer-mst';
import { CustomerDetailsPopUpComponent } from '../Pop-Up-Windows/customer-details-pop-up/customer-details-pop-up.component';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  
  constructor( public addDialog1: MatDialog,) { }

  cust: CustomerMst = new CustomerMst;

  
  selectCustomer() {
    const dialogConfigAdd1 = new MatDialogConfig();
    dialogConfigAdd1.disableClose = false;
    dialogConfigAdd1.autoFocus = true;
    dialogConfigAdd1.position = { right: `10px`, top: `20px` }
    dialogConfigAdd1.width = "40%";
    dialogConfigAdd1.height = "100%";
   
    this.addDialog1.open(CustomerDetailsPopUpComponent, dialogConfigAdd1)

    this.addDialog1.afterAllClosed.subscribe((result: any) => {
      this.cust = result;
      console.log("customer popup window" + this.cust)
    });

  }

}
