import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { WebsiteClass } from '../modelclass/website-class';
import { WebsiteFeatureMst } from '../modelclass/websiteModelClass/website-feature-mst';
import { WebsiteMainMst } from '../modelclass/websiteModelClass/website-main-mst';
import { WebsiteServiceMst } from '../modelclass/websiteModelClass/website-service-mst';

import { SharedserviceService } from './sharedservice.service';

@Injectable({
  providedIn: 'root'
})
export class WebsiteServiceService {

  webObject:WebsiteClass=new WebsiteClass;
  
  websiteMainMst:WebsiteMainMst= new WebsiteMainMst;
  
 
  public websiteFeatureMst = new BehaviorSubject<WebsiteFeatureMst[]>([]);
  public websiteServiceMst = new BehaviorSubject<WebsiteServiceMst[]>([]);
  banner: any;
  constructor(private service : SharedserviceService) {

   }
getConstants(){


  // this.service.getWebsiteConstants().subscribe(data=>{this.webObject=data;

  // console.log(this.webObject)


  // this.websiteMainMst=this.webObject.websiteMainMst;
 

  // this.banner=this.websiteMainMst.searchbannertexth1;

  // console.log("home banner in service"+this.banner);
  // this.websiteFeatureMst.next(this.webObject.websiteFeatureMst);
  // this.websiteServiceMst.next(this.webObject.websiteServiceMst);


  //   })

  return this.webObject;
}


getWebsiteHome(){
  console.log("mainffffffff"+this.webObject);
  return this.websiteMainMst;
}
getWebsiteFeature(){
  console.log("featureeeeeeeeeeee in service"+this.webObject);
  return this.websiteFeatureMst.asObservable();
}
getWebsiteService(){
  return this.websiteServiceMst.asObservable();
}









}
