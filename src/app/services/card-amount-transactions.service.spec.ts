import { TestBed } from '@angular/core/testing';

import { CardAmountTransactionsService } from './card-amount-transactions.service';

describe('CardAmountTransactionsService', () => {
  let service: CardAmountTransactionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CardAmountTransactionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
