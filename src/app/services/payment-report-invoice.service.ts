import { Injectable } from '@angular/core';


// Payment Report
import { SharedserviceService } from './sharedservice.service';
import { PaymentHdr } from '../modelclass/payment-hdr';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class PaymentReportInvoiceService {
  
  voucherDate: any;
  voucherNO: any;
  branchcode: any;

  companyName: any;
  totalAmount=0;

  constructor( private sharedserviceService: SharedserviceService,public datepipe: DatePipe) { }

  setPaymentReportRequest(paymentHdr: PaymentHdr) {
    
    this.voucherNO=paymentHdr.voucherNumber;
    this.voucherDate=paymentHdr.voucherDate;
    this.companyName=paymentHdr.companyMst.companyName;
    this.branchcode=paymentHdr.branchCode;
        this.getStockCompanyMSt( this.companyName);
        this.getStockVoucherNumber(this.voucherNO);
        this.getVoucherDate(this.voucherDate); 
        this.getBranchCode(this.branchcode)   
  }
  gettotalAmount(totalAmount: number) {
    return this.totalAmount;
  }
  getVoucherDate(voucherDate: any) {
    return this.voucherDate;
  }
  getStockVoucherNumber(voucherNumber: any) {
    return this.voucherNO;
  }
  getStockCompanyMSt(companyName: any) {
    return this.companyName;
  }
  getBranchCode(branchcode: any) {
    return this.branchcode;
  }

}
