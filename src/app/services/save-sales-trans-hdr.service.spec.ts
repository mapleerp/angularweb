import { TestBed } from '@angular/core/testing';

import { SaveSalesTransHdrService } from './save-sales-trans-hdr.service';

describe('SaveSalesTransHdrService', () => {
  let service: SaveSalesTransHdrService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SaveSalesTransHdrService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
