import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { CustomerMst } from '../modelclass/customer-mst';
import { LocalCustomerMst } from '../modelclass/local-customer-mst';
import { SalesTransHdr } from '../modelclass/sales-trans-hdr';
import { SharedserviceService } from './sharedservice.service';

@Injectable({
  providedIn: 'root'
})
export class SaveSalesTransHdrService {
 
  salesTransHdr:SalesTransHdr=new SalesTransHdr;
  localCustomerMst:LocalCustomerMst=new LocalCustomerMst;
  date= new Date();
  constructor( private sharedserviceService: SharedserviceService) { }

  SaveSalesTransHdr(customerResp: CustomerMst) {

    this.salesTransHdr.customerMst = customerResp;
    this.salesTransHdr.branchCode = environment.myBranch;
    this.salesTransHdr.salesMode = "POS";
    this.salesTransHdr.voucherType = "SALESTYPE";
    this.salesTransHdr.creditOrCash = "CASH";
    this.salesTransHdr.isBranchSales = "N";
    this.salesTransHdr.invoiceAmount = 0.0;
    this.salesTransHdr.userId = environment.username;
    this.salesTransHdr.localCustomerMSt=this.localCustomerMst
    this.salesTransHdr.voucherDate=this.date;

    this.sharedserviceService.saveSalesHdr( this.salesTransHdr).subscribe(
      subscribedata=>{this.salesTransHdr=subscribedata;
       this.sharedserviceService.savesalesTransHdrHistory(this.salesTransHdr);
      });
    
  return this.salesTransHdr;
  }
}

