import { TestBed } from '@angular/core/testing';

import { SalesDtlsService } from './sales-dtls.service';

describe('SalesDtlsService', () => {
  let service: SalesDtlsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalesDtlsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
