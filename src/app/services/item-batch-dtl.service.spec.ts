import { TestBed } from '@angular/core/testing';

import { ItemBatchDtlService } from './item-batch-dtl.service';

describe('ItemBatchDtlService', () => {
  let service: ItemBatchDtlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemBatchDtlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
