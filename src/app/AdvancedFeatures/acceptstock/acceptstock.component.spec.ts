import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptstockComponent } from './acceptstock.component';

describe('AcceptstockComponent', () => {
  let component: AcceptstockComponent;
  let fixture: ComponentFixture<AcceptstockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AcceptstockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptstockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
