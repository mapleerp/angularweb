import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { StockTransferInDtl } from 'src/app/modelclass/stock-transfer-in-dtl';
import { StockTransferInHdr } from 'src/app/modelclass/stock-transfer-in-hdr';
import { Voucherstock } from 'src/app/modelclass/voucherstock';
import { VoucherNoPopupComponent } from 'src/app/Pop-Up-Windows/voucher-no-popup/voucher-no-popup.component';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-acceptstock',
  templateUrl: './acceptstock.component.html',
  styleUrls: ['./acceptstock.component.css']
})
export class AcceptstockComponent implements OnInit {
  voucherIn: any;
  fromBranch: any;
  intentNo: any;
  intentdate: any;

  voucher: Voucherstock = new Voucherstock;

  stockTransferInHdr: StockTransferInHdr = new StockTransferInHdr;
  stockTransferInDtl: StockTransferInDtl = new StockTransferInDtl;

  receiveVoucherNo: Voucherstock = new Voucherstock;
  constructor(private service: SharedserviceService, public addDialog1: MatDialog) { }

  ngOnInit(): void {
    this.receiveVoucherDtls();
  }
  fetchDataStockTransHdr() {
    this.stockTransferInHdr.voucherNumber = this.voucherIn;
    this.stockTransferInHdr.fromBranch = this.fromBranch;
    this.stockTransferInHdr.intentNumber = this.intentNo;
    this.stockTransferInHdr.inVoucherDate = this.intentdate;

 

  }

  refreshDataStockTransHdr() {
    this.stockTransferInHdr.voucherNumber = this.voucherIn;
    this.stockTransferInHdr.fromBranch = this.fromBranch;
    this.stockTransferInHdr.intentNumber = this.intentNo;
    this.stockTransferInHdr.inVoucherDate = this.intentdate;


  }

  acceptStockDataStockTransHdr() {
    this.stockTransferInHdr.voucherNumber = this.voucherIn;
    this.stockTransferInHdr.fromBranch = this.fromBranch;
    this.stockTransferInHdr.intentNumber = this.intentNo;
    this.stockTransferInHdr.inVoucherDate = this.intentdate;

    
  }


  onkeypressEventvoucherNumber() {
    const dialogConfigAdd1 = new MatDialogConfig();
    dialogConfigAdd1.disableClose = false;
    dialogConfigAdd1.autoFocus = true;
    dialogConfigAdd1.position = { right: `10px`, top: `20px` }
    dialogConfigAdd1.width = "40%";
    dialogConfigAdd1.height = "100%";

    this.addDialog1.open(VoucherNoPopupComponent, dialogConfigAdd1)


  }

  receiveVoucherDtls() {
    console.log('receiveVoucherDtls');
    // this.service.getVoucherDtl().subscribe((data: any) => {
    //   this.receiveVoucherNo = data;
    //   console.log(this.receiveVoucherNo)
    // });
  }

}
