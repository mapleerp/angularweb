import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { DayEndClosureHdr } from 'src/app/modelclass/day-end-closure-hdr';
import { ItemStockPopUp } from 'src/app/modelclass/item-stock-pop-up';
import { StockRequestDtl } from 'src/app/modelclass/stock-request-dtl';
import { StockRequestHdr } from 'src/app/modelclass/stock-request-hdr';
import { ItemPopupComponent } from 'src/app/Pop-Up-Windows/item-popup/item-popup.component';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { StockRequestInvoiceService } from 'src/app/services/stock-request-invoice.service';
import { environment } from 'src/environments/environment.prod';
@Component({
  selector: 'app-stock-request',
  templateUrl: './stock-request.component.html',
  styleUrls: ['./stock-request.component.css']
})
export class StockRequestComponent implements OnInit {
  branches: BranchMst[] = [];
  itemName: any;
  receiveItem: ItemStockPopUp = new ItemStockPopUp;
  itemId: any;
  rate: any;
  barcode: any;
  batch: any;
  unit: any;
  qty: any;
  toBranch: any;
  dpVoucherDate: any;
  dpStkTrasferDate: any;
  IntentNo: any;
  branchCode: any = environment.myBranch;
  newAmount: number = 0.0;
  count: number = 0;
  stockRequestDtl: StockRequestDtl = new StockRequestDtl();
  stockRequestHdr: StockRequestHdr = new StockRequestHdr();
  unitId:any;
  stockHdr: StockRequestHdr = new StockRequestHdr();
  stockDtl: StockRequestDtl = new StockRequestDtl();
  dayEndClosureHdr: DayEndClosureHdr = new DayEndClosureHdr();
  dayEndProcessDate: any;
  stockTransDate: any;
  dateComparison: any;
  storeName: any;
  showAllStockRequestDtl: StockRequestDtl[] = [];
  showAllUnholded: StockRequestHdr[] = [];
  sumAmount: any;
  // rowseletion = false;
  stockVerification: String;
  chkQty: number;
  constructor(private router: Router, private sharedserviceService: SharedserviceService,
     public datepipe: DatePipe, public addDialog1: MatDialog,
     public stockRequestInvoiceService:StockRequestInvoiceService) {

  }

  ngOnInit(): void {

    this.sharedserviceService.getBranches().subscribe((data: any) => {
      this.branches = data
      console.log(this.branches);
    });

    this.receiveItems();
  }
  onKeypressEventItem() {
    const dialogConfigAdd1 = new MatDialogConfig();
    dialogConfigAdd1.disableClose = false;
    dialogConfigAdd1.autoFocus = true;
    dialogConfigAdd1.width = "40%";
    dialogConfigAdd1.height = "100%";

    this.addDialog1.open(ItemPopupComponent, dialogConfigAdd1)

    this.addDialog1.afterAllClosed.subscribe((result: any) => {
    });
  }

  receiveItems() {
    console.log('receiveItems');
    this.sharedserviceService.getitemsDtl().subscribe((data: any) => {
      this.receiveItem = data;
      this.itemId = this.receiveItem.itemId;
      this.itemName = this.receiveItem.itemName;
      this.rate = this.receiveItem.mrp;
      this.barcode = this.receiveItem.barcode;
      this.batch = this.receiveItem.batch;
      this.unit = this.receiveItem.unitName;
      this.unitId=this.receiveItem.unitId;
    });
  }
  //function to calculate Amount, Cess and Tax Amounts..................................
  calculateAmount() {
    this.newAmount = this.rate * this.qty;
    console.log(this.newAmount + 'Amount calculated');
  }

  //--------------add -----------------
  addStockRequest() {
    if (this.toBranch == null) {
      alert("Please Select the Branch!!!")
      return;
    } else if (this.itemName == null) {
      alert("Please Select Item Name!!!")
      return;
    } else if (this.qty == null || this.qty == 0) {
      alert("Please Enter Qty!!!")
      return;
    }
    this.sharedserviceService.getMaxDayEndClosure(this.branchCode).subscribe(
      (data) => {
        this.dayEndClosureHdr = data;
        if (!this.dayEndClosureHdr == null) {

          this.dayEndProcessDate = this.dayEndClosureHdr.processDate;
          this.stockTransDate = this.dpVoucherDate;
          this.dateComparison = this.stockTransDate.compareTo(this.dayEndProcessDate);
          if (this.dateComparison <= 0) {
            alert("Stock transfer on previous date is not possible");
            return;
          }
        }
      }
    );

    this.storeName = null;
    if (null != this.storeName) {
      this.storeName = this.stockRequestDtl.storeName;
    } else {
      this.storeName = "MAIN";
    }
    this.sharedserviceService.getQtyFromItemBatchMstByItemIdAndQty(this.itemId,this.batch,this.storeName).subscribe(
      (data) => {
        this.chkQty = data;
        console.log(data);
        console.log(this.chkQty + 'Stock verification-----------------');
        if (this.chkQty < this.qty)
             {
              alert('Not in Stock!!!');

            this.itemName = '';
            this.barcode = '';
            this.unit = '';
            this.qty = '';
            this.batch = '';
            this.rate = '';   
            return;
              }
             else{

              if (this.stockHdr.id == null) {
                console.log('New stockRequest hdr ');
                // this.stockRequestHdr = new StockRequestHdr();
                this.stockRequestHdr.intentNumber = this.IntentNo;
                this.stockRequestHdr.toBranch = this.toBranch;
                this.stockRequestHdr.voucherType = "STOUT";
                this.stockRequestHdr.fromBranch = environment.myBranch;
                let dpVoucherDate = new Date();
                console.log(dpVoucherDate + 'daaaatttteeedaaaatttteee');
                this.stockRequestHdr.voucherDate = this.datepipe.transform(dpVoucherDate, 'yyyy-MM-dd');
                this.chkQty = 0.0;
                this.sharedserviceService.getQtyFromItemBatchMstByItemIdAndQty(this.itemId,this.batch,this.storeName).subscribe(
                  (data) => {
                    this.chkQty = data;
                    console.log(data);
                    console.log(this.chkQty + 'Stock verification-----------------');
                    if (this.chkQty < this.qty)
                         {
                          alert('Not in Stock!!!');
          
                        this.itemName = '';
                        this.barcode = '';
                        this.unit = '';
                        this.qty = '';
                        this.batch = '';
                        this.rate = '';   
                        return;
                          }
                          else{
                            this.sharedserviceService.saveStockRequestHdr(this.stockRequestHdr).subscribe(
                              (data) => {
                                this.stockHdr = data;
                                console.log(data);
                                console.log(data.id + 'header id jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj');
                                this.addStockRequestDtl(this.stockHdr);
                      
                      
                                console.log('Add item--StockRequestHdr data Send');
                              },
                              (error: any) => {
                                console.log('Data not send');
                              }
                            );
                          }
          
          
                        },
                        (error: any) => {
                          console.log('Data stock not send');
                        }
                      );
          
              }
              else {
                this.addStockRequestDtl(this.stockHdr);
              }


             }
                


            },
            (error: any) => {
              console.log('Data stock not send');
            }
          );


  }
  // add item button - Post (save)

  addStockRequestDtl(stockHdr: StockRequestHdr) {
    if (stockHdr != null) {
      this.stockRequestDtl.stockRequestHdr = this.stockHdr;
      console.log(this.stockRequestDtl.stockRequestHdr + 'Header id');
      this.stockRequestDtl.slNo = this.count + 1;
      this.stockRequestDtl.batch = this.batch;
      this.stockRequestDtl.qty = this.qty;
      this.stockRequestDtl.rate = this.rate
      this.stockRequestDtl.amount = this.newAmount;
      this.stockRequestDtl.mrp = this.receiveItem.standardPrice;
      this.stockRequestDtl.itemId.id = this.itemId;
      this.stockRequestDtl.barcode = this.barcode;
      this.stockRequestDtl.unitId=this.unitId;
      console.log(this.unitId+"unit id in add itemmmmmmmm")
      this.stockRequestDtl.taxRate = 0.0;
      this.stockRequestDtl.storeName = "MAIN";
      this.sharedserviceService
        .saveStockRequestDtl(this.stockRequestDtl)
        .subscribe(
          (data) => {
            this.stockDtl = data;
            console.log(data);
            console.log('Add item--stockRequestDtl data Send');
            console.log('show all started ------' + this.stockRequestDtl.stockRequestHdr.id)

            this.ShowStockRequestDetails(stockHdr);
            // Alert message item added in table.................................................
            // if (!this.rowseletion) {
            //   alert('Item Added');
            //   return;
            // }
          },
          (error: any) => {
            console.log('Data not send');
          }
        );
      this.itemName = '';
      this.barcode = '';
      this.unit = '';
      this.qty = '';
      this.batch = '';
      this.rate = '';
      this.newAmount = 0;
    }
  }

  // Show calculation details .............................................
  ShowStockRequestDetails(stockHdr: StockRequestHdr) {
    this.sumAmount = 0;
    this.sharedserviceService.getShowAllStockRequestDtl(stockHdr.id).subscribe((data: any) => {
      console.log(data);
      this.showAllStockRequestDtl = data;
      console.log(this.showAllStockRequestDtl + 'showing all list data');


    });
    this.sharedserviceService.stockRequestsumOfTotalAmount(stockHdr.id).subscribe((data: any) => {
      this.sumAmount = data;

      console.log(data);
    });
  }

  // Delete item (button) in table...........................................................
  deleteItem(stockRequestDtl: StockRequestDtl) {
    if (stockRequestDtl.id == null) {
      return
    }
    this.sharedserviceService
      .deleteStockRequestSelectedItems(stockRequestDtl.id)
      .subscribe((data) => {
        this.ShowStockRequestDetails(this.stockHdr);
        // this.rowseletion = false;
        alert('deleted');
        return;
      });
  }

  clearAll() {
    this.itemName = '';
    this.barcode = '';
    this.unit = '';
    this.qty = '';
    this.batch = '';
    this.rate = '';
    this.IntentNo = '';
    this.dpVoucherDate = '';
    this.toBranch = '';
    this.showAllStockRequestDtl = [];
    this.showAllUnholded = [];
    this.count = 0;
    this.sumAmount = '';
    this.stockRequestDtl = new StockRequestDtl();
    this.stockRequestHdr = new StockRequestHdr();
  }
  HoldStock() {
    console.log("-----hold stock function starting- ")
    this.showAllUnholded = [];
    this.clearAll();
    console.log("-----unhold stock function starting- ")
  }
  UnHoldStock() {
    console.log("-----unhold stock function starting- ")
    this.sharedserviceService.getHoldedStockRequest().subscribe((data: any) => {
      console.log("-----showing return data value- " + data)
      this.showAllUnholded = data;
      console.log("-----return value setting in showallUnholded List- " + this.showAllUnholded)
      console.log("-----unhold stock function ending- ")
    });
  }
  // Final save button........................................................
  finalSave() {
    console.log("-----final save function starting- ")
    console.log("-----setting values in hdr- ")
    this.stockRequestHdr.intentNumber = this.IntentNo;
    this.stockRequestHdr.toBranch = this.toBranch;
    this.stockRequestHdr.voucherType = "STOUT";
    this.stockRequestHdr.fromBranch = environment.myBranch;
    let dpVoucherDate = new Date();
    this.stockRequestHdr.voucherDate = this.datepipe.transform(dpVoucherDate, 'yyyy-MM-dd');

    console.log('---- get current financial year----2022-2023------');
    var fiscalyear = "";
    var today = new Date();
    if ((today.getMonth() + 1) <= 3) {
      fiscalyear = (today.getFullYear() - 1) + "-" + today.getFullYear()
    } else {
      fiscalyear = today.getFullYear() + "-" + (today.getFullYear() + 1)
    }
    console.log(fiscalyear);

    console.log('---------------stockVerification started-----------');
    this.sharedserviceService
      .retrieveAllItemBatchDtlforStockTransfer(this.stockHdr.id)
      .subscribe((data) => {
        this.stockVerification = data;
        console.log('---------------stockVerification null checking-----------');
        if (!this.stockVerification == null) {
          console.log('stockVerification-' + data);
          return;
        }

        console.log('---------------generateVoucherNumber started-----------');
        this.sharedserviceService.generateVoucherNumber(fiscalyear + environment.myBranch).subscribe((data: any) => {
          this.stockRequestHdr.voucherNumber = data;
          console.log(data + "------generateVoucherNumber-----vocher number")
          console.log(this.stockHdr.id)
          console.log(data + "-----setting-generateVoucherNumber-in hdr-")
          this.stockHdr.voucherNumber = data;
          console.log(data + "-----final save url calling hdr-")
          this.sharedserviceService
            .updateStockRequestHdr(this.stockHdr, this.stockHdr.id)
            .subscribe((data) => {
              console.log('final result of stockRequestHdr after updation' + data),
                (this.stockRequestHdr = data);
                console.log("stock request report-----started")
                this.stockRequestInvoiceService.setStockRequestBill(this.stockHdr);
 
                 this.router.navigate(['StockRequestBIll']);
            });
          console.log("-----final save url ending hdr-")
          this.clearAll();
        });
        console.log("-----generateVoucherNumber url ending hdr-")
      });
    console.log("-----stockVerification url ending- ")
    console.log("-----final save function ending- ")
  }

  highlightRowHolded(dts: StockRequestHdr) {
    this.IntentNo = dts.intentNumber;
    this.dpVoucherDate = dts.voucherDate;
    this.toBranch = dts.toBranch;
    this.stockHdr=dts;
    this.ShowStockRequestDetails(dts);
  }
  highlightRowTable(dtls: StockRequestDtl) {
    this.itemName = dtls.itemId.itemName;
    this.barcode = dtls.barcode;
    this.unit = dtls.unitId;
    this.qty = dtls.qty;
    this.batch = dtls.batch;
    this.rate = dtls.rate;

  }

}