import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MapleErpDeploymentRoutingModule } from './maple-erp-deployment-routing.module';
import { MapleErpDeploymentComponent } from './maple-erp-deployment.component';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import {MatToolbarModule} from '@angular/material/toolbar';
import { MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import {MatRadioModule} from '@angular/material/radio';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDialogModule} from '@angular/material/dialog';



@NgModule({
  declarations: [
    MapleErpDeploymentComponent,

  ],
  imports: [
    CommonModule,
    MapleErpDeploymentRoutingModule,
    FormsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatTableModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    MatRadioModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatDialogModule,
    

  ]
})
export class MapleErpDeploymentModule { }
