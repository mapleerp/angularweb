import { DatePipe } from '@angular/common';
import { newArray } from '@angular/compiler/src/util';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Network } from '@ngx-pwa/offline';


import { Subscription } from 'rxjs';
import { AddDialogDeploymentComponent } from '../dialogWindows/add-dialog-deployment/add-dialog-deployment.component';
import { BranchMst } from '../modelclass/branch-mst';
import { JarVersionMst } from '../modelclass/jar-version-mst';
import { SharedserviceService } from '../services/sharedservice.service';
import { environment } from 'src/environments/environment.prod';
@Component({
  selector: 'app-maple-erp-deployment',
  templateUrl: './maple-erp-deployment.component.html',
  styleUrls: ['./maple-erp-deployment.component.css']
})
export class MapleErpDeploymentComponent implements OnInit {

  networkStatus$;

  @Input() data:string | undefined
  
  sub: Subscription = new Subscription;
  sub1: Subscription = new Subscription;
  sub3: Subscription = new Subscription;
  sub4: Subscription = new Subscription;
  sub5: Subscription = new Subscription;
  sub6: Subscription = new Subscription;
  jarVersionMst:JarVersionMst[]=[];
  selectedUpdates:JarVersionMst[]=[];
  branchMst:BranchMst[]=[];
  selectedobjList:JarVersionMst[]=[];
  branchname='';
  allupgradeStatus:String=''
  selectId:any;
  selectbranch:any[]=[];
  mymodel: any;
  checkboxStatusdlt=false;
  checkboxStatusus=false;
  companyname=environment.company;
 
 
  constructor(private sharedserviceService:SharedserviceService, protected network: Network,
    public datepipe: DatePipe, public addDialog: MatDialog) {{ this.networkStatus$ = this.network.onlineChanges; } }

  ngOnInit(): void {
   

   
    this.selectbranch= new Array<any>();
  

    this.sub1=this.sharedserviceService.getBranchNames().subscribe(data => {this.branchMst=data})
   
  this.sub= this.sharedserviceService.getAllBranchcodes(this.companyname).subscribe(data =>{this.jarVersionMst=data})
  }

  UpgradeStatus()
  {

    console.log("UpgradeStatus Changed.........!!"+this.allupgradeStatus)
   
    let j=0;
    if(this.allupgradeStatus=='YES')

    
             {
  for( j=0;j<this.jarVersionMst.length;j++){
 
    this.jarVersionMst[j].upgradeStatus='YES'
    console.log(this.jarVersionMst[j].upgradeStatus)
              }
             }
  if(this.allupgradeStatus=='NO')
              {
                console.log("UpgradeStatus Changed.........!!111111"+this.allupgradeStatus)
for( j=0;j<this.jarVersionMst.length;j++){

  this.jarVersionMst[j].upgradeStatus='NO'
 
                            }
                          }
                        }

                        getSelection(name: any)

                        {
                          console.log(this.getSelection);
                          return this.getSelection;

                         
                        }
                        onSubmit()
                        {

                        }   
                        
                        onKeypressEvent(x: any){
  

                          this.mymodel = x.target.value;
                          console.log("sadfghjk"+this.mymodel)
                          if(this.mymodel=='')
                          {
                            this.sub= this.sharedserviceService.getAllBranchcodes(this.companyname).subscribe(data =>{this.jarVersionMst=data})
                          }
                         
                          this.sharedserviceService.getSearch(this.mymodel).subscribe(data =>
                            {
                              this.jarVersionMst=data;
                             
                        
                            })
                         
                        
                        }

             update()
               {
                this.sub5=this.sharedserviceService.saveupdates(this.jarVersionMst).subscribe(data =>
                  {
                    this.jarVersionMst=data;
                   
              
                  })
                  this.checkboxStatusdlt=false;
                  this.checkboxStatusus=false;
                this. refreshTable();
                }


              // getSelectedBranch(e:any,id:any)
               getSelectedBranch(e:any,selectedobj:JarVersionMst)
                {

                  
                  if(e.checked)
                  {
                    console.log(selectedobj+'checked');
                    this.selectbranch.push(selectedobj.id);
                  
                    this.selectedobjList.push(selectedobj);
                    console.log(this.selectedobjList+'List');
                    this.checkboxStatusdlt=true;
                    this.checkboxStatusus=true;
                  }

                  else{
                      this.selectbranch=this.selectbranch.filter((m: any)=>m!=selectedobj.id);
                    console.log(selectedobj.id+'unchecked');
                     
                    for(let i=0;i<=this.selectedobjList.length;i++)
                        {
                    if(JSON.stringify(this.selectedobjList[i]) === JSON.stringify(selectedobj))
                       {
                         delete this.selectedobjList[i];
                       
                         console.log(this.selectedobjList+'Listttt');

                       }
                      }
                     
                      }
                      console.log(this.selectedobjList.length+'length');
                     
                
                  if(this.selectbranch.length==0)
                  {
                    this.checkboxStatusdlt=false;
                    this.checkboxStatusus=false;
                  }
                   
                  

                  console.log(this.selectbranch)
                }   
                
                addData()
                {
                  const dialogConfigAdd = new MatDialogConfig();
                  dialogConfigAdd.disableClose=false;
                  dialogConfigAdd.autoFocus=true;
                  dialogConfigAdd.width="60%";
                  dialogConfigAdd.height="40%";
                  this.addDialog.open(AddDialogDeploymentComponent,dialogConfigAdd);


                }
                deleteData()
                {
                  this.sharedserviceService.deleteDetails(this.selectbranch).
                  subscribe( data =>
                   {
                     console.log('uytfdfghjk'+data);
                     console.log('Data Send')
                   },
                  error =>console.log(error)
                  );
                
                  this.selectbranch=[];
                  this.selectedobjList=[];
                  this.refreshTable();
                 
                  
                }
                updateSelected()
                {
                this.sub6= this.sharedserviceService.upDateSeletedcolum(this.selectedobjList).
                 subscribe( data =>
                  {
                    console.log('uytfdfghjk'+data);
                    console.log('Data Send')
                  },
                 error =>console.log(error)
                 );
                 this.selectedobjList=[];
                 this.checkboxStatusus=false;
                 this.checkboxStatusdlt=false;
                }
                refreshTable()
                {
                  this.sharedserviceService.getAllBranchcodes(this.companyname).subscribe(data =>{this.jarVersionMst=data})
                  this.checkboxStatusdlt=false;
                  this.checkboxStatusus=false;
                  this.selectedobjList=[];
                  this.jarVersionMst=[];
                }
                    
                ngOnDestroy(): void {
                  this.sub.unsubscribe();
                  this.sub1.unsubscribe();
                  this.sub6.unsubscribe();
                  this.sub3.unsubscribe();
                  this.sub4.unsubscribe();
                  this.sub5.unsubscribe();
                  console.log('service unsubscribed..!!')
                  }
              

                    


  }
  



  
    
