import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MapleErpDeploymentComponent } from './maple-erp-deployment.component';

const routes: Routes = [{ path: '', component: MapleErpDeploymentComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MapleErpDeploymentRoutingModule { }
