import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapleErpDeploymentComponent } from './maple-erp-deployment.component';

describe('MapleErpDeploymentComponent', () => {
  let component: MapleErpDeploymentComponent;
  let fixture: ComponentFixture<MapleErpDeploymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapleErpDeploymentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapleErpDeploymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
