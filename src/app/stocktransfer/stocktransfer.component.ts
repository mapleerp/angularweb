import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { EMPTY } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { BranchMst } from '../modelclass/branch-mst';
import { ItemMst } from '../modelclass/item-mst';
import { ItemStockPopUp } from '../modelclass/item-stock-pop-up';
import { StockTransferOutDtl } from '../modelclass/stock-transfer-out-dtl';
import { StockTransferOutHdr } from '../modelclass/stock-transfer-out-hdr';
import { ItemPopupComponent } from '../Pop-Up-Windows/item-popup/item-popup.component';
// import { ItemStockPopUpComponent } from '../Pop-Up-Windows/item-stock-pop-up/item-stock-pop-up.component';
import { datas } from '../Pop-Up-Windows/search-pop-up/search-pop-up.component';
import { SharedserviceService } from '../services/sharedservice.service';
import { StockTransferInvoiceService } from '../services/stock-transfer-invoice.service';

@Component({
  selector: 'app-stocktransfer',
  templateUrl: './stocktransfer.component.html',
  styleUrls: ['./stocktransfer.component.css']
})
export class StocktransferComponent implements OnInit {
  intentNumber: any;
  date: any;
  toBranch: any;
  itemName: any;
  barcode: any;
  unit: any;
  qty: any;
  batch: any;
  rate: any;
  totalAmount: any;
  unitId: any;
  itemCode:any;
  itemId: any;
  // itemMst: any;
  // stockTransferDate:any;
  // store:any;
  // expiryDate:any;
  mrp: any;
  expDate:any;

  stockTransferOutHdr: StockTransferOutHdr = new StockTransferOutHdr();
  stockTransferOutDtl: StockTransferOutDtl = new StockTransferOutDtl();

  receiveItem: ItemStockPopUp = new ItemStockPopUp;

  branches: BranchMst[] = [];
  stockHdr: StockTransferOutHdr = new StockTransferOutHdr();
  stockDtl: StockTransferOutDtl = new StockTransferOutDtl();
  stocktransdetail: StockTransferOutDtl[] = [];
  sumAmount: any;
  finalSaveStockTrans: any;
  unDeletedRow: any;
  showAllUnholded: any[];
  allItemBatchDtl: any;
  
  // router: any;
  
  // itemMst: any []= [];
  // branches:any[]=[];
  // itemMst: ItemMst=new ItemMst;
  // itemMst: ItemMst []= [];

  constructor(private service: SharedserviceService,public addDialog1: MatDialog,public stockTransferInvoiceService:StockTransferInvoiceService,private router: Router) { }

  ngOnInit(): void {
    this.receiveItemDtls();
    // this.branches=this.service.getBranches();
    this.service.getBranches().subscribe((data: any) => {
    this.branches = data
    console.log(this.branches);
    });
    this.unholdStockTransfer();
    sessionStorage.removeItem("hdr");
  }

  finalSaveStockTransfer() {
    //final save

    this.stockHdr.intentNumber= this.intentNumber;
    this.stockHdr.voucherDate = this.date;
    this.stockHdr.toBranch = this.toBranch;
    

  
     this.finalSaveUrl(this.stockHdr);

      this.clear();
    }

   
  clear(){
    this.stockTransferOutHdr=new StockTransferOutHdr;
    this.stockTransferOutDtl=new StockTransferOutDtl;
    this.stockHdr=new StockTransferOutHdr;
    this.stockDtl=new StockTransferOutDtl;
    this.stocktransdetail=[];
   
    this.intentNumber="";
    this.date="";
    this.itemId="";
    this.itemName="";
    this.barcode="";
    this.unit="";
    this.qty=0;
    this.toBranch="";
    this.batch="";
    this.rate="";
    this.sumAmount="";
    this.mrp="";
    sessionStorage.removeItem("hdr");
    this.showAllUnholded = [];
   }

  saveStocktransferHdr() {
    //function to save stock transfer header
  
    this.stockHdr = new StockTransferOutHdr();
    // sessionStorage.removeItem("hdr");
  
    this.stockHdr.intentNumber= this.intentNumber;
    this.stockHdr.voucherDate = this.date;
    this.stockHdr.toBranch = this.toBranch;
    this.stockHdr.fromBranch = environment.myBranch;
    this.stockHdr.voucherType = "STOUT";
    
    this.service.saveStockTransferHdrInService(this.stockHdr).subscribe((data: any) => {
    console.log("inside stock transfer out hdr-save")
    console.log(data)
    this.stockHdr = data;  
    console.log(this.stockHdr)
    
    sessionStorage.setItem("hdr", this.stockHdr.id) 
    console.log("hdr id session inside subscribe" + sessionStorage.getItem("hdr")) 

    this.saveDtl();
    });  
    
    console.log("header id session after subscribe"+ sessionStorage.getItem("hdr"))   
  }
  
  addStockTransfer() {
    //add item
    // sessionStorage.removeItem("hdr");
  
    if (this.toBranch == null) {
      alert("Please Select the Branch!!!")
      return;
    } else if (this.itemName == null) {
      alert("Please Select Item Name!!!")
      return;
    } else if (this.qty == null || this.qty == 0) {
      alert("Please Type Qty!!!")
      return;
    }
    console.log(sessionStorage.getItem("hdr")+" header id session before add item");
    console.log(this.stockHdr);

    if (sessionStorage.getItem("hdr") == undefined||sessionStorage.getItem("hdr")==null) {
    this.saveStocktransferHdr();     
     
    }else{

    console.log("In dtl function "  );  

    this.saveDtl();

    }
    
}

saveDtl(){
  console.log("Testing item name " + this.itemName);  

     //stock transfer dtl save................
  
    console.log("From HDR  from this." + this.stockHdr);
    console.log(this.stockHdr)
         
    this.stockTransferOutDtl.stockTransferOutHdr= this.stockHdr;
      
    console.log("Stocktransfer header id = " + this.stockTransferOutDtl.stockTransferOutHdr.id);
    this.stockHdr.id = sessionStorage.getItem("hdr");

    // this.stockTransferOutDtl.itemName=this.itemName;
    this.stockTransferOutDtl.batch = this.batch;
    this.stockTransferOutDtl.qty = this.qty;
    this.stockTransferOutDtl.rate = this.rate
    this.stockTransferOutDtl.amount = this.qty * this.rate;
    // this.stockTransferOutDtl.mrp = this.receiveItem.standardPrice;
    this.stockTransferOutDtl.itemId = this.itemId;
    this.stockTransferOutDtl.barcode = this.barcode;
    this.stockTransferOutDtl.taxRate = 0.0;
    this.stockTransferOutDtl.storeName = "MAIN";
    // this.stockTransferOutDtl.itemId.itemName=this.itemName;
    
    this.stockTransferOutDtl.unitId = this.unitId;
    console.log('unit id '+this.unitId);

    this.stockTransferOutDtl.mrp=this.mrp;
    console.log('Mrp'+this.mrp);
    this.stockTransferOutDtl.itemCode=this.itemCode;
    this.stockTransferOutDtl.expiryDate=this.expDate;

    
     
    this.service.saveStockTransferDtl(this.stockTransferOutDtl).subscribe((data: any) => {
    this.stockDtl = data;
    console.log(data)
    console.log("dtlllllllllllllllllllllllll" + this.stockDtl);
        
    this.allStockDetails(this.stockHdr.id);
               
    });
    this.clearInAddItem();
   }

  holdStockTransfer() {
        
    console.log("-----hold stock  function starting- ")
    this.showAllUnholded = [];
    this.stocktransdetail=[];
  this.clear();

    if (this.unDeletedRow == null) {
    this.sumAmount="";
  }
    
    // this.unholdStockTransfer();
    
    // this.UnHoldStock();
    // console.log("-----unhold stock  function starting- ")

    // alert("Details Saved Successfully!!!")
    // sessionStorage.removeItem("hdr");
    
  }

  unholdStockTransfer() {
    console.log("-----unhold stock  function starting- ")
    // alert("Please Select Stock Transfer Details!!!")

    this.service.getHoldedStockTransfer().subscribe((data: any) => {
    console.log("-----showing return data value- "+data)
    this.showAllUnholded = data;
     
    console.log("-----return value setting in showallUnholded List- "+ this.showAllUnholded) 
    console.log("-----unhold stock  function ending- ")
    
    });
 
  }



  onKeypressEventItem() {
    const dialogConfigAdd1 = new MatDialogConfig();
    dialogConfigAdd1.disableClose = false;
    dialogConfigAdd1.autoFocus = true;
    // dialogConfigAdd1.position = { right: `240px`, top: `20px` }
    dialogConfigAdd1.width = "40%";
    dialogConfigAdd1.height = "100%";

    this.addDialog1.open(ItemPopupComponent, dialogConfigAdd1)

    this.addDialog1.afterAllClosed.subscribe((result: any) => {
      // this.cust = result;
      // console.log("customer popup window" + this.cust)
    });
  }
  receiveItemDtls() {
    console.log('receiveItemDtls');
    this.service.getitemsDtl().subscribe((data: any) => {
      this.receiveItem = data;
      this.itemId = this.receiveItem.itemId;
      this.itemName = this.receiveItem.itemName;
      // this.rate = this.receiveItem.standardPrice;
      this.rate=this.receiveItem.mrp;
      this.barcode = this.receiveItem.barcode;
      this.batch = this.receiveItem.batch;
      this.unit = this.receiveItem.unitName;
      this.unitId=this.receiveItem.unitId;
      this.mrp= this.receiveItem.mrp;
      this.itemCode=this.receiveItem.itemcode;
      this.expDate=this.receiveItem.expDate;
    });
  }

  clearInAddItem(){
    this.itemId="";
    this.itemName="";
    this.barcode="";
    this.unit="";
    this.qty=0;
    // this.toBranch="";
    this.batch="";
    this.rate="";
    this.totalAmount="";
    this.mrp="";
  }

  deleteStockTranferDtlRow(stkDtlid:any){
    
    this.service.deleteStockTranferDtl(stkDtlid).subscribe((data:any)=>{
      this.unDeletedRow = data;
      console.log(data);

      this.allStockDetails(this.stockHdr.id);

              });
        if (this.unDeletedRow == null) {
          this.sumAmount="";
                   
         }
         alert("Deleted Successfully!!!");
   }

  getunholdedDtl(hdr:any){
    this.stockHdr=hdr;
    sessionStorage.setItem("hdr", this.stockHdr.id) 
    this.intentNumber=this.stockHdr.intentNumber;
    this.toBranch=this.stockHdr.toBranch;

    this.allStockDetails(hdr.id);
      
  }
  amountCalculation(header:any){
    this.service.sumOfTotalAmount(header).subscribe((data: any) => {
      this.sumAmount = data;
      console.log(data);
      });  
  }

  allStockDetails(header:any){
  this.service.getAllStockDetails(header).subscribe((data: any) => {
    this.stocktransdetail = data;         
    console.log(data)
    if(this.stocktransdetail.length>0)
      {

       this.amountCalculation(header);

    }
    
    });  
  }

  finalSaveUrl(stockHdr:StockTransferOutHdr){
    console.log("IN final Save Header");
    console.log(this.stockHdr.id);
      this.service.finalSaveStockTranfer(this.stockHdr.id,this.stockHdr).subscribe((data:any)=>{
      this.stockHdr = data;
      console.log('In final save - subscribe() ')
      console.log(this.stockHdr);
      
      this.stockTransferInvoiceService.setStockRequestBill(this.stockHdr);
 
      this.router.navigate(['StockTransferBill']);
    });
  }
}

