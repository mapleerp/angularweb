import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemListPopUpComponent } from './item-list-pop-up.component';

describe('ItemListPopUpComponent', () => {
  let component: ItemListPopUpComponent;
  let fixture: ComponentFixture<ItemListPopUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemListPopUpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemListPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
