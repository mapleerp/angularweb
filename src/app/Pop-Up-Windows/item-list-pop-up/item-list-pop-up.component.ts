import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ItemMst } from 'src/app/modelclass/item-mst';
import { SaleOrderPopupService } from 'src/app/services/sale-order-popup.service';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-item-list-pop-up',
  templateUrl: './item-list-pop-up.component.html',
  styleUrls: ['./item-list-pop-up.component.css'],
})
export class ItemListPopUpComponent implements OnInit {
  constructor(
    private saleOrderPopupService: SaleOrderPopupService,
    public dialogRef: MatDialogRef<ItemListPopUpComponent>,
    private sharedserviceService: SharedserviceService // public searchDialog: MatDialogRef<SearchPopUpComponent>
  ) {}
  searchfiled: any;
  selectedName: any;
  i = 0;
  tdatas: itemDetails[] = [];
  items: ItemMst[] = [];
  mymodel: any;

  ngOnInit(): void {
    this.sharedserviceService.getAllItems().subscribe((info) => {
      this.items = info;

      console.log(info);
    });
  }
  highlightRow(dts: ItemMst) {
    this.selectedName = dts.itemName;

    console.log('Mouse clicked itm' + this.selectedName);
    this.searchfiled = dts.itemName;

    this.saleOrderPopupService.addItemName(dts);
  }

  closePopUp(){
    this.dialogRef.close();
    }

  // onKeypressEvent(searchfiled: any) {
  //   this.sharedserviceService
  //     .getItemMstBySearchableString(this.searchfiled)
  //     .subscribe((info) => {
  //       this.tdatas = info;
  //       for (let i = 0; this.tdatas.length - 1; i++) {
  //         this.tdatas[i].itemName = info[i][0];
  //         this.tdatas[i].barCode = info[i][1];
  //         this.tdatas[i].standardPrice = info[i][3];
  //         this.tdatas[i].cess = info[i][4];
  //         this.tdatas[i].taxRate = info[i][5];
  //         this.tdatas[i].unitId = info[i][7];
  //       }
  //     });
  // }
}
export class itemDetails {
  name: any;
  unitName: any;
  unitPrice: any;
  unitsInStock: any;
  qty: any;
  id: any;
  barCode: any;
  barCodeLine1: any;
  barCodeLine2: any;
  barcodeFormat: any;
  bestBefore: any;
  binNo: any;
  branchCode: any;
  brandId: any;
  categoryId: any;
  cess: any;
  cgst: any;
  hsnCode: any;
  isDeleted: any;
  isKit: any;
  itemCode: any;
  itemDescription: any;
  itemDiscount: any;
  itemDtl: any;
  itemGenericName: any;
  itemGroupId: any;
  itemName: any;
  taxRate: any;
  standardPrice: any;
  Amount: any;
  productQty: number | undefined;
  reorderWaitingPeriod: any;
  unitId: any;
  serviceOrGoods: any;
}
