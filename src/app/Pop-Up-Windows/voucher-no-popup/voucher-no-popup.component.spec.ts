import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VoucherNoPopupComponent } from './voucher-no-popup.component';

describe('VoucherNoPopupComponent', () => {
  let component: VoucherNoPopupComponent;
  let fixture: ComponentFixture<VoucherNoPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VoucherNoPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VoucherNoPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
