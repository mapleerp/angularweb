import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Voucherstock } from 'src/app/modelclass/voucherstock';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-voucher-no-popup',
  templateUrl: './voucher-no-popup.component.html',
  styleUrls: ['./voucher-no-popup.component.css']
})
export class VoucherNoPopupComponent implements OnInit {

  constructor(private sharedserviceService: SharedserviceService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public VoucherNoPopup: MatDialogRef<VoucherNoPopupComponent>) { }

    mymodel: any;
    voucher: Voucherstock[] = []; 
    searchfiled = '';

  ngOnInit(): void {
 this.sharedserviceService.getVoucherValue(this.searchfiled).subscribe((data: any) =>
      this.voucher = data);
    console.log(this.voucher);
  }
  onKeypressEvent(x: any) {
    this.mymodel = x.target.value;
    console.log('voucher no popup' + this.mymodel);
  }

  highlightRow(dts: Voucherstock) {
    console.log(dts.voucherNumber)
    this.sharedserviceService.addVoucherDetail(dts);
    this.VoucherNoPopup.close(dts);
  }
}
