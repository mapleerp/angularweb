import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AccountHeads } from 'src/app/modelclass/account-heads';
import { CustomerMst } from 'src/app/modelclass/customer-mst';
import { SaleOrderPopupService } from 'src/app/services/sale-order-popup.service';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-customer-details-pop-up',
  templateUrl: './customer-details-pop-up.component.html',
  styleUrls: ['./customer-details-pop-up.component.css'],
})
export class CustomerDetailsPopUpComponent implements OnInit {
  constructor(
    private sharedserviceService: SharedserviceService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private saleOrderPopupService: SaleOrderPopupService,
    public customerDtlPopup: MatDialogRef<CustomerDetailsPopUpComponent>
  ) {}

  customerDtls: AccountHeads[] = [];
  mouseClickselection: CustomerMst[] = [];
  customersearchfiled = '';
  mymodel: any;
  ngOnInit(): void {
    // this.  getCustmerDtls()
    this.sharedserviceService
      .getAllCustomerDetailsfromserver()
      .subscribe((data) => {
        this.customerDtls = data;
      });
  }

  onKeypressEvent(x: any) {
    this.mymodel = x.target.value;
    console.log('sadfghjk' + this.mymodel);
  }

  highlightRow(dts: AccountHeads) {
    // this.selectedName = dts.itemName;
    // dd.productQty=this.productQty;
    // console.log('Mouse clicked itm'+this.selectedName);
    this.customersearchfiled = dts.accountName;
    this.saleOrderPopupService.addCustomerDtl(dts);
    this.customerDtlPopup.close(dts);
    // this.sharedserviceService.setInfo(dts);
    // this.mouseClickselection.itemName=dts.itemName;
    // this.mouseClickselection.barCode=dts.barCode;
    // this.mouseClickselection.itemName=dts.itemName;
    // this.mouseClickselection.taxRate=dts.taxRate;
    // this.mouseClickselection.standardPrice=dts.standardPrice

    // console.log(this.mouseClickselection.itemName+'mouseselectionName')
  }

  // getCustmerDtls()
  // {
  //   console.log("getCustmerDtls called")

  //   this.customerDtls=this.saleOrderPopupService.getCustmerDtls()
  // }
}
