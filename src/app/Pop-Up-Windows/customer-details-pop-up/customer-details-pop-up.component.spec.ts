import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerDetailsPopUpComponent } from './customer-details-pop-up.component';

describe('CustomerDetailsPopUpComponent', () => {
  let component: CustomerDetailsPopUpComponent;
  let fixture: ComponentFixture<CustomerDetailsPopUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerDetailsPopUpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerDetailsPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
