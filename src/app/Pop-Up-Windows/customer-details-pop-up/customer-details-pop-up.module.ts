import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerDetailsPopUpRoutingModule } from './customer-details-pop-up-routing.module';
import { CustomerDetailsPopUpComponent } from './customer-details-pop-up.component';
import { FormsModule } from '@angular/forms';
import {MatToolbarModule} from '@angular/material/toolbar';
import { MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


@NgModule({
  declarations: [
    CustomerDetailsPopUpComponent
  ],
  imports: [
    CommonModule,
    CustomerDetailsPopUpRoutingModule,
    FormsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatTableModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    Ng2SearchPipeModule
  ]
})
export class CustomerDetailsPopUpModule { }
