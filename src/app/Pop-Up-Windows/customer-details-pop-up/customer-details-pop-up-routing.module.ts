import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerDetailsPopUpComponent } from './customer-details-pop-up.component';

const routes: Routes = [{ path: '', component: CustomerDetailsPopUpComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerDetailsPopUpRoutingModule { }
