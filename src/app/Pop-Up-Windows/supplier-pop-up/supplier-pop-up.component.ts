import { Component, Inject, OnInit } from '@angular/core';
import { Supplier } from 'src/app/modelclass/supplier';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SaleOrderPopupService } from 'src/app/services/sale-order-popup.service';
import { AccountHeads } from 'src/app/modelclass/account-heads';

@Component({
  selector: 'app-supplier-pop-up',
  templateUrl: './supplier-pop-up.component.html',
  styleUrls: ['./supplier-pop-up.component.css']
})
export class SupplierPopUpComponent implements OnInit {
  supplierDtl:Supplier[]=[];
  
  searchfield='';
  mymodel:any;
  mouseClickselection: Supplier = new Supplier();
  constructor(private sharedserviceService:SharedserviceService,@Inject(MAT_DIALOG_DATA) public data:any,private saleOrderPopupService:SaleOrderPopupService,public supplierPopUp: MatDialogRef<SupplierPopUpComponent>) { }
  //public supplierPopUp: MatDialogRef<SupplierPopUpComponent>
  ngOnInit(): void {
   

    this.sharedserviceService.getPartyDetails(this.searchfield,"SUPPLIER", "BOTH").subscribe((data: any) =>{this.accountHeads=data});
  }
  onKeypressEvent(x: any){
    this.mymodel = x.target.value;
    this.sharedserviceService.getPartyDetails( this.mymodel,"SUPPLIER", "BOTH").subscribe((data: any) =>{this.accountHeads=data});
  }
  highlightRow(dts:AccountHeads){
    this.searchfield=dts.accountName;
    this.saleOrderPopupService.addSupplierDtl(dts);
    this.supplierPopUp.close(dts)
    
  }
  cancel(){
    this.supplierPopUp.close();
  }
  addSup(){
    // this.addingToTable.push(this.mouseClickselection);
    this.supplierPopUp.close();
  }

  //.....................new purchase Save program with party..///


  accountHeads:AccountHeads[]=[];

}
