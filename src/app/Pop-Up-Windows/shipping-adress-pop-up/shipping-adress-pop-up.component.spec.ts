import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShippingAdressPopUpComponent } from './shipping-adress-pop-up.component';

describe('ShippingAdressPopUpComponent', () => {
  let component: ShippingAdressPopUpComponent;
  let fixture: ComponentFixture<ShippingAdressPopUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShippingAdressPopUpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShippingAdressPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
