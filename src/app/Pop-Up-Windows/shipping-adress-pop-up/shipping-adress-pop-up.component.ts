import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { ShippingAddressComponent } from 'src/app/dialogWindows/shipping-address/shipping-address.component';
import { CustomerMst } from 'src/app/modelclass/customer-mst';
import { ShippingAddress } from 'src/app/modelclass/shipping-address';
import { SaleOrderPopupService } from 'src/app/services/sale-order-popup.service';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-shipping-adress-pop-up',
  templateUrl: './shipping-adress-pop-up.component.html',
  styleUrls: ['./shipping-adress-pop-up.component.css']
})
export class ShippingAdressPopUpComponent implements OnInit {

  constructor(public shippingDtlPopUp: MatDialog,private sharedserviceService:SharedserviceService,
    public shippingAddrsDtl: MatDialogRef<ShippingAdressPopUpComponent>,
    private saleOrderPopupService:SaleOrderPopupService) { }

  searchfiled='';
  mymodel: any;
  addQtyGroup: any;
  productQty!: number;
  receivecust:CustomerMst=new CustomerMst;
  shippingDtls:any;
  selectedShippingDtl:ShippingAddress=new ShippingAddress;


  ngOnInit(): void {


    this.saleOrderPopupService.getCustomerDtls().subscribe(
      (data: any)=>{this.receivecust=data;
        console.log(this.receivecust.customerName+"getCustomerDtls")
    }
    );

    this.sharedserviceService.getAllShippingDtlsfromserver(this.receivecust.id).subscribe(data =>{this.shippingDtls=data})

  }

  onKeypressEvent(x: any) {
    this.mymodel = x.target.value;
    console.log('sadfghjk' + this.mymodel);

    //  this.sharedserviceService.searchPopup(this.mymodel).subscribe(data =>
    //   {
    //     this.tdatas=data;

    //   })
  }

 
  close() {

    this.shippingAddrsDtl.close();
   
  }
  
  keytab(event: any) {
  
  }

  highlightRow(names:any)
  {
    this.selectedShippingDtl=names;
    console.log( this.selectedShippingDtl+" this.selectedShippingDtl")
    this.saleOrderPopupService.addShippingDtls(this.selectedShippingDtl);

    this.shippingAddrsDtl.close();
   
  }
  addNewShippingAddrs()
  {
    
    const dialogConfigShippingDtl = new MatDialogConfig();
    dialogConfigShippingDtl.disableClose=false;
    dialogConfigShippingDtl.autoFocus=true;
 
    dialogConfigShippingDtl.width="40%";
    dialogConfigShippingDtl.height="60%";
   
    this.shippingDtlPopUp.open(ShippingAddressComponent,dialogConfigShippingDtl)
  }

}
