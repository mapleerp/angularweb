import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchPopUpRoutingModule } from './search-pop-up-routing.module';
import { SearchPopUpComponent } from './search-pop-up.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import {MatToolbarModule} from '@angular/material/toolbar';
import { MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


@NgModule({
  declarations: [
    SearchPopUpComponent
  ],
  imports: [
    CommonModule,
    SearchPopUpRoutingModule,
    FormsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatTableModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    Ng2SearchPipeModule,
    ReactiveFormsModule,
  ]
})
export class SearchPopUpModule { }
