import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchPopUpComponent } from './search-pop-up.component';

const routes: Routes = [{ path: '', component: SearchPopUpComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchPopUpRoutingModule { }
