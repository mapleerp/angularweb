import { sharedStylesheetJitUrl } from '@angular/compiler';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';

import { observable } from 'rxjs';
import { CustomerMst } from 'src/app/modelclass/customer-mst';
import { SaleOrderPopupService } from 'src/app/services/sale-order-popup.service';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-search-pop-up',
  templateUrl: './search-pop-up.component.html',
  styleUrls: ['./search-pop-up.component.css'],
})
export class SearchPopUpComponent implements OnInit {

  constructor(
    private sharedserviceService: SharedserviceService,
    public searchDialog: MatDialogRef<SearchPopUpComponent>, public fb: FormBuilder,
    private saleOrderPopupService: SaleOrderPopupService,
    public itemPopUp: MatDialogRef<SearchPopUpComponent>
  ) { }

  branchname: any;
  mymodel: any;
  tdatas: datas[] = [];
  addingToTable: datas[] = [];
  mouseClickselection: datas = new datas();

  productQty!: number;
  selectedName: any;
  d: any;
  searchfiled = '';
  dataSource = new MatTableDataSource(this.tdatas);
  retrievedPerson: any;
  i = 0;
  addQtyGroup: any;

  @ViewChild('txtInput')
  txtInput!: ElementRef;

  ngOnInit(): void {
    // this.sharedserviceService.AllDataPopup().subscribe(data =>
    //   {
    //     this.tdatas=data;

    //   })
    this.addQtyGroup = this.fb.group({
      productQty: new FormControl(Number)
    })

    // var tempr = localStorage.getItem('testObject');
    // this.retrievedPerson = JSON.parse(
    //   localStorage.getItem('testObject') || '{}'
    // );
    // console.log(tempr + 'tempr data');
    // console.log(this.tdatas);
    // this.sharedserviceService.getGlobalData().subscribe((info) => {
    //   this.tdatas = info;
    // });

    this.sharedserviceService.GetSearchItemByNameWithComp(this.searchfiled).
      subscribe((info) => {
        this.tdatas = info

        for (let i = 0; i < this.tdatas.length; i++) {
          this.tdatas[i].itemName = info[i][0];
          this.tdatas[i].barCode = info[i][1];
          this.tdatas[i].unitId = info[i][2];
          this.tdatas[i].standardPrice = info[i][3];
          this.tdatas[i].cess = info[i][4];
          this.tdatas[i].taxRate = info[i][5];
          this.tdatas[i].categoryId = info[i][6];

          this.tdatas[i].categoryId = info[i][8];
          this.tdatas[i].hsnCode = info[i][9];
          // this.tdatas[i].itemCode=info[i][10];
          // this.tdatas[i].itemCode=info[i][11];
          // this.tdatas[i].itemCode=info[i][12];
          // this.tdatas[i].itemCode=info[i][13];
          this.tdatas[i].itemCode = info[i][14];

        }

        console.log(info);

      });
  }

  //Item Pop Up - Search Function 

  onKeypressEvent(x: any) {
    this.mymodel = x.target.value;
    console.log('sadfghjk' + this.mymodel);

    this.sharedserviceService.searchPopup(this.mymodel).subscribe(data => {
      this.tdatas = data;

      for (let i = 0; i < this.tdatas.length; i++) {
        this.tdatas[i].itemName = data[i][0];
        this.tdatas[i].barCode = data[i][1];
        this.tdatas[i].unitId = data[i][2];
        this.tdatas[i].standardPrice = data[i][3];
        this.tdatas[i].cess = data[i][4];
        this.tdatas[i].taxRate = data[i][5];
        this.tdatas[i].categoryId = data[i][6];

        this.tdatas[i].categoryId = data[i][8];
        this.tdatas[i].hsnCode = data[i][9];
        // this.tdatas[i].itemCode=info[i][10];
        // this.tdatas[i].itemCode=info[i][11];
        // this.tdatas[i].itemCode=info[i][12];
        // this.tdatas[i].itemCode=info[i][13];
        this.tdatas[i].itemCode = data[i][14];

      }
    })
  }

  highlightRow(dts: datas) {
    this.selectedName = dts.itemName;
    // dd.productQty=this.productQty;
    console.log('Mouse clicked itm' + this.selectedName);
    this.searchfiled = dts.itemName;
    // this.sharedserviceService.setInfo(dts);

    // this.mouseClickselection.itemName = dts.itemName;
    // this.mouseClickselection.barCode = dts.barCode;
    // this.mouseClickselection.itemName = dts.itemName;
    // this.mouseClickselection.taxRate = dts.taxRate;
    // this.mouseClickselection.standardPrice = dts.standardPrice;
    // this.mouseClickselection.hsnCode = dts.hsnCode;

    this.saleOrderPopupService.addItemDtl(dts);
    this.searchDialog.close(dts);
    // console.log(this.mouseClickselection.itemName + 'mouseselectionName');
  }

  submitBtn(qty: any) {
    // if(qty==0 || typeof qty== 'undefined')
    // {
    //   alert("Please enter Qty")
    //   return;
    // }
    // console.log(qty)
    // this.sharedserviceService.qtySharing(qty);
    // this.addingToTable.push(this.mouseClickselection);

    // this.mouseClickselection.qty = qty;
    // this.productQty = 0;
    // this.searchfiled = '';
    // console.log(this.mouseClickselection.itemName + 'nnnaammmeee');
    // if (this.mouseClickselection.itemName != null) {
    //   this.sharedserviceService.setInfo(this.mouseClickselection);
    // }
    // this.mouseClickselection = new datas();
    // console.log('data added to tables');

    this.searchDialog.close();
  }
  close() {
    this.searchDialog.close();
  }

  keytab(event: any) {
    console.log(event[this.i].name + 'keyarrow');
  }
}

export class datas {
  name: any;
  unitPrice: any;
  unitsInStock: any;
  qty: any;
  id: any;
  barCode: any;
  barCodeLine1: any;
  barCodeLine2: any;
  barcodeFormat: any;
  bestBefore: any;
  binNo: any;
  branchCode: any;
  brandId: any;
  categoryId: any;
  cess: any;
  cgst: any;
  hsnCode: any;
  isDeleted: any;
  isKit: any;
  itemCode: any;
  itemDescription: any;
  itemDiscount: any;
  itemDtl: any;
  itemGenericName: any;
  itemGroupId: any;
  itemName: any;
  taxRate: any;
  standardPrice: any;
  Amount: any;
  productQty: number | undefined;
  reorderWaitingPeriod: any;
  unitId: any;
  serviceOrGoods: any;
}
