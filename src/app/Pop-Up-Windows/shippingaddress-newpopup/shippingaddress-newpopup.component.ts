import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { ShippingAddressComponent } from 'src/app/dialogWindows/shipping-address/shipping-address.component';
import { AccountHeads } from 'src/app/modelclass/account-heads';
import { ShippingAddress } from 'src/app/modelclass/shipping-address';
import { SaleOrderPopupService } from 'src/app/services/sale-order-popup.service';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-shippingaddress-newpopup',
  templateUrl: './shippingaddress-newpopup.component.html',
  styleUrls: ['./shippingaddress-newpopup.component.css']
})
export class ShippingaddressNewpopupComponent implements OnInit {

  constructor(
    public shippingDtlPopUp: MatDialog,private sharedserviceService:SharedserviceService,
    public shippingAddrsDtl: MatDialogRef<ShippingaddressNewpopupComponent>,
    private saleOrderPopupService:SaleOrderPopupService
  ) { }
  searchfiled='';
  mymodel: any;
  addQtyGroup: any;
  productQty!: number;
  receivecust:AccountHeads=new AccountHeads;
  shippingDtls:any;
  selectedShippingDtl:ShippingAddress=new ShippingAddress;

  ngOnInit(): void {
    this.saleOrderPopupService.getCustomerDtls().subscribe(
      (data: any)=>{this.receivecust=data;
        console.log(this.receivecust.accountName+"getCustomerDtls")
    }
    );

    this.sharedserviceService.getAllShippingDtlsfromserver(this.receivecust.id).subscribe(data =>
      {this.shippingDtls=data
      console.log(this.shippingDtls);
    })
  }
  onKeypressEvent(x: any) {
    this.mymodel = x.target.value;
    console.log('sadfghjk' + this.mymodel);
  }

 
  close() {
    this.shippingAddrsDtl.close();  
  }
  
  highlightRow(names:any)
  {
    this.selectedShippingDtl=names;
    console.log( this.selectedShippingDtl+" this.selectedShippingDtl")
    this.saleOrderPopupService.addShippingDtls(this.selectedShippingDtl);
    this.shippingAddrsDtl.close();  
  }
  
  addNewShippingAddrs()
  {    
    const dialogConfigShippingDtl = new MatDialogConfig();
    dialogConfigShippingDtl.disableClose=false;
    dialogConfigShippingDtl.autoFocus=true; 
    dialogConfigShippingDtl.width="40%";
    dialogConfigShippingDtl.height="60%";  
    this.shippingDtlPopUp.open(ShippingAddressComponent,dialogConfigShippingDtl)
  }

}
