import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShippingaddressNewpopupComponent } from './shippingaddress-newpopup.component';

describe('ShippingaddressNewpopupComponent', () => {
  let component: ShippingaddressNewpopupComponent;
  let fixture: ComponentFixture<ShippingaddressNewpopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShippingaddressNewpopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShippingaddressNewpopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
