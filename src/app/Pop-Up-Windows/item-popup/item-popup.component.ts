import { DatePipe } from '@angular/common';
import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ItemStockPopUp } from 'src/app/modelclass/item-stock-pop-up';
import { SaleOrderPopupService } from 'src/app/services/sale-order-popup.service';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import {
  datas,
  SearchPopUpComponent,
} from '../search-pop-up/search-pop-up.component';

@Component({
  selector: 'app-item-popup',
  templateUrl: './item-popup.component.html',
  styleUrls: ['./item-popup.component.css'],
})
export class ItemPopupComponent implements OnInit {
  constructor(
    private sharedserviceService: SharedserviceService,
    public datepipe: DatePipe,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public itemPopup: MatDialogRef<ItemPopupComponent>
  ) {}
  
  item:ItemStockPopUp[]=[];
  searchfiled = '';
  mymodel: any;
  date= new Date(); 
  dateConversion:any;
 

  ngOnInit(): void {
    this.dateConversion = this.datepipe.transform(this.date, 'y-MM-dd')
    console.log(this.date+"dateee")
 
  this.sharedserviceService.getItemDetailsfromserver(this.dateConversion,this.searchfiled).subscribe((data:any)=>{
    this.item=data
   

    for (let i = 0; i < this.item.length; i++) {

	
      this.item[i].itemName = data[i][0];
      this.item[i].barcode = data[i][1];
      this.item[i].unitName = data[i][2];
      this.item[i].mrp = data[i][3];
      this.item[i].qty = data[i][4];
      this.item[i].cess = data[i][5];
      this.item[i].tax = data[i][6];

      this.item[i].itemId = data[i][7];
      this.item[i].unitId = data[i][8];

      this.item[i].batch = data[i][9];
      this.item[i].itemcode = data[i][10];
      this.item[i].expDate = data[i][11];
      this.item[i].itemPriceLock = data[i][12];
      this.item[i].store = data[i][13];
     

    }
    
  });

   
  } onKeypressEvent(x: any) {
    this.mymodel = x.target.value;
    console.log('Item popup' + this.mymodel);
  }

  highlightRow(dts: ItemStockPopUp) {
   
   
    console.log(dts.itemName)
    this.sharedserviceService.additemDtl(dts);
    this.itemPopup.close(dts);
    
  }

}
