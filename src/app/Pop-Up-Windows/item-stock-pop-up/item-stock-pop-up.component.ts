import { DatePipe } from '@angular/common';
import { ChangeDetectorRef, Component, HostListener, Inject, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { BarcodeBatchMst } from 'src/app/modelclass/barcode-batch-mst';
import { DayEndClosureHdr } from 'src/app/modelclass/day-end-closure-hdr';
import { FinancialYearMst } from 'src/app/modelclass/financial-year-mst';

import { ItemBatchMst } from 'src/app/modelclass/item-batch-mst';
import { ItemStockPopUp } from 'src/app/modelclass/item-stock-pop-up';
import { MultiUnitMst } from 'src/app/modelclass/multi-unit-mst';
import { UnitMst } from 'src/app/modelclass/unit-mst';
import { SalesTransHdr } from 'src/app/modelclass/sales-trans-hdr';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { datas } from '../search-pop-up/search-pop-up.component';
import { environment } from 'src/environments/environment.prod';
import { CustomerMst } from 'src/app/modelclass/customer-mst';
import { ParamValueConfig } from 'src/app/modelclass/param-value-config';
import { LocalCustomerMst } from 'src/app/modelclass/local-customer-mst';
import { SalesDtl } from 'src/app/modelclass/sales-dtl';
import { TaxMst } from 'src/app/modelclass/tax-mst';
import { Summary } from 'src/app/modelclass/summary';
import { ItemBatchDtlService } from 'src/app/services/item-batch-dtl.service';
import { SaveSalesTransHdrService } from 'src/app/services/save-sales-trans-hdr.service';
import{SalesDtlsService} from 'src/app/services/sales-dtls.service'
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';

export type DialogDataSubmitCallback<T> = (row: T) => void;
@Component({
  selector: 'app-item-stock-pop-up',
  templateUrl: './item-stock-pop-up.component.html',
  styleUrls: ['./item-stock-pop-up.component.css']
})
export class ItemStockPopUpComponent implements OnInit {

  constructor( private sharedserviceService: SharedserviceService,public fb:FormBuilder,
    private changeDetectorRef: ChangeDetectorRef,
    public datepipe: DatePipe, @Inject(MAT_DIALOG_DATA)
    public data: { callback: DialogDataSubmitCallback<any>; defaultValue: SalesDtl },
  
    public itemBatchDtlService:ItemBatchDtlService,public saveSalesTransHdrService:SaveSalesTransHdrService,
    private salesDtlsService:SalesDtlsService,
    public itemStockPopUp: MatDialogRef<ItemStockPopUpComponent>) { }

  //itemBatchMst:ItemBatchMst[]=[];
  searchfiled:any;
  productQty:any='';
  localCustomer='';
  addQtyGroup: any;
  stockPopUp:any;
  itemStockDtl:ItemStockPopUp[]=[];
  itemSelected:ItemStockPopUp=new ItemStockPopUp;
  financialYear:any;
  maxOfDay:DayEndClosureHdr=new DayEndClosureHdr;
  barCodeMst:BarcodeBatchMst=new BarcodeBatchMst;
  getItem:datas=new datas;
  getUnitByItem:UnitMst=new UnitMst;
  getMulti:MultiUnitMst=new MultiUnitMst;
  salesTransHdr:SalesTransHdr= new SalesTransHdr;
   paramval:ParamValueConfig=new ParamValueConfig;
customerResp:CustomerMst=new CustomerMst;
localCustomerMst:LocalCustomerMst=new LocalCustomerMst;
salesDtl:SalesDtl=new SalesDtl;
salesDtlArr: SalesDtl[] = [];
qty:number=0;
txtposDiscount='';
 mrpRateIncludingTax=0.0;
 getTaxMst:TaxMst[]=[];
 dateCov:any;
 input:any;
 getSalesBillDtl:SalesDtl[]=[];
 salesTransHdrHistory: SalesTransHdr= new SalesTransHdr;
 summary:Summary=new Summary;
 salesDtlAddTable:SalesDtl[]=[];
date= new Date();
dateConversion:any;
checkQty:any;
mouseClickStatus:boolean=false;

public keypressed:any;
tableIndexNo=-1;

rowSelected:any;

chkQty=0.0;

displayedColumns: string[] = ['itemname', 'mrp','qty','barcode', 'batch','unit','itemcode','expiry','store'];
  // dataSource = this.itemStockDtl;
  dataSource:MatTableDataSource<ItemStockPopUp>= new MatTableDataSource([]) ;
  selection = new SelectionModel<ItemStockPopUp>(false, [])
  @ViewChild("input2") myInputField: ElementRef;
  @ViewChild("btn1") myBtnField: ElementRef;
  ngOnInit(): void {

   //.....new program...//
 
//used for filterring in a single column

  //  this.dataSource.filterPredicate = function(data:ItemStockPopUp, filter: string): boolean {
  //   return data.itemName.toLowerCase().includes(filter) 
  //   //  || data.barcode.toLowerCase().includes(filter); 
  // //   // || data.itemName.toString() === filter;
  // };
  

    this.dateConversion = this.datepipe.transform(this.date, 'y-MM-dd')
    console.log(this.date+"dateee")
    this.addQtyGroup = this.fb.group({
      productQty: new FormControl(Number)
    })
    this.onKeypressEventforItemSearchFromItemBatchDtl()

  
  }

  close() 
  {
   this.itemStockPopUp.close();
  }

///...........................new Program......................................//

//PopUpData Calling..//
onKeypressEventforItemSearchFromItemBatchDtl()
{
  if(typeof this.searchfiled=="undefined")
     {
      this.searchfiled='';
     }
  this.sharedserviceService.getStockItemPopupSearchwithBatch(this.dateConversion,this.searchfiled).subscribe(
   data=>{ this.itemStockDtl=data;
    // this.dataSource = new MatTableDataSource( this.itemStockDtl);
    this.dataSource.data =this.itemStockDtl;
    this.itemSelected=this.itemStockDtl[0];
    console.log('In ONInit() function')
    console.log(this.itemSelected)
    // if(this.itemSelected!=null){
    //   this.tableIndexNo=0;
    // }
    // var filteredData = this.dataSource.filteredData;
    console.log(this.dataSource.data)
  },
     (error: any) => { console.log(error), 
      alert("Unable to Fetch Data") }
  )

  console.log("onKeypressEventforItemSearchFromItemBatchDtl function called")
}
//PopUpData Calling..//

//  PopUpData Clicked...//

highlightRowforItemSearchFromItemBatchDtl(itemBatchMst:ItemStockPopUp)
{
  this.mouseClickStatus=true;
  // this.itemBatchDtlService.selectItemDtlsToSerivce(itemBatchMst);
 if(environment.clientOrCloud=='CLIENT'){
  this.sharedserviceService.getitemByNameReqParam(itemBatchMst.itemName).subscribe(
    subscribedData=>{ this.getItem=subscribedData;
    if (this.getItem!=null)
  {

  this.sharedserviceService.getQtyFromItemBatchMstByItemAndQty(this.getItem.id,itemBatchMst.batch,itemBatchMst.store).
  subscribe(subscribedData=>{
   this. checkQty=subscribedData;
   console.log(this. checkQty+"this. checkQtyttyyyyyyyyyyyyyy")
   if(typeof this. checkQty=="undefined")
   {
    alert("not in stock...!!")
    return;

   }});
  }
});

 }
 if(environment.clientOrCloud=='CLOUD'){
   console.log('In quantity check - cloud'+itemBatchMst.itemId)
  this.sharedserviceService.getQtyFromItemBatchMstByItemAndQty(itemBatchMst.itemId,itemBatchMst.batch,itemBatchMst.store).
  subscribe(subscribedData=>{
   this. checkQty=subscribedData;
   console.log(subscribedData)
   console.log(this. checkQty+"checkQty in cloud")
   if(typeof this. checkQty=="undefined")
   {
    alert("not in stock...!!")
    this.qtyFocus();
    return;

   }});
 }

  this.searchfiled=itemBatchMst.itemName;
   this.itemSelected=itemBatchMst;
   console.log(itemBatchMst.store+" store name in hightlight row");
  console.log("highlightRowforItemSearchFromItemBatchDtl function called")
}
//  PopUpData Clicked...//


//...Selected Data Added..//
addItemFRomPopUPToBillTable(qty:any)

{
  if(this.productQty=='')
  {
    alert("Please Add QTY")
    return;
  }

  // if(this.mouseClickStatus==false)
  // {
  //   alert("Please Select Item")
  //   return;
  // }

  if(this.productQty>this.checkQty){
    alert('Not in Stock');
    return;
  }

  if(environment.clientOrCloud=='CLIENT'){
    this.checkingWheatherSalesTransHdrSaved()
  }
  else if(environment.clientOrCloud=='CLOUD'){
    this.salesDtl = new SalesDtl();
    this.salesDtl.itemName = this.itemSelected.itemName;
    this.salesDtl.barcode = this.itemSelected.barcode;
    this.salesDtl.batch = this.itemSelected.batch;
    this.salesDtl.qty = this.productQty;
    this.salesDtl.itemId = this.itemSelected.itemId;

    this.salesDtl.mrp = this.itemSelected.standardPrice;
    console.log(this.salesDtl.mrp);
    this.salesDtl.rate = this.salesDtl.mrp * this.productQty;
    this.salesDtl.amount = this.salesDtl.mrp * this.productQty;
    this.salesDtl.unitName = this.itemSelected.unitName;
    this.salesDtl.unitId = this.itemSelected.unitId;
    this.salesDtl.taxRate=this.itemSelected.tax;
    this.salesDtl.expiryDate=this.itemSelected.expDate;
    this.salesDtl.standardPrice=this.itemSelected.standardPrice;
    this.salesDtl.store=this.itemSelected.store;
    // this.salesDtlArr.push(this.salesDtl);
    this.sharedserviceService.PostSalesDtlToCloudPosWindow( this.salesDtl);
  }  
  console.log("addItemFRomPopUPToBillTable function called")
  this.mouseClickStatus=false;
  this.close() 

}

checkingWheatherSalesTransHdrSaved()
{

  this.salesTransHdrHistory= this.sharedserviceService.getsalesTransHdrHistory();
  this.saveSalesDetails();

  console.log("checkingWheatherSalesTransHdrSaved function called")

}

saveSalesDetails()
{

   this.salesDtl.salesTransHdr= this.salesTransHdrHistory;
    this.salesDtlsService.saveSalesDtsl( this.salesDtl.salesTransHdr,this.itemSelected,this.productQty)
    console.log("saveSalesDetails function called")

}

//...Selected Data Added..//

//events

@HostListener('window:keydown', ['$event'])
handleKeyboardEvent(event: KeyboardEvent,) {
  this.keypressed = event.keyCode;
  console.log(" KEY PRESSED {}",this.keypressed)
  if(this.keypressed==40)
  {
    // this.tableIndexNo=0
    this.tableIndexNo=this.tableIndexNo+1;
    console.log(this.dataSource.data[this.tableIndexNo])
    this.searchfiled=this.dataSource.data[this.tableIndexNo].itemName;
    console.log('index'+this.tableIndexNo);
    this.highlightRowforItemSearchFromItemBatchDtl(this.dataSource.data[this.tableIndexNo])
    console.log("DOWN ARROW KEY PRESSED")


  }
  if(this.keypressed==38)
  {

    this.tableIndexNo=this.tableIndexNo-1;
    console.log(this.dataSource.data[this.tableIndexNo]);
    this.searchfiled=this.dataSource.data[this.tableIndexNo].itemName;
    console.log('index'+this.tableIndexNo);
   this.highlightRowforItemSearchFromItemBatchDtl(this.dataSource.data[this.tableIndexNo])
    console.log("UP ARROW KEY PRESSED")
  }

  if(this.keypressed==8||this.keypressed==46){
    this.applyFilter(event);
    this.tableIndexNo=-1;
    this.dataSource.data=this.itemStockDtl;
    console.log("-------------------------------")
    console.log(this.dataSource.data)
    console.log("DELETE OR BACKSPACE KEY PRESSED");
  }

  if(this.keypressed==13){
    console.log("ENTER KEY PRESSED")
    console.log(this.searchfiled+" item name in enter key")
    // this.myInputField.nativeElement.focus();
    if(this.searchfiled.length==0){
      console.log('itemname is empty');
    }
    if(this.productQty==''){
      console.log('focus to qty field if productQty is empty');
    this.qtyFocus();
    return;
    }
    else{
      console.log('focus to add button  if productQty is present');
      this.myBtnField.nativeElement.focus();
    }
  }
}


tableSelectionDown(i:any,temData:any)
{
  if(this.rowSelected === i) 
  {this.rowSelected = -1;
  }
  else this.rowSelected = i;
  console.log(temData)
}


applyFilter(event:KeyboardEvent) {
  // this.changeDetectorRef.detectChanges();
  const filterValue = (event.target as HTMLInputElement).value;
  console.log(filterValue.trim()+" item name search in popup")
  if(filterValue.trim().length==0){
    console.log("search value is empty")
    this.dataSource.data=this.itemStockDtl;
  }
  // if(this.keypressed==8||this.keypressed==46){
  //   console.log('In applyFilter()- backspace')
  //   this.dataSource.data=this.itemStockDtl;
  //   console.log(this.dataSource.data);
  // }
  else{
    this.dataSource.filter = filterValue.trim().toLowerCase();
    this.dataSource.data=this.dataSource.filteredData;
    console.log(this.dataSource.data)
  }  
  if(event.key=="ENTER") {
    console.log('---- ENTER KEY PRESSED ----');
      
  }
  //  this.changeDetectorRef.detectChanges();
}

qtyFocus(){
  this.myInputField.nativeElement.focus();
  // if(this.productQty=='')
  // {
  //   // alert("Please Add QTY")
  //   return;
  // }
  // else{
  //   this.addItemFRomPopUPToBillTable(this.productQty);
  // }
}

// changeFocus(name) {
//   if(name) this.$refs[name].focus();
// }

// pressEnter(from,to){
//   to.focus();
// }

// applyFilter(search) {
  
//   if(search.length==0){
//     this.dataSource.data=this.itemStockDtl;
//   }
//   this.dataSource.filter = search.trim().toLowerCase();
//   this.dataSource.data=this.dataSource.filteredData;
//   console.log(this.dataSource.data)
// }
// highlight(row: any){
//   this.selectedRowIndex = row.position;
//   console.log(this.selectedRowIndex);
// }

// arrowUpEvent(row: object, index: number){
//   console.log(index);
//  var nextrow = this.dataSource[index - 2];
//  this.highlight(nextrow);
// }

// arrowDownEvent(row: object, index: number){
//  console.log(index);
//  var nextrow = this.dataSource[index];
//   this.highlight(nextrow);
// }
}

