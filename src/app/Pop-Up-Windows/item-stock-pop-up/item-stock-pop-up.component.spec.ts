import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemStockPopUpComponent } from './item-stock-pop-up.component';

describe('ItemStockPopUpComponent', () => {
  let component: ItemStockPopUpComponent;
  let fixture: ComponentFixture<ItemStockPopUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemStockPopUpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemStockPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
