import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { GstinputdetailComponent } from './reportPages/gstinputdetail/gstinputdetail.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './loginpage/login/login.component';
import { HomepageComponent } from './homepage/homepage/homepage.component';
import { StockMovementViewComponent } from './reportPages/stock-movement-view/stock-movement-view.component';
import { DayEndReportComponent } from './reportPages/day-end-report/day-end-report.component';
import { HomepageHotCakesComponent } from './homepage/homepage-hot-cakes/homepage-hot-cakes.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { DatePipe } from '@angular/common';
import { SalesSummaryReportComponent } from './reportPages/sales-summary-report/sales-summary-report.component';
import { SalesDetailsReportComponent } from './reportPages/sales-details-report/sales-details-report.component';
import { ChartsModule } from 'ng2-charts';
import { DashBoardComponent } from './dash-board/dash-board.component';
import {} from '@ngx-pwa/offline';
import { AddDialogDeploymentComponent } from './dialogWindows/add-dialog-deployment/add-dialog-deployment.component';
import { SearchPopUpComponent } from './Pop-Up-Windows/search-pop-up/search-pop-up.component';

import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { SearchPipePipe } from './customPipes/search-pipe.pipe';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { PosWindowComponent } from './pos-window/pos-window.component';
import { PurchaseWindowComponent } from './purchase-window/purchase-window.component';
import { CustomerRegistrationComponent } from './masters/customer-registration/customer-registration.component';
import { CompanyRegistrationComponent } from './masters/company-registration/company-registration.component';
import { PointOfSalesWindowComponent } from './point-of-sales-window/point-of-sales-window.component';
import { InvoiceBillGerationComponent } from './reportPages/invoice-bill-geration/invoice-bill-geration.component';

import { SideNavBarComponent } from './MainMenus/side-nav-bar/side-nav-bar.component';
import { MenuHeaderComponent } from './MainMenus/menu-header/menu-header.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDividerModule } from '@angular/material/divider';
import { MenuPageComponent } from './MainMenus/menu-page/menu-page.component';

// import { UserregistrationComponent } from './masters/userregistration/userregistration.component';
import { ItemcreationComponent } from './masters/itemcreation/itemcreation.component';

import { QtyEditWindowComponent } from './dialogWindows/qty-edit-window/qty-edit-window.component';

import { SaleOrderWindowComponent } from './sale-order-window/sale-order-window.component';
import { UserregistrationComponent } from './masters/userregistration/userregistration.component';

import { CustomerDetailsPopUpComponent } from './Pop-Up-Windows/customer-details-pop-up/customer-details-pop-up.component';
import { SaleOrderPopupService } from './services/sale-order-popup.service';
import { FrontPageComponent } from './front-page/front-page.component';
import { ShippingAddressComponent } from './dialogWindows/shipping-address/shipping-address.component';

import { DashboardConfigComponent } from './dashboard-config/dashboard-config.component';
import { DashboardSettingsComponent } from './dashboard-settings/dashboard-settings.component';

import { CategorycreationComponent } from './masters/categorycreation/categorycreation.component';
import { from } from 'rxjs';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ShippingAdressPopUpComponent } from './Pop-Up-Windows/shipping-adress-pop-up/shipping-adress-pop-up.component';
import { ItemStockPopUpComponent } from './Pop-Up-Windows/item-stock-pop-up/item-stock-pop-up.component';
import { AddsupplierComponent } from './masters/addsupplier/addsupplier.component';
import { ItempropertiesComponent } from './masters/itemproperties/itemproperties.component';
import { ItemPopupComponent } from './Pop-Up-Windows/item-popup/item-popup.component';
import { WebUserComponent } from './masters/web-user/web-user.component';

import { MatTabsModule } from '@angular/material/tabs';
import { SalesReceiptwindowComponent } from './masters/sales-receiptwindow/sales-receiptwindow.component';
import { PosInvoiceBillComponent } from './reportPages/pos-invoice-bill/pos-invoice-bill.component';
import { GroupwiseSalesDetailReportComponent } from './groupwise-sales-detail-report/groupwise-sales-detail-report.component';

import { WholesaleComponent } from './wholesale/wholesale.component';
import { ReceiptModeWiseReportComponent } from './reportPages/receipt-mode-wise-report/receipt-mode-wise-report.component';
import { MonthlySalesReportComponent } from './reportPages/monthly-sales-report/monthly-sales-report.component';
import { ProfitAndLossReportComponent } from './reportPages/profit-and-loss-report/profit-and-loss-report.component';
import { PosFinalBIllComponent } from './reportPages/pos-final-bill/pos-final-bill.component';

import { WholeSalesBillReportComponent } from './reportPages/whole-sales-bill-report/whole-sales-bill-report.component';

import { SupplierPopUpComponent } from './Pop-Up-Windows/supplier-pop-up/supplier-pop-up.component';

import { TableListAndDetailsComponent } from './KOT Table/table-list-and-details/table-list-and-details.component';
import { CategoryDetailsComponent } from './KOT Table/category-details/category-details.component';
import { TableDetailsComponent } from './KOT Table/table-details/table-details.component';

import { PurchaseFinalBillComponent } from './reportPages/purchase-final-bill/purchase-final-bill.component';

import { KotComponent } from './Kot/kot/kot.component';
import { SearchComponent } from './eShopping_Project/component/search/search.component';
import { CartDetailsComponent } from './eShopping_Project/Components/cart-details/cart-details.component';
import { CartStatusComponent } from './eShopping_Project/Components/cart-status/cart-status.component';
import { CheckoutComponent } from './eShopping_Project/Components/checkout/checkout.component';
import { OrderHistoryComponent } from './eShopping_Project/Components/order-history/order-history.component';
import { ProductCategoryMenuComponent } from './eShopping_Project/Components/product-category-menu/product-category-menu.component';
import { ProductDetailsComponent } from './eShopping_Project/Components/product-details/product-details.component';
import { ProductListComponent } from './eShopping_Project/Components/product-list/product-list.component';
import { Routes } from '@angular/router';
import { EshopappComponent } from './eShopping_Project/eshopapp/eshopapp.component';
import { EshopLoginComponent } from './eShopping_Project/Components/eshop-login/eshop-login.component';

import { WaiterloginComponent } from './WaiterLogin/waiterlogin/waiterlogin.component';
import { ItemWiseSalesReportComponent } from './reportPages/item-wise-sales-report/item-wise-sales-report.component';
import { ItemListPopUpComponent } from './Pop-Up-Windows/item-list-pop-up/item-list-pop-up.component';
import { UserWiseSalesReportComponent } from './user-wise-sales-report/user-wise-sales-report.component';
import { PurchaseConsolidatedReportComponent } from './reportPages/purchase-consolidated-report/purchase-consolidated-report.component';
import { SalesAndStockConsolidatedReportComponent } from './reportPages/sales-and-stock-consolidated-report/sales-and-stock-consolidated-report.component';
import { PoswindowComponent } from './poswindow/poswindow.component';
import { WebsiteComponent } from './website/website.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { WebsiteServiceService } from './services/website-service.service';
import { PartyCreationComponent } from './masters/party-creation/party-creation.component';
import { CloudwholesaleComponent } from './cloudwholesale/cloudwholesale.component';
import { ShippingaddressNewpopupComponent } from './Pop-Up-Windows/shippingaddress-newpopup/shippingaddress-newpopup.component';

import { StockRequestComponent } from './AdvancedFeatures/stock-request/stock-request.component';
import { UserSubsidiaryAssociationComponent } from './user-subsidiary-association/user-subsidiary-association.component';
import { SwitchbranchComponent } from './switchbranch/switchbranch.component';
import {StocktransferComponent} from './stocktransfer/stocktransfer.component';
import { SalesmancreationComponent } from './masters/salesmancreation/salesmancreation.component'
import { PurchaseBillComponent } from './reportPages/purchase-bill/purchase-bill.component'
import { StockRequestReportComponent } from './invoiceBills/stock-request-report/stock-request-report.component';
import { ReceiptReportComponent } from './invoiceBills/receipt-report/receipt-report.component';
import { ReceiptWindowComponent } from './Accounts/receipt-window/receipt-window.component';
import { StockTransferReportComponent } from './invoiceBills/stock-transfer-report/stock-transfer-report.component';
import { StockTransferOutReportsComponent } from './reportPages/stock-transfer-out-reports/stock-transfer-out-reports.component';
import { StockTransferOutReportPrintComponent } from './invoiceBills/stock-transfer-out-report-print/stock-transfer-out-report-print.component';
import { StockTransferOutReportPrintSummaryComponent } from './invoiceBills/stock-transfer-out-report-print-summary/stock-transfer-out-report-print-summary.component';
// Payment.........................
import { PaymentsWindowComponent } from './Accounts/payments-window/payments-window.component';
import { PaymentsReportWindowComponent } from './invoiceBills/payments-report-window/payments-report-window.component';
import { JournalComponent } from './Accounts/journal/journal.component';
import { AcceptstockComponent } from './AdvancedFeatures/acceptstock/acceptstock.component';
import { VoucherNoPopupComponent } from './Pop-Up-Windows/voucher-no-popup/voucher-no-popup.component';
import { BranchcreationwindowComponent } from './masters/branchcreationwindow/branchcreationwindow.component';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomepageComponent,

    GstinputdetailComponent,
    StockMovementViewComponent,
    DayEndReportComponent,
    HomepageHotCakesComponent,
    SalesSummaryReportComponent,
    SalesDetailsReportComponent,
    DashBoardComponent,
    SearchPipePipe,
    DashboardComponent,

    PosWindowComponent,
    PurchaseWindowComponent,
    CustomerRegistrationComponent,

    PointOfSalesWindowComponent,
    InvoiceBillGerationComponent,

    SideNavBarComponent,
    MenuHeaderComponent,
    MenuPageComponent,

    QtyEditWindowComponent,
    CompanyRegistrationComponent,

    SaleOrderWindowComponent,

    UserregistrationComponent,

    DashboardSettingsComponent,

    ItemcreationComponent,
    FrontPageComponent,
    ShippingAddressComponent,
    DashboardConfigComponent,

    CategorycreationComponent,
    ItemStockPopUpComponent,
    ShippingAdressPopUpComponent,
    AddsupplierComponent,
    ItempropertiesComponent,
    ItemPopupComponent,
    WebUserComponent,
    SalesReceiptwindowComponent,
    PosInvoiceBillComponent,
    GroupwiseSalesDetailReportComponent,

    UserWiseSalesReportComponent,
    WholesaleComponent,
    ReceiptModeWiseReportComponent,
    MonthlySalesReportComponent,
    ProfitAndLossReportComponent,
    PosFinalBIllComponent,

    WholeSalesBillReportComponent,

    SupplierPopUpComponent,

    TableListAndDetailsComponent,
    CategoryDetailsComponent,
    TableDetailsComponent,
    PurchaseFinalBillComponent,

    TableListAndDetailsComponent,
    CategoryDetailsComponent,
    TableDetailsComponent,
    KotComponent,

    SearchComponent,
    CartDetailsComponent,
    CartStatusComponent,
    CheckoutComponent,
    OrderHistoryComponent,
    ProductCategoryMenuComponent,
    ProductDetailsComponent,
    ProductListComponent,
    EshopappComponent,
    EshopLoginComponent,

    WaiterloginComponent,
    ItemWiseSalesReportComponent,
    ItemListPopUpComponent,
    PurchaseConsolidatedReportComponent,
    SalesAndStockConsolidatedReportComponent,
    PoswindowComponent,
    WebsiteComponent,
    SignUpComponent,
    PartyCreationComponent,
    CloudwholesaleComponent,
    ShippingaddressNewpopupComponent,

    StockRequestComponent,

    UserSubsidiaryAssociationComponent,
    SwitchbranchComponent,
    StocktransferComponent,
    SalesmancreationComponent,

    PurchaseBillComponent,

    StockRequestReportComponent,
    ReceiptReportComponent,
    ReceiptWindowComponent,
    StockTransferReportComponent,
// Payment.......................................................................
    PaymentsReportWindowComponent,
    PaymentsWindowComponent,

    StockTransferOutReportsComponent,
    StockTransferOutReportPrintComponent,
    StockTransferOutReportPrintSummaryComponent,

    JournalComponent,
    AcceptstockComponent,
    VoucherNoPopupComponent,
    BranchcreationwindowComponent,


  ],
  imports: [
    BrowserModule,
    MatSidenavModule,
    AppRoutingModule,
    MatDividerModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatTableModule,
    FormsModule,
    HttpClientModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    ChartsModule,
    ReactiveFormsModule,
    MatDialogModule,
    Ng2SearchPipeModule,
    MatTabsModule,
  ],
  providers: [
    DatePipe,
    SaleOrderPopupService,
    WebsiteServiceService,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    AddDialogDeploymentComponent,
    InvoiceBillGerationComponent,
    SearchPopUpComponent,
    QtyEditWindowComponent,
    CustomerDetailsPopUpComponent,
    ItemStockPopUpComponent,
    PosInvoiceBillComponent,
    SupplierPopUpComponent,
    CustomerDetailsPopUpComponent,
    ShippingAddressComponent,
    ShippingAdressPopUpComponent,
    TableDetailsComponent,
    ItemListPopUpComponent,
  ],
})
export class AppModule {}
