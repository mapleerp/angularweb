import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccessloginGuard } from './authorizations/accesslogin.guard';
import { DashBoardComponent } from './dash-board/dash-board.component';

import { HomepageHotCakesComponent } from './homepage/homepage-hot-cakes/homepage-hot-cakes.component';

import { HomepageComponent } from './homepage/homepage/homepage.component';
import { MaplesystemAdminComponent } from './homepage/maplesystem-admin/maplesystem-admin.component';
import { LoginComponent } from './loginpage/login/login.component';

import { PosWindowComponent } from './pos-window/pos-window.component';

import { MenuPageComponent } from './MainMenus/menu-page/menu-page.component';
import { PointOfSalesWindowComponent } from './point-of-sales-window/point-of-sales-window.component';

import { DayEndReportComponent } from './reportPages/day-end-report/day-end-report.component';
import { GstinputdetailComponent } from './reportPages/gstinputdetail/gstinputdetail.component';
import { InvoiceBillGerationComponent } from './reportPages/invoice-bill-geration/invoice-bill-geration.component';
import { SalesDetailsReportComponent } from './reportPages/sales-details-report/sales-details-report.component';
import { SalesSummaryReportComponent } from './reportPages/sales-summary-report/sales-summary-report.component';
import { StockMovementViewComponent } from './reportPages/stock-movement-view/stock-movement-view.component';
import { CustomerRegistrationComponent } from './masters/customer-registration/customer-registration.component';
import { UserregistrationComponent } from './masters/userregistration/userregistration.component';
import { BranchCreationComponent } from './masters/branch-creation/branch-creation.component';
import { SaleOrderWindowComponent } from './sale-order-window/sale-order-window.component';
import { ItemcreationComponent } from './masters/itemcreation/itemcreation.component';
import { FrontPageComponent } from './front-page/front-page.component';
import { CompanyRegistrationComponent } from './masters/company-registration/company-registration.component';
import { CategorycreationComponent } from './masters/categorycreation/categorycreation.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PurchaseWindowComponent } from './purchase-window/purchase-window.component';
import { AddsupplierComponent } from './masters/addsupplier/addsupplier.component';
import { WebUserComponent } from './masters/web-user/web-user.component';
import { UserWiseSalesReportComponent } from './user-wise-sales-report/user-wise-sales-report.component';
import { MonthlySalesReportComponent } from './reportPages/monthly-sales-report/monthly-sales-report.component';
import { ReceiptModeWiseReportComponent } from './reportPages/receipt-mode-wise-report/receipt-mode-wise-report.component';
import { ProfitAndLossReportComponent } from './reportPages/profit-and-loss-report/profit-and-loss-report.component';
import { GroupwiseSalesDetailReportComponent } from './groupwise-sales-detail-report/groupwise-sales-detail-report.component';
import { WholesaleComponent } from './wholesale/wholesale.component';
import { PosFinalBIllComponent } from './reportPages/pos-final-bill/pos-final-bill.component';
import { WholeSalesBillReportComponent } from './reportPages/whole-sales-bill-report/whole-sales-bill-report.component';
import { PurchaseFinalBillComponent } from './reportPages/purchase-final-bill/purchase-final-bill.component';
import { OrderHistoryComponent } from './eShopping_Project/Components/order-history/order-history.component';
import { CheckoutComponent } from './eShopping_Project/Components/checkout/checkout.component';
import { CartDetailsComponent } from './eShopping_Project/Components/cart-details/cart-details.component';
import { ProductDetailsComponent } from './eShopping_Project/Components/product-details/product-details.component';
import { ProductListComponent } from './eShopping_Project/Components/product-list/product-list.component';
import { EshopLoginComponent } from './eShopping_Project/Components/eshop-login/eshop-login.component';
import { KotComponent } from './Kot/kot/kot.component';
import { WaiterloginComponent } from './WaiterLogin/waiterlogin/waiterlogin.component';
import { ItemWiseSalesReportComponent } from './reportPages/item-wise-sales-report/item-wise-sales-report.component';
import { PurchaseConsolidatedReportComponent } from './reportPages/purchase-consolidated-report/purchase-consolidated-report.component';
import { SalesAndStockConsolidatedReportComponent } from './reportPages/sales-and-stock-consolidated-report/sales-and-stock-consolidated-report.component';
import { PoswindowComponent } from './poswindow/poswindow.component';
import { WebsiteComponent } from './website/website.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { PartyCreationComponent } from './masters/party-creation/party-creation.component';

import { StockRequestComponent } from './AdvancedFeatures/stock-request/stock-request.component';
import { SwitchbranchComponent } from './switchbranch/switchbranch.component';
import { UserSubsidiaryAssociationComponent } from './user-subsidiary-association/user-subsidiary-association.component';
import { SalesmancreationComponent } from './masters/salesmancreation/salesmancreation.component';

import { PurchaseBillComponent } from './reportPages/purchase-bill/purchase-bill.component';

import { StockRequestReportComponent } from './invoiceBills/stock-request-report/stock-request-report.component';
import { ReceiptReportComponent } from './invoiceBills/receipt-report/receipt-report.component';
import { ReceiptWindowComponent } from './Accounts/receipt-window/receipt-window.component';
import { StockTransferReportComponent } from './invoiceBills/stock-transfer-report/stock-transfer-report.component';
// Payment....................
import { PaymentsReportWindowComponent } from './invoiceBills/payments-report-window/payments-report-window.component';
import { StockTransferOutReportPrintComponent } from './invoiceBills/stock-transfer-out-report-print/stock-transfer-out-report-print.component';
import { StockTransferOutReportsComponent } from './reportPages/stock-transfer-out-reports/stock-transfer-out-reports.component';
import { StockTransferOutReportPrintSummaryComponent } from './invoiceBills/stock-transfer-out-report-print-summary/stock-transfer-out-report-print-summary.component';
import { PaymentsWindowComponent } from './Accounts/payments-window/payments-window.component';
import { CloudwholesaleComponent } from './cloudwholesale/cloudwholesale.component';
import { StocktransferComponent } from './stocktransfer/stocktransfer.component';
import { AcceptstockComponent } from './AdvancedFeatures/acceptstock/acceptstock.component';
import { JournalComponent } from './Accounts/journal/journal.component';





const routes: Routes = [
 

  { path: 'MainMenuPage/reports', redirectTo: 'homeHotCakes' },
  { path: 'PosFinalBIll', component: PosFinalBIllComponent },
   { path: 'StockRequestBIll', component: StockRequestReportComponent },
   { path: 'StockTransferBill', component: StockTransferReportComponent },
   { path: 'stocktransferoutreportprint', component: StockTransferOutReportPrintComponent },
  //  Payment Report.....................
  
   { path: 'PaymentsBill', component: PaymentsReportWindowComponent },
   { path: 'ReceiptReport', component: ReceiptReportComponent },
  { path: 'WholeSalesBill', component: WholeSalesBillReportComponent },
  { path: 'homeHotCakes/MainMenuPage', redirectTo: 'MainMenuPage' },
  { path: 'stocktransferoutreportprintsummary', component: StockTransferOutReportPrintSummaryComponent },
  { path: 'webUserRegistration', component: WebUserComponent },
  { path: 'PurchaseBill', component: PurchaseBillComponent },
  { path: '', component: WebsiteComponent },
  { path: 'SignUp', component: SignUpComponent },
  { path: 'Login', component: FrontPageComponent },
  {
    path: 'loginpage',
    canActivate: [AccessloginGuard],
    component: MenuPageComponent,

    children: [
      { path: 'PosWindow', component:  PoswindowComponent},
      {path: 'loginpage/PosWindow', redirectTo: 'PosWindow' },
      { path: 'CommonUserPosWindow', component: PointOfSalesWindowComponent},
      { path: 'loginpage/CommonUserPosWindow', redirectTo: 'CommonUserPosWindow' },

      { path: 'CloudWholesale', component: CloudwholesaleComponent},
      { path: 'loginpage/CloudWholesale', redirectTo: 'CloudWholesale' },

      { path: 'KotComponent', component: KotComponent },
      { path: 'CustomerCreation', component: CustomerRegistrationComponent },
      { path: 'UserCreation', component: UserregistrationComponent },
      { path: 'BranchCreation', component: BranchCreationComponent },
      { path: 'SaleOders', component: SaleOrderWindowComponent },
      { path: 'ItemCreation', component: ItemcreationComponent },
      { path: 'PartyCreation', component: PartyCreationComponent },
      { path: 'Categorycreation', component: CategorycreationComponent },
      { path: 'Purchase', component: PurchaseWindowComponent },
      { path: 'StockMovementReport', component: StockMovementViewComponent },
      { path: 'DayEndReport', component: DayEndReportComponent },
      { path: 'SalesSummaryReport', component: SalesSummaryReportComponent },
      { path: 'DashBoard', component: DashboardComponent },
      { path: 'SalesDetailsReport', component: SalesDetailsReportComponent },
      { path: 'AddSupplier', component: AddsupplierComponent },

      { path: 'StockTransferOutReport', component: StockTransferOutReportsComponent },
      { path: 'StockRequest', component: StockRequestComponent },
      { path: 'Journal', component: JournalComponent },
      { path: 'AcceptStock', component: AcceptstockComponent},
      { path: 'loginpage/AcceptStock', redirectTo: 'AcceptStock' },
      { path: 'StockTransfer', component: StocktransferComponent },
      { path: 'ReceiptWindow', component:  ReceiptWindowComponent},
    // Payment Window.............................
      { path: 'PaymentsWindow', component:  PaymentsWindowComponent},


      { path: 'SwitchToBranch', component: SwitchbranchComponent },
      { path: 'UserBranchAssociation', component: UserSubsidiaryAssociationComponent },
      { path: 'SalesManCreation', component: SalesmancreationComponent },

      { path: 'MonthlySalesReport', component: MonthlySalesReportComponent },
      {
        path: 'ReceiptModeWiseReport',
        component: ReceiptModeWiseReportComponent,
      },
      { path: 'SalesAndStockConsolidatedReport', component: SalesAndStockConsolidatedReportComponent },
      
      { path: 'ItemWiseSalesReport', component: ItemWiseSalesReportComponent },
      {
        path: 'PurchaseConsolidatedReport',
        component: PurchaseConsolidatedReportComponent,
      },
      { path: 'ProfitAndLossReport', component: ProfitAndLossReportComponent },
      {
        path: 'GroupwiseSalesDetailReport',
        component: GroupwiseSalesDetailReportComponent,
      },
      {
        path: 'UserWiseSalesReports',
        component: UserWiseSalesReportComponent,
      },
      { path: 'Wholesale', component: WholesaleComponent },

      {
        path: 'salesdeletedreport',
        loadChildren: () =>
          import(
            './reportPages/sales-details-deltd-report/sales-details-deltd-report.module'
          ).then((m) => m.SalesDetailsDeltdReportModule),
      },
      {
        path: 'salesBillDetails',
        loadChildren: () =>
          import(
            './reportPages/sales-bill-details/sales-bill-details.module'
          ).then((m) => m.SalesBillDetailsModule),
      },
      {
        path: 'hsnCodeWiseSalesReport',
        loadChildren: () =>
          import(
            './reportPages/hsn-code-wise-sales-report/hsn-code-wise-sales-report.module'
          ).then((m) => m.HsnCodeWiseSalesReportModule),
      },
      {
        path: 'DailySalesReport',
        loadChildren: () =>
          import(
            './reportPages/daily-sales-report/daily-sales-report.module'
          ).then((m) => m.DailySalesReportModule),
      },
      {
        path: 'TrailBalanceReport',
        loadChildren: () =>
          import(
            './reportPages/trail-balance-report/trail-balance-report.module'
          ).then((m) => m.TrailBalanceReportModule),
      },
      {
        path: 'StatementsOfaccount',
        loadChildren: () =>
          import(
            './reportPages/statements-ofaccount/statements-ofaccount.module'
          ).then((m) => m.StatementsOfaccountModule),
      },
      {
        path: 'DayEndSalesSummary',
        loadChildren: () =>
          import(
            './reportPages/day-end-sales-summary/day-end-sales-summary.module'
          ).then((m) => m.DayEndSalesSummaryModule),
      },
      {
        path: 'StockTransferOutReport',
        loadChildren: () =>
          import(
            './reportPages/stock-transfer-out-report/stock-transfer-out-report.module'
          ).then((m) => m.StockTransferOutReportModule),
      },
      {
        path: 'StockTransferOutDetailsReport',
        loadChildren: () =>
          import(
            './reportPages/stock-transfer-out-detail-report/stock-transfer-out-detail-report.module'
          ).then((m) => m.StockTransferOutDetailReportModule),
      },
      {
        path: 'CommandPage',
        loadChildren: () =>
          import('./masters/command-page/command-page.module').then(
            (m) => m.CommandPageModule
          ),
      },
      {
        path: 'StockReport',
        loadChildren: () =>
          import('./reportPages/stock-report/stock-report.module').then(
            (m) => m.StockReportModule
          ),
      },
      {
        path: 'PurchaseDetailsReport',
        loadChildren: () =>
          import(
            './reportPages/purchase-details-report/purchase-details-report.module'
          ).then((m) => m.PurchaseDetailsReportModule),
      },
      {
        path: 'PurchaseSummaryReport',
        loadChildren: () =>
          import(
            './reportPages/purchase-summary-report/purchase-summary-report.module'
          ).then((m) => m.PurchaseSummaryReportModule),
      },
      {
        path: 'HsnCodePurchaseReport',
        loadChildren: () =>
          import(
            './reportPages/hsn-code-purchase-report/hsn-code-purchase-report.module'
          ).then((m) => m.HsnCodePurchaseReportModule),
      },
      {
        path: 'BranchCreation',
        loadChildren: () =>
          import('./masters/branch-creation/branch-creation.module').then(
            (m) => m.BranchCreationModule
          ),
      },
    ],
  },

  {
    path: 'home',
    canActivate: [AccessloginGuard],
    component: HomepageComponent,

    children: [
      { path: 'gstinputdetail', component: GstinputdetailComponent },
      { path: 'StockMovementReport', component: StockMovementViewComponent },
      { path: 'DayEndReport', component: DayEndReportComponent },
    ],
  },
  {
    path: 'mapleSystems',
    canActivate: [AccessloginGuard],
    component: MaplesystemAdminComponent,
    children: [
      {
        path: 'salesdeletedreport',
        loadChildren: () =>
          import(
            './reportPages/sales-details-deltd-report/sales-details-deltd-report.module'
          ).then((m) => m.SalesDetailsDeltdReportModule),
      },
      {
        path: 'mapleErpDeployment',
        loadChildren: () =>
          import('./maple-erp-deployment/maple-erp-deployment.module').then(
            (m) => m.MapleErpDeploymentModule
          ),
      },
      {
        path: 'SearchPopUp',
        loadChildren: () =>
          import('./Pop-Up-Windows/search-pop-up/search-pop-up.module').then(
            (m) => m.SearchPopUpModule
          ),
      },

      { path: 'PosWindow', component: PosWindowComponent },

      {
        path: 'DailySalesReportTable',
        loadChildren: () =>
          import(
            './reportPages/daily-sales-report-table/daily-sales-report-table.module'
          ).then((m) => m.DailySalesReportTableModule),
      },
      { path: 'PosWindow', component: PointOfSalesWindowComponent },
     
    ],
  },

  {
    path: 'homeHotCakes',
    canActivate: [AccessloginGuard],
    component: HomepageHotCakesComponent,
    children: [
      { path: 'gstinputdetail', component: GstinputdetailComponent },
      { path: 'StockMovementReport', component: StockMovementViewComponent },
      { path: 'DayEndReport', component: DayEndReportComponent },
      { path: 'SalesSummaryReport', component: SalesSummaryReportComponent },
      { path: 'DashBoard', component: DashBoardComponent },
      { path: 'SalesDetailsReport', component: SalesDetailsReportComponent },
      {
        path: 'salesdeletedreport',
        loadChildren: () =>
          import(
            './reportPages/sales-details-deltd-report/sales-details-deltd-report.module'
          ).then((m) => m.SalesDetailsDeltdReportModule),
      },
      {
        path: 'salesBillDetails',
        loadChildren: () =>
          import(
            './reportPages/sales-bill-details/sales-bill-details.module'
          ).then((m) => m.SalesBillDetailsModule),
      },
      {
        path: 'hsnCodeWiseSalesReport',
        loadChildren: () =>
          import(
            './reportPages/hsn-code-wise-sales-report/hsn-code-wise-sales-report.module'
          ).then((m) => m.HsnCodeWiseSalesReportModule),
      },
      {
        path: 'DailySalesReport',
        loadChildren: () =>
          import(
            './reportPages/daily-sales-report/daily-sales-report.module'
          ).then((m) => m.DailySalesReportModule),
      },
      {
        path: 'TrailBalanceReport',
        loadChildren: () =>
          import(
            './reportPages/trail-balance-report/trail-balance-report.module'
          ).then((m) => m.TrailBalanceReportModule),
      },
      {
        path: 'StatementsOfaccount',
        loadChildren: () =>
          import(
            './reportPages/statements-ofaccount/statements-ofaccount.module'
          ).then((m) => m.StatementsOfaccountModule),
      },
      {
        path: 'DayEndSalesSummary',
        loadChildren: () =>
          import(
            './reportPages/day-end-sales-summary/day-end-sales-summary.module'
          ).then((m) => m.DayEndSalesSummaryModule),
      },
      {
        path: 'StockTransferOutReport',
        loadChildren: () =>
          import(
            './reportPages/stock-transfer-out-report/stock-transfer-out-report.module'
          ).then((m) => m.StockTransferOutReportModule),
      },
      {
        path: 'StockTransferOutDetailsReport',
        loadChildren: () =>
          import(
            './reportPages/stock-transfer-out-detail-report/stock-transfer-out-detail-report.module'
          ).then((m) => m.StockTransferOutDetailReportModule),
      },
      {
        path: 'CommandPage',
        loadChildren: () =>
          import('./masters/command-page/command-page.module').then(
            (m) => m.CommandPageModule
          ),
      },
      {
        path: 'StockReport',
        loadChildren: () =>
          import('./reportPages/stock-report/stock-report.module').then(
            (m) => m.StockReportModule
          ),
      },
      {
        path: 'PurchaseDetailsReport',
        loadChildren: () =>
          import(
            './reportPages/purchase-details-report/purchase-details-report.module'
          ).then((m) => m.PurchaseDetailsReportModule),
      },
      {
        path: 'PurchaseSummaryReport',
        loadChildren: () =>
          import(
            './reportPages/purchase-summary-report/purchase-summary-report.module'
          ).then((m) => m.PurchaseSummaryReportModule),
      },
      {
        path: 'HsnCodePurchaseReport',
        loadChildren: () =>
          import(
            './reportPages/hsn-code-purchase-report/hsn-code-purchase-report.module'
          ).then((m) => m.HsnCodePurchaseReportModule),
      },
      {
        path: 'BranchCreation',
        loadChildren: () =>
          import('./masters/branch-creation/branch-creation.module').then(
            (m) => m.BranchCreationModule
          ),
      },
    ],
  },
  {
    path: 'homepageLakshmiBakery',
    canActivate: [AccessloginGuard],
    loadChildren: () =>
      import(
        './homepage/homepage-lakshmi-bakery/homepage-lakshmi-bakery.module'
      ).then((m) => m.HomepageLakshmiBakeryModule),
  },
  {
    path: 'salesBillDetails',
    loadChildren: () =>
      import('./reportPages/sales-bill-details/sales-bill-details.module').then(
        (m) => m.SalesBillDetailsModule
      ),
  },
  {
    path: 'hsnCodeWiseSalesReport',
    loadChildren: () =>
      import(
        './reportPages/hsn-code-wise-sales-report/hsn-code-wise-sales-report.module'
      ).then((m) => m.HsnCodeWiseSalesReportModule),
  },
  {
    path: 'DailySalesReport',
    loadChildren: () =>
      import('./reportPages/daily-sales-report/daily-sales-report.module').then(
        (m) => m.DailySalesReportModule
      ),
  },
  {
    path: 'TrailBalanceReport',
    loadChildren: () =>
      import(
        './reportPages/trail-balance-report/trail-balance-report.module'
      ).then((m) => m.TrailBalanceReportModule),
  },
  {
    path: 'StatementsOfaccount',
    loadChildren: () =>
      import(
        './reportPages/statements-ofaccount/statements-ofaccount.module'
      ).then((m) => m.StatementsOfaccountModule),
  },
  {
    path: 'DayEndSalesSummary',
    loadChildren: () =>
      import(
        './reportPages/day-end-sales-summary/day-end-sales-summary.module'
      ).then((m) => m.DayEndSalesSummaryModule),
  },
  {
    path: 'StockTransferOutReport',
    loadChildren: () =>
      import(
        './reportPages/stock-transfer-out-report/stock-transfer-out-report.module'
      ).then((m) => m.StockTransferOutReportModule),
  },
  {
    path: 'StockTransferOutDetailsReport',
    loadChildren: () =>
      import(
        './reportPages/stock-transfer-out-detail-report/stock-transfer-out-detail-report.module'
      ).then((m) => m.StockTransferOutDetailReportModule),
  },
  {
    path: 'CommandPage',
    loadChildren: () =>
      import('./masters/command-page/command-page.module').then(
        (m) => m.CommandPageModule
      ),
  },
  {
    path: 'StockReport',
    loadChildren: () =>
      import('./reportPages/stock-report/stock-report.module').then(
        (m) => m.StockReportModule
      ),
  },
  {
    path: 'PurchaseDetailsReport',
    loadChildren: () =>
      import(
        './reportPages/purchase-details-report/purchase-details-report.module'
      ).then((m) => m.PurchaseDetailsReportModule),
  },
  {
    path: 'PurchaseSummaryReport',
    loadChildren: () =>
      import(
        './reportPages/purchase-summary-report/purchase-summary-report.module'
      ).then((m) => m.PurchaseSummaryReportModule),
  },
  {
    path: 'HsnCodePurchaseReport',
    loadChildren: () =>
      import(
        './reportPages/hsn-code-purchase-report/hsn-code-purchase-report.module'
      ).then((m) => m.HsnCodePurchaseReportModule),
  },
  {
    path: 'BranchCreation',
    loadChildren: () =>
      import('./masters/branch-creation/branch-creation.module').then(
        (m) => m.BranchCreationModule
      ),
  },
  {
    path: 'maplesystemAdmin',
    loadChildren: () =>
      import('./homepage/maplesystem-admin/maplesystem-admin.module').then(
        (m) => m.MaplesystemAdminModule
      ),
  },
  {
    path: 'mapleErpDeployment',
    loadChildren: () =>
      import('./maple-erp-deployment/maple-erp-deployment.module').then(
        (m) => m.MapleErpDeploymentModule
      ),
  },
  {
    path: 'AddDialogDeployment',
    loadChildren: () =>
      import(
        './dialogWindows/add-dialog-deployment/add-dialog-deployment.module'
      ).then((m) => m.AddDialogDeploymentModule),
  },
  {
    path: 'SearchPopUp',
    loadChildren: () =>
      import('./Pop-Up-Windows/search-pop-up/search-pop-up.module').then(
        (m) => m.SearchPopUpModule
      ),
  },
  {
    path: 'DailySalesReportTable',
    loadChildren: () =>
      import(
        './reportPages/daily-sales-report-table/daily-sales-report-table.module'
      ).then((m) => m.DailySalesReportTableModule),
  },

  {
    path: 'CustomerDetailsPopUp',
    loadChildren: () =>
      import(
        './Pop-Up-Windows/customer-details-pop-up/customer-details-pop-up.module'
      ).then((m) => m.CustomerDetailsPopUpModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
