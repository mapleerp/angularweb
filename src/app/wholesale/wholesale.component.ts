import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment.prod';
import { QtyEditWindowComponent } from '../dialogWindows/qty-edit-window/qty-edit-window.component';
import { CustomerMst } from '../modelclass/customer-mst';
import { SalesBillDetails } from '../modelclass/sales-bill-details';
import { SalesDtl } from '../modelclass/sales-dtl';
import { SalesTransHdr } from '../modelclass/sales-trans-hdr';
import { SalesTypeMst } from '../modelclass/sales-type-mst';
import { ShippingAddress } from '../modelclass/shipping-address';
import { ItemStockPopUpComponent } from '../Pop-Up-Windows/item-stock-pop-up/item-stock-pop-up.component';
import { datas } from '../Pop-Up-Windows/search-pop-up/search-pop-up.component';
import { ShippingAdressPopUpComponent } from '../Pop-Up-Windows/shipping-adress-pop-up/shipping-adress-pop-up.component';
import { CustomerService } from '../services/customer.service';
import { SaleOrderPopupService } from '../services/sale-order-popup.service';
import { SalesDtlsDeletionService } from '../services/sales-dtls-deletion.service';
import { SalesDtlsService } from '../services/sales-dtls.service';
import { SharedserviceService } from '../services/sharedservice.service';
import { Summary } from '../modelclass/summary';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-wholesale',
  templateUrl: './wholesale.component.html',
  styleUrls: ['./wholesale.component.css'],
})
export class WholesaleComponent implements OnInit {
  constructor(
    private sharedserviceService: SharedserviceService,
    private customerService: CustomerService,
    private salesDtlsService: SalesDtlsService,
    public salesDtlsDeletionService: SalesDtlsDeletionService,
    private saleOrderPopupService: SaleOrderPopupService,
    public shippingDtlPopUp: MatDialog,
    public editQtyAdd: MatDialog,
    public addDialog1: MatDialog,
    private router: Router, public datepipe: DatePipe,
  ) {}
  salestypeMst: SalesTypeMst[] = [];
  receivecust: any;
  selectcustName: any;
  ShippingAdress: ShippingAddress = new ShippingAddress();
  isCustomerSelected = false;
  public addingToTable: datas[] = [];
  recivdata: datas = new datas();
  totalAmount: number = 0.0;
  editRowqty: any;
  name: any;
  wholesalesDtlAddTables: SalesDtl[] = [];
  salesDtl: SalesDtl = new SalesDtl();
  SalesBillDetails: SalesBillDetails = new SalesBillDetails();
  dateCov: any;
  salesType = '';
  selectSalesMan = '';
  holdedPos: SalesTransHdr[] = [];
  salesTransHdr: SalesTransHdr = new SalesTransHdr();
  getSalesBillDtl: SalesDtl[] = [];
  summary: Summary = new Summary();

  ngOnInit(): void {
    this.sharedserviceService.salesDtlfromShardService().subscribe((data) => {
      this.wholesalesDtlAddTables = data;

      this.totalAmount = 0.0;
      for (let i = 0; i <= this.wholesalesDtlAddTables.length - 1; i++) {
        this.totalAmount =
          this.wholesalesDtlAddTables[i].amount + this.totalAmount;
      }
    });

    this.getAllSalesType();
    this.receiveCustomerDtls();
    this.receiveShippingDtl();
    // this.AddingToTables();
    // this.totalPrice();
  }
  getAllSalesType() {
    this.sharedserviceService.getSalesTypes().subscribe((data) => {
      this.salestypeMst = data;
      console.log(this.salestypeMst);
    });
  }
  //-------here we are keypress event for select customer--------------//
  onKeypressEventForSelectCustmer() {
    this.customerService.selectCustomer();
  }

  receiveCustomerDtls() {
    console.log('receiveCustomerDtls');
    this.saleOrderPopupService.getCustomerDtls().subscribe((data: any) => {
      this.receivecust = data;
console.log("in receiveCustomerDtls function")
console.log(data)
      this.selectcustName = this.receivecust.accountName;
      console.log(this.selectcustName + 'customer name');
    });
  }

  onKeypressEventForShippingDtl() {
    console.log('Shipping address caleed');

    const dialogConfigShippingDtl = new MatDialogConfig();
    dialogConfigShippingDtl.disableClose = false;
    dialogConfigShippingDtl.autoFocus = true;

    dialogConfigShippingDtl.width = '50%';
    dialogConfigShippingDtl.height = '80%';

    this.shippingDtlPopUp.open(
      ShippingAdressPopUpComponent,
      dialogConfigShippingDtl
    );
  }
  receiveShippingDtl() {
    console.log('receiveShippingDtl from services');
    this.saleOrderPopupService.getShippingDtls().subscribe((data: any) => {
      this.ShippingAdress = data;
      console.log(this.ShippingAdress + 'receiveShippingDtl');
    });
  }

  // AddingToTables() {
  //   this.addingToTable.push(this.recivdata);
  //   this.removeNull();
  //   console.log('data added to tables');
  // }

  // removeNull() {
  //   this.addingToTable = this.addingToTable.filter(Boolean);
  // }

  // deleteTabledata(i: any) {
  //   delete this.addingToTable[i];
  //   this.removeNull();
  //   this.totalPrice();
  // }

  totalPrice() {
    this.totalAmount = 0.0;

    console.log('TotalPrice function called');
    let num: number;
    for (let data of this.addingToTable) {
      num = +data.Amount;
      this.totalAmount = num + this.totalAmount;
    }
  }

  // editQty(i: any) {
  //   this.editRowqty = i;
  //   const dialogConfigAdd = new MatDialogConfig();
  //   dialogConfigAdd.disableClose = false;
  //   dialogConfigAdd.autoFocus = true;
  //   dialogConfigAdd.width = '30%';
  //   dialogConfigAdd.height = '30%';
  //   dialogConfigAdd.data = {
  //     callback: this.callBack.bind(this),
  //     defaultValue: this.name,
  //   };
  //   this.editQtyAdd.open(QtyEditWindowComponent, dialogConfigAdd);
  // }

  callBack(name: any) {
    this.name = name;
    this.addingToTable[this.editRowqty].qty = this.name;
    this.addingToTable[this.editRowqty].Amount =
      this.addingToTable[this.editRowqty].standardPrice *
      this.addingToTable[this.editRowqty].qty;
    this.totalPrice();
  }

  onKeypressEvent(x: any) {
    this.wholeSalesDetails();
    if (this.selectcustName == null) {
      alert('Please select the customer first!!!');
      return;
    }
    if (this.selectSalesMan == '') {
      alert('Please enter the sales man name !!!');
      return;
    }
    if (this.salesType.length == 0) {
      alert('Please select the sales type !!!');
      return;
    }
    const dialogConfigAdd1 = new MatDialogConfig();
    dialogConfigAdd1.disableClose = false;
    dialogConfigAdd1.autoFocus = true;
    //dialogConfigAdd1.position= { right: `10px`, top: `20px` }
    dialogConfigAdd1.width = '80%';
    dialogConfigAdd1.height = '70%';
    //this.addDialog1.open(SearchPopUpComponent,{ disableClose: true });
    dialogConfigAdd1.data = {
      callback: this.callBack.bind(this),
      defaultValue: this.salesDtl,
    };
    this.addDialog1.open(ItemStockPopUpComponent, dialogConfigAdd1);
    console.log('popupcalled');
  }

  ngOnDestroy(): void {
    console.log('page destroyed from WHSALE');
    this.addingToTable = [];
    console.log(this.addingToTable);
    this.recivdata = new datas();
    this.totalAmount = 0.0;
    this.wholesalesDtlAddTables.length = 0;
    this.sharedserviceService.PostSalesDtlToPosWindow(
      this.wholesalesDtlAddTables
    );
    this.receivecust = new CustomerMst();
    this.saleOrderPopupService.addCustomerDtl(this.receivecust);
    this.ShippingAdress = new ShippingAddress();
    this.saleOrderPopupService.addShippingDtls(this.ShippingAdress);
    this.selectSalesMan = '';
    this.salesType = '';
    this.holdedPos.length = 0;
  }

  finalSaveWholesale() {
    this.dateCov = new Date();
    this.dateCov= this.datepipe.transform(this.dateCov , 'yyyy-MM-dd');
    this.sharedserviceService
      .updateSalesTransHdr(
        this.wholesalesDtlAddTables[0].salesTransHdr,
        this.dateCov
      )
      .subscribe((data) => {
        this.SalesBillDetails = data;
        this.salesDtlsService.WholesaleBillReport(
          this.SalesBillDetails,
          this.ShippingAdress
        );
        this.router.navigate(['WholeSalesBill']);

        // this.posFinalBillService.setPosFinalBill(this.SalesBillDetails);
        // this.router.navigate(['PosFinalBIll']);
      });
  }

  wholeSalesDetails() {
    this.salesDtlsService.wholeSalesDetails(
      this.salesType,
      this.selectcustName,
      this.selectSalesMan,
      this.receivecust.id
    );
  }

  holdedSalesDtls() {
    this.sharedserviceService
      .getHoldedPossDtls(environment.myBranch)
      .subscribe((data) => {
        this.holdedPos = data;
        if (this.holdedPos.length == 0) {
          alert('No Holded Bills');
          return;
        }
        for (let i = 0; i <= this.holdedPos.length - 1; i++) {
          this.holdedPos[i].id = data[i][2];
          this.holdedPos[i].userId = data[i][0];
          this.holdedPos[i].invoiceAmount = data[i][1];
        }
        console.log(this.holdedPos);
      });
  }

  itemDeletionForSales(salesdtl: SalesDtl) {
    this.salesDtlsDeletionService.salesDetailsDeletion(salesdtl);
    this.sharedserviceService
      .getSalesDtl(this.wholesalesDtlAddTables[0].salesTransHdr)
      .subscribe((data) => {
        this.wholesalesDtlAddTables = data;
        this.sharedserviceService.PostSalesDtlToPosWindow(
          this.wholesalesDtlAddTables
        );
      });
  }

  unholdbillOfSales(saleshdr: SalesTransHdr) {
    console.log('unholdbill');
    this.salesTransHdr = saleshdr;
    this.unHoldOfSales();
  }
  unHoldOfSales() {
    console.log('unHold');
    if (this.salesTransHdr != null) {
      if (this.salesTransHdr.id != null) {
        this.sharedserviceService
          .getSalesTransHdr(this.salesTransHdr.id)
          .subscribe((data) => {
            this.salesTransHdr = data;
            this.sharedserviceService
              .getSalesDtl(this.salesTransHdr)
              .subscribe((data) => {
                this.wholesalesDtlAddTables = data;
              });
          });
        this.sharedserviceService
          .getSalesBillDtl(this.salesTransHdr)
          .subscribe((getSalesBillDt) => {
            this.getSalesBillDtl = getSalesBillDt;
            this.sharedserviceService.PostSalesDtlToPosWindow(
              this.getSalesBillDtl
            );

            this.sharedserviceService
              .getSalesWindowsSummary(this.salesTransHdr.id)
              .subscribe((data) => {
                this.summary = data;
                this.salesTransHdr.invoiceAmount = this.summary.totalAmount;
                this.sharedserviceService.postSalesSummaryToService(
                  this.summary
                );
              });
          });
      }
    }
  }
}
