import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PosWindowComponent } from './pos-window.component';

describe('PosWindowComponent', () => {
  let component: PosWindowComponent;
  let fixture: ComponentFixture<PosWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PosWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PosWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
