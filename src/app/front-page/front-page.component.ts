import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment.prod';
import { UserMst } from '../modelclass/user-mst';
import { SharedserviceService } from '../services/sharedservice.service';

@Component({
  selector: 'app-front-page',
  templateUrl: './front-page.component.html',
  styleUrls: ['./front-page.component.css']
})
export class FrontPageComponent implements OnInit {
  userMst: UserMst = new UserMst;

  username: any;
  passwords: any;
  companyname = environment.company;
  public loginStatus: boolean = false;

  userObject:UserMst=new UserMst;
  constructor(
    private router: Router,
    private sharedserviceService: SharedserviceService
  ) {}

  ngOnInit(): void {}

  update() {
    this.sharedserviceService.updateLoginStatus(
      this.loginStatus,
      this.companyname,
      this.username,
      this.userMst
    );
  }

  onSubmit() {
    // if(this.username=='amdc123'&& this.passwords=='amdc123'&&this.companyname=='AMDC')
    //    {
    //   alert("Authenticated..!!")
    //   this.loginStatus=true;
    //   console.log(this.loginStatus)

    //   this.router.navigate(['/home'])
    //   this.update();

    //   }
    //   else if (this.username==environment.username&& this.passwords==environment.password&&this.companyname==environment.company)
    //   {
    //     alert("Authenticated..!!")
    //     this.loginStatus=true;
    //     console.log(this.loginStatus)

    //     this.router.navigate(['/homeHotCakes/DashBoard'])
    //     this.update();
    //   }
    //   else if (this.username=='lakshmi123'&& this.passwords=='lakshmi123'&&this.companyname=='LAKSHMIBAKERS')
    //   {
    //     alert("Authenticated..!!")
    //     this.loginStatus=true;
    //     console.log(this.loginStatus)

    //     this.router.navigate(['/homepageLakshmiBakery']);
    //     this.update();
    //   }

    //   else
    //   {
    //     alert("Incorrect Password or Username")
    //       return;
    //   }
    this.getWebLogin();

    console.log(this.loginStatus);
    this.update();
  }
  getWebLogin() {
    this.sharedserviceService
      .getWebLogin(environment.company, this.passwords, this.username)
      .subscribe((data) => {
        this.userMst = data;

        console.log(data)
        if (this.userMst == null) {
          if (
            this.username == 'admin' &&
            this.passwords == 'admin' &&
            this.companyname == environment.company
          ) {
            this.loginStatus = true;
            console.log('Login Successful');
            this.update();
            this.router.navigate(['/loginpage/DashBoard']);
          } else {
            alert('Incorrect Password or Username');
            return;
          }
        } else {
          this.loginStatus = true;
          this.companyname=this.userMst.companyMst.id;
          console.log(this.companyname+' after login')
          console.log('Login Successful');
          this.update();
          // this.router.navigate(['/homeHotCakes/DashBoard']);

          this.router.navigate(['/loginpage/DashBoard']);
        }
      });
    if (this.username == 'admin' && this.passwords == 'admin') {
      this.loginStatus = true;
      console.log('Login Successful');
      this.update();
      //this.router.navigate(['/mapleSystems'])
      this.router.navigate(['/loginpage/DashBoard']);
    }
  }

  signUp(){
    this.router.navigate(['SignUp']);
  }
}