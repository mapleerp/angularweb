import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { UserMst } from 'src/app/modelclass/user-mst';
import { DashBoardConfigMst } from '../modelclass/dash-board-config-mst';
import { Dashboardreporttype } from '../modelclass/dashboardreporttype';
import { SharedserviceService } from '../services/sharedservice.service';

@Component({
selector: 'app-dashboard-config',
templateUrl: './dashboard-config.component.html',
styleUrls: ['./dashboard-config.component.css']
})
export class DashboardConfigComponent implements OnInit {
userMst:UserMst[]=[]
dashboardreport:Dashboardreporttype[]=[];
dashboardConfigForm:any = FormGroup;
dashBoardConfigMst: DashBoardConfigMst= new DashBoardConfigMst;
showDashBoardConfigMst:DashBoardConfigMst[]=[];
loadDashBoardConfigMst:DashBoardConfigMst[]=[];

constructor(private sharedserviceService:SharedserviceService,public fb:FormBuilder) { }

ngOnInit(): void {
this.sharedserviceService.getUserMst().subscribe(data => {this.userMst=data }) //load all usernames
this.sharedserviceService.showDashBoardDetails().subscribe(data=>{this.loadDashBoardConfigMst=data;})
this.dashboardConfigForm = this.fb.group({
userId: new FormControl(),
dashBoardName: new FormControl(),
reportName: new FormControl(),
})
}
submitForm()
{
//to eliminate duplication of rows
for(var row of this.loadDashBoardConfigMst){
console.log(row.id+' '+row.userId+' '+row.dashBoardName+' '+row.reportName+' for loop');
if((this.dashboardConfigForm.value.userId==row.userId)&&(this.dashboardConfigForm.value.dashBoardName==row.dashBoardName)&&(this.dashboardConfigForm.value.reportName==row.reportName))
{
console.log("Inside if for duplicate entry checking")
alert('Duplicate row selection - Cannot Save!!')
return;
}
}
console.log('reportname='+this.dashboardConfigForm.value.reportName);
//null checking
if(this.dashboardConfigForm.value.userId===null){
alert('Select a user name');
return;
}
if(this.dashboardConfigForm.value.dashBoardName===null){
alert('Select a dashboard name');
return;
}
if(this.dashboardConfigForm.value.reportName===null){
alert('Select a report type');
return;
}
//save to dashboardconfigmst
this.dashBoardConfigMst = Object.assign(this.dashBoardConfigMst, this.dashboardConfigForm.value); 
console.log(this.dashBoardConfigMst)
this.sharedserviceService.saveDashBoardConfiguration(this.dashBoardConfigMst).subscribe( data =>
{
console.log(data);
console.log('DashBoardConfigMst Send')
alert("Dashboard configuration saved!!")
this.clear();
this.showAll();
},
( error: any) =>
{
console.log('Data not send');
alert("Unable to save!!")
this.clear();
}
); 
}

showAll()
{
if(this.dashboardConfigForm.value.userId===null||typeof this.dashboardConfigForm.value.userId==='undefined'){
//show all details when no username selected
console.log('Show all dashboard details');
this.sharedserviceService.showDashBoardDetails().subscribe(data=>
{
this.showDashBoardConfigMst=data;
})
}
else{
//show details of a particular user
console.log(this.dashboardConfigForm.value.userId+'show details of a particular user');
this.sharedserviceService.showDashBoardDetailsByUserId(this.dashboardConfigForm.value.userId).subscribe(data=>
{
this.showDashBoardConfigMst=data;
})
}
}
clear(){
this.dashboardConfigForm.reset();
console.log('form cleared')
}
// refresh()
// {
// //reload window
// window.location.reload();
// }
getReportName()
{
//display report names of a dashboard after dashboard name selection ---> cascading or dependancy in dropdownlist
this.sharedserviceService.getReportNameByDashBoardName(this.dashboardConfigForm.value.dashBoardName).subscribe(data => {this.dashboardreport=data, console.log(data) })
}
getUsernameById(user:any)
{
//to display userName(from UserMst) instead of userId(from DashBoardConfigMst) in table 
const userNameById = this.userMst.find((data) => data.id === user)
if (!userNameById) {
// when the user not found
return ''
}
return userNameById.userName
}
highlightandDeleteRowById(dts: DashBoardConfigMst) {
//to display selected row in form controls
this.dashboardConfigForm = this.fb.group({
id: dts.id, 
});


this.sharedserviceService.deleteDadhboardDetailsById(dts.id).subscribe((data) => {
alert('Deleted!!!');
console.log('Data deleted');
this.showAll();
},
( error: any) =>
{
console.log('Data not send');
alert("Unable to delete!!")
}
);
}
deleteById(){
//delete selected dashboard details by id
this.sharedserviceService.deleteDadhboardDetailsById(this.dashboardConfigForm.value.id).subscribe((data) => {
alert('Deleted!!!');
console.log('Data deleted');
this.clear();
this.showAll();
},
( error: any) =>
{
console.log('Data not deleted');
alert("Unable to delete!!")
this.clear();
}
);
}
}
