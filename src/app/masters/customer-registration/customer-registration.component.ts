import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { CustomerMst } from 'src/app/modelclass/customer-mst';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-customer-registration',
  templateUrl: './customer-registration.component.html',
  styleUrls: ['./customer-registration.component.css'],
})
export class CustomerRegistrationComponent implements OnInit {
  constructor(
    public fb: FormBuilder,
    private sharedserviceService: SharedserviceService
  ) {}
  customerRegistrationForm: any = FormGroup;

  newCustReg: CustomerMst = new CustomerMst();
  companyid: string = '';
  CustRegDetails: CustomerMst[] = [];
  branchName: string = environment.myBranch;
  nameWithMob: boolean = false;
  search: any;
  deletedById: any;

  ngOnInit(): void {
    this.customerRegistrationForm = this.fb.group({
      customerName: new FormControl(),
      customerAddress: new FormControl(),
      customerMail: new FormControl(),
      customerContact: new FormControl(),
      customerGst: new FormControl(),
      creditPeriod: new FormControl(),
      customerDiscount: new FormControl(),
      customerState: new FormControl(),
      priceTypeId: new FormControl(),
      customerGroup: new FormControl(),
      discountProperty: new FormControl(),
    });

    this.getAllCustomerDetails();
  }

  submitForm() {
    if (this.customerRegistrationForm.value.customerName == null) {
      alert('Enter the Customer name!!!');
      return;
    }
    Object.assign(this.newCustReg, this.customerRegistrationForm.value);
    console.log(this.newCustReg);
    this.nameWithMob = this.newCustReg.customerName.includes(
      this.newCustReg.customerContact
    );

    if (!this.nameWithMob) {
      alert('Please add customer 1st phone number to the end of customer name');
      return;
    }

    if (this.customerRegistrationForm.value.customerAddress == null) {
      alert('Please generate Address!!!');
      return;
    }
    if (this.customerRegistrationForm.value.customerMail == null) {
      alert('Enter the Email!!!');
      return;
    }
    if (this.customerRegistrationForm.value.customerContact == null) {
      alert('Enter the Contact!!!');
      return;
    }
    if (this.customerRegistrationForm.value.customerGst == null) {
      alert('Enter the Gst!!!');
      return;
    }
    if (this.customerRegistrationForm.value.creditPeriod == null) {
      alert('Enter the creditPeriod!!!');
      return;
    }
    if (this.customerRegistrationForm.value.customerDiscount == null) {
      alert('Enter the Discount!!!');
      return;
    }
    if (this.customerRegistrationForm.value.customerState == null) {
      alert('select the State!!!');
      return;
    }
    if (this.customerRegistrationForm.value.priceTypeId == null) {
      alert('select the priceType!!!');
      return;
    }
    if (this.customerRegistrationForm.value.customerGroup == null) {
      alert('select the Group!!!');
      return;
    }
    if (this.customerRegistrationForm.value.discountProperty == null) {
      alert('select the discountProperty!!!');
      return;
    }

    this.sharedserviceService
      .showItemCodeBarCodeItemIdForm(this.branchName)
      .subscribe((data: any) => {
        console.log(data + 'Customer ID ');
        this.newCustReg.id = data + 'CU';
        if (typeof this.newCustReg.id != 'undefined') {
          this.companyid = environment.company;
          this.sharedserviceService
            .custRegToServer(this.newCustReg, this.companyid)
            .subscribe((data) => {
              this.newCustReg = data;
              alert('Customer Created');
            });
        } else {
          return;
        }
      });

    // this.companyid="COMPANY1";

    // this.sharedserviceService.custRegToServer(this.newCustReg,this.companyid).subscribe(data =>{this.newCustReg=data})

    this.getAllCustomerDetails();
  }

  getAllCustomerDetails() {
    this.sharedserviceService
      .getAllCustomerDetailsfromserver()
      .subscribe((data) => {
        // this.CustRegDetails = data;
      });
  }

  highlightRow(dts: CustomerMst) {
    this.customerRegistrationForm = this.fb.group({
      customerName: dts.customerName,
      customerAddress: dts.customerAddress,
      customerMail: dts.customerMail,
      customerContact: dts.customerContact,
      customerGst: dts.customerGst,
      creditPeriod: dts.creditPeriod,
      customerDiscount: dts.customerDiscount,
      customerState: dts.customerState,
      priceTypeId: dts.priceTypeId,
      customerGroup: dts.customerGroup,
      discountProperty: dts.discountProperty,
    });

    this.deletedById = dts.id;
  }

  showAll() {
    this.sharedserviceService
      .getAllCustomerDetailsfromserver()
      .subscribe((data) => {
        //  this.CustRegDetails=data
      });
  }
  clear() {}
}
