import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms'
import { CurrencyMst } from 'src/app/modelclass/currency-mst';
import { Supplier } from 'src/app/modelclass/supplier';
import { AccountHeads } from 'src/app/modelclass/account-heads';

@Component({
  selector: 'app-addsupplier',
  templateUrl: './addsupplier.component.html',
  styleUrls: ['./addsupplier.component.css']
})
export class AddsupplierComponent implements OnInit {
  country:any;
  states:any;
  search: any;
  supId:any;
  mob:string="";
  regexp:any;
  searchfind:boolean=false;
  supplierEdit:Supplier=new Supplier;
  branchCode:any=environment.myBranch;
  currencyMst:CurrencyMst[]=[];
  showSupplier:Supplier[]=[];
  accHead:AccountHeads[]=[];
  supplier:Supplier=new Supplier;
  supplierCreationForm:any=FormGroup;
  isEditBtnDisabled: boolean= true;
  isSaveBtnDisabled:boolean=false;
  constructor(private sharedserviceService:SharedserviceService,public fb:FormBuilder) { }

  ngOnInit(): void {
    this.sharedserviceService.getCountries().subscribe((data:any) => {this.country=data })
    this.sharedserviceService.getCurrencyMst().subscribe((data:any) => {this.currencyMst=data })
    this.showAllSupplier();
    this.supplierCreationForm = this.fb.group({
      supplierName: new FormControl(),
      company: new FormControl(),
      address: new FormControl(),
      cerditPeriod: new FormControl(),
      phoneNo: new FormControl(),
      emailid: new FormControl(),
      supGST: new FormControl(),
      country: new FormControl(),
      state: new FormControl(),
      currencyId: new FormControl(),
      })
  }
  getstate()
  {
    this.sharedserviceService.getStateByCountry(this.supplierCreationForm.value.country).subscribe((data:any) => {this.states=data, console.log(data) })
  }
  saveSupplier(){
    if(this.supplierCreationForm.value.supplierName==null){
      alert("Enter supplier name!");
      return;
    }
    if(this.supplierCreationForm.value.company==null){
      alert("Enter company name!");
      return;
    }
    if(this.supplierCreationForm.value.address==null){
      alert("Enter company address!");
      return;
    }
    if(this.supplierCreationForm.value.state==null){
      alert("Select country and state!");
      return;
    }
    if(this.supplierCreationForm.value.cerditPeriod==null)
    {
      this.supplierCreationForm.value.cerditPeriod=0;
    }
    //phone number validation
    this.mob=String(this.supplierCreationForm.value.phoneNo);
    if (this.mob!=undefined||this.mob!=null)
      {
      const arrayMob = Array.from(this.mob);
      if((arrayMob.length==10)||arrayMob.length==11){      }
      else{
        alert("Enter a 10 or 11 digit number");
        return;
      }
    }
  // email validation
    this.regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    this.searchfind = this.regexp.test(this.supplierCreationForm.value.emailid);
    console.log(this.searchfind);
    if(this.searchfind==false){
      alert("Enter a valid Email ID!")
      return;
    }
    // this.getAccountHeadByName(this.supplierCreationForm.value.supplierName);
    //Save function
    this.sharedserviceService.generateVoucherNumber(environment.myBranch+"SU").subscribe((data:any)  =>
    {  
        this.supplier=Object.assign(this.supplier,this.supplierCreationForm.value);
        this.supplier.id=data;
        this.supplier.id=this.supplier.id+environment.company;
        this.supplier.branchCode=environment.myBranch;
        this.sharedserviceService.saveSupplierCreationForm(this.supplier).subscribe( (data:any) =>
        {
          console.log(data);
          console.log('Data Send')
          alert("Supplier saved!!")
          this.showAllSupplier();
          this.clear();  
        },
        (       error: any) =>{console.log(error)
          alert("Supplier cannot be saved!")
          this.clear();}
       ); 
    },
    (       error: any) =>{console.log(error)
      alert("Cannot be saved!")});
  }
  editSupplier(){
    //edit function
    this.supplier=Object.assign(this.supplierEdit,this.supplierCreationForm.value);
     this.sharedserviceService.updateSupplier(this.supplierEdit,this.supId).subscribe(data=>
      {
      console.log(data);
      alert("Supplier Details Edited successfully");
      this.isEditBtnDisabled=true;
      this.isSaveBtnDisabled=false;
     },
     (error: any) =>
     {
      console.log(error)
      alert("Supplier cannot be saved!")
      this.clear();
    }
    );
 
  }
  showAllSupplier(){
    //show all function
    this.sharedserviceService.showSupplierDetails().subscribe((data: any)=>
    {
      this.showSupplier=data;
    })
  }
  clear(){
    this.supplierCreationForm.reset();
    this.isEditBtnDisabled=true;
    this.isSaveBtnDisabled=false;
  }
  // getAccountHeadByName(supName:any){
  //   this.sharedserviceService.getAccountHead(supName).subscribe((data: any)=>
  //   {
  //     this.accHead=data;
  //     if(this.accHead!=null){
  //       alert('Supplier already registered!');
  //       return;
  //     }
  //   })
  // }
  highlightRow(dts: Supplier) {
    this.supplierCreationForm = this.fb.group({
      id:dts.id,
      supplierName: dts.supplierName,
      company: dts.company,
      address: dts.address,
      cerditPeriod: dts.cerditPeriod,
      phoneNo: dts.phoneNo,
      emailid: dts.emailid,
      supGST: dts.supGST,
      country: dts.country,
      state: dts.state,
      currencyId: dts.currencyId,
      });    
      this.isEditBtnDisabled=false;
      this.isSaveBtnDisabled=true;
      this.supId=dts.id;
      this.supplierEdit=dts;
      // console.log(this.supplierEdit)
  }
}
