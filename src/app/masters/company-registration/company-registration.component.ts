import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CompanyMst } from 'src/app/modelclass/company-mst';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-company-registration',
  templateUrl: './company-registration.component.html',
  styleUrls: ['./company-registration.component.css']
})
export class CompanyRegistrationComponent implements OnInit {

  constructor(private service : SharedserviceService,public fb:FormBuilder) { }
  countryString:any
  countryName:any
  companyRegistrationForm:any = FormGroup;
  companyMst: CompanyMst= new CompanyMst;
  testComapmyMSt:CompanyMst[]=[];
  companyMst1:any
  ngOnInit(): void {
    this.companyRegistrationForm = this.fb.group({
      companyName: new FormControl(),
      companyGst: new FormControl(),
      country: new FormControl(),
      currencyName: new FormControl(),
      state: new FormControl(),
    
    })
  }
  submitForm()
  {
    console.log(this.companyRegistrationForm.value)
   

    this.companyMst = Object.assign(this.companyMst, this.companyRegistrationForm.value);
       console.log(this.companyMst);
       //  this.service.saveCompanyCreationCreation().subscribe(data=>{this.testComapmyMSt=data
       // ;console.log(this.testComapmyMSt+"teestt");});
       
        this.service.saveCompanyCreationCreation(this.companyMst).subscribe(data=>{this.companyMst1=data;
          if(this.companyMst1 != null)
          {
   
             this.companyRegistrationForm.reset();
             alert("Company saved successfully...");
   
          }})
 
       
  }
}
