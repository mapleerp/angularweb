import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms'
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { UserMst } from 'src/app/modelclass/user-mst';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { environment } from 'src/environments/environment.prod';


@Component({
  selector: 'app-userregistration',
  templateUrl: './userregistration.component.html',
  styleUrls: ['./userregistration.component.css']
})
export class UserregistrationComponent implements OnInit {
  // userRegistrationForm:any = FormGroup;
  userMst:UserMst=new UserMst
  getUserMst:UserMst[]=[];
  branchMst:BranchMst[]=[];
  branchName:string=environment.myBranch;
  fullName:any='';
  userName: any='';
  search:any='';
  showMsg=false;
  password: any='';
  confirmPassword: any='';
  branchCode: any='';
  searchfind: boolean=true;
  passwordMatchCheck:boolean=false;
  notSaveMsg=false
  regexp:any;
  constructor(private sharedserviceService:SharedserviceService,public fb:FormBuilder) { }

  ngOnInit(): void {
    this.sharedserviceService.getBranchNames(). subscribe(data => {this.branchMst=data})

    // this.userRegistrationForm = this.fb.group({
    //   userName: new FormControl(),
    //   fullName: new FormControl(),
    //   password: new FormControl(),
    //   confirmPassword: new FormControl(),
    //   branchCode: new FormControl(),
    // })
    this.showAll();
  }
  submitForm()
  {
    if(this.userName==''){
      return;
    }
    if(this.branchCode==''){
      return;
    }
    if(this.password==''||this.confirmPassword==''){
      return;
    }
    this.checkEmail();
    // this.regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    // this.searchfind = this.regexp.test(this.fullName);
    // console.log(this.searchfind);
    // if (this.searchfind == false) {
    // //  alert("Enter a valid email id")
    //   return;
    // }

      // this.userMst = Object.assign(this.userMst, this.userRegistrationForm.value);
   
      this.userMst.userName=this.userName;
      this.userMst.fullName=this.fullName;
      this.userMst.branchCode=this.branchCode;
      if(this.password===(this.confirmPassword))
       {
        this.userMst.password=this.password;   
      this.passwordMatch();
        console.log(this.userMst)
      this.sharedserviceService.saveuserRegistrationForm(this.userMst,this.branchName).subscribe( data =>
        {
          console.log(data);
          console.log('Data Send')
          this.showMsg= true;
          // alert("User created!!")
          this.showAll();
          this.clear();
        },
        (       error: any) =>
            {
              console.log('Data not send');
              // alert("Unable to create user!!")
              this.notSaveMsg=true;
              this.clear();
            }
       );
      }
      // else
      // alert("Password Mismatch")
  }
  showAll()
  {
    this.sharedserviceService.showUserDetails().subscribe(data=>
      {
        this.getUserMst=data;
      })
  }
  clear(){
    // this.userRegistrationForm.reset();
    this.userName='';
    this.fullName='';
    this.branchCode='';
    this.password='';
    this.confirmPassword='';
    this.userMst=new UserMst();
  }

  sortdesc(){
    this.getUserMst= this.getUserMst.sort(function(a,b){
    if(a.userName<b.userName){
    return 1;
    }
    else{
    return -1;
    }
    });
    }
    sortasc(){
    this.getUserMst= this.getUserMst.sort(function(a,b){
    if(a.userName>b.userName){
    return 1;
    }
    else{
    return -1;
    }
    });
    }

    checkEmail(){
      this.regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    this.searchfind = this.regexp.test(this.fullName);
    console.log(this.searchfind);
    if (this.searchfind == false) {
    //  alert("Enter a valid email id")
      return;
    }
    else{
      this.searchfind=true;
    }
    }

    //password matching
passwordMatch(){
  if(this.password===(this.confirmPassword))
  {
   this.userMst.password=this.password;   
   this.passwordMatchCheck=false;
  }
  else{
    this.passwordMatchCheck=true;
    return;
  // alert("Password Mismatch");
  }
}
}
