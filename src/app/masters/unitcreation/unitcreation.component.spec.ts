import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitcreationComponent } from './unitcreation.component';

describe('UnitcreationComponent', () => {
  let component: UnitcreationComponent;
  let fixture: ComponentFixture<UnitcreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnitcreationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitcreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
