import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { SalesManMst } from 'src/app/modelclass/sales-man-mst';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-salesmancreation',
  templateUrl: './salesmancreation.component.html',
  styleUrls: ['./salesmancreation.component.css']
})
export class SalesmancreationComponent implements OnInit {
  id: any;
  salesManName: any='';
  branchCode: any='';
  oldId: any;
  processInstanceId: any;
  taskId: any;
  contactNumber: any='';
  salesmanmst: SalesManMst = new SalesManMst();
  mob: string = "";
  showMsg=false;
  search:any;
  salesManMst: SalesManMst[] = [];
  branchList: BranchMst[] = [];
  salesManDtl: any;
  phoneCheck=false;

  constructor(private service: SharedserviceService,private router: Router) { }

  ngOnInit(): void {
    this.service.getBranchNames().subscribe((data: any) => {
      this.branchList = data
      console.log(this.branchList);
    });
    this.getSalesMan();
    
  }
  savesalesman() {
    // if(this.salesManName==null){
    //   alert('Enter a name');
    //   return;
    // }

    // if(this.branchCode==null){
    //   alert('Select a branch');
    //   return;
    // }
  this.phoneNumberChecking();
    this.salesmanmst.salesManName = this.salesManName;
    this.salesmanmst.branchCode = this.branchCode;
    this.salesmanmst.contactNumber = this.contactNumber;

    this.service.savesalesman(this.salesmanmst).subscribe((data: any) => {
      console.log(data);
      console.log('Data send')
      this.showMsg=true;
      
      // alert("Salesman saved!!")
      this.clearSalesman();
      this.getSalesMan();
  
    });
 
  }

  clearSalesman() {
    this.salesManName = "";
    this.branchCode = "";
    this.contactNumber="";
  }

  getSalesMan() {
    this.service.getSalesMan().subscribe((data: any) => {
      console.log(data)
      this.salesManMst = data;
    });

  }
  deleteSalesman(salesmanid: any) {
    this.service.deleteSalesman(salesmanid).subscribe((data: any) => {
      console.log(this.id)
      this.salesManDtl = data;
      this.getSalesMan();
    });

  }

  sortdesc(){
    this.salesManMst= this.salesManMst.sort(function(a,b){
    if(a.salesManName<b.salesManName){
    return 1;
    }
    else{
    return -1;
    }
    });
    }
    sortasc(){
    this.salesManMst= this.salesManMst.sort(function(a,b){
    if(a.salesManName>b.salesManName){
    return 1;
    }
    else{
    return -1;
    }
    });
    }
  
phoneNumberChecking(){

  this.mob = String(this.contactNumber);
  if (this.mob != undefined || this.mob != null) {
    const arrayMob = Array.from(this.mob);
    if ((arrayMob.length == 10)) { 
      this.phoneCheck=false;
    }
    else {
      this.phoneCheck=true;
      // alert("Enter 10 digit number")
      return;
    }
  }
}

dashBoardClick(){
  this.router.navigate(['/loginpage/DashBoard']);
}

posClick(){
  this.router.navigate(['/loginpage/PosWindow']);
}

partyCreateClick(){
  this.router.navigate(['/loginpage/PartyCreation']);
}

purchaseClick(){
  this.router.navigate(['/loginpage/Purchase']);
}

itemClick(){
  this.router.navigate(['/loginpage/ItemCreation']);
}
}
