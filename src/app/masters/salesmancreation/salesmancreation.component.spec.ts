import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesmancreationComponent } from './salesmancreation.component';

describe('SalesmancreationComponent', () => {
  let component: SalesmancreationComponent;
  let fixture: ComponentFixture<SalesmancreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesmancreationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesmancreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
