import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesReceiptwindowComponent } from './sales-receiptwindow.component';

describe('SalesReceiptwindowComponent', () => {
  let component: SalesReceiptwindowComponent;
  let fixture: ComponentFixture<SalesReceiptwindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesReceiptwindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesReceiptwindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
