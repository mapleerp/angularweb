import { DatePipe } from '@angular/common';
import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { SharedserviceService } from 'src/app/services/sharedservice.service';


@Component({
  selector: 'app-command-page',
  templateUrl: './command-page.component.html',
  styleUrls: ['./command-page.component.css']
})
export class CommandPageComponent implements OnInit,OnDestroy {

  branchname:any;
  command:any;
  branchMst:BranchMst[]=[];
  sub: Subscription = new Subscription;
  constructor(private sharedserviceService:SharedserviceService,
    public datepipe: DatePipe) { }

  ngOnInit(): void {
    this.sub= this.sharedserviceService.getBranchNames().subscribe(data => {this.branchMst=data})
   

  }

  onSubmit()
  {
    if(this.branchname=='ALL BRANCH')

           {
           for (let branch of this.branchMst )
           {
             this.sharedserviceService.getCommandForAllPage(branch.branchCode,this.command)
           }
           }

     else
           {

            this.sharedserviceService.getCommandByBranchPage(this.branchname,this.command)
           }      
   

  }
  getdetails()
  {

  }
  publishmenu()
  {
    
  }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
    console.log('service unsubscribed..!!')
    }

}
