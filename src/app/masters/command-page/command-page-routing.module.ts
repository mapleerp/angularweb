import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommandPageComponent } from './command-page.component';

const routes: Routes = [{ path: '', component: CommandPageComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommandPageRoutingModule { }
