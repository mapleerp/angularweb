import { Component, OnInit } from '@angular/core';
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-branchcreationwindow',
  templateUrl: './branchcreationwindow.component.html',
  styleUrls: ['./branchcreationwindow.component.css']
})
export class BranchcreationwindowComponent implements OnInit {

   // branchMst:BranchMst[]=[];
branchMst:BranchMst=new BranchMst;
myBranchs:any=['Y','N'];
states:any=['Kerala','TamiNadu','Karnataka'];

mob: string = "";
regexp: any;
branchTelNo:any='';
searchfind: boolean = true;
branchEmail:any='';
branchName:any='';
branchCode:any='';

branchGst: any='';
branchWebsite: any='';
branchAddress1: any='';
branchAddress2: any='';
accountNumber: any='';
bankName: any='';
bankBranch: any='';
ifsc: any='';
myBranch: any='';
branchstate: any='';

showMsg=false;
phoneNumberCheck=false;

  constructor(private sharedserviceService:SharedserviceService) { }

  ngOnInit(): void {
  }

  submitForm()
  {
    if(this.branchName==''){
      // alert("Enter Branch Name");
       return;
    }

    if(this.branchCode==''){
      // alert("Enter Branch Code");
      return;
    }
    this.phoneNumberChecking();
    this.emailChecking();


    this.sharedserviceService.generateVoucherNumber(environment.company+"BR").subscribe((data:any)  =>
    {
     
      // this.branchMst = Object.assign(this.branchMst, this.branchCreationForm.value);
      this.branchMst.id=data+environment.company;
      console.log(this.branchMst);
      this.branchMst.branchName=this.branchName;
      this.branchMst.branchCode=this.branchCode;
      this.branchMst.branchGst=this.branchGst;
      this.branchMst.branchEmail=this.branchEmail;
      this.branchMst.branchTelNo=this.branchTelNo;
      this.branchMst.branchWebsite=this.branchWebsite;
      this.branchMst.branchState=this.branchstate;
      this.branchMst.branchAddress1=this.branchAddress1;
      this.branchMst.branchAddress2=this.branchAddress2;
      this.branchMst.accountNumber=this.accountNumber;
      this.branchMst.bankName=this.bankName;
      this.branchMst.bankBranch=this.bankBranch;
      this.branchMst.ifsc=this.ifsc;
      this.branchMst.myBranch=this.myBranch;
      // this.sharedserviceService.saveBranchCreation(this.branchMst)
      //.subscribe( data =>
      //   {
      //     console.log(data);
      //     console.log('Data Send')
      //   },
      //   (       error: any) =>console.log(error)
      //  );
       
      this.sharedserviceService.saveBranch(this.branchMst).subscribe((branch:any)=>{
        console.log(data);
        console.log('Branch saved');
        this.showMsg=true;
        // alert("Branch Saved");
        this.clear();
      })
    });

  }

  clear(){
    // this.branchCreationForm.reset();
    this.branchName='';
    this.branchCode='';
    this.branchGst='';
    this.branchEmail='';
    this.branchTelNo='';
    this.branchWebsite='';
    this.branchstate='';
    this.branchAddress1='';
    this.branchAddress2='';
    this.accountNumber='';
    this.bankName='';
    this.bankBranch='';
    this.ifsc='';
    this.myBranch='';

    console.log('clear function')
  }

  emailChecking(){
    this.regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    this.searchfind = this.regexp.test(this.branchEmail);
    console.log(this.searchfind);
    if (this.searchfind == false) {
      // alert("Enter a valid email id")
      return;
    }
  }

  phoneNumberChecking(){
    this.mob = String(this.branchTelNo);
    if (this.mob != undefined || this.mob != null) {
      const arrayMob = Array.from(this.mob);
      if ((arrayMob.length == 10)) {
        this.phoneNumberCheck=false;
       }
      else {
        // alert("Enter 10 digit number")
        this.phoneNumberCheck=true;
        return;
      }
    }
  }

}
