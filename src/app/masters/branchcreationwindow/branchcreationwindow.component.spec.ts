import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchcreationwindowComponent } from './branchcreationwindow.component';

describe('BranchcreationwindowComponent', () => {
  let component: BranchcreationwindowComponent;
  let fixture: ComponentFixture<BranchcreationwindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BranchcreationwindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchcreationwindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
