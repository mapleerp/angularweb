import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { WebUser } from 'src/app/modelclass/web-user';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-web-user',
  templateUrl: './web-user.component.html',
  styleUrls: ['./web-user.component.css'],
})
export class WebUserComponent implements OnInit {
  webUserRegistration: any = FormGroup;

  webUser: WebUser = new WebUser();
  webUser1: any;
  valid: boolean = true;
  mob: any;
  constructor(
    private router: Router,
    private service: SharedserviceService,
    public fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.webUserRegistration = this.fb.group({
      userName: new FormControl(),
      emailId: new FormControl(),

      phoneNo: new FormControl(),
    });
  }
  submitForm() {
    console.log(this.webUserRegistration.value);

    this.webUser = Object.assign(this.webUser, this.webUserRegistration.value);
    console.log(this.webUser);
    if (this.webUser.userName == null) {
      alert('Please enter the user name');
      return;
    }
    if (this.webUser.emailId == null) {
      alert('Please enter the email id');
      return;
    }
    if (this.webUser.phoneNo == null) {
      alert('Please enter the phone number');
      return;
    }

    this.mob = String(this.webUser.phoneNo);

    if (this.mob != undefined || this.mob != null) {
      const arrayMob = Array.from(this.mob);
      console.log(arrayMob);
      console.log('Number Length:' + arrayMob.length);
      if (arrayMob.length == 10 || arrayMob.length == 0) {
      } else {
        alert('Enter a 10 digit number');
        return;
      }
    }

    this.service.saveWebUser(this.webUser).subscribe((data) => {
      this.webUser1 = data;
      if (this.webUser1 != null) {
        this.webUserRegistration.reset();
        alert('User details saved successfully...');
        this.router.navigate(['loginpage']);
      }
    });
  }
}
