import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from '@angular/forms';
import { datas } from 'src/app/Pop-Up-Windows/search-pop-up/search-pop-up.component';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { CategoryMst } from 'src/app/modelclass/category-mst';
import { UnitMst } from 'src/app/modelclass/unit-mst';
import { environment } from 'src/environments/environment.prod';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-itemcreation',
  templateUrl: './itemcreation.component.html',
  styleUrls: ['./itemcreation.component.css'],
})
export class ItemcreationComponent implements OnInit {
  // itemCreationForm: any = FormGroup;
  itemdata: datas = new datas();
  branchName: string = environment.myBranch;
  itemMstDtl: datas[] = [];
  itemidaftersave:any;
  savedItemDtl: any;
  showMsg=false;
  categoryMst: CategoryMst[] = [];
  unitMst: UnitMst[] = [];

  getParentCategory: CategoryMst[] = [];

  barcd: string = '';
  itemcd: string = '';
  itemid: any;
  search: any;
  searchItem: any;

      itemName: any='';
      barCode: any='';
      unitId: any='';
      id: any;
      taxRate: any='';
      itemCode: any='';
      categoryId: any='';
      hsnCode: any='';
      standardPrice: any='';
      cess: any='';
      reorderWaitingPeriod: any='';
      bestBefore: any='';
      barcodeFormat: any='';
     
  msg = '';
  // mainUrl = '';
  selectedFile: File;
  imagePreview: any;

  notSaveMsg=false;

  // private baseUrl = 'http://localhost:8185';
  url: string = '';

  constructor(
    private sharedserviceService: SharedserviceService,
    public fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.showAll();
    this.sharedserviceService.getCategoryMst().subscribe((data) => {
      this.categoryMst = data;
    });
    this.sharedserviceService.getUnitMst().subscribe((data: any) => {
      this.unitMst = data;
    });
    // this.itemCreationForm = this.fb.group({
    //   itemName: new FormControl(),
    //   barCode: new FormControl(),
    //   unitId: new FormControl(),
    //   id: new FormControl(),
    //   taxRate: new FormControl(),
    //   itemCode: new FormControl(),
    //   categoryId: new FormControl(),
    //   hsnCode: new FormControl(),
    //   standardPrice: new FormControl(),
    //   cess: new FormControl(),
    //   reorderWaitingPeriod: new FormControl(),
    //   bestBefore: new FormControl(),
    //   barcodeFormat: new FormControl(),
    // });
  }
  barcodeForm() {
    this.sharedserviceService
      .showItemCodeBarCodeItemIdForm(this.branchName)
      .subscribe((data: any) => {
        console.log(data + 'Barcode Send');
        this.barcd = data;
      });
  }
  itemCodeForm() {
    this.sharedserviceService
      .showItemCodeBarCodeItemIdForm(this.branchName)
      .subscribe((data: any) => {
        console.log(data + 'Item Code Send');
        this.itemcd = data;
      });
  }
  submitForm() {
    console.log('ANY' + this.itemid);
    // if (this.itemCreationForm.value.itemName == null) {
    //   alert('Enter the Item name!!!');
    //   return;
    // }
    if (this.itemName == '') {
      // alert('Enter the Item name!!!');
      return;
    }
    if (this.barcd == '') {
      // alert('Please generate Barcode!!!');
      return;
    }
    if (this.barcd.trim().length==0) {
      // alert('Please generate Barcode!!!');
      return;
    }
    if (this.unitId == '') {
      // alert('Select the Unit!!!');
      return;
    }
    if (this.taxRate == '') {
      // alert('Select the Tax rate!!!');
      return;
    }
    if (this.itemcd == '') {
      // alert('Please generate Item code!!!');
      return;
    }
    if (this.categoryId == '') {
      // alert('Select the Category!!!');
      return;
    }
    if (this.hsnCode == '') {
      // alert('Enter the HSN code!!!');
      return;
    }
    if (this.standardPrice == '') {
      // alert('Enter the Selling price!!!');
      return;
    }
    if (this.cess == '') {
      // alert('Enter the Cess price!!!');
      return;
    }
    if (this.reorderWaitingPeriod == '') {
      // alert('Enter the Reorder waiting period!!!');
      return;
    }
    if (this.bestBefore == '') {
      // alert('Enter the Best before period!!!');
      return;
    }
    if (this.barcodeFormat == '') {
      // alert('Select the Barcode format!!!');
      return;
    }

    this.itemIdCreation();
    // this.itemdata.serviceOrGoods='service'
    // console.log('item ID Send outside'+this.itemid);
    // this.sharedserviceService.saveItemCreationForm(this.itemdata).subscribe( data =>
    //   {
    //         console.log(data);
    //         console.log('Data Send')
    //   },
    // (       error: any) =>console.log(error)
    //   );
    // alert("Item saved!!");
    //this.clear();
    // this.showAll();
  }
  showAll() {
    this.search = null;

    this.sharedserviceService.AllDataPopup().subscribe((info) => {
      // searchItem: new FormControl();
      this.itemMstDtl = info;
      // this.itemCreationForm.reset();
    });
  }
  sortdesc(){
    this.itemMstDtl= this.itemMstDtl.sort(function(a,b){
    if(a.itemName<b.itemName){
    return 1;
    }
    else{
    return -1;
    }
    });
    }
    sortasc(){
    this.itemMstDtl= this.itemMstDtl.sort(function(a,b){
    if(a.itemName>b.itemName){
    return 1;
    }
    else{
    return -1;
    }
    });
    }
  itemIdCreation() {
    this.sharedserviceService
      .showItemCodeBarCodeItemIdForm(this.branchName)
      .subscribe((data: any) => {
        this.itemid = data;
        // console.log(data);
        console.log('item ID Send' + data);
        if (typeof this.itemid != 'undefined') {
          console.log('if statement excecuted');
          this.itemdata.isDeleted = 'N';
          // this.itemdata = Object.assign(
          //   this.itemdata,
          //   this.itemCreationForm.value
          // );
          this.itemdata.id = data;
          this.itemdata.id = this.itemdata.id + 'I' + environment.company;
          this.itemid = this.itemdata.id;
          this.itemdata.itemName=this.itemName;
          this.itemdata.barCode=this.barcd;
          this.itemdata.unitId=this.unitId;
          this.itemdata.taxRate=this.taxRate;
          this.itemdata.itemCode=this.itemcd;
          this.itemdata.categoryId=this.categoryId;
          this.itemdata.hsnCode=this.hsnCode;
          this.itemdata.standardPrice=this.standardPrice;
          this.itemdata.cess=this.cess;
          this.itemdata.reorderWaitingPeriod=this.reorderWaitingPeriod;
          this.itemdata.barcodeFormat=this.barcodeFormat;
          this.itemdata.bestBefore=this.bestBefore;
          console.log(this.itemdata.itemName + 'itemname');

          this.itemdata.serviceOrGoods = 'service';
          console.log('item ID Send outside' + this.itemid);
          this.sharedserviceService
            .saveItemCreationForm(this.itemdata, this.url)
            .subscribe(
              (data) => {
                this.itemidaftersave=data.id;
                // this.itemMstDtl.push(data);
                // this.itemCreationForm = this.fb.group({
                //   itemName: new FormControl(),
                //   barCode: new FormControl(),
                //   unitId: new FormControl(),

                //   taxRate: new FormControl(),
                //   itemCode: new FormControl(),
                //   categoryId: new FormControl(),
                //   hsnCode: new FormControl(),
                //   standardPrice: new FormControl(),
                //   cess: new FormControl(),
                //   reorderWaitingPeriod: new FormControl(),
                //   bestBefore: new FormControl(),
                //   barcodeFormat: new FormControl(),
                // });

                console.log(data);
                this.showMsg= true;
                console.log('Data Send');
                this.clear();
                this.search = null;
                this.showAll();
                if (null != data) {
                }
              },
              (error: any) => {
                console.log(error), this.notSaveMsg=true;
                // alert('Unable to Save');
              }
            );
        } else {
          console.log('else statement excecuted');
          return;
        }
      });
  }
  clear() {
    this.itemdata = new datas();
    this.itemid = undefined;
    // this.itemCreationForm.reset();
    this.itemName='';
    this.barcd='';
    this.itemcd='';
    this.unitId='';
    this.id='';
    this.taxRate='';
    this.itemCode='';
    this.categoryId='';
    this.hsnCode='';
    this.standardPrice='';
    this.cess='';
    this.reorderWaitingPeriod='';
    this.bestBefore='';
    this.barcodeFormat='';
  }

  onKeypressEvent(x: any) {
    // this.mymodel = x.target.value;
    // console.log('sadfghjk' + this.mymodel);
    //  this.sharedserviceService.searchPopup(this.mymodel).subscribe(data =>
    //   {
    //     this.tdatas=data;
    //   })
  }

  renderPdf() {
    let element = document.getElementById('contentToConvert');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'sheet1');
    XLSX.writeFile(wb, 'ItemMaster.xlsx');
  }

  getParentNameById(num: any) {
    const categoryNameById = this.categoryMst.find((data) => data.id === num);
    if (!categoryNameById) {
      // we not found the parent
      return '';
    }
    return categoryNameById.categoryName;
  }

  getUnitNameById(num: any) {
    const unitNameById = this.unitMst.find((data) => data.id === num);
    if (!unitNameById) {
      // we not found the parent
      return '';
    }
    return unitNameById.unitName;
  }

  highlightRow(dts: datas) {
    // this.itemCreationForm = this.fb.group({
    //   itemName: dts.itemName,
    //   barCode: dts.barCode,
    //   unitId: dts.unitId,
    //   id: dts.id,
    //   taxRate: dts.taxRate,
    //   itemCode: dts.itemCode,
    //   categoryId: dts.categoryId,
    //   hsnCode: dts.hsnCode,
    //   standardPrice: dts.standardPrice,
    //   cess: dts.cess,
    //   reorderWaitingPeriod: dts.reorderWaitingPeriod,
    //   bestBefore: dts.bestBefore,
    //   barcodeFormat: dts.barcodeFormat,
    // });
   
      this.itemName= dts.itemName;
      this.barCode=dts.barCode;
      this.unitId= dts.unitId;
      this.id= dts.id;
      this.taxRate= dts.taxRate;
      this.itemCode= dts.itemCode;
      this.categoryId= dts.categoryId;
      this.hsnCode= dts.hsnCode;
      this.standardPrice= dts.standardPrice;
      this.cess= dts.cess;
      this.reorderWaitingPeriod= dts.reorderWaitingPeriod;
      this.bestBefore= dts.bestBefore;
      this.barcodeFormat= dts.barcodeFormat;
  
  }

  deleteItem() {
    this.sharedserviceService
      .deletItemById(this.id)
      .subscribe((data) => {
        this.showAll();
      });
  }

  onFileUpload(event) {
    if (!event.target.files[0] || event.target.files[0].length == 0) {
      this.msg = 'You must select an image';
      this.imagePreview = '';

      return;
    }

    let imageType = event.target.files[0].type;
    if (imageType.match(/image\/*/) == null) {
      this.msg = 'Only images are supported';
      this.imagePreview = '';
      return;
    }

    this.selectedFile = event.target.files[0];

    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result;
    };
    reader.readAsDataURL(this.selectedFile);
    // this.uploadFile();
  }
  uploadFile() {
    //Upload file here send a binary data

    const formData: FormData = new FormData();
    formData.append('file', this.selectedFile);

    this.sharedserviceService.imageUpload(formData).subscribe((data) => {
      this.url = data;
      console.log('firsttttttttttttttttt' + this.url);
    });
    // console.log('secondddddddddddddd' + this.url);
  }

  
}
