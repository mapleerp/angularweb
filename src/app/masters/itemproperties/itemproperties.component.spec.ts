import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItempropertiesComponent } from './itemproperties.component';

describe('ItempropertiesComponent', () => {
  let component: ItempropertiesComponent;
  let fixture: ComponentFixture<ItempropertiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItempropertiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItempropertiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
