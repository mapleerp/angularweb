import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountHeads } from 'src/app/modelclass/account-heads';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-party-creation',
  templateUrl: './party-creation.component.html',
  styleUrls: ['./party-creation.component.css']
})
export class PartyCreationComponent implements OnInit {
//ngModel variables

custOrSup: any='';
partyName: any='';
companyName: any='';
address: any='';
creditPeriod: any='';
phoneNumber: any='';
group: any='';
discountPercent: any='';
discountProperty: any='';
invoiceFormat: any='';
emailId: any='';
gstNumber: any='';
country="INDIA";
state="KERALA";
currencyName: any='';
priceType: any='';
mob: string = "";
regexp: any;
searchfind: boolean = true;
accountheads: AccountHeads = new AccountHeads();
id: any;
bankName:any='';
bankAccountName:any='';
bankIfsc:any='';

accountHeads: AccountHeads[] = [];

assetAccounts:any[];
invoiceFormatMst:any[]=[];
priceTypeMst:any[]=[];
countryList:any[]=[];
stateList:any[]=[];
showMsg:any;
phoneCheck=false;
  constructor(private service: SharedserviceService,private router: Router) { 

  }

  ngOnInit(): void {
    this.service.getAssetAccountsforGroup().subscribe((data:any)=>{
      console.log(data)
      this.assetAccounts=data;
    })

    this.service. getInvoiceFormat().subscribe((data:any)=>{
      console.log(data)
      this.invoiceFormatMst=data;
    })

    this.service.getPriceType().subscribe((data:any)=>{
  console.log(data)
  this.priceTypeMst=data;
  })
  this.service.getCountries().subscribe((data:any)=>{
    console.log(data)
     this.countryList=data;
    })

    this.service.getStateByCountry("INDIA").subscribe((data:any)=>{
      this.stateList=data
      console.log(data)
      });

      this.showAllAccounts();
  }
  saveAccounts() {
    if (this.partyName == '') {
      // alert("Enter party name")
      return; 
    }

    if(this.address==''){
      return;
    }

    if(this.gstNumber==''){
      return;
    }
    
    if(this.custOrSup==''){
      return;
    } 

    if(this.priceType==''){
      return;
    }

    if(this.creditPeriod==''){
      return;
    }

    if(this.gstNumber.length>=13){
      this.accountheads.customerType="REGULAR"
      console.log(this.accountheads.customerType+" customer type")
    }
    else{
      this.accountheads.customerType="CONSUMER"
      console.log(this.accountheads.customerType+" customer type")
    }

    this.phoneNumberChecking();
    this.emailChecking();
   
    
    this.accountheads.customerOrSupplier = this.custOrSup;
    this.accountheads.accountName = this.partyName + this.phoneNumber;
    this.accountheads.partyAddress1 = this.address;
    this.accountheads.partyMail = this.emailId;
    this.accountheads.partyGst = this.gstNumber;
    this.accountheads.customerContact = this.phoneNumber;
    this.accountheads.priceTypeId = this.priceType;
    this.accountheads.creditPeriod = this.creditPeriod;
    this.accountheads.customerDiscount = this.discountPercent;
    this.accountheads.customerCountry = this.country;
    // this.accountheads.customerGstType = this.gstNumber;
    this.accountheads.discountProperty = this.discountProperty;
    this.accountheads.taxInvoiceFormat = this.invoiceFormat;
    this.accountheads.customerState = this.state;
    this.accountheads.customerGroup = this.group;
    this.accountheads.companyName=this.companyName;
    this.accountheads.bankName=this.bankName;
    this.accountheads.bankAccountName=this.bankAccountName;
    this.accountheads.bankIfsc=this.bankIfsc;
    this.service.generateVoucherNumber("CU").subscribe((data: any) => {

      this.id = data;
      console.log(this.id)
      this.accountheads.id = this.id + environment.company;
      // this.accounts.id=this.id+environment.company+environment.myBranch;     
      console.log(this.accountheads.id)
      
      this.service.saveAccounts(this.accountheads).subscribe((data: any) => {
        console.log(data);
        this.showMsg=true;
        console.log(data.id + 'Data Send')
        // alert("Party saved!!")
        this.clear();
        this.showAllAccounts();
      }
      );

    });
    
  }

  clear() {
    // this.accounts = new this.accounts();
    this.custOrSup = "";
    this.partyName = "";
    this.companyName = "";
    this.address = "";
    this.creditPeriod = "";
    this.phoneNumber = "";
    this.group = "";
    this.discountPercent = "";
    this.discountProperty = "";
    this.invoiceFormat = "";
    this.emailId = "";
    this.gstNumber = "";
    this.country = "";
    this.state = "";
    this.currencyName = "";
    this.priceType = "";
    this.bankName="";
    this.bankAccountName="";
    this.bankIfsc="";
  }
  showAllAccounts() {
    this.service.showAllAccounts().subscribe((data: any) => {
      console.log(data)
      this.accountHeads = data;
    });


  }

  getState(c:any){
    this.service.getStateByCountry(c).subscribe((data:any)=>{
      this.stateList=data
      console.log(data)
      });
  }

  dashBoardClick(){
    this.router.navigate(['/loginpage/DashBoard']);
  }

  posClick(){
    this.router.navigate(['/loginpage/PosWindow']);
  }

  partyCreateClick(){
    this.router.navigate(['/loginpage/PartyCreation']);
  }

  purchaseClick(){
    this.router.navigate(['/loginpage/Purchase']);
  }

  itemClick(){
    this.router.navigate(['/loginpage/ItemCreation']);
  }

  sortdesc(){
    this.accountHeads= this.accountHeads.sort(function(a,b){
    if(a.accountName<b.accountName){
    return 1;
    }
    else{
    return -1;
    }
    });
    }
    sortasc(){
    this.accountHeads= this.accountHeads.sort(function(a,b){
    if(a.accountName>b.accountName){
    return 1;
    }
    else{
    return -1;
    }
    });
    }

    phoneNumberChecking(){
      this.mob = String(this.phoneNumber);
      if (this.mob != undefined || this.mob != null) {
        const arrayMob = Array.from(this.mob);
        if ((arrayMob.length == 10)) { 
          this.phoneCheck=false;
        }
        else {
          this.phoneCheck=true;
          // alert("Enter 10 digit number")
          return;
        }
      }
    }

    emailChecking(){
      this.regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
      this.searchfind = this.regexp.test(this.emailId);
      console.log(this.searchfind);
      if (this.searchfind == false) {
        // alert("Enter a valid email id")
        return;
      }
    }

}
