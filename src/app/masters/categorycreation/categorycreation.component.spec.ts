import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategorycreationComponent } from './categorycreation.component';

describe('CategorycreationComponent', () => {
  let component: CategorycreationComponent;
  let fixture: ComponentFixture<CategorycreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategorycreationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategorycreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
