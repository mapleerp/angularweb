import { Component, OnInit } from '@angular/core';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { CategoryMst } from 'src/app/modelclass/category-mst';
import { environment } from 'src/environments/environment.prod';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms'

@Component({
  selector: 'app-categorycreation',
  templateUrl: './categorycreation.component.html',
  styleUrls: ['./categorycreation.component.css']
})
export class CategorycreationComponent implements OnInit {
  categoryMst:CategoryMst[]=[]

  showCategoryMst:CategoryMst[]=[]
  getParentCategory:CategoryMst[]=[]

  categoryCreationForm:any= FormGroup;
  categoryMst1:CategoryMst=new CategoryMst;

  constructor(private sharedserviceService:SharedserviceService,public fb:FormBuilder) { }

  ngOnInit(): void {
    this.sharedserviceService.getCategoryMst().subscribe(data => {this.categoryMst=data })
    this.categoryCreationForm = this.fb.group({
      categoryName: new FormControl(),
      parentId: new FormControl(),     
    })
  }
  saveCategory(){
    if(this.categoryCreationForm.value.categoryName===null){
      alert('Enter a Category Name')
      return
    }
    this.sharedserviceService.showItemCodeBarCodeItemIdForm(environment.myBranch+"CAT").subscribe((data:any)  =>
    {  
        this.categoryMst1=Object.assign(this.categoryMst1,this.categoryCreationForm.value);
        this.categoryMst1.id=data;
        this.categoryMst1.id=this.categoryMst1.id+environment.company;
        console.log(this.categoryMst1);
        this.sharedserviceService.saveCategoryCreationForm(this.categoryMst1).subscribe( (data:any) =>
        {
          console.log(data);
          console.log('Data Send')
          alert("Category created!!")
          this.clear();
          this.sharedserviceService.getCategoryMst().subscribe(data => {this.categoryMst=data })
          this.showAllCategory();
        },
        (       error: any) =>{console.log(error)
          alert("Category cannot be saved!")
          this.clear();}
       ); 
    },
    (       error: any) =>{console.log(error)
      alert("Cannot be saved!")}); 
  }
  clear(){
    this.categoryCreationForm.reset();
  }
  showAllCategory(){
    this.sharedserviceService.showCategoryDetails().subscribe((data: any)=>
      {
        this.showCategoryMst=data;
        this.getParentCategory=data;
      })
  }
  getParentNameById(num: any) {
    const categoryNameById =  this.getParentCategory.find((data) => data.id === num)
    if (!categoryNameById) {
      // we not found the parent
      return ''
    }
    return categoryNameById.categoryName
  }
}
