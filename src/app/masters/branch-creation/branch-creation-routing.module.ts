import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BranchCreationComponent } from './branch-creation.component';

const routes: Routes = [{ path: '', component: BranchCreationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BranchCreationRoutingModule { }
