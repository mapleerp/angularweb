import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms'
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-branch-creation',
  templateUrl: './branch-creation.component.html',
  styleUrls: ['./branch-creation.component.css']
})
export class BranchCreationComponent implements OnInit {

  // branchMst:BranchMst[]=[];
branchMst:BranchMst=new BranchMst;
  branchCreationForm:any = FormGroup;
  myBranchs:any=['Y','N'];
  states:any=['Kerala','TamiNadu','Karnataka'];

  mob: string = "";
  regexp: any;
  branchTelNo:any;
  searchfind: boolean = false;
  branchEmail:any;
  branchName:any;
  branchCode:any;

  constructor(private sharedserviceService:SharedserviceService,public fb:FormBuilder) { }

  ngOnInit(): void {

    this.branchCreationForm = this.fb.group({
      branchName: new FormControl(),
      branchCode: new FormControl(),
      branchGst: new FormControl(),
      branchEmail: new FormControl(),
      branchTelNo: new FormControl(),
      branchWebsite: new FormControl(),
      branchAddress1: new FormControl(),
      branchAddress2: new FormControl(),
      accountNumber: new FormControl(),
      bankName: new FormControl(),
      bankBranch: new FormControl(),
      ifsc: new FormControl(),
      myBranch: new FormControl(),
      branchstate: new FormControl(),
    })
    
  }

  

  submitForm()
  {

    if(this.branchName==null){
      alert("Enter Branch Name");
        return;
    }

    if(this.branchCode==null){
      alert("Enter Branch Code");
        return;
    }

    this.mob = String(this.branchTelNo);
    if (this.mob != undefined || this.mob != null) {
      const arrayMob = Array.from(this.mob);
      if ((arrayMob.length == 10)) { }
      else {
        alert("Enter 10 digit number")
        return;
      }
    }

this.regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    this.searchfind = this.regexp.test(this.branchEmail);
    console.log(this.searchfind);
    if (this.searchfind == false) {
      alert("Enter a valid email id")
      return;
    }

    this.sharedserviceService.showItemCodeBarCodeItemIdForm(environment.company+"BR").subscribe((data:any)  =>
    {
     
      this.branchMst = Object.assign(this.branchMst, this.branchCreationForm.value);
      this.branchMst.id=data+environment.company;
      console.log(this.branchMst);
      // this.sharedserviceService.saveBranchCreation(this.branchMst)
      //.subscribe( data =>
      //   {
      //     console.log(data);
      //     console.log('Data Send')
      //   },
      //   (       error: any) =>console.log(error)
      //  );
       
      this.sharedserviceService.saveBranch(this.branchMst).subscribe((branch:any)=>{
        console.log(data);
        console.log('Branch saved');
        alert("Branch Saved");
        this.rest();
      })
    });
  }

  rest()
  {
    this.branchCreationForm.reset();
    console.log('restttt')
  }

}
