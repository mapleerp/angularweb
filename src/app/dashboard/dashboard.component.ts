import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { DayEndWebReport } from '../modelclass/day-end-web-report';
import { SharedserviceService } from '../services/sharedservice.service';
import { Subscription } from 'rxjs';
import { ChartsModule, Label } from 'ng2-charts';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Router } from '@angular/router';
import { UserMst } from '../modelclass/user-mst';
import { BranchMst } from '../modelclass/branch-mst';
import { UserSubsidiary } from '../modelclass/user-subsidiary';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  notSaveswitchMsg=false;
  notSaveMsg=false;
  notSMsg=false;
  userId: any;
  branchId: any;
  switchbranchId:any;
  users: UserMst[] = [];
  branches: BranchMst[] = [];
  userSub: UserSubsidiary = new UserSubsidiary();
  usub: UserSubsidiary[] = [];
  sub: UserSubsidiary = new UserSubsidiary();
  search: any;
  branchByUser:UserSubsidiary[]=[];

  
  userName:any;
  userMst:UserMst=new UserMst;
  userid:any;




  constructor(public datepipe: DatePipe,private router: Router, private sharedserviceService: SharedserviceService,) { }
  


  ngOnInit(): void {

    this.sharedserviceService.getBranchNames(). subscribe(data => {this.branches=data})
    this.sharedserviceService.getAllUsers(). subscribe(data => {this.users=data
    console.log(data)})
    this.showAll()
    this.userMst=this.sharedserviceService.getUserMstInLogin();
    
      this.getBranchByUser();
  

  }

  branchassociationsave() {

    if(this.userId==null ){
      this.notSaveMsg=true;
      return;
    }else{
      this.notSaveMsg=false;
    }
    if(this.branchId==null){
      this.notSMsg=true;
      return;
    }else{
      this.notSMsg=false;
    }

   
    this.userSub = new UserSubsidiary();
    this.userSub.userId = this.userId;
    this.userSub.branchId = this.branchId;
    

    this.sharedserviceService.saveUserSubsidiary(this.userSub).subscribe(
      (data: any) => {
        console.log(data);
        console.log('Data saved');
        // alert('User Subsidiary Association saved!!');
        this.clear();
        this.showAll();
      },
      (error: any) => {
        console.log(error);
       
        this.clear();
      }
    );
  }

  // highlightRow(usub: UserSubsidiary) {}

  showAll(){
    this.sharedserviceService.showAllUserBranchAssociation().subscribe((data:any)=>{
      this.usub=data;
    })
  }

  clear(){
      this.userId="";
      this.branchId="";
      
  }

  getUserNameById(user:any){
    const userNameById = this.users.find((data) => data.id === user);
    if (!userNameById) {
      // we not found the parent
      return '';
    }
    return userNameById.userName;
  }

  getBranchNameById(branch:any){
    const branchNameById = this.branches.find((data) => data.id === branch);
    if (!branchNameById) {
      // we not found the parent
      return '';
    }
    return branchNameById.branchCode;
  }

  deleteUserById(indx:any){
    this.sharedserviceService.deleteUserBranchAssociationById(indx).subscribe((data:any)=>{});
    this.showAll();
  }
  getBranchByUser(){
    this.sharedserviceService.getAllBranchesByUserId(this.userMst.id).subscribe((data:any)=>{
      this.branchByUser=data;
      console.log(data)
      console.log('branch by user id')
    })
  }

  setBranch(){
    this.sharedserviceService.setmyBranch(this.switchbranchId);
    this.switchbranchId="";
    this.router.navigate(['/loginpage']);
  }




    dashBoardClick(){
      this.router.navigate(['/loginpage/DashBoard']);
    }

    StockTransferOutReport(){
      this.router.navigate(['/loginpage/StockTransferOutReport']);
    }
    hsnCodeWiseSalesReport(){
      this.router.navigate(['/loginpage/hsnCodeWiseSalesReport']);
    }

    StockMovementReport(){
      this.router.navigate(['/loginpage/StockMovementReport']);
    }
    PurchaseConsolidatedReport(){
      this.router.navigate(['/loginpage/PurchaseConsolidatedReport']);
    }
    StatementsOfaccount(){
      this.router.navigate(['/loginpage/StatementsOfaccount']);
    }
    GroupwiseSalesDetailReport(){
      this.router.navigate(['/loginpage/GroupwiseSalesDetailReport']);
    }
    ItemWiseSalesReport(){
      this.router.navigate(['/loginpage/ItemWiseSalesReport']);
    }
    posClick(){
      this.router.navigate(['/loginpage/PosWindow']);
    }
  
    partyCreateClick(){
      this.router.navigate(['/loginpage/PartyCreation']);
    }
  
    purchaseClick(){
      this.router.navigate(['/loginpage/Purchase']);
    }
    itemClick(){
      this.router.navigate(['/loginpage/ItemCreation']);
    }
    userClick(){
      this.router.navigate(['/loginpage/UserCreation']);
    }
    branchClick(){
      this.router.navigate(['/loginpage/BranchCreation']);
    }
    receiptClick(){
      this.router.navigate(['/loginpage/ReceiptWindow']);
    }
    paymentClick(){
      this.router.navigate(['/loginpage/PaymentsWindow']);
    }

    journalClick(){
      this.router.navigate(['/loginpage/Journal']);
    }
  }
