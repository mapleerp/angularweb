

import { AccountHeads } from "./account-heads";
import { CompanyMst } from "./company-mst";
import { CustomerMst } from "./customer-mst";

export class ShippingAddress {

    id:any; 
    shippingAddressName:string='';
    address:string='';
    state:string='';
    pincode:string='';
    phoneNumber:string='';
    locationLatitude:string='';
    locationLongitude:any;
    accountHeads:AccountHeads=new AccountHeads;
    companyMst:CompanyMst= new CompanyMst;

    // id:string='';
    // customerMst: CustomerMst= new CustomerMst;
    // companyMst:CompanyMst= new CompanyMst;
    // siteName:string='';
    // addressLine1:string='';
    // addressLine2:string='';
    // mobileNumber:string='';
    // landMark:string='';
    // customerName:string='';
}
