import { CompanyMst } from "./company-mst";
import { ItemMst } from "./item-mst";
import { StockRequestHdr } from "./stock-request-hdr";

export class StockRequestDtl {

    id:any

    slNo:any
   
    batch:any;
   
    qty:any;
   
    rate:any;
   
    itemCode:any;
   
    taxRate:any;
   
    expiryDate:any;
   
    amount:any;
   
    unitId:any;
   
   
    mrp:any;

    barcode:any;
   
    itemId: ItemMst = new ItemMst;	
   
    stockRequestHdr: StockRequestHdr = new StockRequestHdr;	
    companyMst: CompanyMst = new CompanyMst;	
   
    
   
     intentDtlId:any;
   
     displaySerial:any;
    
      storeName:any;
    
       processInstanceId:any;
    
      taskId:any;
}
