import { PaymentHdr } from "./payment-hdr";

export class PaymentDtl {
  id:any;
	accountId:any;
	modeOfPayment:any;
	remark:any;
	amount:any;
	instrumentNumber:any;
	bankAccountNumber:any;
	instrumentDate:any;
	transDate:any;
	creditAccountId:any;
	paymenthdr:any;

  paymenthdr_id: PaymentHdr = new PaymentHdr;
  
  memberId:any;
	oldId:any;
	processInstanceId:any;
	taskId:any;
}
