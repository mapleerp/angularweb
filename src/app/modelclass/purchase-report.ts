export class PurchaseReport {

    supplierName:any;
    supplierInvNo:any;
    voucherNumber:any;
    voucherDate:any;
    itemName:any;
    qty:any;
    purchseRate:any;
    taxRate:any;
    unit:any;
    amount:any;
    amountTotal:any;
    supplierGst:any;
    taxAmount:any;
    additionalExpense:any;
    importDuty:any;

    itemCode:any;
    batch:any;
    expiry:any;
    unitPrice:any;

    supplierEmail:any;
    incoiceDate:any;
    netCost:any;
    supplierAddress:any;
    supplierState:any;

    discount:any;
}
