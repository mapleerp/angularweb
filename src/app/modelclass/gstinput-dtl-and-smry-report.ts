export class GSTInputDtlAndSmryReport {

    id:String='';
    invoiceDate:String='';
    invoiceNumber:String='';
    supplierName:String='';
    supplierGst:String='';
    branch:String='';
    totalPurchaseExcludingGst:Number=0.0;
    gstAmount:Number=0.0;
    excemptedPurchase:Number=0.0;
    totalPurchase:Number=0.0;
    gstPercent:String='';

}
