import { CompanyMst } from "./company-mst";
export class UnitMst {
    id:any
    branchCode:any
    oldId:any
    processInstanceId:any
    taskId:any
    unitDescription:any
    unitName:any
    unitPrecision:any
    companyMst: CompanyMst = new CompanyMst;
}
