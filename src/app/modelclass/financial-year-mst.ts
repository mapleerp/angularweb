import { CompanyMst } from "./company-mst";

export class FinancialYearMst {

    id:any;

    UserName:any;
    startDate:any;
    endDate:any;
    financialYear:any;
    currentFinancialYear:any;
    openCloseStatus:any;
	companyMst:CompanyMst=new CompanyMst;

    processInstanceId:any;
    taskId:any;
}
