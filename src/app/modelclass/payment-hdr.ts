import { CompanyMst } from "./company-mst";

export class PaymentHdr {
  id:any;
	voucherNumber:any;
	userId:any;
	branchCode:any;
	machineId:any;
	voucherDate:any;
	voucherType:any;
	
  companyMst: CompanyMst = new CompanyMst;
	
	processInstanceId:any;
	taskId:any;
	oldId:any;
}
