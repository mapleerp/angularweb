import { CompanyMst } from "./company-mst";

export class Supplier {
    id:any;
    accountId:any;
    address:any;
    branchCode:any;
    cerditPeriod:any;
    company:any;
    country:any;
    currencyId:any;
    deleted:any;
    emailid:any;
    oldId:any;
    phoneNo:any;
    processInstanceId:any;
    state:any;
    supGST:any;
    supplierId:any;
    supplierName:any;
    taskId:any;
    userId:any;
    companyMst: CompanyMst = new CompanyMst;
}
