import { CompanyMst } from './company-mst';
import { KotTableHdr } from './kot-table-hdr';

export class KotTableDtl {
  id: any;
  processInstanceId: any;
  taskId: any;
  kotTableHdr: KotTableHdr = new KotTableHdr();
  qty: any;
  rate: any;
  itemId: any;
  kotNumber: any;
  orderTakerTime: any;
  customization: any;
  customizationRate: any;
  kitchenStatus: any;
  kitchenStatusUpdatedTime: any;
  orderDeliveryTime: any;
  companyMst: CompanyMst = new CompanyMst();
}
