import { CompanyMst } from './company-mst';

export class KotTableHdr {
  id: any;
  processInstanceId: any;
  taskId: any;
  tableName: any;
  tableStatus: any;
  lastOrderPlacedTime: any;
  lastDeliveryTime: any;
  waitingTime: any;
  overallKitchenStatus: any;
  billedStatus: any;
  reservationStartTime: any;
  reservationEndTime: any;
  waiterName: any;
  companyMst: CompanyMst = new CompanyMst();
}
