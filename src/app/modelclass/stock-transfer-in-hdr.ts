export class StockTransferInHdr {
    id: any;
    intentNumber: any;
    voucherDate: any;
    voucherNumber: any;
    voucherType: any;
    branchCode: any;
    deleted: any;
    fromBranch: any;
    inVoucherNumber: any;
    inVoucherDate: any;
    statusAcceptStock: any;
    narration: any;
    processInstanceId: any;
}
