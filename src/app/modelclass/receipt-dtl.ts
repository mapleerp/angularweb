import { ReceiptHdr } from "./receipt-hdr";

export class ReceiptDtl {
    id:any;
    account:any;
    instdate:any;
    modeOfpayment:any;
    amount:any;
    depositBank:any;
    remark:any;
    instnumber:any;
    invoiceNumber:any;
    bankAccountNumber:any;
    instrumentDate:any;
    transDate:any;
    memberId:any;
    oldId:any;
    branchCode:any;
    debitAccountId:any;
    invoiceDate:any;
    receiptHdr:any;
    processInstanceId:any;
    taskId:any;
    receipt_hdr_id:ReceiptHdr = new ReceiptHdr;
}
