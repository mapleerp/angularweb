import { CompanyMst } from './company-mst';

export class AddKotWaiter {
  id: any;
  status: any;
  oldId: any;
  waiterName: any;
  branchCode: any;
  companyMst: CompanyMst = new CompanyMst();
  processInstanceId: any;
  taskId: any;
}
