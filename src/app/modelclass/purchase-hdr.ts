import { CompanyMst } from "./company-mst";
export class PurchaseHdr {
  id:any;
  branchCode:any;
  currency:any;
  deletedStatus:any;
  discount:any;
  enableBatchStatus:any;
  fcInvoiceTotal:any;
  finalSavedStatus:any
  invoiceTotal:any;
  machineId:any;
  narration:any;
  oldId:any;
  pONum:any;
  poDate:any;
  processInstanceId:any;
  purchaseType:any;
  supplierId:any;
  supplierInvDate:any;
  supplierInvNo:any;
  tansactionEntryDate:any;
  taskId:any;
  userId:any;
  voucherDate:any;
  voucherNumber:any;
  voucherType:any;
  companyMst: CompanyMst = new CompanyMst;
  postedToServer:any;
  ourVoucherDate:any;
}
