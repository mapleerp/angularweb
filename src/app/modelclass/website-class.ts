import { WebsiteFeatureMst } from "./websiteModelClass/website-feature-mst";
import { WebsiteMainMst } from "./websiteModelClass/website-main-mst";
import { WebsiteServiceMst } from "./websiteModelClass/website-service-mst";

export class WebsiteClass {
    public websiteMainMst: WebsiteMainMst
    public websiteFeatureMst:WebsiteFeatureMst[]
    public websiteServiceMst:WebsiteServiceMst[]



}
