export class TaxSummaryDetails {
  taxPercentage: any;
  taxAmount: any;
  taxRate: any;

  sgstTaxRate: any;
  sgstAmount: any;

  cgstTaxRate: any;
  cgstAmount: any;

  igstTaxRate: any;
  igstAmount: any;

  rate: any;
  amount: any;
  sumOfigstAmount: any;
  sumOfsgstAmount: any;
  sumOfcgstAmount: any;
}
