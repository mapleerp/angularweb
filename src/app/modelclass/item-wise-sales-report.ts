export class ItemWiseSalesReport {
  id: any;
  invoiceDate: any;
  itemName: any;
  customerName: any;
  qty: any;
  unitPrice: any;
  batch: any;
  branchCode: any;
}
