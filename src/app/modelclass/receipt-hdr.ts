import { CompanyMst } from "./company-mst";

export class ReceiptHdr {
  id:any;
  memberId:any;
  voucherDate:any;
  oldId:any;
  voucherNumber:any;
  branchCode:any;
  transdate:any;
  voucherType:any;
  processInstanceId:any;
  taskId:any;
  
  companyMst: CompanyMst = new CompanyMst;
}
