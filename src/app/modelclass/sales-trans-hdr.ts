import { AccountHeads } from './account-heads';
import { CompanyMst } from './company-mst';
import { CustomerMst } from './customer-mst';
import { LocalCustomerMst } from './local-customer-mst';

export class SalesTransHdr {
  id = null;

  voucherNumber = null;

  customiseSalesMode: any;
  takeOrderNumber: any;
  customerId: any;
  salesManId: any;
  currencyId: any;
  deliveryBoyId: any;
  salesMode: any;
  invoiceNumberPrefix: any;
  creditOrCash: any;
  numericVoucherNumer: any;
  customerMst: CustomerMst = new CustomerMst();
  salesReceiptsVoucherNumber: any;
  creditAmount: any;
  voucherType: any;
  userId: any;
  isBranchSales: any;
  cardNo: any;
  companyId: any;
  branchCode: any;
  customerName: any;
  fcInvoiceAmount: any;
  editedStatus: any;
  kotNumber: any;
  processInstanceId: any;
  taskId: any;
  invoiceAmount: any;
  voucherDate: any;
  sourceBranchCode: any;
  cardType: any;
  cashPay: any;
  cardAmount: any;
  paidAmount: any;
  changeAmount: any;
  sodexoAmount: any;
  amountTendered: any;
  discount:any;
  localCustomerMSt: LocalCustomerMst = new LocalCustomerMst();
  companyMst: CompanyMst = new CompanyMst();
  accountHeads:AccountHeads=new AccountHeads;
  shippingAddressId:any;
  // private PatientMst patientMst;
}
