import { JournalHdr } from "./journal-hdr";

export class JournalDtl {
    id:any;
    accountHead:any;
    remarks:any;
    debitAmount:any;
    creditAmount:any;
    processInstanceId:any;
    taskId:any;
    journalHdr:JournalHdr=new JournalHdr;
}
