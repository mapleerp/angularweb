import { CompanyMst } from "./company-mst";

export class JournalHdr {
    id:any;
    voucherNumber:any;
    voucherDate:any;
    transDate:any;
    branchCode:any;
    processInstanceId:any;
    taskId:any;
    companyMst:CompanyMst=new CompanyMst;
}
