import { CompanyMst } from "./company-mst";

export class SalesTypeMst {
    id:any;
    salesType:any;
    salesPrefix:any;
    branchCode:any;
    companyMst:CompanyMst = new CompanyMst;
    processInstanceId:any;
    taskId:any;
    oldId:any;
}
