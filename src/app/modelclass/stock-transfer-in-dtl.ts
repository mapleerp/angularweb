export class StockTransferInDtl {
    id: any;
    batch: any;
    qty: any;
    rate: any;
    itemCode: any;
    taxRate: any;
    expiryDate: any;
    amount: any;
    unitId: any;
    itemId: any;
    companyMst: any;
    processInstanceId: any;
    taskId: any;
}
