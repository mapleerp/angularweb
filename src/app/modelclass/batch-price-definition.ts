import { CompanyMst } from "./company-mst";

export class BatchPriceDefinition {

      id:any;
	
	  itemId:any;

	  priceId:any;

	  amount:any;

	  itemName:any;

	  priceType:any;

	  unitId:any;

	  branchCode:any;

	  batch:any;
	
	 startDate:any;
	 endDate:any;
	

	 companyMst:CompanyMst=new CompanyMst;

	   processInstanceId:any;

	  taskId:any;
	
}
