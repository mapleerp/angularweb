import { CompanyMst } from "./company-mst";

export class BranchMst {

    id:any;
    branchName:any;
    branchCode:any;
    branchGst:any;
    branchState:any;
    myBranch:any;
    branchAddress1:any;
    branchAddress2:any;
    branchTelNo:any;
    branchEmail:any;
    bankName:any;
    accountNumber:any;
    bankBranch:any;
    branchPlace:any;
    ifsc:any;
    branchWebsite:any;
    itemPublish:any;
    companyMst: CompanyMst = new CompanyMst;
}
