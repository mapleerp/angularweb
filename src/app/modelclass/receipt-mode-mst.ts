import { CompanyMst } from "./company-mst";

export class ReceiptModeMst {

    id: any;

    status: any;

    receiptMode: any;
    parentId: any;
    parentName: any;

    companyMst: CompanyMst = new CompanyMst;
    creditCardStatus: any;
    processInstanceId: any;
    taskId: any;
}
