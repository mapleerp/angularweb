import { datas } from "../Pop-Up-Windows/search-pop-up/search-pop-up.component";
import { CustomerMst } from "./customer-mst";

export class WholeSaleBillDtls {

    itemDtlInWeb:datas[]=[];
    customer:CustomerMst=new CustomerMst;
    userName:string=''
    loginDate:any;
    branch:string='';
    voucherNumber:string='';
    invoiceAmount:number=0.0;
}
