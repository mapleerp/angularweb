import { CompanyMst } from './company-mst';

export class AddKotTable {
  id: any;
  status: any;
  tableName: any;
  branchCode: any;
  oldId: any;
  companyMst: CompanyMst = new CompanyMst();
  processInstanceId: any;
  taskId: any;
}
