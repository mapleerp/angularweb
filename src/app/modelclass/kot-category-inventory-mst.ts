import { CompanyMst } from './company-mst';

export class KotCategoryInventoryMst {
  id: any;
  processInstanceId: any;
  taskId: any;
  parentItem: any;
  childItem: any;
  leaf: String = '';
  companyMst: CompanyMst = new CompanyMst();
}
