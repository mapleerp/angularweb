import { BranchMst } from "./branch-mst";
import { CompanyMst } from "./company-mst";
import { SalesDtl } from "./sales-dtl";
import { SalesReceipts } from "./sales-receipts";
import { SalesTransHdr } from "./sales-trans-hdr";

export class SalesBillDetails {
    branchCode:any;
    minVoucherNumericNo:any;
    maxVoucherNumericNo:any;
    salesMode:any;
    voucherDate:any;
    companyMst:CompanyMst=new CompanyMst;
    salesTransHdr: SalesTransHdr=new SalesTransHdr;
    arrayOfSalesDtl:SalesDtl[]=[];
    arrayOfSalesReceipts:SalesReceipts[]=[];
    logo:any;
    taxAmount=0;
    balanceAmount=0;
    branchMst:BranchMst=new BranchMst;
    totalAmount=0;
    
}
