export class SalesSummaryReport {
    voucherNumber:any;
    customerName:any;
    amount:any;
    totalAmount:any;
    voucherDate:any;
    customerType:any;
    ebs:any;
    cash:any;
    paytm:any;
    sbi:any;
    yesbank:any;
    coupon:any;
    sodexo:any;
    gpay:any;
    phonepay:any;
    credit:any;
    foodPand:any;
    uberSale:any;
    onlineSale:any;
    zomato:any;
    swiggy:any;
    receiptMode:any;

}