export class PurchaseDetailsReport {

    supplierName:String='';
    voucherNumber:String='';
    voucherDate:any='';
    itemName:String='';
    qty:number=0;
    purchseRate:number=0;
    taxRate:number=0;
    unit:number=0;
    amount:number=0;
    amountTotal:number=0;
    supplierInvNo:String='';
    supplierGst:String='';
    taxAmount:number=0;
    taxSplit:number=0;

}
