export class ItemStockPopUp {


    barcode:any;
    unitName:any;
    itemName:any;
    cess:any;
    tax:any;
    unitId:any;
    itemId:any;
    itemcode:any;
    mrp:any;
    qty:any;
    batch:any;
    expDate:any;
    itemPriceLock:any;
    stockName:any;
    store:any;
    standardPrice:any
    processInstanceId:any;
    taskId:any;
    imageUrl:any; 
    
}
