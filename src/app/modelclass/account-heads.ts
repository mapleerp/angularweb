import { CompanyMst } from './company-mst';

export class AccountHeads {


    id:any;



    accountName:any;


    parentId:any;


    groupOnly:any;

    machineId:any;


    taxId:any;

    oldId:any;




    voucherType:any;

    currencyId:any;


    rootParentId:any;

    serialNumber:any;



    processInstanceId:any;

    taskId:any;

    customerPin:any;
    customerPlace:any;




    partyAddress1:any;

    partyAddress2:any;

    partyMail:any;

    partyGst:any;

    customerContact:any;


    priceTypeId:any;

    creditPeriod:any;

    customerDiscount:any;
    customerCountry:any;
    customerGstType:any;

    customerType:any;

    discountProperty:any;

    bankName:any;

    taxInvoiceFormat:any;

    bankAccountName:any;
    bankIfsc:any;

    drugLicenseNumber:any;
    customerGroup:any;
    customerState:any;
    companyName:any;
    customerRank: any;

    companyMst: CompanyMst = new CompanyMst;
    customerOrSupplier: any;
}
