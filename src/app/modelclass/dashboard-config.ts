import { CompanyMst } from './company-mst';

export class DashboardConfig {

    id:any;
    userId:any;
    branchCode:any;
    reportType:any;
    dashboard:any;
    companyMst: CompanyMst = new CompanyMst;

}
