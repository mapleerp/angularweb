import { CompanyMst } from "./company-mst";
import { ItemMst } from "./item-mst";
import { StockTransferOutHdr } from "./stock-transfer-out-hdr";

export class StockTransferOutDtl {
    id: any;
    slNo: any;
    batch: any;
    qty: any;
    rate: any;
    itemCode: any;
    taxRate: any;
    expiryDate: any;
    amount: any;
    unitId: any;
    mrp: any;
    barcode: any;
    intentDtlId: any;
    displaySerial: any;
    storeName: any;
    stockTransferOutHdr: StockTransferOutHdr = new StockTransferOutHdr();
    companyMst: CompanyMst=new CompanyMst;
    processInstanceId: any;
    taskId: any;
    totalAmount:any;
    itemId:any;   
    }
