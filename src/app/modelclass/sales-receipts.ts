import { SalesOrderTransHdr } from "./sales-order-trans-hdr";
import { SalesTransHdr } from "./sales-trans-hdr";

export class SalesReceipts {

    Id:any;
	receiptMode:any;
	branchCode:any;
	userId:any;
	receiptAmount:any;
	rereceiptDate:any;
	salesTransHdr: SalesTransHdr = new SalesTransHdr;
	salesOrderTransHdr: SalesOrderTransHdr = new SalesOrderTransHdr;
	accountId:any;
	 voucherNumber:any;
	fcAmount:any;
	soStatus:any;
	processInstanceId:any;
	taskId:any;
}
