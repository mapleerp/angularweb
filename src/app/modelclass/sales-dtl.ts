import { SalesTransHdr } from "./sales-trans-hdr";

export class SalesDtl {

   
    id:any;

    itemId:any;

    rate:number=0.0;

    cgstTaxRate:number=0.0;

    sgstTaxRate:number=0.0;

expiryDate:any;
    qty:any;

    addCessRate:number=0.0;
    discount:number=0.0;

    amount:number=0.0;

    itemTaxaxId:any;

    unitId:any;

    itemName:any;
    amountTendered:any




    batch:any;

    barcode:any;

    taxRate:number=0.0;

    mrp:number=0.0;

    itemCode:any;

    unitName:any;
    returnedQty:number=0.0;
    status:any;
    offerReferenceId:any;
    schemeId:any;

    standardPrice:any;
    costPrice:any;

    printKotStaus:any;
    updatedTime:any;
    fcAmount:any;
    fcRate:any;
    fcIgstRate:any;
    fcCgst:any;
    fcSgst:any;
    fcTaxRate:any;
    fcTaxAmount:any;
    fcCessRate:any;
    fcCessAmount:any;
    fcMrp:any;
    fcStandardPrice:any;
    fcDiscount:any;
    fcIgstAmount:any;
    igstTaxRate:number=0.0;
    listPrice:any;
    cessRate:any;
    rateBeforeDiscount:any;
    processInstanceId:any;
    taskId:any;
    salesTransHdr:SalesTransHdr=new SalesTransHdr;
    igstAmount:number=0.0;
    store:any;
    cgstAmount:number=0.0;
    sgstAmount:number=0.0;
    cessAmount:number=0.0;
    addCessAmount:number=0.0;


	    
}
