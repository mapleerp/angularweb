import { CompanyMst } from "./company-mst";
import { ItemMst } from "./item-mst";

export class StockTransferOutHdr {
    id: any;
    intentNumber: any;
    voucherDate: any;
    voucherNumber: any;
    voucherType: any;
    toBranch: any;
    deleted: any;
    fromBranch: any;
    inVoucherNumber: any;
    inVoucherDate: any;
    status: any;
    processInstanceId: any;
    taskId: any;
    postedToServer: any;
    companyMst: CompanyMst = new CompanyMst;
}
