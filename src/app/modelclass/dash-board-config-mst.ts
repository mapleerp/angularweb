import { CompanyMst } from "./company-mst";
export class DashBoardConfigMst {
    id:any;
    dashBoardName:any;
    reportName:any;
    userId:any;
    companyMst:CompanyMst = new CompanyMst;
}
