export class AccountBalanceReport {
  openingBalance: any;
  branchCode: any;

  branchAddress: any;
  branchPhone: any;

  branchName: any;
  companyName: any;
  branchEmail: any;
  branchWebsite: any;
  accountHeads: any;
  startDate: any;
  endDate: any;
  date: any;
  description: any;
  remark: any;
  debit: any;
  credit: any;
  slNo: any;

  vDate: any;

  openingBalanceType: any;
  closingBalanceType: any;
  ClosingBalance: any;

  accountClassId: any;
}
