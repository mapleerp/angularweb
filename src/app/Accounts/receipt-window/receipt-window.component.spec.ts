import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiptWindowComponent } from './receipt-window.component';

describe('ReceiptWindowComponent', () => {
  let component: ReceiptWindowComponent;
  let fixture: ComponentFixture<ReceiptWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReceiptWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
