import { Component, OnInit } from '@angular/core';
import { AccountHeads } from 'src/app/modelclass/account-heads';
import { ReceiptDtl } from 'src/app/modelclass/receipt-dtl';
import { ReceiptHdr } from 'src/app/modelclass/receipt-hdr';
import { ReceiptModeMst } from 'src/app/modelclass/receipt-mode-mst';
import { Supplier } from 'src/app/modelclass/supplier';
import { environment } from 'src/environments/environment.prod';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

// For Pop UP
import {
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
} from '@angular/material/dialog';
// Report
import * as XLSX from 'xlsx';
import { Subscription } from 'rxjs/internal/Subscription';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { SaleOrderPopupService } from 'src/app/services/sale-order-popup.service';
import { ReceiptReportInvoiceService } from 'src/app/services/receipt-report-invoice.service';
import { SupplierPopUpComponent } from 'src/app/Pop-Up-Windows/supplier-pop-up/supplier-pop-up.component';
@Component({
  selector: 'app-receipt-window',
  templateUrl: './receipt-window.component.html',
  styleUrls: ['./receipt-window.component.css']
})
export class ReceiptWindowComponent implements OnInit {

 
  
  // For Pop UP..............................................
  supp: Supplier = new Supplier(); 
  branchCode: any = environment.myBranch;
  rowseletion = false;
  receiveSupp: any;
  supName: any;
  suppGST: any;
  supID: any;
  supInvDate: any;
  ourVoucherDate: any;
  narrate: any;
  supInvNo: any;
  vouchNo: any;
  invTotal: any;
  invTot: number = 0;
  ponumber: any;
  selectSuppName: any;


  // For Mode of receipt Drop Down Menu..........................
  receiptMOdeMst: ReceiptModeMst[] = [];
  accountHeads: AccountHeads[]=[];
  // For Deposit Bank Drop Down Menu.............................
  receiptMode = '';
  // For Mode of receipt BKRY-CASH disable Bank,Number,date Drop Down Menu
  accountName:'';
  isDisabled=true;

// Final Save....................................................
  receiptDtl: ReceiptDtl = new ReceiptDtl();
  receiptHdr: ReceiptHdr = new ReceiptHdr();  
  dateConv:any;

  transdate: any;
  account: any;
  remark: any;
  amount: any;
  instnumber: any;
  supplierName: any;
  dateConvSec='';
  voucherNumber: any;
  receiptvouNumber: any;
  instrumentDate: any;
  date: string;
  totalAmount: any;

  // Report - PDF.......................................................
  sub: Subscription = new Subscription;
  pdf: string;
  id: any;
  // Report - EXCEL.......................................................
 

  constructor(
    // For pop Up
    public addDialog1: MatDialog,
    private sharedserviceService: SharedserviceService,
    private saleOrderPopupService: SaleOrderPopupService,
    private router: Router,
    public datepipe: DatePipe,
    public receiptReportServService:ReceiptReportInvoiceService

  ) {}

  ngOnInit(): void {

    // For Pop Up.................................................
    this.receiveSupplierDtls();
    //For Mode of receipt Drop down menu..........................//
    this.sharedserviceService
    .getAllReceiptModeWithoutCash()
    .subscribe((data) => {
      this.receiptMOdeMst = data;
    });
   // For Deposit Bank Drop Down Menu..............................
   this.sharedserviceService
   .fetchAccountHeadsByAccountName()
   .subscribe((data) => {
     this.accountHeads = data;    
   });  
  }





  // CALLING FUNCTIONS
  // For popup open .............................................
  onKeypressEventSupplier() {
    const dialogConfigAdd1 = new MatDialogConfig();
    dialogConfigAdd1.disableClose = false;
    dialogConfigAdd1.autoFocus = true;
    // dialogConfigAdd1.position = { right: `10px`, top: `20px` }
    dialogConfigAdd1.width = '80%';
    dialogConfigAdd1.height = '70%';

    this.addDialog1.open(SupplierPopUpComponent, dialogConfigAdd1);
    this.addDialog1.afterAllClosed.subscribe((result: any) => {
      this.supp = result;
      console.log('supplier popup window' + this.supp);
    });
  }
  receiveSupplierDtls() {
    console.log('receiveSupplierDtls');
    this.saleOrderPopupService.getSupplierDtls().subscribe((data: any) => {
      this.receiveSupp = data;
      this.supName = this.receiveSupp.accountName;
      this.suppGST = this.receiveSupp.partyGst;
      this.supID = this.receiveSupp.id;
      console.log(this.supName + 'supplier name');
    });
  }





  // For Mode of receipt BKRY-CASH disable Bank,Number,date Drop Down Menu
  hide(receiptMode:any){
    if(receiptMode==environment.myBranch+"-CASH"){
      this.isDisabled=false;
    }else{
      this.isDisabled=true;
    }   
  }





// Final Save.........................................................
  // Saving in table
    // Getting id from ReceiptHdr
    
      // Null check conding for Account, Date,Receipt
  finalSave(){

    if (this.supName == null) {
      alert('Please select the Account!!!');
      return;
    }
    if (this.dateConvSec == null) {
      alert('Please select the Date!!!');
      return;
    }
    if (this.receiptMode == null) {
      alert('Select the Mode of Receipt!!!');
      return;
    }

    // if (this.amount == null) {
    //   alert('Enter the amount!!!');
    //   return;
    // }

// Saving Hdr.....................
    console.log(" final save inside-------------------------- ")
   
      this.receiptHdr = new ReceiptHdr();     
      // FOR VOUCHER DATE
      this.date=this.datepipe.transform(this.dateConvSec , 'yyyy-MM-dd');
   
    console.log(this.dateConvSec+'date')
           
      this.receiptHdr.transdate=this.dateConvSec;
      this.receiptHdr.voucherDate=this.date;
      this.receiptHdr.branchCode=this.branchCode;
      this.receiptHdr.voucherNumber=this.voucherNumber;
 
      // this.receiptHdr.memberId
      // this.receiptHdr.oldId
      // this.receiptHdr.processInstanceId
      // this.receiptHdr.taskId      
      // this.receiptHdr.voucherType
      // this.receiptHdr.companyMst

      // Calling Hdr URL
      console.log(" receiptHdr save inside-------------------------- "+this.receiptHdr)
      this.sharedserviceService.saveReceiptHdr( this.receiptHdr).subscribe(
        (data) => {
          this.receiptHdr = data;
          this.receiptHdr.id = data.id;
          console.log(data);
          console.log(data.id);
          this.addReceiptDtl(this.receiptHdr);      
          console.log('Add receiptHdr data Send');
          this.updateHdr(this.receiptHdr.id);
          console.log(this.receiptHdr.id+"in final save for updation")
        },
        (error: any) => {
          console.log('Data not send');          
        }
      );
  }





  // Generating VOUCHER NUMBER INSIDE ReceiptHdr 
  updateHdr(id: any) {
    this.sharedserviceService.generateVoucherNumber("RECEIPT") .subscribe((data) => {
      
      this.receiptHdr.voucherNumber = data;      
      console.log("id hdr"+this.receiptHdr.id)

      this.sharedserviceService
      .updateReceiptHdr(this.receiptHdr, id)
      .subscribe((data) => {
        console.log('final result of receiptHdr hdr after updation' + data),
          (this.receiptHdr = data);

          //-------------receipt window Frontend Report - Start
          this.receiptReportServService.setReceiptReportRequest(this.receiptHdr);
 
          this.router.navigate(['ReceiptReport']);
          //-------------receipt window Frontend Report - End


          //-------------receipt window report
         
          // Final Save Message
          // if (!this.rowseletion) {
          //   alert('Receipt Saved');
          //   return;
          // }
      });  
    });   
  }





  // adding receiptDtl
  addReceiptDtl(receiptHdr: ReceiptHdr) {
    if (receiptHdr != null) {
      this.receiptDtl = new ReceiptDtl();
      this.receiptDtl.receipt_hdr_id = this.receiptHdr.id;
      console.log(this.receiptDtl.receipt_hdr_id + 'Header id');        
        
      this.receiptDtl.account = this.supID;
      this.receiptDtl.invoiceNumber=this.receiptvouNumber;
      this.receiptDtl.modeOfpayment = this.receiptMode;
      this.receiptDtl.transDate=this.dateConvSec;
      this.receiptDtl.instdate=this.dateConvSec;


      // debitAccountid - when selecting (Maple)Branch-CASH in Mode of Receipt Option Box
      if(this.receiptMode==environment.myBranch+"-CASH"){
        this.sharedserviceService.getAccountHeadByName(environment.myBranch+"-CASH").subscribe(
          data=>{
            this.receiptDtl.debitAccountId=data.id          
            console.log(this.receiptDtl.debitAccountId+ "receipt mode id");
            // VALUESETIINGINRECEIPTDL FUNCTION IS CALLED INSIDE THE SUBSCRIBE BRACKETS
            // SO THAT EXECUTION HAPPENS INSIDE SUBSCRIBE.

            // IF REMAINING COMMANDS PLACED OUTSIDE EXECUTION TERMINSTES
            // TO AVOID THAT REMAINING COMMANDS ARE PUT INSIDE FUNCTION
            // AND PLACED INSIDE THE SUBSCRIBE BRACKETS
            this.valueSettingInReceiptDtl();
          } 
         );        
         console.log(this.receiptDtl.debitAccountId+ "receipt mode id");


      // debitAccountid - when not selecting (Maple)Branch-CASH in Mode of Receipt Option Box
      }else{      
        this.sharedserviceService.getAccountHeadByName(this.accountName).subscribe(
          data=>{
            this.receiptDtl.debitAccountId=data.id
            console.log(data.id + "deposit bank id");
            // PLACE THE SAME FUNCTION INSIDE SUBSCRIBE BRACKET FOR SAME REASON
            this.valueSettingInReceiptDtl();
          } 
         );
         console.log(this.receiptDtl.debitAccountId + 'accountName id');       
      }              
    }  
  }

  
  // FUNCTION TO SET INSIDE (cash or non cash) CONDITION 

  // TO RUN REMAINING CODE INSIDE INSIDE SUBSCRIBE,
  // SO THAT EXECUTION DOES NOT END WHEN IT GOES OUT OF SUBSCRIBE BRACKETS
  // PLACE THIS CONDTION INSIDE SUBSCRIBE BRACKETS
  valueSettingInReceiptDtl() {
    this.receiptDtl.branchCode=this.branchCode;
    this.receiptDtl.amount = this.amount;
    this.receiptDtl.depositBank = this.accountName;
    this.receiptDtl.remark = this.remark;
    this.receiptDtl.instnumber = this.instnumber;
    this.receiptDtl.instrumentDate = this.instrumentDate;


    // Save receiptDtl after getting Hdr id
    this.sharedserviceService
      .saveReceiptDtl(this.receiptDtl, this.receiptHdr.id)
      .subscribe(
        (data) => {
          this.receiptDtl = data;           
          console.log(data);
          console.log('Add receiptDtl data Send');

               //Calling URL for total amount
        //   this.sharedserviceService
        //   .getSummaryReceiptTotal(this.receiptHdr. id)
        //  .subscribe((data) => {
        //     console.log('getSummaryReceiptTotal' + data),
        //      (this.totalAmount = data);
        //       });


        //   if (!this.rowseletion) {
        //     alert('Receipt Saved');
        //     return;
        //   }    
        // },
        // (error: any) => {
        //   console.log('Data not send');            
        // }

        
        }
      ); 
  }


  // CLEAR BUTTON - function..........................................
  clearAll() {
    this.account='';   
    this.vouchNo=0;
    this.amount='';  
    this.remark='';
    this.instnumber='';  
    this.instrumentDate='';
    // this.total=0;
    this.supp = new Supplier(); 
    this.branchCode= 0;
    this.receiveSupp= 0;
    this.supName= '';
    this.suppGST= 0;
    this.supID= 0;
    this.supInvDate= 0;
    this.ourVoucherDate= 0;
    this.narrate= 0;
    this.supInvNo= 0;
    this.vouchNo= 0;
    this.invTotal= 0;
    this.invTot= 0;
    this.ponumber= 0;
    this.selectSuppName= 0;
    this.receiptvouNumber='';
    this.dateConvSec='';
    this.receiptMode = '';
    this.accountName='';

    this.receiptDtl = new ReceiptDtl();
    this.receiptHdr = new ReceiptHdr();  
  }





   // Report - PDF...............................................
  pdfgenerate(id:any){
    this.sharedserviceService
    .receiptWindowPdfForWeb(id)
    .subscribe((data: any) => {
      console.log(data);   
      console.log('receipt print out --------------------');
    });
  }

  renderBackendPdf(){

    console.log("header id"+this.receiptHdr.id)
    this.pdfgenerate(this.receiptHdr.id);
  }
 
// Trial to generate through direct url
  // renderPdf(){
  //   this.id=this.receiptHdr.id;
  //   this.pdf = environment.hostURL +
  //     ':' +
  //     environment.port +
  //     '/' +
  //     environment.company +
  //     '/webreceiptwindowreportpdf?hdrid=' +
  //     this.id;
  // }

  renderFrontendPdf(){
    
  }
 



  // Report - EXCEL...............................................
  renderExcel()
  {
    {    
      let element=document.getElementById('contentToConvert');
      const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
      const wb:XLSX.WorkBook=XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb,ws,"sheet1");
      XLSX.writeFile(wb,"-"+this.voucherNumber+'-'+this.date+".xlsx")
    }
    // ngOnDestroy(): void {
    //   this.sub.unsubscribe();
    //   console.log('service unsubscribed..!!')
    //   }
  } 
}


