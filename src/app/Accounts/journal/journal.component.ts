import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AccountHeads } from 'src/app/modelclass/account-heads';
import { JournalDtl } from 'src/app/modelclass/journal-dtl';
import { JournalHdr } from 'src/app/modelclass/journal-hdr';
import { SupplierPopUpComponent } from 'src/app/Pop-Up-Windows/supplier-pop-up/supplier-pop-up.component';
import { SaleOrderPopupService } from 'src/app/services/sale-order-popup.service';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-journal',
  templateUrl: './journal.component.html',
  styleUrls: ['./journal.component.css']
})
export class JournalComponent implements OnInit {
  date:any;
  crAccount:any;
  drAccount:any;
  amount:any;
  remarks:any;

  receiveCreditAccount: any;
  receiveDeditAccount:any;
  accountType:any;
  supName:any;

  drTotal:any;
  crTotal:any;

  cash="CASH";
  pettyCash=environment.myBranch+"-PETTY CASH";
  branchCash=environment.myBranch+"-CASH";

  journalHdr:JournalHdr=new JournalHdr;
  journalDtl:JournalDtl=new JournalDtl;

  dateCov:any;
  accoutHeads:AccountHeads=new AccountHeads;
  accounts:AccountHeads[]=[];
  journalDtlArr:JournalDtl[]=[];
  journalDtlArray:JournalDtl[]=[];

  constructor( public addDialog1: MatDialog,
    private sharedserviceService: SharedserviceService,
    private saleOrderPopupService: SaleOrderPopupService,
    public datepipe: DatePipe) { }

  ngOnInit(): void {
    this.sharedserviceService.showAllAccounts().subscribe((data:any)=>{
      this.accounts=data;
    })
    this.receiveAccountHeads();
  }

  onKeypressEvent(type:any) {
    this.accountType=type;
    const dialogConfigAdd1 = new MatDialogConfig();
    dialogConfigAdd1.disableClose = false;
    dialogConfigAdd1.autoFocus = true;
    // dialogConfigAdd1.position = { right: `10px`, top: `20px` }
    dialogConfigAdd1.width = '80%';
    dialogConfigAdd1.height = '70%';
    this.addDialog1.open(SupplierPopUpComponent, dialogConfigAdd1);
    console.log( this.accountType);
  }

  receiveAccountHeads(){
      console.log('receive credit account');
    this.saleOrderPopupService.getSupplierDtls().subscribe((data: any) => {
       if(this.accountType=='CR'){
      this.receiveCreditAccount = data;
      this.crAccount=this.receiveCreditAccount.accountName;
      console.log(this.crAccount + 'credit account');
    }
    if(this.accountType=='DR'){
      this.receiveDeditAccount = data;
      this.drAccount=this.receiveDeditAccount.accountName;
      console.log(this.drAccount + 'debit account');
    }
    });
  }

  saveJournal(){
    console.log(this.crAccount.trim().toLowerCase());
    if(this.crAccount.trim().toLowerCase()==this.cash.toLowerCase()){
      alert("Cash entry is not possible");
      return;
    }

    if(this.drAccount.trim().toLowerCase()==this.cash.toLowerCase()){
      alert("Cash entry is not possible");
      return;
    }

    if(this.crAccount.trim().toLowerCase()==this.pettyCash.toLowerCase()){
      alert("Cash entry is not possible");
      return;
    }

    if(this.drAccount.trim().toLowerCase()==this.pettyCash.toLowerCase()){
      alert("Cash entry is not possible");
      return;
    }

    if(this.crAccount.trim().toLowerCase()==this.branchCash.toLowerCase()){
      alert("Cash entry is not possible");
      return;
    }

    if(this.drAccount.trim().toLowerCase()==this.branchCash.toLowerCase()){
      alert("Cash entry is not possible");
      return;
    }

    if(this.date==null){
      alert("Please Select Date!!");
      return;
    }

    if(this.crAccount==null||this.drAccount==null){
      alert("Please Select Account!!");
      return;
    }

    if(this.amount==null){
      alert("Please Enter Credit/Debit  Amount!!");
      return;
    }

    console.log("header in save function")
    console.log(this.journalHdr)
    if(this.journalHdr.id==null){
      this.journalHdr=new JournalHdr;

      // this.dateCov= this.datepipe.transform(this.date, 'dd-MM-yyyy');
      this.journalHdr.voucherDate=this.date;
      this.journalHdr.branchCode=environment.myBranch;
      this.journalHdr.transDate=this.dateCov;

      this.sharedserviceService.saveJournalHdr(this.journalHdr).subscribe((data:any)=>{
        this.journalHdr=data;
        console.log("Header data after save......")
        console.log(this.journalHdr);
        this.saveJournalDetails();
      })

    }
    else{
      this.saveJournalDetails();
    }


  }

  saveJournalDetails(){
    this.journalDtl=new JournalDtl();
    this.journalDtl.journalHdr=this.journalHdr;

    this.sharedserviceService.getAccountHeadByName(this.crAccount).subscribe((account:any)=>{
      console.log("Account id and name "+account.id+"  "+account.accountName);

    //setting and saving credit account values
      this.journalDtl.accountHead=account.id;
      this.journalDtl.creditAmount=this.amount;
      this.journalDtl.debitAmount=0.0;
      this.journalDtl.remarks=this.remarks;

      this.sharedserviceService.saveJournalDtl(this.journalHdr.id,this.journalDtl).subscribe((dtl:any)=>{
        this.journalDtl=dtl;
        console.log("sales dtl credit account after save......")
        console.log(this.journalDtl);
        this.journalDtlArr.push(this.journalDtl);
        //set total
        this.setTotal();
        //setting and saving debit account values
        this.sharedserviceService.getAccountHeadByName(this.drAccount).subscribe((account:any)=>{
          console.log("Account id and name "+account.id+"  "+account.accountName);
        this.journalDtl=new JournalDtl();
        this.journalDtl.journalHdr=this.journalHdr;
        this.journalDtl.accountHead=account.id;
        this.journalDtl.creditAmount=0.0;
        this.journalDtl.debitAmount=this.amount;
        this.journalDtl.remarks=this.remarks;

        this.sharedserviceService.saveJournalDtl(this.journalHdr.id,this.journalDtl).subscribe((dtl:any)=>{
          this.journalDtl=dtl;
          console.log("sales dtl credit account after save......")
          console.log(this.journalDtl);
          this.journalDtlArr.push(this.journalDtl);
          //set total
          this.setTotal();
          this.clearAfterSave();
        });
        });
      })
    });
  }

  setTotal(){
    this.sharedserviceService.getJournalDebitTotal(this.journalHdr.id).subscribe((data:any)=>{
      this.drTotal=data;
    });
   this.sharedserviceService.getJournalCreditTotal(this.journalHdr.id).subscribe((data:any)=>{
      this.crTotal=data;
    });
  }

  finalSubmit(){
    // this.sharedserviceService.getJournalDtl(this.journalHdr).subscribe((data:any)=>{
    //   this.journalDtlArray=data;
    // })
    if(this.drTotal==this.crTotal){

      console.log('---- get current financial year----2022-2023------');
      var fiscalyear = "";
      var today = new Date();
      if ((today.getMonth() + 1) <= 3) {
        fiscalyear = (today.getFullYear() - 1) + "-" + today.getFullYear()
      } else {
        fiscalyear = today.getFullYear() + "-" + (today.getFullYear() + 1)
      }
      console.log(fiscalyear);

  this.sharedserviceService.generateVoucherNumber(fiscalyear+"JOURNAL").subscribe((data:any)=>{
    this.journalHdr.voucherNumber=data;
    this.sharedserviceService.updateJournalFinalSave(this.journalHdr).subscribe((data:any)=>{
      console.log('update journal')
      console.log(data);
      this.journalHdr=data;
    })
  })
    }
    else{
      alert('Credit and Debit Amount Total should be Equal!!!')
    }
  }

  clearAfterSave(){
    this.crAccount=null;
    this.drAccount=null;
    this.amount=null;
    this.remarks=null;
  }
  clearAllAfterFinalSave(){
    this.journalDtlArr.length=0; 
    this.drTotal=null;
    this.crTotal=null;
    this.journalHdr=new JournalHdr;
   }

   //for displaying accountName in table
   getAccountNameById(num: any) {
    const account = this.accounts.find((data) => data.id === num);
    if (!account) {
      // we not found the parent
      return '';
    }
    return account.accountName;
  }
}
