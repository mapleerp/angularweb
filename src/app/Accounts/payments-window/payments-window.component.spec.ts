import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentsWindowComponent } from './payments-window.component';

describe('PaymentsWindowComponent', () => {
  let component: PaymentsWindowComponent;
  let fixture: ComponentFixture<PaymentsWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentsWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentsWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
