import { Component, OnInit } from '@angular/core';

// For Supplier pop Up
import {
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
} from '@angular/material/dialog';
import { Supplier } from 'src/app/modelclass/supplier';
import { SupplierPopUpComponent } from 'src/app/Pop-Up-Windows/supplier-pop-up/supplier-pop-up.component';
import { SaleOrderPopupService } from 'src/app/services/sale-order-popup.service';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { environment } from 'src/environments/environment.prod';

// For Mode of Payment Drop Down Menu
import { AccountHeads } from 'src/app/modelclass/account-heads';
import { ReceiptModeMst } from 'src/app/modelclass/receipt-mode-mst';

// Final Save

// Saving Hdr
import { PaymentHdr } from 'src/app/modelclass/payment-hdr';
import { PaymentDtl } from 'src/app/modelclass/payment-dtl';
import { DatePipe } from '@angular/common';


// Payment Report
import { PaymentReportInvoiceService } from 'src/app/services/payment-report-invoice.service';
import { Router } from '@angular/router';




@Component({
  selector: 'app-payments-window',
  templateUrl: './payments-window.component.html',
  styleUrls: ['./payments-window.component.css']
})
export class PaymentsWindowComponent implements OnInit {

   // For Supplier pop Up..............................................
   supp: Supplier = new Supplier(); 
   branchCode: any = environment.myBranch;
   rowseletion = false;
   receiveSupp: any;
   supName: any;
   suppGST: any;
   supID: any;
   supInvDate: any;
   ourVoucherDate: any;
   narrate: any;
   supInvNo: any;
   vouchNo: any;
   invTotal: any;
   invTot: number = 0;
   ponumber: any;
   selectSuppName: any;

  // For Mode of payment BKRY-CASH disable Bank,Number,date Drop Down Menu
  isDisabled=true;
  accountName:'';

  // Final Save.........................................
  
  // Null check
  dateConvSec='';
  paymentMode = '';
  paymentvouNumber:any;


  // For Mode of payment Drop Down Menu..........................
  receiptModeMst: ReceiptModeMst[] = [];
  accountHeads: AccountHeads[]=[];
  date: string;

  // Saving Hdr
  paymentHdr: PaymentHdr = new PaymentHdr();
  paymentDtl: PaymentDtl = new PaymentDtl();  


  
  // Saving Dtl
  instnumber: any;
  remark: any;
  amount: any;
  instrumentNumber: any;
  instrumentDate: any;
  
 



  constructor(
     // For Supplier pop Up
     public addDialog1: MatDialog,
     private sharedserviceService: SharedserviceService,
     private saleOrderPopupService: SaleOrderPopupService,
    //  Final Save

    // Saving Hdr
    public datepipe: DatePipe,

    // Payement Report Front-End
    public paymentReportService: PaymentReportInvoiceService,
    private router: Router,
    
  ) { }

  ngOnInit(): void {

    // For Supplier Pop Up.................................................
    this.receiveSupplierDtls();
    //For Mode of receipt Drop down menu..........................//
    this.sharedserviceService
    .getAllReceiptModeWithoutCash()
    .subscribe((data) => {
      this.receiptModeMst = data;
    });
   // For Deposit Bank Drop Down Menu..............................
   this.sharedserviceService
   .fetchAccountHeadsByAccountName()
   .subscribe((data) => {
     this.accountHeads = data;    
   }); 
  }

  // For popup open .............................................
  onKeypressEventSupplier() {
    const dialogConfigAdd1 = new MatDialogConfig();
    dialogConfigAdd1.disableClose = false;
    dialogConfigAdd1.autoFocus = true;
    // dialogConfigAdd1.position = { right: `10px`, top: `20px` }
    dialogConfigAdd1.width = '80%';
    dialogConfigAdd1.height = '70%';

    this.addDialog1.open(SupplierPopUpComponent, dialogConfigAdd1);
    this.addDialog1.afterAllClosed.subscribe((result: any) => {
      this.supp = result;
      console.log('supplier popup window' + this.supp);
    });
  }
  receiveSupplierDtls() {
    console.log('receiveSupplierDtls');
    this.saleOrderPopupService.getSupplierDtls().subscribe((data: any) => {
      this.receiveSupp = data;
      this.supName = this.receiveSupp.accountName;
      this.suppGST = this.receiveSupp.partyGst;
      this.supID = this.receiveSupp.id;
      console.log(this.supName + 'supplier name');
    });
  }





  // For Mode of payment BKRY-CASH disable Bank,Number,date Drop Down Menu
  hide(paymentMode:any){
    if(paymentMode==environment.myBranch+"-CASH"){
      this.isDisabled=false;
    }else{
      this.isDisabled=true;
    }   
  }





  // Null check 
  finalSave(){
    if (this.supName == null) {
      alert('Please select the Account!!!');
      return;
    }
    if (this.dateConvSec == null) {
      alert('Please select the Date!!!');
      return;
    }
    if (this.paymentMode == null) {
      alert('Select the Mode of Payment!!!');
      return;
    }



      // Saving Hdr.....................
      console.log(" final save inside-------------------------- ")
      
        this.paymentHdr = new PaymentHdr();     
        // FOR VOUCHER DATE
        this.date=this.datepipe.transform(this.dateConvSec , 'yyyy-MM-dd');

      console.log(this.dateConvSec+'date')
          
        // this.paymentHdr.transdate=this.dateConvSec;
        this.paymentHdr.voucherDate=this.date;
        this.paymentHdr.branchCode=this.branchCode;
        this.paymentHdr.voucherNumber=this.paymentvouNumber;
    
      
        console.log(" paymentHdr save inside-------------------------- "+this.paymentHdr)
          this.sharedserviceService.savePaymentHdr( this.paymentHdr).subscribe(
            (data) => {
              this.paymentHdr = data;
              this.paymentHdr.id = data.id;
              console.log(data);
              console.log(data.id);
              this.addPaymentDtl(this.paymentHdr);      
              console.log('Add paymentHdr data Send');
              this.updateHdr(this.paymentHdr.id);
              console.log(this.paymentHdr.id+"in final save for updation")
            },
            (error: any) => {
              console.log('Data not send');          
            }
          );
  }

  updateHdr(id: any) {
    this.sharedserviceService.generateVoucherNumber("PYN")
    .subscribe((data) => {
      
      this.paymentHdr.voucherNumber = data;      
      console.log("id hdr"+this.paymentHdr.id)

        this.sharedserviceService.updatePaymentHdr(this.paymentHdr, id)
        .subscribe((data) => {
          console.log('final result of paymentHdr hdr after updation' + data),
            (this.paymentHdr = data);

            //-------------Payment window Frontend Report - Start
            this.paymentReportService.setPaymentReportRequest(this.paymentHdr);  
            this.router.navigate(['PaymentsBill']);
            //-------------Payment window Frontend Report - End
         
        // Final Save Message
          // if (!this.rowseletion) {
          //   alert('Payment Saved');
          //   return;
          // }
      });  
    });   
  }
  addPaymentDtl(paymentHdr: PaymentHdr) {
    if (paymentHdr != null) {
      this.paymentDtl = new PaymentDtl();
      this.paymentDtl.paymenthdr_id = this.paymentHdr.id;
      console.log(this.paymentDtl.paymenthdr_id + 'Header id');        
        
      this.paymentDtl.accountId = this.supID;
      // this.paymentDtl.invoiceNumber=this.paymentvouNumber;
      this.paymentDtl.modeOfPayment = this.paymentMode;
      this.paymentDtl.transDate=this.dateConvSec;
      this.paymentDtl.instrumentDate=this.dateConvSec;


      // debitAccountid - when selecting (Maple)Branch-CASH in Mode of Payment Option Box
      if(this.paymentMode==environment.myBranch+"-CASH"){
        this.sharedserviceService.getAccountHeadByName(environment.myBranch+"-CASH").subscribe(
          data=>{
            this.paymentDtl.creditAccountId=data.id          
            console.log(this.paymentDtl.creditAccountId+ "payment mode id");
            // VALUESETIINGIPAYMENTDL FUNCTION IS CALLED INSIDE THE SUBSCRIBE BRACKETS
            // SO THAT EXECUTION HAPPENS INSIDE SUBSCRIBE.

            // IF REMAINING COMMANDS PLACED OUTSIDE EXECUTION TERMINSTES
            // TO AVOID THAT REMAINING COMMANDS ARE PUT INSIDE FUNCTION
            // AND PLACED INSIDE THE SUBSCRIBE BRACKETS
            this.valueSettingInPaymentDtl();
          } 
         );        
         console.log(this.paymentDtl.creditAccountId+ "payment mode id");


      // debitAccountid - when not selecting (Maple)Branch-CASH in Mode of Payment Option Box
      }else{      
        this.sharedserviceService.getAccountHeadByName(this.accountName).subscribe(
          data=>{
            this.paymentDtl.creditAccountId=data.id
            console.log(data.id + "deposit bank id");
            // PLACE THE SAME FUNCTION INSIDE SUBSCRIBE BRACKET FOR SAME REASON
            this.valueSettingInPaymentDtl();
          } 
         );
         console.log(this.paymentDtl.creditAccountId + 'accountName id');       
      }              
    }  
  }


  valueSettingInPaymentDtl() {
    // this.paymentDtl.branchCode=this.branchCode;
    this.paymentDtl.amount = this.amount;
    this.paymentDtl.bankAccountNumber = this.accountName;
    this.paymentDtl.remark = this.remark;
    this.paymentDtl.instrumentNumber = this.instrumentNumber;
    this.paymentDtl.instrumentDate = this.instrumentDate;

     // Save paymentDtl after getting Hdr id
     this.sharedserviceService
     .savepaymentDtl(this.paymentDtl, this.paymentHdr.id)
     .subscribe(
       (data) => {
         this.paymentDtl = data;           
         console.log(data);
         console.log('Add paymentDtl data Send');       
       }
     ); 
  }

  clearAll() {
    this.accountName='';   
    this.paymentvouNumber=0;
    this.amount='';  
    this.remark='';
    this.instrumentNumber='';  
    this.instrumentDate='';
    // this.total=0;
    this.supp = new Supplier(); 
    this.branchCode= 0;
    this.receiveSupp= 0;
    this.supName= '';
    this.suppGST= 0;
    this.supID= 0;
    this.supInvDate= 0;
    this.ourVoucherDate= 0;
    this.narrate= 0;
    this.supInvNo= 0;
    this.vouchNo= 0;
    this.invTotal= 0;
    this.invTot= 0;
    this.ponumber= 0;
    this.selectSuppName= 0;
    this.paymentvouNumber='';
    this.dateConvSec='';
    this.paymentMode = '';
    this.accountName='';

    this.paymentDtl = new PaymentDtl();
    this.paymentHdr = new PaymentHdr(); 
  }
}
