import { DatePipe } from '@angular/common';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { Subscription } from 'rxjs';
import { DayEndWebReport } from '../modelclass/day-end-web-report';
import { SharedserviceService } from '../services/sharedservice.service';

@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.component.html',
  styleUrls: ['./dash-board.component.css']
})
export class DashBoardComponent implements OnInit {

  yesterdayDate:any
    dayEndWebReport:DayEndWebReport[]=[];
    getReportStatus=false;
    getexcelReportStatus=false;
    value:any;
    dataArrayInput:number[]=[];
    dataweek1:number[]=[];
    dataweek2:number[]=[];
    dataweek3:number[]=[];
    dataweek4:number[]=[];
    dataweek5:number[]=[];
    dataweek6:number[]=[];
    dataweek7:number[]=[];
   
    sub: Subscription = new Subscription;
    public chartType = <const>'bar';
    // public chartLabel1: string ='';
    // public chartLabel2: string ='';
    // public chartLabel3: string ='';
    // public chartLabel4: string ='';
    // public chartLabel5: string ='';
    // public chartLabel6: string ='';
    // public chartLabel7: string ='';
    // public chartLabel: string ='';
    // public labelArray: string[]=[];
    // public labelArray1: string[]=[];
    // public labelArray2: string[]=[];
    // public labelArray3: string[]=[];
    // public labelArray4: string[]=[];
    // public labelArray5: string[]=[];
    // public labelArray6: string[]=[];
    // public labelArray7: string[]=[];
  public dataArray: number[] = this.dataArrayInput;
  public dataArray1: number[] = this.dataweek1;
  


  barChartOptions: ChartOptions = {
    responsive: true,
  };
  barChartLabels: Label[] =['KDRH','KDRM','KDRR','KLNR','KTRA','KTRA2','KTYM2','KTYM3','PNTH'];
  // barChartLabels1: Label[] =this.labelArray1;
  // barChartLabels2: Label[] =this.labelArray2;
  // barChartLabels3: Label[] =this.labelArray3;
  // barChartLabels4: Label[] =this.labelArray4;
  // barChartLabels5: Label[] =this.labelArray5;
  // barChartLabels6: Label[] =this.labelArray6;
  // barChartLabels7: Label[] =this.labelArray7;
  // public chartDatasets1: Array<{data:any | number[], label:any}> =   [    {data: this.dataweek1, label:this.chartLabel1},  ];
  // public chartDatasets2: Array<{data:any | number[], label:string}> = [  {data: this.dataweek2, label:  this.chartLabel2},  ];
  // public chartDatasets3: Array<{data:any | number[], label:string}> = [  {data: this.dataweek3, label:  this.chartLabel3},  ];
  // public chartDatasets4: Array<{data:any | number[], label:string}> = [  {data: this.dataweek4, label:  this.chartLabel4},  ];
  // public chartDatasets5: Array<{data:any | number[], label:string}> = [  {data: this.dataweek5, label:  this.chartLabel5},  ];
  // public chartDatasets6: Array<{data:any | number[], label:string}> = [  {data: this.dataweek6, label:  this.chartLabel6},  ];
  // public chartDatasets7: Array<{data:any | number[], label:string}> = [  {data: this.dataweek7, label:  this.chartLabel7},  ];

  barChartType: ChartType = 'bar';
  barChartLegend = true;

  barChartPlugins = [];
  
  

  public chartDatasets: Array<{data:any | number[], label:string}> = [
    {data: this.dataweek1, label: 'Day 1'},
    {data: this.dataweek2, label: 'Day 2'},
    {data: this.dataweek3, label: 'Day 3'},
    {data: this.dataweek4, label: 'Day 4'},
    {data: this.dataweek5, label: 'Day 5'},
    {data: this.dataweek6, label: 'Day 6'},
    {data: this.dataweek7, label: 'Day 7'},
  ];



  constructor(private router: Router,private sharedserviceService:SharedserviceService,
    public datepipe: DatePipe) { }

  ngOnInit(): void {
    console.log('Initialized')
    this.graphicalDayEndDashBoard();
  }

  findsum(data: any)
  {
  this.value=data;
   let j=0;
  for( j=0;j<data.length;j++){
     
    this.dataArrayInput[j]=this.value[j].physicalCash;
   // this.labelArray[j]=this.value[j].branchCode;
    console.log('data collected')
  
    }
 
    console.log('dataarrayinput-'+this.dataArrayInput)
  console.log('data collected')

}

graphicalDayEndDashBoard()
{
  
//   for( let i=1;i<=7;i++)
//   {
//     this.dataArrayInput.length=0;
//    // this.labelArray.length=0;
//     let currentDate = new Date();
//   currentDate.setDate(currentDate.getDate()-i);
//   this.yesterdayDate = this.datepipe.transform(currentDate, 'y-MM-dd')
//   console.log(this.yesterdayDate)
//  this.sharedserviceService.getDayEndReports(this.yesterdayDate).
// subscribe(data => {this.dayEndWebReport=data ; this.findsum(this.dayEndWebReport); 
// console.log(this.dayEndWebReport[6]);
// if(this.dayEndWebReport!=null)
// {
//   this.getReportStatus=false;
//   this.getexcelReportStatus=true;
 
//  if(i==1)
//  {
//   // this.labelArray1.length=0;
//    this.dataweek1.length=0;
//   let j=0;
//   for( j=0;j<this.dataArrayInput.length;j++){
//    this.dataweek1[j]=this.dataArrayInput[j];
//    //this.labelArray1[j]=this.labelArray[j];
//   // this.chartLabel1=this.yesterdayDate;
//   }
//    console.log('One Data'+this.dataweek1);
//    //console.log('One label'+this.labelArray1);
//    //console.log('yestrday'+this.chartLabel1);
//  }
//  if(i==2)
//  {
//   //this.labelArray2.length=0;
//   this.dataweek2.length=0;
//   let j=0;
//   for( j=0;j<this.dataArrayInput.length;j++){
//    this.dataweek2[j]=this.dataArrayInput[j];
//   // this.labelArray2[j]=this.labelArray[j];
//   // this.chartLabel2=this.yesterdayDate;
//   }
//    console.log('2 Data'+this.dataweek2);
//    //console.log('2 label'+this.labelArray2);
//  }
//  if(i==3)
//  {
//  // this.labelArray3.length=0;
//   this.dataweek3.length=0;
//   let j=0;
//   for( j=0;j<this.dataArrayInput.length;j++){
//    this.dataweek3[j]=this.dataArrayInput[j];
//    //this.labelArray3[j]=this.labelArray[j];
//   // this.chartLabel3=this.yesterdayDate;
//   }
//    console.log('3 Data'+this.dataweek3);
//   // console.log('3 label'+this.labelArray3);
//  }
//  if(i==4)
//  {
//   let j=0;
//  // this.labelArray4.length=0;
//   this.dataweek4.length=0;
//   for( j=0;j<this.dataArrayInput.length;j++){
//    this.dataweek4[j]=this.dataArrayInput[j];
//   // this.labelArray4=this.labelArray;
//   // this.chartLabel4=this.yesterdayDate;
//   }
//    console.log('4 Data'+this.dataweek4);
//    //console.log('4 label'+this.labelArray4);
//  }
//  if(i==5)
//  {
//   let j=0;
//   //this.labelArray5.length=0;
//   this.dataweek5.length=0;
//   for( j=0;j<this.dataArrayInput.length;j++){
//    this.dataweek5[j]=this.dataArrayInput[j];
//   // this.labelArray5=this.labelArray;
//    //this.chartLabel5=this.yesterdayDate;
//   }
//    console.log('5 Data'+this.dataweek5);
//    //console.log('5 label'+this.labelArray5);
//  }
//  if(i==6)
//  {
//   let j=0;
//   //this.labelArray6.length=0;
//   this.dataweek6.length=0;
//   for( j=0;j<this.dataArrayInput.length;j++){
//    this.dataweek6[j]=this.dataArrayInput[j];
//    //this.labelArray6[j]=this.labelArray[j];
//    //this.chartLabel6=this.yesterdayDate;
//   }
//    console.log('6 Data'+this.dataweek6);
//   // console.log('6 label'+this.labelArray6);
//  }
//  if(i==7)
//  {
//   let j=0;
//   //this.labelArray7.length=0;
//   this.dataweek7.length=0;
//   for( j=0;j<this.dataArrayInput.length;j++){
//    this.dataweek7[j]=this.dataArrayInput[j];
//    //this.labelArray7[j]=this.labelArray[j];
//   // this.chartLabel7=this.yesterdayDate;
//   }
//    console.log('7 Data'+this.dataweek7);
//   // console.log('7 label'+this.labelArray7);
//  }

  
//  }})


     

//   console.log('service completed')
//   console.log('array'+this.dataweek1)

//   } 
 
  
}


}
