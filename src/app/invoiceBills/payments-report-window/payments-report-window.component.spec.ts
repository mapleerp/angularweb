import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentsReportWindowComponent } from './payments-report-window.component';

describe('PaymentsReportWindowComponent', () => {
  let component: PaymentsReportWindowComponent;
  let fixture: ComponentFixture<PaymentsReportWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentsReportWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentsReportWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
