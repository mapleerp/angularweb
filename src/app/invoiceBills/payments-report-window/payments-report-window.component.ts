import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountHeads } from 'src/app/modelclass/account-heads';
import { CompanyMst } from 'src/app/modelclass/company-mst';
import { PaymentVoucher } from 'src/app/modelclass/payment-voucher';
import { PaymentReportInvoiceService } from 'src/app/services/payment-report-invoice.service';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-payments-report-window',
  templateUrl: './payments-report-window.component.html',
  styleUrls: ['./payments-report-window.component.css']
})
export class PaymentsReportWindowComponent implements OnInit {

  
  voucherNO: any;
  voucherDate: any;
  voucherNumber: any;
  branchcode: any;
  date: any;

  accountName: any;
  modeOfPayment: any;
  instrumentNumber: any;
  instrumentDate: any;
  remark: any;
  amount: any;
  // paymentHdr: PaymentHdr=new PaymentHdr;
  // receiptDtlArray:ReceiptDtl[]=[];
  paymentVoucher:PaymentVoucher[]=[]; 

  constructor(private router: Router, private sharedserviceService: SharedserviceService,
    private paymentReportInvoiceService: PaymentReportInvoiceService, public datepipe: DatePipe,
    ) { }

  accountHeads:AccountHeads=new AccountHeads;
  companyMst:CompanyMst=new CompanyMst;

  ngOnInit(): void {
    this.voucherNumber=this.paymentReportInvoiceService.voucherNO;   
    this.branchcode=this.paymentReportInvoiceService.branchcode;
      console.log("voucher date "+this.voucherDate)

    this.voucherDate= this.datepipe.transform(this.paymentReportInvoiceService.voucherDate , 'yyyy-MM-dd');

    this.sharedserviceService.paymentReportPdf(this.voucherNumber,this.branchcode,this.voucherDate).subscribe((data:any)=>{
      this.paymentVoucher=data;
      console.log(this.paymentVoucher);
    })
  }
  click()
  {
    window.print();
  }
  close(){ 
    history.back();
  }

}
