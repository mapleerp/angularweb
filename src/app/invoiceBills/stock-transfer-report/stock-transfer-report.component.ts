import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CompanyMst } from 'src/app/modelclass/company-mst';
import { StockTransferOutReport } from 'src/app/modelclass/stock-transfer-out-report';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { StockTransferInvoiceService } from 'src/app/services/stock-transfer-invoice.service';

@Component({
  selector: 'app-stock-transfer-report',
  templateUrl: './stock-transfer-report.component.html',
  styleUrls: ['./stock-transfer-report.component.css']
})
export class StockTransferReportComponent implements OnInit {
  voucherDate:  any;
  tobranch:  any;

 
  stockRequestDtl: StockTransferOutReport [] = [];
  showAllStockTransferDtl: StockTransferOutReport[]=[];
  constructor(public stockTransferInvoiceService:StockTransferInvoiceService,
    public sharedserviceService:SharedserviceService,
    private router: Router) { }
  companyMst:CompanyMst=new CompanyMst;
  billInvoiceNumber:any;
  invoiceBillDate:any;
  totalAmount=0;
  ngOnInit(): void {

    this.billInvoiceNumber=this.stockTransferInvoiceService.voucherNumber;
    this.voucherDate=this.stockTransferInvoiceService.voucherDate;
    console.log(this.voucherDate);
    this.tobranch=this.stockTransferInvoiceService.toBranch;
    // this.showAllStockRequestDtl=this.stockTransferInvoiceService.getStockRequestArray();
    this.sharedserviceService.getStockTransferReport(this.billInvoiceNumber, this.voucherDate).subscribe(
      data=>{
        this.showAllStockTransferDtl = data;
        console.log(data)
        console.log("report data"+this.showAllStockTransferDtl)
        for(let i=0;i<this.showAllStockTransferDtl.length;i++){
          this.totalAmount=this.totalAmount+this.showAllStockTransferDtl[i].amount;
        }});
    console.log(this.showAllStockTransferDtl);
    
    
  }
  click()
  {
    window.print();
  }
  close(){
   
    history.back();
  }
}
