import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockTransferOutReportPrintSummaryComponent } from './stock-transfer-out-report-print-summary.component';

describe('StockTransferOutReportPrintSummaryComponent', () => {
  let component: StockTransferOutReportPrintSummaryComponent;
  let fixture: ComponentFixture<StockTransferOutReportPrintSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockTransferOutReportPrintSummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockTransferOutReportPrintSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
