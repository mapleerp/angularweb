import { Component, OnInit } from '@angular/core';
import { StockTransferOutReport } from 'src/app/modelclass/stock-transfer-out-report';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-stock-transfer-out-report-print-summary',
  templateUrl: './stock-transfer-out-report-print-summary.component.html',
  styleUrls: ['./stock-transfer-out-report-print-summary.component.css']
})
export class StockTransferOutReportPrintSummaryComponent implements OnInit {
  stockTransferOutReport:StockTransferOutReport[]=[];
  fdate:any;
  tdate:any;
  totalAmount:number=0.0;
  constructor(private sharedserviceService: SharedserviceService) { }

  ngOnInit(): void {
    // this.stockTransferOutReport=history.state;
    this.fdate=this.sharedserviceService.getFromDateForReport();
    this.tdate=this.sharedserviceService.getToDateForReport();
    console.log('from date: '+this.fdate);
    console.log('to date: '+this.tdate);

    this.sharedserviceService.stockOutReportSummaryBetweenDate(this.fdate,this.tdate).subscribe((data:any)=>{
      console.log('Print Summary of Stock Transfer Out Report');
      console.log(data);
      this.stockTransferOutReport=data;
      for(let i=0;i<this.stockTransferOutReport.length;i++){
        this.totalAmount=this.totalAmount+this.stockTransferOutReport[i].amount;
      }
    })
  }
  click(){
    window.print();
  }

  close(){
    history.back();
  }

}
