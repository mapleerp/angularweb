import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CompanyMst } from 'src/app/modelclass/company-mst';
import { StockRequestReport } from 'src/app/modelclass/stock-request-report';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { StockRequestInvoiceService } from 'src/app/services/stock-request-invoice.service';

@Component({
  selector: 'app-stock-request-report',
  templateUrl: './stock-request-report.component.html',
  styleUrls: ['./stock-request-report.component.css']
})
export class StockRequestReportComponent implements OnInit {
  voucherDate:  any;
  tobranch:  any;

 
  stockRequestDtl: StockRequestReport [] = [];
  showAllStockRequestDtl: StockRequestReport[]=[];
  constructor(public stockRequestInvoiceService:StockRequestInvoiceService,
    public sharedserviceService:SharedserviceService,
    private router: Router) { }
  companyMst:CompanyMst=new CompanyMst;
  billInvoiceNumber:any;
  invoiceBillDate:any;
  totalAmount=0;
  ngOnInit(): void {

    this.billInvoiceNumber=this.stockRequestInvoiceService.voucherNumber;
    this.voucherDate=this.stockRequestInvoiceService.voucherDate;
    console.log(this.voucherDate);
    this.tobranch=this.stockRequestInvoiceService.toBranch;
    // this.showAllStockRequestDtl=this.stockRequestInvoiceService.getStockRequestArray();
    this.sharedserviceService.getStockRequestReport(this.billInvoiceNumber, this.voucherDate).subscribe(
      data=>{
        this.showAllStockRequestDtl = data;
        console.log(data)
        console.log("report data"+this.showAllStockRequestDtl)
        for(let i=0;i<this.showAllStockRequestDtl.length;i++){
          this.totalAmount=this.totalAmount+this.showAllStockRequestDtl[i].amount;
        }});
    console.log(this.showAllStockRequestDtl);
    
    
  }
  click()
  {
    window.print();
  }
  close(){
   
    history.back();
  }
}
