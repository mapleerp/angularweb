import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountHeads } from 'src/app/modelclass/account-heads';
import { CompanyMst } from 'src/app/modelclass/company-mst';
import { ReceiptDtl } from 'src/app/modelclass/receipt-dtl';
import { ReceiptHdr } from 'src/app/modelclass/receipt-hdr';
import { ReceiptInvoice } from 'src/app/modelclass/receipt-invoice';
import { ReceiptReportInvoiceService } from 'src/app/services/receipt-report-invoice.service';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-receipt-report',
  templateUrl: './receipt-report.component.html',
  styleUrls: ['./receipt-report.component.css']
})
export class ReceiptReportComponent implements OnInit {
  voucherNumber: any;
  voucherDate: any;

  account: any;
  modeOfReceipt: any;
  instrumentNo: any;
  instrumentDate: any;
  remark: any;
  amount: any;
  receiptHdr:ReceiptHdr=new ReceiptHdr;
  receiptDtlArray:ReceiptDtl[]=[];
  receiptInvoice:ReceiptInvoice[]=[]; 
  
  constructor(private router: Router, private sharedserviceService: SharedserviceService,
    private receiptReportServService: ReceiptReportInvoiceService,
    ) { }

  accountHeads:AccountHeads=new AccountHeads;
  companyMst:CompanyMst=new CompanyMst;
  ngOnInit(): void {  
    this.voucherNumber=this.receiptReportServService.voucherNumber;
    this.voucherDate=this.receiptReportServService.voucherDate;

    this.sharedserviceService.receiptReportPdf(this.voucherNumber,this.voucherDate).subscribe((data:any)=>{
      this.receiptInvoice=data;
    })
  }
  click()
  {
    window.print();
  }
  close(){ 
    history.back();
  }
}
