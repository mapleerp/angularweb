import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StockTransferOutHdr } from 'src/app/modelclass/stock-transfer-out-hdr';
import { StockTransferOutReport } from 'src/app/modelclass/stock-transfer-out-report';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-stock-transfer-out-report-print',
  templateUrl: './stock-transfer-out-report-print.component.html',
  styleUrls: ['./stock-transfer-out-report-print.component.css']
})
export class StockTransferOutReportPrintComponent implements OnInit {
  stockTransferOutHdr:StockTransferOutHdr=new StockTransferOutHdr;
  stockTransferOutReport:StockTransferOutReport[]=[];
  totalAmount:number=0.0;
  // voucherNumber:any;
  // voucherDate:any;
  constructor(public router: Router,private sharedserviceService: SharedserviceService) { }

  ngOnInit(): void {
    this.stockTransferOutHdr=history.state;
    console.log('StockTransferOutHdr object in StockTransferOutReportPrintComponent');
    console.log(this.stockTransferOutHdr);

    // this.voucherNumber=this.sharedserviceService.getVoucherNumberForReport();
    // this.voucherDate=this.sharedserviceService.getVoucherDateForReport();
    this.sharedserviceService.getStockTransferInvoice(this.stockTransferOutHdr.voucherNumber,this.stockTransferOutHdr.voucherDate).subscribe((data:any)=>{
    this.stockTransferOutReport=data;
    console.log('StockTransferOutReport List')
    console.log(this.stockTransferOutReport);
    for(let i=0;i<this.stockTransferOutReport.length;i++){
      this.totalAmount=this.totalAmount+this.stockTransferOutReport[i].amount;
    }
    console.log(this.totalAmount+" total amount calculation")
})
  }

  click(){
    window.print();
  }

  close(){
    history.back();
  }
}
