import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockTransferOutReportPrintComponent } from './stock-transfer-out-report-print.component';

describe('StockTransferOutReportPrintComponent', () => {
  let component: StockTransferOutReportPrintComponent;
  let fixture: ComponentFixture<StockTransferOutReportPrintComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockTransferOutReportPrintComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockTransferOutReportPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
