import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-side-nav-bar',
  templateUrl: './side-nav-bar.component.html',
  styleUrls: ['./side-nav-bar.component.css']
})
export class SideNavBarComponent implements OnInit {

  companyName:any;
  constructor(private router: Router,private sharedserviceService: SharedserviceService,) { }

  ngOnInit(): void {
    this.companyName=this.sharedserviceService.getCompanyName();
  }
  signout(){
    this.router.navigate(['']);
    return this.sharedserviceService.logout();
  }
  selectUser(){
    if(environment.myBranch==undefined){
      alert('Please switch your branch');
      return;
    }

    if(environment.user=='COMMONUSER'){
      console.log("in common POS window")
      this.router.navigate(['/loginpage/CommonUserPosWindow']);
    }
    else if(environment.user=='COMPANYUSER'){
      console.log("in staff user POS window")
      this.router.navigate(['/loginpage/PosWindow']);
    }

  }



  selectUserWholesale(){
    if(environment.myBranch==undefined){
      alert('Please switch your branch');
      return;
    }
    if(environment.clientOrCloud=='CLOUD'){
      console.log("in cloud wholesale window")
      this.router.navigate(['/loginpage/CloudWholesale']);
    }
  }


  checkUserPurchase(){
    if(environment.myBranch==undefined){
      alert('Please switch your branch');
      return;
    }
    this.router.navigate(['/loginpage/Purchase']);
  }

  checkUserItemCreate(){
    if(environment.myBranch==undefined){
      alert('Please switch your branch');
      return;
    }
    this.router.navigate(['/loginpage/ItemCreation']);

  }

  checkUserBranch(page:any){
    if(environment.myBranch==undefined){
      alert('Please switch your branch');
      return;
    }
    if(page=="AcceptStock"){
      this.router.navigate(['loginpage/AcceptStock']);
    }
    
  }
}
