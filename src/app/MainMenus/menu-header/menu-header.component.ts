import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-menu-header',
  templateUrl: './menu-header.component.html',
  styleUrls: ['./menu-header.component.css']
})
export class MenuHeaderComponent implements OnInit {

  userName:string=''
  constructor(private router: Router,private sharedserviceService:SharedserviceService) { }

  ngOnInit(): void {
   this.userName= this.sharedserviceService.getUserName();

  
  }

  
  logout()
  {
    this.sharedserviceService.logout();
    this.router.navigate([''])
    
    
  }

}
