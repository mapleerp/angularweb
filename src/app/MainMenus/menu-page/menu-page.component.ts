import { Component, OnInit } from '@angular/core';
import { AccountHeads } from 'src/app/modelclass/account-heads';
import { CustomerMst } from 'src/app/modelclass/customer-mst';
import { datas } from 'src/app/Pop-Up-Windows/search-pop-up/search-pop-up.component';
import { SaleOrderPopupService } from 'src/app/services/sale-order-popup.service';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-menu-page',
  templateUrl: './menu-page.component.html',
  styleUrls: ['./menu-page.component.css'],
})
export class MenuPageComponent implements OnInit {
  tdatas: datas[] = [];
  receivecust: any;
  selectcustName: any;
  customerDtls: AccountHeads[] = [];
  constructor(
    private saleOrderPopupService: SaleOrderPopupService,
    private sharedserviceService: SharedserviceService
  ) {}

  ngOnInit(): void {
    this.sharedserviceService.AllDataPopup().subscribe((info) => {
      this.tdatas = info;

      if (this.tdatas != null) {
        this.sharedserviceService.globaldata(this.tdatas);
        localStorage.setItem('testObject', JSON.stringify(this.tdatas));
        // alert('Initialized..!');
      } else {
        return;
      }
    });

    this.sharedserviceService
      .getAllCustomerDetailsfromserver()
      .subscribe((data) => {
        // this.customerDtls = data;
      });

    this.saleOrderPopupService.custDtlsentToSalesPopupService(
      this.customerDtls
    );
  }

  custDtlsentToSalesPopupService() {
    console.log(
      'custDtlsentToSalesPopupService called' + this.customerDtls[0].accountName
    );
    this.saleOrderPopupService.custDtlsentToSalesPopupService(
      this.customerDtls
    );
  }
}
