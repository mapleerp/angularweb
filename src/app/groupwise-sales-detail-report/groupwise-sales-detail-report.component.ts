import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { environment } from 'src/environments/environment.prod';
import { BranchMst } from '../modelclass/branch-mst';
import { CategoryMst } from '../modelclass/category-mst';
import { GroupWiseSalesDtl } from '../modelclass/group-wise-sales-dtl';
import { SharedserviceService } from '../services/sharedservice.service';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-groupwise-sales-detail-report',
  templateUrl: './groupwise-sales-detail-report.component.html',
  styleUrls: ['./groupwise-sales-detail-report.component.css'],
})
export class GroupwiseSalesDetailReportComponent implements OnInit {
  branch: BranchMst[] = [];
  category: CategoryMst[] = [];
  groupname: any = [];
  todate: any;
  fromdate: any;
  branchname: any;
  categoryid: String = '';
  temp = '';
  groupWiseSalesDtl: GroupWiseSalesDtl[] = [];
  constructor(
    private sharedserviceService: SharedserviceService,

    public fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.getBranch();
    this.getCategory();
  }
  getCategory() {
    this.sharedserviceService.getCategoryMst().subscribe((data: any) => {
      this.category = data;
    });
  }

  getBranch() {
    this.sharedserviceService.getBranchNames().subscribe((data: any) => {
      this.branch = data;
    });
  }

  addCategory() {
    this.sharedserviceService
      .getCategoryById(this.categoryid)
      .subscribe((data: any) => {
        if (this.groupname.indexOf(data.categoryName) === -1) {
          this.groupname.push(data.categoryName);

          this.temp = this.temp.concat(data.categoryName) + ';';
        }
        console.log(this.temp);
      });
  }

  submit() {
    if (this.fromdate == null) {
      alert('Select the From Date!!!');
      return;
    }
    if (this.todate == null) {
      alert('Select the To Date!!!');
      return;
    }
    if (this.branchname == null) {
      alert('Select the Branch!!!');
      return;
    }
    if (this.categoryid == null) {
      alert('Select the Category!!!');
      return;
    }
    this.sharedserviceService
      .getGroupWiseSalesReport(
        this.fromdate,
        this.todate,
        this.branchname,
        this.temp
      )
      .subscribe((data: any) => {
        this.groupWiseSalesDtl = data;
        console.log(data);
      });
  }
  renderPdf() {
    if(this.groupWiseSalesDtl.length == 0){
      return;
    }
    let element = document.getElementById('contentToConvert');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'sheet1');
    XLSX.writeFile(
      wb,
      'Group-Wise-Sales-Report' + '-' + this.fromdate + '-' + this.todate + '.xlsx'
    );
  }
}
