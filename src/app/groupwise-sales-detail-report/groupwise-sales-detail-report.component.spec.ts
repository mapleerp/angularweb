import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupwiseSalesDetailReportComponent } from './groupwise-sales-detail-report.component';

describe('GroupwiseSalesDetailReportComponent', () => {
  let component: GroupwiseSalesDetailReportComponent;
  let fixture: ComponentFixture<GroupwiseSalesDetailReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupwiseSalesDetailReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupwiseSalesDetailReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
