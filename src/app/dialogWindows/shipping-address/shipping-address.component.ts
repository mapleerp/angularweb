import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { AccountHeads } from 'src/app/modelclass/account-heads';
import { CustomerMst } from 'src/app/modelclass/customer-mst';
import { ShippingAddress } from 'src/app/modelclass/shipping-address';
import { SaleOrderPopupService } from 'src/app/services/sale-order-popup.service';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-shipping-address',
  templateUrl: './shipping-address.component.html',
  styleUrls: ['./shipping-address.component.css']
})
export class ShippingAddressComponent implements OnInit {

  addShippingDtl:any = FormGroup;
  ShippingArees:ShippingAddress=new ShippingAddress
  shippingAddName:any;
  addr:any;
  state:any;
  pincode:any;
  mobNumber:any;

  constructor(public shippmingDtlDialog: MatDialogRef<ShippingAddressComponent>,   
              private sharedserviceService:SharedserviceService, 
              private saleOrderPopupService:SaleOrderPopupService) { }

  receivecust:AccountHeads=new AccountHeads;
    // receivecust:CustomerMst=new CustomerMst;
  ngOnInit(): void {

    this.saleOrderPopupService.getCustomerDtls().subscribe(
      (data: any)=>{this.receivecust=data;
        console.log(this.receivecust.accountName+"getCustomerDtls")
    }
    );
  
    // this.addShippingDtl = this.fb.group({
    //   customerName:new FormControl(),
    //   addressLine1:new FormControl(),
    //   addressLine2:new FormControl(),
    //   mobileNumber:new FormControl(),
    //   landMark:new FormControl(),
    // })   
  }

  continueWithAddress()
  {
    // console.log( this.addShippingDtl)
    // if(Object.keys(this.addShippingDtl.value)==null)
    // {
    //   console.log('no Data')
    //   return;

    // }

    this.ShippingArees.shippingAddressName=this.shippingAddName;
    this.ShippingArees.address=this.addr;
    this.ShippingArees.state=this.state;
    this.ShippingArees.pincode=this.pincode;
    this.ShippingArees.phoneNumber=this.mobNumber;
    this.ShippingArees.accountHeads=this.receivecust;

    // this.ShippingArees = Object.assign(this.ShippingArees, this.addShippingDtl.value);
    //   console.log(this.ShippingArees);
    //  this.ShippingArees.accountHeads=this.receivecust;
      this.sharedserviceService.sentShippingDtltoService(this.ShippingArees).subscribe((data:any)=>{
        console.log(data);
      })
      this.shippmingDtlDialog.close()
  }

  CloseAddress()
  {
    this.shippmingDtlDialog.close();
  }

 
}
