import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SalesDtl } from 'src/app/modelclass/sales-dtl';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
export type DialogDataSubmitCallback<T> = (row: T) => void;

@Component({
  selector: 'app-qty-edit-window',
  templateUrl: './qty-edit-window.component.html',
  styleUrls: ['./qty-edit-window.component.css']
})
export class QtyEditWindowComponent implements OnInit {

  input: FormControl = new FormControl('');
  constructor(private sharedserviceService:SharedserviceService, private formBuilder: FormBuilder,public editDialog: MatDialogRef<QtyEditWindowComponent>,
    @Inject(MAT_DIALOG_DATA)  public data:any
    // public data: { callback: DialogDataSubmitCallback<any>; defaultValue: any },
   ) { }
  editedproductQty:any 
  editedqtydetails:Editedqtydetails=new Editedqtydetails
  form:any=FormGroup;
  salesDtl:SalesDtl=new SalesDtl;
  ngOnInit(): void {

    // this.form = this.formBuilder.group({
    //   filename: ''
    // })
    // if (this.data.defaultValue) {
    //   console.log(this.data.defaultValue+'valueeee')
    //   this.input.patchValue(this.data.defaultValue);
    // }
    this.editedproductQty=this.data.salesdtl.qty;
    this.salesDtl=this.data.salesdtl;
    console.log(this.salesDtl)
  }

  EdittedBtn(qty:any)
  {
    this.editedqtydetails.edittedQtydtls=qty;
    this.sharedserviceService.edittedQtyTosent(this.editedqtydetails);
    console.log("qty editted and sent to shared srvice"+this.editedqtydetails.edittedQtydtls)
    this.editDialog.close();
  }

  close()
  {
    this.editedqtydetails=new Editedqtydetails
    this.sharedserviceService.edittedQtyTosent(this.editedqtydetails);
    this.editDialog.close();
  }

  editQtytoSaleWindow(){
    this.salesDtl.qty=this.editedproductQty;
    this.salesDtl.amount=this.editedproductQty*this.salesDtl.mrp;
    this.editDialog.close(this.salesDtl);
  }


}
export class Editedqtydetails {

  edittedQtydtls:any;
}