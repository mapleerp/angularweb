import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QtyEditWindowComponent } from './qty-edit-window.component';

describe('QtyEditWindowComponent', () => {
  let component: QtyEditWindowComponent;
  let fixture: ComponentFixture<QtyEditWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QtyEditWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QtyEditWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
