import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDialogDeploymentComponent } from './add-dialog-deployment.component';

describe('AddDialogDeploymentComponent', () => {
  let component: AddDialogDeploymentComponent;
  let fixture: ComponentFixture<AddDialogDeploymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddDialogDeploymentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDialogDeploymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
