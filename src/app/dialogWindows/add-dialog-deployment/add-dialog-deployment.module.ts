import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddDialogDeploymentRoutingModule } from './add-dialog-deployment-routing.module';
import { AddDialogDeploymentComponent } from './add-dialog-deployment.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AddDialogDeploymentComponent
  ],
  imports: [
    CommonModule,
    AddDialogDeploymentRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AddDialogDeploymentModule { }
