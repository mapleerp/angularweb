import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { JarVersionMst } from 'src/app/modelclass/jar-version-mst';
import { SharedserviceService } from 'src/app/services/sharedservice.service';


@Component({
  selector: 'app-add-dialog-deployment',
  templateUrl: './add-dialog-deployment.component.html',
  styleUrls: ['./add-dialog-deployment.component.css']
})
export class AddDialogDeploymentComponent implements OnInit {

  constructor(public addDialog: MatDialogRef<AddDialogDeploymentComponent>,public fb:FormBuilder,
    private sharedserviceService:SharedserviceService) { }

  addDeployment:any = FormGroup;
  jarVersionMstDialog:JarVersionMst=new JarVersionMst();
  sub: Subscription = new Subscription;

  ngOnInit(): void {

    this.addDeployment = this.fb.group({
      checksum:new FormControl(),
	companyMstId:new FormControl(),
	current_version:new FormControl(),
	fileName:new FormControl(),
	version:new FormControl(),
	passWord:new FormControl(),
	supscriptionEnds:new FormControl(),
	userId:new FormControl(),
	upgradeStatus:new FormControl(),
    })
  }

  close()
  {
    this.addDialog.close();
  }
  submit()
  {
    if(Object.keys(this.addDeployment)==null)
    {
      console.log('no Data')
      return;

    }
    this.jarVersionMstDialog = Object.assign(this.jarVersionMstDialog, this.addDeployment.value);
      console.log(this.jarVersionMstDialog);
     
     this.sub= this.sharedserviceService.saveupDeploymentDetails(this.jarVersionMstDialog).
     subscribe( data =>
      {
        console.log(data);
        console.log('Data Send')
      },
     error =>console.log(error)
     );
      
   
     this.addDialog.close();
  }

}
