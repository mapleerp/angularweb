import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddDialogDeploymentComponent } from './add-dialog-deployment.component';

const routes: Routes = [{ path: '', component: AddDialogDeploymentComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddDialogDeploymentRoutingModule { }
