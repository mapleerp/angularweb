import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageHotCakesComponent } from './homepage-hot-cakes.component';

describe('HomepageHotCakesComponent', () => {
  let component: HomepageHotCakesComponent;
  let fixture: ComponentFixture<HomepageHotCakesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomepageHotCakesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageHotCakesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
