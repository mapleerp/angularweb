import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Network } from '@ngx-pwa/offline';
import { SharedserviceService } from 'src/app/services/sharedservice.service';



@Component({
  selector: 'app-homepage-hot-cakes',
  templateUrl: './homepage-hot-cakes.component.html',
  styleUrls: ['./homepage-hot-cakes.component.css']
})
export class HomepageHotCakesComponent implements OnInit {
 
  networkStatus$;
  constructor(private router: Router,private sharedserviceService:SharedserviceService,
    protected network: Network,
    public datepipe: DatePipe) { this.networkStatus$ = this.network.onlineChanges; }
    yesterdayDate:any
    


  


  ngOnInit(): void {

 }

  logout()
  {
  
      this.sharedserviceService.logout()
    
    this.router.navigate([''])
    
    
  }

 

}
