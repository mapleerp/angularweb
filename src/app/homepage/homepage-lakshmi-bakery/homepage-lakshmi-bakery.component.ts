import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-homepage-lakshmi-bakery',
  templateUrl: './homepage-lakshmi-bakery.component.html',
  styleUrls: ['./homepage-lakshmi-bakery.component.css']
})
export class HomepageLakshmiBakeryComponent implements OnInit {

  constructor(private router: Router,private sharedserviceService:SharedserviceService) { }

  ngOnInit(): void {
  }
  logout()
  {
    this.sharedserviceService.logout();
    this.router.navigate([''])
    
    
  }

}
