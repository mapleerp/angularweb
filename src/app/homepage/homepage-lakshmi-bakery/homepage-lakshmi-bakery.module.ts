import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomepageLakshmiBakeryRoutingModule } from './homepage-lakshmi-bakery-routing.module';
import { HomepageLakshmiBakeryComponent } from './homepage-lakshmi-bakery.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import { MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';


@NgModule({
  declarations: [
    HomepageLakshmiBakeryComponent
  ],
  imports: [
    CommonModule,
    HomepageLakshmiBakeryRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatTableModule
  ]
})
export class HomepageLakshmiBakeryModule { }
