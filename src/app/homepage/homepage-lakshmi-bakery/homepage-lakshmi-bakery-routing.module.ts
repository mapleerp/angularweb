import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccessloginGuard } from 'src/app/authorizations/accesslogin.guard';
import { DayEndReportComponent } from 'src/app/reportPages/day-end-report/day-end-report.component';
import { StockMovementViewComponent } from 'src/app/reportPages/stock-movement-view/stock-movement-view.component';
import { HomepageLakshmiBakeryComponent } from './homepage-lakshmi-bakery.component';

const routes: Routes = [{ path:'',
 component: HomepageLakshmiBakeryComponent ,
 children:[
 {path:'StockMovementReport', component:StockMovementViewComponent},
 {path:'DayEndReport', component:DayEndReportComponent}, ]},
 ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomepageLakshmiBakeryRoutingModule { }
