import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageLakshmiBakeryComponent } from './homepage-lakshmi-bakery.component';

describe('HomepageLakshmiBakeryComponent', () => {
  let component: HomepageLakshmiBakeryComponent;
  let fixture: ComponentFixture<HomepageLakshmiBakeryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomepageLakshmiBakeryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageLakshmiBakeryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
