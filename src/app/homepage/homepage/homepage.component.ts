import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedserviceService } from 'src/app/services/sharedservice.service';


@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  constructor(private router: Router,private sharedserviceService:SharedserviceService) { }

  ngOnInit(): void {
  }
  

  logout()
  {
    this.sharedserviceService.logout()
    this.router.navigate([''])

  }
  

}


