import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { datas, SearchPopUpComponent } from 'src/app/Pop-Up-Windows/search-pop-up/search-pop-up.component';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

@Component({
  selector: 'app-maplesystem-admin',
  templateUrl: './maplesystem-admin.component.html',
  styleUrls: ['./maplesystem-admin.component.css']
})
export class MaplesystemAdminComponent implements OnInit {

  constructor(private router: Router,private sharedserviceService:SharedserviceService,public addDialog1: MatDialog) { }

  branchname='';
  recivdata:datas= new datas;
  tdatas:datas[] =[];
  updateUserInfo: any;
  qty:number | undefined
  ngOnInit(): void {

    this.sharedserviceService.getNewUserInfo().subscribe(info => {
      this.recivdata = info;
     
    })
    const dataSubscribe = this.sharedserviceService.setInfoo
    console.log(dataSubscribe.name+'printed')
   
    console.log( this.branchname+'dddddd')
  this.sharedserviceService.AllDataPopup().subscribe(info => {
    this.tdatas = info;
    this.sharedserviceService.globaldata(this.tdatas);
    localStorage.setItem('testObject', JSON.stringify(this.tdatas));

   
   
  })
 
  //this.sharedserviceService.globaldata(this.tdatas)
 
  }

  logout()
  {
    this.sharedserviceService.logout();
    this.router.navigate([''])
    
    
  }

  onKeypressEvent(x: any)
  {
    const dialogConfigAdd1 = new MatDialogConfig();
                  dialogConfigAdd1.disableClose=false;
                  dialogConfigAdd1.autoFocus=true;
                  dialogConfigAdd1.width="40%";
                  dialogConfigAdd1.height="100%";
                   this.addDialog1.open(SearchPopUpComponent,{ disableClose: true });
                  console.log("popupcalled")
                 
                  
  }

}
