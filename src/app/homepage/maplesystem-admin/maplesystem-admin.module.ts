import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaplesystemAdminRoutingModule } from './maplesystem-admin-routing.module';
import { MaplesystemAdminComponent } from './maplesystem-admin.component';
import { FormsModule } from '@angular/forms';
import {MatToolbarModule} from '@angular/material/toolbar';
import { MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import {MatDividerModule} from '@angular/material/divider';
import {MatSidenavModule} from '@angular/material/sidenav';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
  declarations: [
    MaplesystemAdminComponent
  ],
  imports: [
    CommonModule,
    MaplesystemAdminRoutingModule,
    FormsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatMenuModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    MatDividerModule,
    MatSidenavModule,
    BrowserAnimationsModule,
  

  ]
})
export class MaplesystemAdminModule { }
