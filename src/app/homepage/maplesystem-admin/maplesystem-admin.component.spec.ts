import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaplesystemAdminComponent } from './maplesystem-admin.component';

describe('MaplesystemAdminComponent', () => {
  let component: MaplesystemAdminComponent;
  let fixture: ComponentFixture<MaplesystemAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MaplesystemAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaplesystemAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
