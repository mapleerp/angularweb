import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MaplesystemAdminComponent } from './maplesystem-admin.component';

const routes: Routes = [{ path: '', component: MaplesystemAdminComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaplesystemAdminRoutingModule { }
