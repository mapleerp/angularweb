import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { SaleOrderPopupService } from '../services/sale-order-popup.service';
import { SalesDtlsDeletionService } from '../services/sales-dtls-deletion.service';
import { SalesDtlsService } from '../services/sales-dtls.service';
import { CustomerService } from '../services/customer.service';
import { SharedserviceService } from '../services/sharedservice.service';
import { SalesTypeMst } from '../modelclass/sales-type-mst';
import { ShippingAddress } from '../modelclass/shipping-address';
import { datas } from '../Pop-Up-Windows/search-pop-up/search-pop-up.component';
import { SalesDtl } from '../modelclass/sales-dtl';
import { SalesBillDetails } from '../modelclass/sales-bill-details';
import { SalesTransHdr } from '../modelclass/sales-trans-hdr';
import { Summary } from '../modelclass/summary';
import { environment } from 'src/environments/environment.prod';
import { AccountHeads } from '../modelclass/account-heads';
import { ItemStockPopUpComponent } from '../Pop-Up-Windows/item-stock-pop-up/item-stock-pop-up.component';
import { ShippingAdressPopUpComponent } from '../Pop-Up-Windows/shipping-adress-pop-up/shipping-adress-pop-up.component';
import { SalesManMst } from '../modelclass/sales-man-mst';
import { ShippingaddressNewpopupComponent } from '../Pop-Up-Windows/shippingaddress-newpopup/shippingaddress-newpopup.component';
import { SalesReceipts } from '../modelclass/sales-receipts';

@Component({
  selector: 'app-cloudwholesale',
  templateUrl: './cloudwholesale.component.html',
  styleUrls: ['./cloudwholesale.component.css']
})
export class CloudwholesaleComponent implements OnInit {

  constructor(
    private sharedserviceService: SharedserviceService,
    private customerService: CustomerService,
    private salesDtlsService: SalesDtlsService,
    public salesDtlsDeletionService: SalesDtlsDeletionService,
    private saleOrderPopupService: SaleOrderPopupService,
    public shippingDtlPopUp: MatDialog,
    public editQtyAdd: MatDialog,
    public addDialog1: MatDialog,
    private router: Router, public datepipe: DatePipe,
  ) { }
  salestypeMst: SalesTypeMst[] = [];
  salesManMst:SalesManMst[]=[];
  receivecust: any;
  selectcustName: any;
  ShippingAdress: ShippingAddress = new ShippingAddress();
  shipping: ShippingAddress = new ShippingAddress();
  isCustomerSelected = false;
  public addingToTable: datas[] = [];
  recivdata: datas = new datas();
  totalAmount: number = 0.0;
  editRowqty: any;
  name: any;
  wholesalesDtlAddTables: SalesDtl[] = [];
  salesDtl: SalesDtl = new SalesDtl();
  SalesBillDetails: SalesBillDetails = new SalesBillDetails();
  dateCov: any;
  salesType = '';
  selectSalesMan = '';
  holdedPos: SalesTransHdr[] = [];
  salesTransHdr: SalesTransHdr = new SalesTransHdr();
  getSalesBillDtl: SalesDtl[] = [];
  summary: Summary = new Summary();
  accountHeads:AccountHeads=new AccountHeads;
  gstnumber:any;
  salesTransHdrArray:SalesTransHdr[]=[];
  salesReceipts:SalesReceipts=new SalesReceipts;
  salesReceiptsArray:SalesReceipts[]=[];
  date= new Date();
  ngOnInit(): void {
    this.sharedserviceService.salesDtlfromShardService().subscribe((data) => {
      this.wholesalesDtlAddTables = data;

      this.totalAmount = 0.0;
      for (let i = 0; i <= this.wholesalesDtlAddTables.length - 1; i++) {
        this.totalAmount =
          this.wholesalesDtlAddTables[i].amount + this.totalAmount;
      }
    });

    this.getAllSalesType();
    this.getAllSalesMan();
    this.receiveCustomerDtls();
    this.receiveShippingDtl();
    // this.AddingToTables();
    // this.totalPrice();
  }
  getAllSalesType() {
    this.sharedserviceService.getSalesTypes().subscribe((data) => {
      this.salestypeMst = data;
      console.log(this.salestypeMst);
    });
  }

  getAllSalesMan(){
    this.sharedserviceService.getSalesMan().subscribe((data) => {
      this.salesManMst = data;
      console.log(this.salesManMst);
    });
  }
//-------here we are keypress event for select customer--------------//
onKeypressEventForSelectCustmer() {
  this.customerService.selectCustomer();
}

receiveCustomerDtls() {
  console.log('receiveCustomerDtls');
  this.saleOrderPopupService.getCustomerDtls().subscribe((data: any) => {
    this.receivecust = data;
console.log("in receiveCustomerDtls function")
console.log(data)
    this.selectcustName = this.receivecust.accountName;
    console.log(this.selectcustName + 'customer name');
  });
}

onKeypressEventForShippingDtl() {
  console.log('Shipping address caleed');

  const dialogConfigShippingDtl = new MatDialogConfig();
  dialogConfigShippingDtl.disableClose = false;
  dialogConfigShippingDtl.autoFocus = true;

  dialogConfigShippingDtl.width = '50%';
  dialogConfigShippingDtl.height = '80%';

  this.shippingDtlPopUp.open(
    ShippingaddressNewpopupComponent,
    dialogConfigShippingDtl
  );
}
receiveShippingDtl() {
  console.log('receiveShippingDtl from services');
  this.saleOrderPopupService.getShippingDtls().subscribe((data: any) => {
    this.ShippingAdress = data;
    this.shipping=data;
    console.log(this.ShippingAdress + 'receiveShippingDtl');
  });
}


totalPrice() {
  this.totalAmount = 0.0;

  console.log('TotalPrice function called');
  let num: number;
  for (let data of this.addingToTable) {
    num = +data.Amount;
    this.totalAmount = num + this.totalAmount;
  }
}


callBack(name: any) {
  this.name = name;
  this.addingToTable[this.editRowqty].qty = this.name;
  this.addingToTable[this.editRowqty].Amount =
    this.addingToTable[this.editRowqty].standardPrice *
    this.addingToTable[this.editRowqty].qty;
  this.totalPrice();
}

onKeypressEvent(x: any) {
  this.wholeSalesDetails();
  if (this.selectcustName == null) {
    alert('Please select the customer first!!!');
    return;
  }
  // if (this.selectSalesMan == '') {
  //   alert('Please enter the sales man name !!!');
  //   return;
  // }
  if (this.salesType.length == 0) {
    alert('Please select the sales type !!!');
    return;
  }
  const dialogConfigAdd1 = new MatDialogConfig();
  dialogConfigAdd1.disableClose = false;
  dialogConfigAdd1.autoFocus = true;
  //dialogConfigAdd1.position= { right: `10px`, top: `20px` }
  dialogConfigAdd1.width = '80%';
  dialogConfigAdd1.height = '70%';
  //this.addDialog1.open(SearchPopUpComponent,{ disableClose: true });
  dialogConfigAdd1.data = {
    callback: this.callBack.bind(this),
    defaultValue: this.salesDtl,
  };
  this.addDialog1.open(ItemStockPopUpComponent, dialogConfigAdd1);
  console.log('popupcalled');
}

ngOnDestroy(): void {
  console.log('page destroyed from WHSALE');
  this.addingToTable = [];
  console.log(this.addingToTable);
  this.recivdata = new datas();
  this.totalAmount = 0.0;
  this.wholesalesDtlAddTables.length = 0;
  this.sharedserviceService.PostSalesDtlToPosWindow(
    this.wholesalesDtlAddTables
  );
  this.receivecust = new AccountHeads();
  this.saleOrderPopupService.addCustomerDtl(this.receivecust);
  this.ShippingAdress = new ShippingAddress();
  this.saleOrderPopupService.addShippingDtls(this.ShippingAdress);
  this.selectSalesMan = '';
  this.salesType = '';
  this.holdedPos.length = 0;
}

finalSaveWholesaleForCloud() {
  this.dateCov = new Date();
  this.dateCov= this.datepipe.transform(this.dateCov , 'yyyy-MM-dd');
  this.salesTransHdr.invoiceAmount=this.totalAmount;
  this.salesTransHdr.voucherType=this.salesType;
  this.salesTransHdr.customerId=this.receivecust.id;
//accountheads by customerId
this.sharedserviceService.getAccountHeads(this.salesTransHdr.customerId).subscribe((data:any)=>{
  this.accountHeads=data;
})

if(this.accountHeads==null){
  return;
}

if(this.accountHeads.customerDiscount){
  this.salesTransHdr.discount=this.accountHeads.customerDiscount;
}
else{
  this.salesTransHdr.discount=0;
}
this.gstnumber=String(this.accountHeads.partyGst)
const arrayPartyGst = Array.from(this.gstnumber);
if(this.accountHeads==null||arrayPartyGst.length<13){
  this.salesTransHdr.salesMode="B2C"
}
else{
  this.salesTransHdr.salesMode="B2B"
}

  this.salesTransHdr.creditOrCash="CREDIT";
  this.salesTransHdr.customiseSalesMode="WHOLESALEWINDOW"
  this.salesTransHdr.userId=this.sharedserviceService.getUserName();
  this.salesTransHdr.branchCode=environment.myBranch;
  this.salesTransHdr.voucherDate=this.dateCov;
  this.salesTransHdr.salesManId=this.selectSalesMan;
  this.salesTransHdr.paidAmount=this.totalAmount;
  this.salesTransHdr.isBranchSales = 'N';
  this.salesTransHdr.shippingAddressId=this.shipping.id;

  this.salesTransHdrArray.push(this.salesTransHdr);

  this.setSalesReceipts();
  console.log("SalesTransHdr for FinalSave")
  console.log(this.salesTransHdrArray);

  console.log("Sales Detail Array for FinalSave");
  console.log(this.wholesalesDtlAddTables);

  console.log("Sales Receipts Array for FinalSave");
  console.log(this.salesReceiptsArray);

  let finalSales = new Map<string, Object[]>();
  finalSales.set('SalesTransHdr', this.salesTransHdrArray);
  finalSales.set('SalesDtlArray', this.wholesalesDtlAddTables);
  finalSales.set('SalesReceiptsArray', this.salesReceiptsArray);

  console.log("in Final Save Cloud"+finalSales);
  this.sharedserviceService.finalSaveforCloudPOS(finalSales).subscribe((data) => {
    console.log(data);
    this.SalesBillDetails=data;
    console.log(data.salesTransHdr.voucherNumber+"in final submit of cloud wholesale")
    console.log(this.ShippingAdress)
    this.salesDtlsService.WholesaleBillReport(
            this.SalesBillDetails,
            this.shipping
          );
          this.router.navigate(['WholeSalesBill']);
// this.sharedserviceService.salesPdfForWeb(data.salesTransHdr.id).subscribe((data:any)=>{
//   console.log(data)
//   console.log("jjjjjjjj")
// })
  });
  // this.sharedserviceService
  //   .updateSalesTransHdr(
  //     this.wholesalesDtlAddTables[0].salesTransHdr,
  //     this.dateCov
  //   )
  //   .subscribe((data) => {
  //     this.SalesBillDetails = data;
  //     this.salesDtlsService.WholesaleBillReport(
  //       this.SalesBillDetails,
  //       this.ShippingAdress
  //     );
  //     this.router.navigate(['WholeSalesBill']);
  //   });
  this.clearAfterFinalSave();
}

wholeSalesDetails() {
  this.salesDtlsService.wholeSalesDetails(
    this.salesType,
    this.selectcustName,
    this.selectSalesMan,
    this.receivecust.id
  );
}

holdedSalesDtls() {
  this.sharedserviceService
    .getHoldedPossDtls(environment.myBranch)
    .subscribe((data) => {
      this.holdedPos = data;
      if (this.holdedPos.length == 0) {
        alert('No Holded Bills');
        return;
      }
      for (let i = 0; i <= this.holdedPos.length - 1; i++) {
        this.holdedPos[i].id = data[i][2];
        this.holdedPos[i].userId = data[i][0];
        this.holdedPos[i].invoiceAmount = data[i][1];
      }
      console.log(this.holdedPos);
    });
}

itemDeletionForSales(salesdtl: SalesDtl) {
  this.salesDtlsDeletionService.salesDetailsDeletion(salesdtl);
  this.sharedserviceService
    .getSalesDtl(this.wholesalesDtlAddTables[0].salesTransHdr)
    .subscribe((data) => {
      this.wholesalesDtlAddTables = data;
      this.sharedserviceService.PostSalesDtlToPosWindow(
        this.wholesalesDtlAddTables
      );
    });
}

unholdbillOfSales(saleshdr: SalesTransHdr) {
  console.log('unholdbill');
  this.salesTransHdr = saleshdr;
  this.unHoldOfSales();
}
unHoldOfSales() {
  console.log('unHold');
  if (this.salesTransHdr != null) {
    if (this.salesTransHdr.id != null) {
      this.sharedserviceService
        .getSalesTransHdr(this.salesTransHdr.id)
        .subscribe((data) => {
          this.salesTransHdr = data;
          this.sharedserviceService
            .getSalesDtl(this.salesTransHdr)
            .subscribe((data) => {
              this.wholesalesDtlAddTables = data;
            });
        });
      this.sharedserviceService
        .getSalesBillDtl(this.salesTransHdr)
        .subscribe((getSalesBillDt) => {
          this.getSalesBillDtl = getSalesBillDt;
          this.sharedserviceService.PostSalesDtlToPosWindow(
            this.getSalesBillDtl
          );

          this.sharedserviceService
            .getSalesWindowsSummary(this.salesTransHdr.id)
            .subscribe((data) => {
              this.summary = data;
              this.salesTransHdr.invoiceAmount = this.summary.totalAmount;
              this.sharedserviceService.postSalesSummaryToService(
                this.summary
              );
            });
        });
    }
  }
}


setSalesReceipts(){
  this.salesReceipts= new SalesReceipts;
  this.salesReceipts.receiptMode='CREDIT'
  this.salesReceipts.receiptAmount=0;
  this.salesReceipts.userId=this.sharedserviceService.getUserName();
  this.salesReceipts.branchCode=environment.myBranch;
  this.dateCov= this.datepipe.transform(this.date, 'dd-MM-yyyy');
  this.salesReceipts.rereceiptDate=this.dateCov;
    this.salesReceiptsArray.push(this.salesReceipts);
}

clearAfterFinalSave(){
  this.salesType='';
  this.selectSalesMan='';
  this.selectcustName=null;
  this.wholesalesDtlAddTables.length = 0;
  // this.receivecust=null;
  this.ShippingAdress= new ShippingAddress();
  this.totalAmount=0.0
}

//increase or decrease quantity and calculate amount and total amount
counterFunction(type:string,item:SalesDtl){
  this.totalAmount=this.totalAmount-item.amount;
    console.log(item.itemName)
   if(type==='add'){
    item.qty=item.qty+1;
   }
   else{
     if(item.qty!=1){
      item.qty=item.qty-1;
     }
   }
   item.amount=item.qty*item.standardPrice;
   this.totalAmount=this.totalAmount+item.amount;
  }

  //delete item from wholesalesDtlAddTables array
  itemDeletion(salesdtl:SalesDtl)
{ 
      this.wholesalesDtlAddTables.forEach((value,index)=>{   
      if(value.itemId==salesdtl.itemId) {
        this.totalAmount=this.totalAmount-salesdtl.amount;
        console.log(index+" "+value.itemId+" "+salesdtl.itemId)
        this.wholesalesDtlAddTables.splice(index,1);
      }
  }); 
}
}
