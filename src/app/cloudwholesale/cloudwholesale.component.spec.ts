import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CloudwholesaleComponent } from './cloudwholesale.component';

describe('CloudwholesaleComponent', () => {
  let component: CloudwholesaleComponent;
  let fixture: ComponentFixture<CloudwholesaleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CloudwholesaleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CloudwholesaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
