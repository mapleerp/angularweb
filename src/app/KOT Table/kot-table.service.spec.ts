import { TestBed } from '@angular/core/testing';

import { KotTableService } from './kot-table.service';

describe('KotTableService', () => {
  let service: KotTableService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KotTableService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
