import { Component, OnInit } from '@angular/core';

import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { KotTableService } from '../kot-table.service';
import { TableDetailsComponent } from '../table-details/table-details.component';

@Component({
  selector: 'app-table-list-and-details',
  templateUrl: './table-list-and-details.component.html',
  styleUrls: ['./table-list-and-details.component.css']
})
export class TableListAndDetailsComponent implements OnInit {

  constructor( public addDialog: MatDialog,
    private kotTableService:KotTableService) { }

    k=[0];

    Waiting='red';
    Running='yellow';
    completed='green';

  tableListDtl=[{'tableName':'table1','tableId':'T1', 'Status':'Waiting'
                  },{'tableName':'table2','tableId':'T2', 'Status':'Running'},{
                    'tableName':'table3','tableId':'T3', 'Status':'completed'
                  },{'tableName':'table4','tableId':'T4', 'Status':'Waiting'},{'tableName':'table5','tableId':'T5', 'Status':'Waiting'}
                ,{'tableName':'table6','tableId':'T6', 'Status':'Running'},{'tableName':'table7','tableId':'T8', 'Status':'Waiting'},
                {'tableName':'table8','tableId':'T8', 'Status':'completed'},{'tableName':'table9','tableId':'T9', 'Status':'completed'}
              ,{'tableName':'table10','tableId':'10', 'Status':'Running'},{'tableName':'table11','tableId':'11', 'Status':'Waiting'},
              {'tableName':'table12','tableId':'12', 'Status':'Waiting'}
              ,{'tableName':'table6','tableId':'T6', 'Status':'Running'},{'tableName':'table7','tableId':'T8', 'Status':'Waiting'},
              {'tableName':'table8','tableId':'T8', 'Status':'completed'},{'tableName':'table9','tableId':'T9', 'Status':'completed'}
            ,{'tableName':'table10','tableId':'10', 'Status':'Running'},{'tableName':'table11','tableId':'11', 'Status':'Waiting'},
            {'tableName':'table12','tableId':'12', 'Status':'Waiting'}];
  ngOnInit(): void {
  }


  selectTable(tableDtls:any)
  {
      
    this.kotTableService.tableDtlsToKotService(tableDtls);
    const dialogConfigAdd = new MatDialogConfig();
    dialogConfigAdd.disableClose=false;
    dialogConfigAdd.autoFocus=true;
    dialogConfigAdd.width="60%";
    dialogConfigAdd.height="60%";
    this.addDialog.open(TableDetailsComponent,dialogConfigAdd);
  }

}
