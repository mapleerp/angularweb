import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableListAndDetailsComponent } from './table-list-and-details.component';

describe('TableListAndDetailsComponent', () => {
  let component: TableListAndDetailsComponent;
  let fixture: ComponentFixture<TableListAndDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableListAndDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableListAndDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
