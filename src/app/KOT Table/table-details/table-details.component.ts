import { Component, OnInit } from '@angular/core';
import { KotTableService } from '../kot-table.service';

@Component({
  selector: 'app-table-details',
  templateUrl: './table-details.component.html',
  styleUrls: ['./table-details.component.css']
})
export class TableDetailsComponent implements OnInit {

  constructor(private kotTableService:KotTableService) { }

  tableName:any

  ngOnInit(): void {

   this.tableName=this.kotTableService. getTableDtls();
   
  }

}
