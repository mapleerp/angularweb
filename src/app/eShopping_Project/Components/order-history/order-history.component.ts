import { Component, OnInit } from '@angular/core';
import { OrderHistory } from '../../ModelClass/order-history';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.css']
})
export class OrderHistoryComponent implements OnInit {

  constructor(private productservice:ProductService) { }

  orderHistoryList: OrderHistory[]=[];

  storage:Storage=sessionStorage;

  ngOnInit(): void {

    this.handleOrderHistory();
  }
  handleOrderHistory() {

    const theEmail=JSON.parse(this.storage.getItem('useEmail'));

    this.productservice.getOrderHistory(theEmail).subscribe(
      data=>{this.orderHistoryList=data;
        for (let i=0;data.length-1;i++)
        {
          this.orderHistoryList[i].dateCreated=data[i][0];
          this.orderHistoryList[i].id=data[i][0];
          this.orderHistoryList[i].orderTrackingNUmber=data[i][0];
          this.orderHistoryList[i].totalPrice=data[i][3];
          this.orderHistoryList[i].totalQuantity=data[i][2];
        }
      console.log(data)}
    )
  
  }


}
