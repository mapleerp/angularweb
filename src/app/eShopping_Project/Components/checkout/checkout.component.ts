import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Country } from '../../ModelClass/country';
import { Order } from '../../ModelClass/order';
import { OrderItem } from '../../ModelClass/order-item';
import { Purchase } from '../../ModelClass/purchase';
import { State } from '../../ModelClass/state';
import { CartService } from '../../services/cart.service';
import { CheckoutService } from '../../services/checkout.service';
import { ProductService } from '../../services/product.service';
import { CheckoutFormValidators } from '../../Validators/checkout-form-validators';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  checkOutFormGroup!: FormGroup;
  totalPrice : number=0;
  totalQty :number=0;

  country:Country[]=[];
  state:State[]=[];

  purchase1:Purchase

  shippAddState:State[]=[];
  billingAddState:State[]=[];
  selectedCountry:Country=new Country

  selecCountry:Country=new Country;

  

  constructor(private formBuilder:FormBuilder, private productService:ProductService,
    private cartService:CartService,private checkoutService:CheckoutService,private router:Router) { }

  ngOnInit(): void {

    this.reviewCartDetails();

    this. getCountries();

    this.checkOutFormGroup=this.formBuilder.group(
      {
        customer:this.formBuilder.group({
          firstName:new FormControl('',[Validators.required,Validators.minLength(2),
          CheckoutFormValidators.noOnlyWhitspace]),
          lastName:new FormControl('',[Validators.required,Validators.minLength(2)]),
          email:new FormControl('',
          [Validators.required,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
          mobileNumber:['']
        }),
        shippingAddress:this.formBuilder.group(
          {
            street:[''],
            city:[''],
            state:[''],
            country:[''],
            zipCode:['']
          }
        ),
        billingAddress:this.formBuilder.group(
          {
            street:[''],
            city:[''],
            state:[''],
            country:[''],
            zipCode:['']
          }
        ),
        creditCard:this.formBuilder.group(
          {
            cardType:[''],
            nameOnCard:[''],
            cardNumber:[''],
            securityCode:[''],
            expMonth:[''],
            expYear:[''],

          }
        ),
      }
    );
  }

  onSubmit()
  {
    console.log("Handling the submit buttom");
   
    if (this.checkOutFormGroup.invalid)
    {
      this.checkOutFormGroup.markAllAsTouched();
      return;
    }
    let order= new Order();
    order.totalPrice=this.totalPrice;
    order.totalQuantity=this.totalQty;

    const cartItems=this.cartService.cartItems;

  let  orderItem:OrderItem[]=[];
  for(let i=0;i<cartItems.length;i++)
  {
    orderItem[i]=new OrderItem(cartItems[i]);
  }

  let purchase = new Purchase();

purchase.customer=this.checkOutFormGroup.controls['customer'].value;


  purchase.shippingAddress=this.checkOutFormGroup.controls['shippingAddress'].value;
  const shippingState:State=JSON.parse(JSON.stringify(purchase.shippingAddress.state));
  const shippingCountry:State=JSON.parse(JSON.stringify(purchase.shippingAddress.country));
  const shippingCity:State=JSON.parse(JSON.stringify(purchase.shippingAddress.city));
  purchase.shippingAddress.state=shippingState.name;
  purchase.shippingAddress.country=shippingCountry.name;
  purchase.shippingAddress.city=shippingCity.name

  purchase.billingingAddress=this.checkOutFormGroup.controls['billingAddress'].value;
  const billingState:State=JSON.parse(JSON.stringify(purchase.billingingAddress.state));
  const billingCountry:State=JSON.parse(JSON.stringify(purchase.billingingAddress.country));
  const bilingCity:State=JSON.parse(JSON.stringify(purchase.billingingAddress.city));
  purchase.billingingAddress.state=billingState.name;
  purchase.billingingAddress.country=billingCountry.name;
  purchase.billingingAddress.city=bilingCity.name


  purchase.order=order;
  purchase.orderItems=orderItem;
  this.purchase1=purchase

  this.checkoutService.placeOrder(purchase).subscribe(

    {
      next:data=>{ 
          
          
        
        alert('Your order has been received'),
    this.resetCart();},
      error:er=>{'There was an error'}
    }
  
   
    
  );

  
  }
  resetCart() {
  
    this.cartService.cartItems=[];
    this.cartService.totalQty.next(0);
    this.cartService.totalPrice.next(0);
    this.checkOutFormGroup.reset();
    // this.router.navigateByUrl("/products")

  }



get firstName()
{
  return this.checkOutFormGroup.get('customer.firstName');
}
get lastName()
{
  return this.checkOutFormGroup.get('customer.lastName');
}
get email()
{
  return this.checkOutFormGroup.get('customer.email');
}


  cpoyShippingaddressToBiilingAdd(event:any)
  {
    if(event.target.checked)
    {
     this.checkOutFormGroup.controls.billingAddress.setValue(
       this.checkOutFormGroup.controls.shippingAddress.value);
     this.billingAddState=this.shippAddState;
    }
    else {
      this.checkOutFormGroup.controls.billingAddress.reset();
      this.billingAddState=[];
    }
  }

  getCountries()
  {

    this.productService.getAllCountryNames().subscribe(
      data=>{
        console.log(data),
        this.country=data}
    )

  }
   getStatesByCountry()
   {
      this.productService.getStatesByCountry(this.selecCountry.id).subscribe
   }

   selectCoutry(selectedCoun:Country)
   {
   console.log(selectedCoun);
   this.selectedCountry=selectedCoun;
   }

   getState(ShiAddr:string)
   {
      const formGroup=this.checkOutFormGroup.get(ShiAddr);


      this.productService.getStatesByCountry(formGroup?.value.country.id).subscribe(
        data=>{
          if(ShiAddr==='shippingAddress')
          {
            this.shippAddState=data;
          }
          else {
            this.billingAddState=data;
          }
         formGroup?.get('state')?.setValue(data[0]);
        
        }
      )
   }

   reviewCartDetails()
   {
     this.cartService.totalQty.subscribe(
       data=>{this.totalQty=data}
     );
     this.cartService.totalPrice.subscribe(
       data=>{this.totalPrice=data}
     );
   }

}
