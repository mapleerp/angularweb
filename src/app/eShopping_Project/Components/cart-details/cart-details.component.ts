import { Component, OnInit } from '@angular/core';
import { CartItem } from '../../ModelClass/cart-item';
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'app-cart-details',
  templateUrl: './cart-details.component.html',
  styleUrls: ['./cart-details.component.css']
})
export class CartDetailsComponent implements OnInit {

  cartItems:CartItem[]=[];
  totalPrice:number=0;
  totalQty:number=0;


  constructor(private cartService:CartService) { }

  ngOnInit(): void {

    this.listCartDetails();
  }
  listCartDetails() {

    this.cartItems=this.cartService.cartItems;
    this.cartService.totalPrice.subscribe(
      data=>this.totalPrice=data
    )  ;
    
    this.cartService.totalQty.subscribe(
      data=>this.totalQty=data
    );
    this.cartService.computeCartTotals();
  }

  incQty(incQty:CartItem)
  {
     this.cartService.addToCart(incQty);
  }
  decQty(decQty:CartItem)
  {
    this.cartService.decQtyaddToCart(decQty);
  }

  remove(removeItem:CartItem)
  {
    this.cartService.remove(removeItem);
  }


}
