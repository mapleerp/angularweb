import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CartItem } from '../../ModelClass/cart-item';
import { CartService } from '../../services/cart.service';
import { ProductService } from '../../services/product.service';
import { Product } from '../product';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

 
  constructor(private productserv:ProductService,
    private route: ActivatedRoute, private cartService:CartService) { }
    ProductId:any
    ProductIdnu:number | undefined
    Produ:Product=new Product;
  
    ngOnInit(): void {
  
      this.route.paramMap.subscribe(()=>{
        this.getProductDetails();
      });
    }
    getProductDetails() {
     
      this.ProductId=  this.route.snapshot.paramMap.get('id');
      this.ProductIdnu=+ this.ProductId;
  
      this.productserv.getProductDetails(this.ProductIdnu).subscribe(data => {this.Produ=data})
    }
  
    addToCart()
    {
       const theCartItem= new CartItem(this.Produ);
       this.cartService.addToCart(theCartItem);
    }
  
}
