import { Page } from "../ModelClass/page";


export class Product {
     id:any
     sku: string | undefined;
     name: string | undefined;
     description: string | undefined;
     unitPrice: number | undefined;
     imageUrl: string | undefined;
     active: boolean | undefined;
     unitsInStock: number | undefined;
     dateCreated:Date | undefined;
     lastUpdated: Date | undefined;

        page:Page | undefined;

    
}
