import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CartItem } from '../../ModelClass/cart-item';
import { CartService } from '../../services/cart.service';
import { ProductService } from '../../services/product.service';
import { Product } from '../product';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  previousCategoryId: number=1;

  constructor(private productserv:ProductService,
    private route: ActivatedRoute,private cartService:CartService) { }

  Produ:Product[]=[];
  currentCategoryIdStr:any | undefined
  currentCategoryId:number=0;
  searcMode:boolean | undefined;
  serchStr:any
  serchkeyword:number=0;
  thePageNumber:number=1;
  thePageSize:number=10;
  theTotalElements:number=0;

  ngOnInit(): void {
    this.route.paramMap.subscribe(()=>{
    this.getProductList();
  });
  }
getProductList()
{
this.searcMode=this.route.snapshot.paramMap.has('keyword');
if(this.searcMode)
{
  this.handleSearchProdutcs();
}
else{
  this.handleListProdutcts();
}
}
  handleSearchProdutcs() {
    this.serchStr=  this.route.snapshot.paramMap.get('keyword');
   this.productserv.searchProdycts(this.serchStr).subscribe(data => {this.Produ=data})
   
  }

handleListProdutcts()
{
  const hasCategoryId:boolean = this.route.snapshot.paramMap.has('id');
  if (hasCategoryId)
  {
    this.currentCategoryIdStr=  this.route.snapshot.paramMap.get('id');
    this.currentCategoryId=+ this.currentCategoryIdStr;
  }
  else {
    this.currentCategoryId=1;
  }

  if(this.previousCategoryId != this.currentCategoryId)
  {
    this.thePageNumber=1;
  }

   this.previousCategoryId =this.currentCategoryId
this.productserv.getProductList(this.currentCategoryId).subscribe(data => {this.Produ=data})

//  this.productserv.getProductListpages(this.thePageNumber-1,this.thePageSize,this.currentCategoryId).subscribe(data => {this.Produ=data,
// this.Produ[0].page?.pageNumber+1}
// )
}

listProductsPageChanges()
{
 
}

addToCart(product:Product)
{

  console.log("AddToCart...!")
 
     const thecatrtItem=new CartItem(product);
     console.log(thecatrtItem)
     this.cartService.addToCart(thecatrtItem);

}
}
