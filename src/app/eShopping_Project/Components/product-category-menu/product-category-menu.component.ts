import { Component, OnInit } from '@angular/core';
import { ProductCategory } from '../../ModelClass/product-category';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-product-category-menu',
  templateUrl: './product-category-menu.component.html',
  styleUrls: ['./product-category-menu.component.css']
})
export class ProductCategoryMenuComponent implements OnInit {

  productcategory:ProductCategory[]=[];
  constructor(private productserv:ProductService) { }

  ngOnInit(): void {
    this.listProductCategories();
  }
  listProductCategories() {
    this.productserv.getproductcategories().subscribe(
      data=> {this.productcategory=data
      }

    );
  }
}
