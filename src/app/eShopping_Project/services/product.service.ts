import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../Components/product';
import { Country } from '../ModelClass/country';
import { Customer } from '../ModelClass/customer';
import { ProductCategory } from '../ModelClass/product-category';
import { State } from '../ModelClass/state';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
 
 
 
 
 

  private baseUrl='http://localhost:8080/getallproducts'
  private baseUrl2='http://localhost:8080/productsearchbycategort?id=';
  private baseUrl3='http://localhost:8080/getallproductscategory'
  private baseUrl4='http://localhost:8080/searchprodudtcs/'
  private baseUrl5='http://localhost:8080/getproductdetaislbyid/'
  private baseUrl6='http://localhost:8080'

  constructor( private httpClient:HttpClient) { }

  getProductDetails(ProductIdnu: number) {

    return this.httpClient.get<Product>(this.baseUrl5+ProductIdnu);
   
  }

  getProductList(categoryId:number)
  {
     
      return this.httpClient.get<Product[]>(this.baseUrl2+categoryId);
     
}

getProductListpages(arg0: number, thePageSize: number, currentCategoryId: number) {

  return this.httpClient.get<Product[]>(this.baseUrl2+currentCategoryId+arg0+thePageSize);
 
}

getproductcategories() {
 return this.httpClient.get<ProductCategory[]>(this.baseUrl3);
}

searchProdycts(serchStr: any) {

  console.log("searchword"+serchStr)
  return this.httpClient.get<Product[]>(this.baseUrl4+serchStr);
}


getAllCountryNames()
{

  console.log(this.baseUrl6+"/getallcountry")
  return this.httpClient.get<Country[]>(this.baseUrl6+"/getallcountry");
}

getStatesByCountry(id:any)
{
  console.log(this.baseUrl6+"/getstatebycountry/"+id)
  return this.httpClient.get<State[]>(this.baseUrl6+"/getstatebycountry/"+id);
}


getUserDetails(email: string, firstName: string) {

  console.log(this.baseUrl6+"/getuserdetails/"+email+'/'+firstName)
  return this.httpClient.get<Customer>(this.baseUrl6+"/getuserdetails/"+email+'/'+firstName);
 

}

getOrderHistory(theEmail:string)
{
  console.log(this.baseUrl6+"/getorderhistory/"+theEmail)
  return this.httpClient.get<any[]>(this.baseUrl6+"/getorderhistory/"+theEmail);
 


}
}
