import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { Product } from '../Components/product';
import { CartItem } from '../ModelClass/cart-item';


@Injectable({
  providedIn: 'root'
})
export class CartService {
 

  cartItems:CartItem[]=[];
  totalPrice:Subject<number>=new BehaviorSubject<number>(0);
  totalQty:Subject<number>=new BehaviorSubject<number>(0);

  //  exixistingItem!: CartItem;

  pro:Product =new Product

  exixistingItem : CartItem= new CartItem(this.pro)



  constructor() { }

 addToCart(theCartItem:CartItem)
 {
   let alreadyExixistCar:boolean=false;
   this.exixistingItem= new CartItem(this.pro);
   
  //  if(this.cartItems.length>0)
  //  {
   if(this.cartItems.length>0)
     for (let tempcartItem of this.cartItems)
     {
       if(tempcartItem.id===theCartItem.id)
       {
         this.exixistingItem=tempcartItem;
         break;
       }
      }
      alreadyExixistCar=(this.exixistingItem.id!=null)
  //  }

   if(alreadyExixistCar)
   {
    this.exixistingItem.quantity++;
   }
   else{
     this.cartItems.push(theCartItem);
   }

   this.computeCartTotals();

 }
  computeCartTotals() {
  let totalPriceValue:number=0;
  let totalQtyValue:number=0;

  for (let curreentitem of this.cartItems)
  {
    totalPriceValue+=curreentitem.quantity*curreentitem.unitPrice;
    totalQtyValue+=curreentitem.quantity;
  }
  this.totalPrice.next(totalPriceValue);
  this.totalQty.next(totalQtyValue);


  }

  decQtyaddToCart(decQty: CartItem) {
     
    decQty.quantity--;

    if(decQty.quantity===0)
    {
      this.remove(decQty);
    }
    else{
      this.computeCartTotals();
    }
  
  }
  remove(decQty: CartItem) {
    const itemIndex=this.cartItems.findIndex(
      tempcartItem=>tempcartItem.id==decQty.id
    );
    if(itemIndex>-1)
    {
      this.cartItems.splice(itemIndex,1);
      this.computeCartTotals();
    }
  }
}
