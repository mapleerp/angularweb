import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Purchase } from '../ModelClass/purchase';


@Injectable({
  providedIn: 'root'
})
export class CheckoutService {

  constructor(private httpClient:HttpClient) { }


  placeOrder(purchase: Purchase): Observable<any>
  {
console.log("Purchase Sent");
console.log(purchase);

     return this.httpClient.post<Purchase>('http://localhost:8080/purchase',purchase)
  }
}
