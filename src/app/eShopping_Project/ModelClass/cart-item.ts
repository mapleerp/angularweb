import { Product } from "../Components/product";


export class CartItem {

    id:any;
    name:any;
    imageUrl:any;
    unitPrice:any;
    quantity:any;

    constructor(product:Product)
    {
        this.id=product.id;
        this.name=product.name;
        this.imageUrl=product.imageUrl;
        this.unitPrice=product.unitPrice;
        this.quantity=1;
    }
}
