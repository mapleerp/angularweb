import { FormControl, ValidationErrors } from "@angular/forms";

export class CheckoutFormValidators {
    static noOnlyWhitspace(control:FormControl): ValidationErrors
    {
if((control.value!=null) && (control.value.trim().length===0))
{
    return {'noOnlyWhitspace':true}
}
   else {

    return null;

   }
      

    }
}
