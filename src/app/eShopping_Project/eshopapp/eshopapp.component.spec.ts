import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EshopappComponent } from './eshopapp.component';

describe('EshopappComponent', () => {
  let component: EshopappComponent;
  let fixture: ComponentFixture<EshopappComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EshopappComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EshopappComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
