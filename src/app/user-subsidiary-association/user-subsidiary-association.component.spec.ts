import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSubsidiaryAssociationComponent } from './user-subsidiary-association.component';

describe('UserSubsidiaryAssociationComponent', () => {
  let component: UserSubsidiaryAssociationComponent;
  let fixture: ComponentFixture<UserSubsidiaryAssociationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserSubsidiaryAssociationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSubsidiaryAssociationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
