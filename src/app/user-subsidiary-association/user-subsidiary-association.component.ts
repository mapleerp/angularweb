import { Component, OnInit } from '@angular/core';
import { BranchMst } from '../modelclass/branch-mst';
import { UserMst } from '../modelclass/user-mst';
import { UserSubsidiary } from '../modelclass/user-subsidiary';
import { SharedserviceService } from '../services/sharedservice.service';

@Component({
  selector: 'app-user-subsidiary-association',
  templateUrl: './user-subsidiary-association.component.html',
  styleUrls: ['./user-subsidiary-association.component.css']
})
export class UserSubsidiaryAssociationComponent implements OnInit {
  userId: any;
  branchId: any;
  users: UserMst[] = [];
  branches: BranchMst[] = [];
  userSub: UserSubsidiary = new UserSubsidiary();
  usub: UserSubsidiary[] = [];
  sub: UserSubsidiary = new UserSubsidiary();
  search: any;
  constructor(private service: SharedserviceService) { }

  ngOnInit(): void {
    this.service.getBranchNames(). subscribe(data => {this.branches=data})
    this.service.getAllUsers(). subscribe(data => {this.users=data
    console.log(data)})
    this.showAll()
    // this.users = this.service.getUser();
    // this.branches = this.service.getBranch();
  }
  save() {

    if(this.userId==null){
      alert("Select a user");
      return;
    }

    if(this.branchId==null){
      alert("Select a branch")
      return;
    }
    this.userSub = new UserSubsidiary();
    this.userSub.userId = this.userId;
    this.userSub.branchId = this.branchId;
    // this.sub = new UserSubsidiary();
    // this.sub = this.service.saveUserSubsidiary(this.userSub);
    // this.usub.push(this.sub);

    this.service.saveUserSubsidiary(this.userSub).subscribe(
      (data: any) => {
        console.log(data);
        console.log('Data saved');
        alert('User Subsidiary Association saved!!');
        this.clear();
        this.showAll();
      },
      (error: any) => {
        console.log(error);
        alert('User Subsidiary Association cannot be saved!');
        this.clear();
      }
    );
  }

  // highlightRow(usub: UserSubsidiary) {}

  showAll(){
    this.service.showAllUserBranchAssociation().subscribe((data:any)=>{
      this.usub=data;
    })
  }

  clear(){
      this.userId="";
      this.branchId="";
  }

  getUserNameById(user:any){
    const userNameById = this.users.find((data) => data.id === user);
    if (!userNameById) {
      // we not found the parent
      return '';
    }
    return userNameById.userName;
  }

  getBranchNameById(branch:any){
    const branchNameById = this.branches.find((data) => data.id === branch);
    if (!branchNameById) {
      // we not found the parent
      return '';
    }
    return branchNameById.branchCode;
  }

  deleteUserById(indx:any){
    this.service.deleteUserBranchAssociationById(indx).subscribe((data:any)=>{});
    this.showAll();
  }

}
