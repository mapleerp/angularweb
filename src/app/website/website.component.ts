import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { WebsiteClass } from '../modelclass/website-class';
import { WebsiteFeatureMst } from '../modelclass/websiteModelClass/website-feature-mst';
import { WebsiteMainMst } from '../modelclass/websiteModelClass/website-main-mst';
import { WebsiteServiceMst } from '../modelclass/websiteModelClass/website-service-mst';
import { SharedserviceService } from '../services/sharedservice.service';

import { WebsiteServiceService } from '../services/website-service.service';
@Component({
  selector: 'app-website',
  templateUrl: './website.component.html',
  styleUrls: ['./website.component.css']
})
export class WebsiteComponent implements OnInit {
  companyname = environment.company;
  hover:any
 

  sectioncss= {'color': 'blueviolet'};
  hometext={'padding-left': '512px;'};
  hometexth1={'color': '#000','font-size': '2rem','margin-bottom': '1rem'};
  aboutimg={' border-radius':' 3rem 0 3rem 3rem'}
  abouttextspan={'font-size': '1rem','text-transform': 'uppercase','font-weight': '500','color': 'green'};
  abouttexth2={'font-size': '1.7rem'};
  abouttextp={'font-size': '0.938rem', 'margin': '1rem 0 1rem'}
  servicebox={ 'padding': '20px','background': '#fff','box-shadow': '2px 2px 18px rgb(14 52 54 / 15% )', 'text-align': 'center', 'border-radius': '2rem'}
  serviceboxi={ 'padding': '10px', 'border-radius': '50%','background': '#f6f6f6', 'color': 'green','font-size': '20px'}
  serviceboxh3={'font-size': '1.1rem','margin': '1rem 0 0.4rem'}
  serviceboxp={'font-size': '00.938rem'}
  featureheading={'text-align': 'center','margin-bottom': '2rem'}
  featurecontainer={ 'display': 'grid','grid-template-columns':' repeat(auto-fit,minmax(240px,auto))','gap':' 3rem','padding': '0 50px'}
  featurecontainerboximg={ 'border-radius': '1rem','height': '220px','object-fit': 'cover','object-position': 'center'}
  featurecontainerboxcontent={'display': 'flex','justify-content': 'space-between','margin-top': '2rem'}
  featurecontainerboxcontenttexth3={'font-weight': '500'}
  featurecontainerboxcontenttextp={'font-size': '0.8rem'}
  newsletterh2={'font-size': '1.7rem','color': '#fff'}
  newsletterform={'background': '#fff','box-shadow':'2px 2px 18px rgb(14 52 54 / 15% )','padding': '6px 10px','border-radius': '5rem'}
  newsletterforminput={'border': 'none','outline': 'none','font-size': '1rem'}
  footer={'background': '#415164','color': '#fff','border-radius': '5rem 0 0 0'}
  footercontainerh2={'font-size': '1.7rem','font-weight': '500','color': '#27ae60'}
  footerbox={'display': 'flex','flex-direction': 'column'}
  footerboxh3={'margin-bottom': '1rem','font-size': '1rem','font-weight': '400','color': '#fff'}
  sociala={'font-size': '20px','margin-right': '1rem'}
  copyright={'padding': '20px','text-align': 'center','color': '#fff','background': 'green'}
  













  // serviceboxhover={'hover'? {' background-color': 'rgb(5, 32, 5)', 'color':' #fff', 'transition': '0.4s all linear' }: {'padding': '20px','background': '#fff','box-shadow': '2px 2px 18px rgb(14 52 54 / 15% )','text-align': 'center','border-radius' : '2rem'}" }


  website:WebsiteClass=new WebsiteClass;
  websiteMainMst:WebsiteMainMst= new WebsiteMainMst;
  public websiteFeatureMst:WebsiteFeatureMst[]= [];
  public websiteServiceMst:WebsiteServiceMst[]= [];
  public websiteFeatureMst1 = new BehaviorSubject<WebsiteFeatureMst[]>([]);
  copyrightp: any;

  webObject:WebsiteClass=new WebsiteClass;



  constructor(private service:WebsiteServiceService, private sharedService:SharedserviceService) { }

  ngOnInit(): void {
  
  this.sharedService.getWebsiteConstants().subscribe(data=>{this.webObject=data;

    console.log(this.webObject)
  
  
    this.websiteMainMst=this.webObject.websiteMainMst;
    this.websiteFeatureMst=this.webObject.websiteFeatureMst;
    this.websiteServiceMst=this.webObject.websiteServiceMst;

    console.log("arraylist");
    console.log("arraylistffffffffffffffffffffff"+this.websiteFeatureMst);
    console.log(this.webObject.websiteFeatureMst[0]);


    console.log("paragraph"+this.webObject.websiteServiceMst[0].profiletextstrong1)

    // for(let i=0;i<this.webObject.websiteFeatureMst.length;i++)
    // {
    //      this.websiteFeatureMst[i].featureboximgurl1(this.webObject.websiteFeatureMst[i].featureboximgurl1);
    //      this.websiteFeatureMst[i].featuresspan1(this.webObject.websiteFeatureMst[i].featuresspan1);
    //      this.websiteFeatureMst[i].featuresstrong1(this.webObject.websiteFeatureMst[i].featuresstrong1);
         
    // }
    // console.log(this.websiteFeatureMst);


       // console.log(this.webObject.websiteFeatureMst[i]+" feature")
       
    // this.banner=this.websiteMainMst.searchbannertexth1;

  
  
    // console.log("home banner in service"+this.banner);
    //this.websiteFeatureMst1.next(this.webObject.websiteFeatureMst);
    //console.log("sdhjko" +this.webObject.websiteFeatureMst[1].featureboximgurl1)
   
    // for(let i=0;i<12;i++){
    //   console.log(this.webObject.websiteFeatureMst[i]+" feature")
    // }
  
  console.log(this.websiteServiceMst)
  
      })
  



    this.getWebsiteMain();
    this.getWebsiteFeature();
    this.getWebsiteService();
  }
  getWebsiteService() {
    this.service.getWebsiteService().subscribe((data: any)=>
    {
      this.websiteServiceMst=data;
      console.log("websiteserviceeeeeeeeeeeeee"+this.websiteServiceMst);

      
    })
  }
  getWebsiteFeature() {
    this.service.getWebsiteFeature().subscribe((data: any)=>
    {
      this.websiteFeatureMst=data;
      console.log("websiteFeaturesssssssssssssssssssss"+data);

      
    })
  }
  getWebsiteMain(){
    //  this.service.getWebsiteHome().subscribe((data: any)=>
    // {
    //   this.websiteMainMst=data;
      
    // })
    // console.log("copyyyyyyyyyyyyyyyyyyyyyyyy"+this.websiteMainMst.copyrightp);
    // this.copyrightp=this.websiteMainMst.copyrightp;
    // console.log("copyyyyyyyyyyyyyyyyyyyyyyyy"+this.copyrightp);
  }



}
