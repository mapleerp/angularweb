import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name:'searchPipe'})
export class SearchPipePipe implements PipeTransform {

  // transform(value: unknown, ...args: unknown[]): unknown {
  //   return null;
  // }
  transform(value: any, args?: any): any {
    if (!args) {
      return value;
    }
    return value.filter((val: { id: string; email: string; }) => {
      let rVal = (val.id.toLocaleLowerCase().includes(args)) || (val.email.toLocaleLowerCase().includes(args));
      return rVal;
    })

  }

}
