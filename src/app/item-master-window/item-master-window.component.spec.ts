import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemMasterWindowComponent } from './item-master-window.component';

describe('ItemMasterWindowComponent', () => {
  let component: ItemMasterWindowComponent;
  let fixture: ComponentFixture<ItemMasterWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemMasterWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemMasterWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
