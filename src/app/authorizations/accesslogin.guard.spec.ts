import { TestBed } from '@angular/core/testing';

import { AccessloginGuard } from './accesslogin.guard';

describe('AccessloginGuard', () => {
  let guard: AccessloginGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AccessloginGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
