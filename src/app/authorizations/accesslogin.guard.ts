import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {SharedserviceService} from '../services/sharedservice.service'
@Injectable({
  providedIn: 'root'
})
export class AccessloginGuard implements CanActivate {
  s: boolean = false;
 

  constructor(private sharedserviceService:SharedserviceService) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
 

     
     /**this.sharedserviceService.getloginStatus().subscribe ((data: boolean) => {this.s=data})**/
     console.log('LoginAccess'+this.s)
     return this.sharedserviceService.getloginStatus();
    
  }
  
}
