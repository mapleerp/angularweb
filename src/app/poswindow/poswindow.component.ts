import { Component, OnInit,ViewChild, ElementRef, HostListener  } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { Editedqtydetails, QtyEditWindowComponent } from '../dialogWindows/qty-edit-window/qty-edit-window.component';
import { AccountHeads } from '../modelclass/account-heads';
import { SalesDtl } from '../modelclass/sales-dtl';
import { SalesReceipts } from '../modelclass/sales-receipts';
import { SalesTransHdr } from '../modelclass/sales-trans-hdr';
import { Summary } from '../modelclass/summary';
import { ItemStockPopUpComponent } from '../Pop-Up-Windows/item-stock-pop-up/item-stock-pop-up.component';
import { datas, SearchPopUpComponent } from '../Pop-Up-Windows/search-pop-up/search-pop-up.component';
import { InvoiceBillGerationComponent } from '../reportPages/invoice-bill-geration/invoice-bill-geration.component';
import { PosInvoiceBillComponent } from '../reportPages/pos-invoice-bill/pos-invoice-bill.component';
import { SaleOrderPopupService } from '../services/sale-order-popup.service';
import { SharedserviceService } from '../services/sharedservice.service';
import { Network } from '@ngx-pwa/offline';
import { ReceiptModeMst } from '../modelclass/receipt-mode-mst';
import { ProductSearchServiceService } from '../services/product-search-service.service';
import { CardAmountTransactionsService } from '../services/card-amount-transactions.service';
import { SalesReceiptsService } from '../services/sales-receipts.service';
import { SalesBillDetails } from '../modelclass/sales-bill-details';
import { PosFinalBillService } from '../services/pos-final-bill.service';
import { SalesDtlsService } from '../services/sales-dtls.service';
import { SalesDtlsDeletionService } from '../services/sales-dtls-deletion.service';
import { ItemStockPopUp } from '../modelclass/item-stock-pop-up';

@Component({
  selector: 'app-poswindow',
  templateUrl: './poswindow.component.html',
  styleUrls: ['./poswindow.component.css']
})
export class PoswindowComponent implements OnInit {

  constructor(private router: Router, 
              private sharedserviceService: SharedserviceService, 
              public addDialog1: MatDialog,
              public addDialog2: MatDialog, 
              public editQtyAdd: MatDialog, 
              public formBuilder: FormBuilder,
              public productSearchServiceService: ProductSearchServiceService,
              public posFinalBillService: PosFinalBillService,
              public salesDtlsService: SalesDtlsService, 
              public salesDtlsDeletionService: SalesDtlsDeletionService,
              public salesReceiptsService: SalesReceiptsService,
              public cardAmountTransactionsService: CardAmountTransactionsService,
              public saleOrderPopupService: SaleOrderPopupService, 
              public datepipe: DatePipe, 
              protected network: Network,) { { this.networkStatus$ = this.network.onlineChanges; }}
              networkStatus$;
  tempnewUser = new BehaviorSubject<any>({});
  PosBilling:any=FormGroup;
  branchname='';
  recivdata:datas= new datas;
  tdatas:datas[] =[];
  updateUserInfo: any;
  qty:number | undefined
  itemqty:any
  addingToTable:datas[]=[];
  //isNotNull:boolean=false;
  totalAmount:number=0.00;
  paidAmount:number=0.00;
  changeAmont:number=0;
  editRowqty:any;
  edittedQty:Editedqtydetails= new Editedqtydetails
  edittedQtyIndex:any
  edtQTY:any
  showSalesSummary:Summary=new Summary;
  salesDtl:SalesDtl=new SalesDtl;
  salesDtls:SalesDtl[]=[];
  salesDtlAddTables:SalesDtl[]=[];
  salesdt:SalesDtl[]=[];
  salesReceipts:SalesReceipts=new SalesReceipts;
  cashPaid=0.0;
  cardAmount=0.0;
  cashToPay=0.0;
  sodexoAmount=0.0;
  changeAmount=0.0;
  salesReceiptVoucherNo=null;
  accountHead:AccountHeads=new AccountHeads;
  salesTransHdr: SalesTransHdr=new SalesTransHdr;
  getSalesDtlforDele:SalesDtl=new SalesDtl;
  mobileNumber:any="";
  mob:string="";
  dateCov:any;
  date= new Date();
  holdedPos:SalesTransHdr[]=[]
    temp:SalesDtl[]=[];
    getSalesBillDtl:SalesDtl[]=[];
    summary:Summary=new Summary;
    receiptMOdeMst:ReceiptModeMst[]=[]
    receiptMode="";
    cardamount=0.0;
    cardSales:CardSales= new CardSales;
    cardSalesList:CardSales[]=[];
    cardSalesCount=0;
    deletCardAmount:any;
    cardTotalAmount=0.00;
    rowClicked:any;
    SalesBillDetails:SalesBillDetails= new SalesBillDetails;

    cardIndex=0;
    showBal=-1;
  
salesReceiptsArray:SalesReceipts[]=[];
salesTransHdrArray:SalesTransHdr[]=[];
salesDtlArray:SalesDtl[]=[];

itemStockDtl:ItemStockPopUp[]=[];
itemName:any;
barcodeqty:any='';
barcodesearchfiled:any='';
salesDtlByBarcode:SalesDtl=new SalesDtl;
dateConversion:any;
batch:any;
rate:any;
notbarcodeMsg:any=false;
qtyVisible=true;
public keypressed:any;
paidBillAmount:number;
totalPaidBillAmount: any=0;
@ViewChild("idbarcd") barCodeField: ElementRef;
@ViewChild("input2") quantityField: ElementRef;
@ViewChild("paidamt") paidAmtField: ElementRef;
@ViewChild("btnfinalsubmit") finalSubmitBtnField: ElementRef;
  ngOnInit(): void {
    this.barcodeqty=1;
    // const element = this.renderer.selectRootElement('#idbarcd');
    // setTimeout(() => element.focus(), 0);
    // this.barCodeField.nativeElement.focus();

    //pos.. Billl Details Collected from Service// 
    this.billDetailTable();
  
    //Get All receipt mode..//
    this.sharedserviceService.getAllReceiptMode().subscribe(data=>{this.receiptMOdeMst=data});
    // this.sharedserviceService.getAllReceiptModeWithoutCash().subscribe(data=>{this.receiptMOdeMst=data}); 
    //Get All receipt mode..//
    this.onKeypressEventforItemSearchFromItemBatchDtl();
  }

  //autofocusing to barcode field
  ngAfterViewInit(){
    this.barCodeField.nativeElement.focus();
  }

  removeNull() {
   
    this.salesDtlAddTables = this.salesDtlAddTables.filter(Boolean);
    }

    totalPrice() {

      this.totalAmount=0.00;

      console.log("TotalPrice function called")
      let num:number
      for(let data of this.addingToTable){
        num=+data.Amount
        this.totalAmount= num+this.totalAmount;
      }
    
    }
    finalBalance(amount:number)
    {
      let balance:number;
      balance=amount-this.totalAmount;
      this.changeAmont=balance;
      console.log(balance+"final balance")
    }


    ngOnDestroy(): void
    {

      this.clearAll();
      console.log("page destroyed")
      this.addingToTable=[];
      console.log(this.addingToTable)
      this.recivdata= new datas;
      this.totalAmount=0.00;
      this.paidAmount=0.00;
      this. changeAmont=0;
      this.editRowqty;
      this.edittedQty;
      this.edittedQtyIndex;
      this.edtQTY;
      this.sharedserviceService.setInfo(this.tempnewUser)
     
    }

    onBlurMethod(){
      this.mob=String(this.mobileNumber);
      // console.log(this.mob +"=="+this.mobileNumber);
      if (this.mob!=undefined||this.mob!=null)
      {
      const arrayMob = Array.from(this.mob);
      console.log(arrayMob);
      console.log("Number Length:"+arrayMob.length);
      if((arrayMob.length==10)||arrayMob.length==0){

      }
      else{
        alert("Enter a 10 digit number");
      }
    }

  
  }
  unholdbill(saleshdr:SalesTransHdr)
  {
    console.log("unholdbill")
    this.salesTransHdr=saleshdr;
    this. unHold();
  }
  unHold()
  {

    console.log("unHold")
    if(this.salesTransHdr!=null)
    {
      if (this.salesTransHdr.id!=null)
      {
        this.sharedserviceService.getSalesTransHdr(this.salesTransHdr.id).subscribe(
          data=>{this.salesTransHdr=data;
          this.sharedserviceService.getSalesDtl(this.salesTransHdr).subscribe(
            data=>{this.salesDtlAddTables=data;
            
            }
          )}
        );
        this.sharedserviceService.getSalesBillDtl(this.salesTransHdr).subscribe(
          getSalesBillDt=>{this.getSalesBillDtl=getSalesBillDt;
            this.sharedserviceService.PostSalesDtlToPosWindow(this.getSalesBillDtl);
          
          this.sharedserviceService.getSalesWindowsSummary(this.salesTransHdr.id).subscribe
        (data=>{this.summary=data;
          this.salesTransHdr.invoiceAmount=this.summary.totalAmount;
        this.sharedserviceService.postSalesSummaryToService(this.summary)});
      
      }

        )
      }
    }
  }

  holdedPosDtls()
  {
   this.sharedserviceService.getHoldedPossDtls(environment.myBranch).subscribe(
     data=>{this.holdedPos=data;
      if(this.holdedPos.length==0)
      {
    
       alert("No Holded Bills")
        return;
      }
      for(let i=0;i<=this.holdedPos.length-1;i++)
      {
        this.holdedPos[i].id=data[i][2];
        this.holdedPos[i].userId=data[i][0];
        this.holdedPos[i].invoiceAmount=data[i][1];
      }
    console.log(this.holdedPos)}
   )
   
  }

  refresh(): void {
    window.location.reload();
}
 
addCardAmount()
{
  if(null!=this.salesTransHdr)
  {
    if(''==this.receiptMode || this.cardamount==0.0)
    {
      alert("Please fill card details")
      return
    }
    else 
    {
      let prcardAmount=0;
      this.sharedserviceService.getSumofSalesReceiptBySalestransHdr(this.salesTransHdr.id).subscribe(
        data=>{prcardAmount=data;
          if((prcardAmount+this.cardamount)>this.totalAmount)
          {
            alert("Card Amount should be less than or equal to invoice Amount");
            return
          }

          this.salesReceipts= new SalesReceipts;
            if(null==this.salesReceiptVoucherNo)
            {
              this.sharedserviceService.showItemCodeBarCodeItemIdForm(environment.myBranch).
              subscribe((data: null)=>{this.salesReceiptVoucherNo=data;
              this.salesReceipts.voucherNumber=this.salesReceiptVoucherNo})
            }
            else {
              this.salesReceipts.voucherNumber=this.salesReceiptVoucherNo;
            }

        }
      )
    }
  }
}
//.................................................................New Program .............................//

onKeypressEventForProductSearch(event:any)
{
  this.posSalesDetails();
   this.productSearchServiceService.callItembatchDtlDailogWindow();
   console.log("onKeypressEventForProductSearch function called")
}


addCardAmountEvent()
{
  this.cardIndex++;
  console.log(this.cardIndex+" cardIndex value in add")
  if ( this.cardamount==0.0)
  {
    alert("Please add Amount");
    return
  }
  if ( this.receiptMode=="")
  {
    this.cardamount=0.0
    alert("Please add Receipt Mode");
    return
  }
  // if(this.receiptMode=='CASH'){
  //   this.receiptMode=environment.myBranch+'-CASH'
  // }
  console.log(this.cardamount)
this.cardSales.cardamount= this.cardamount;
this.cardSales.card=this.receiptMode;

console.log(this.receiptMode+" receipt mode")
if(this.cardTotalAmount+this.cardamount>this.totalAmount){
  alert("Card Amount should be less than or equal to invoice Amount");
  return;
}
this.cardTotalAmount=this.cardTotalAmount+this.cardamount;
  // for showing the balance amount for CASH sale
  // if((this.receiptMode==environment.myBranch+"-"+"CASH" )&& this.cardIndex==1){
  //   this.finalBalance(this.cardamount);
  //   if(this.cardamount>this.totalAmount){
  //     this.cardamount=this.totalAmount;
  //   }   
  //   console.log(this.cardamount+" in mybranch-cash")
  // }
  // else{
    console.log('cash paid '+this.paidBillAmount)
    if(this.paidBillAmount==undefined){
    this.finalBalance(this.cardTotalAmount);
    }
    else{
      this.calculateBalance();
    }
  // }
// if(this.receiptMode!=environment.myBranch+"-"+"CASH" ){
//   this.finalBalance(this.cardTotalAmount);  
// }
console.log(this.cardTotalAmount+" sum of card sales")
this.cardSalesList.push(this.cardSales);
if(environment.clientOrCloud=='CLIENT'){
//Adding to salesReceipts..///
console.log("In adding salesreceipts url client")
this.salesReceiptsService.addingSalesReceipts(this.salesReceiptVoucherNo,this.totalAmount,
  this.cardSales,this.salesDtlAddTables[0].salesTransHdr);
//Adding to SalesReceipts..//
}
else if(environment.clientOrCloud=='CLOUD'){
  this.salesReceipts= new SalesReceipts;
this.salesReceipts.receiptMode=this.cardSales.card;
this.salesReceipts.receiptAmount=this.cardSales.cardamount;
this.salesReceipts.userId=this.sharedserviceService.getUserName();
this.salesReceipts.branchCode=environment.myBranch;
this.dateCov= this.datepipe.transform(this.date, 'dd-MM-yyyy');
this.salesReceipts.rereceiptDate=this.dateCov;
  this.salesReceiptsArray.push(this.salesReceipts);
}
this.cardamount=0.0;  this.receiptMode="";
this.cardSales=new CardSales;

console.log("addCardAmountEvent function called")

  // this.calculateBalance();

}

cardAmountDel(names:any)
{
  this.deletCardAmount=names;
  console.log("cardAmountDel function called")
}

deleteCardAmountEvent()
{
this.cardIndex--;
console.log(this.cardIndex+" cardIndex value in delete")
 this.cardTotalAmount=this.cardTotalAmount-this.cardSalesList[this.deletCardAmount].cardamount;
 console.log(this.cardSalesList[this.deletCardAmount].cardamount)
 if(environment.clientOrCloud=='CLOUD'){
 this.deleteFromSalesReceiptsArray(this.cardSalesList[this.deletCardAmount]);
 }
 delete this.cardSalesList[this.deletCardAmount];
 this.cardSalesList = this.cardSalesList.filter(Boolean);
 this.finalBalance(this.cardTotalAmount);  
 console.log("deleteCardAmountEvent function called")
}

// finalSubmit(generateBill:any)
// {

// //  if (this.cardSalesList.length==0)
// //  {
// //   this.salesReceiptsService.addingSalesReceiptsWithOutCards(this.salesReceiptVoucherNo,this.totalAmount,
// //     this.cardSales,this.salesDtlAddTables[0].salesTransHdr);
// //  }

// console.log(this.cardTotalAmount+" card total amount in final save")
// console.log(this.totalAmount+" total amount in final save")
// if(this.cardIndex>1 && this.cardTotalAmount>this.totalAmount){
//   console.log("Multiple Card entry -mixed mode");
//   alert("Invalid card amount");
//   return
// }

// if(this.cardTotalAmount<this.totalAmount){
//   console.log("paid amount less than bill amount");
//   alert("Invalid amount");
//   return;
// }

// // if(this.cardTotalAmount>this.totalAmount){
// //   console.log("paid amount is greater than bill amount");
// //   alert("Invalid card Amount")
// //   return
// // }
// this.paidAmount=this.totalAmount;
// this.dateCov= this.datepipe.transform(this.date, 'dd-MM-yyyy');
// console.log("paid amount in final submit= "+this.paidAmount)
// this.salesDtlAddTables[0].salesTransHdr.amountTendered=this.paidAmount;
// if(environment.clientOrCloud=='CLIENT'){
// this.sharedserviceService.updateSalesTransHdr(this.salesDtlAddTables[0].salesTransHdr, this.dateCov).subscribe(
//   data=>{this.SalesBillDetails=data;
//     console.log(data.salesTransHdr.voucherNumber+"in final submit of POS")
//   this.posFinalBillService.setPosFinalBill(this.SalesBillDetails);
 
//   this.router.navigate(['PosFinalBIll']);
// });
// }
// else if(environment.clientOrCloud=='CLOUD'){
//   this.salesTransHdr = new SalesTransHdr();
   
//     this.salesTransHdr.branchCode = environment.myBranch;
//     this.salesTransHdr.salesMode = 'POS';
//     this.salesTransHdr.voucherType = 'SALESTYPE';
//     this.salesTransHdr.creditOrCash = 'CASH';
//     this.salesTransHdr.isBranchSales = 'N';
//     this.salesTransHdr.invoiceAmount = this.totalAmount;
//     this.salesTransHdr.paidAmount=this.paidAmount;
//     this.salesTransHdr.userId = this.sharedserviceService.getUserName();
//     this.salesTransHdr.voucherDate = this.dateCov;
//     this.salesTransHdrArray.push(this.salesTransHdr);

//     let finalSales = new Map<string, Object[]>();
//     finalSales.set('SalesTransHdr', this.salesTransHdrArray);
//     finalSales.set('SalesDtlArray', this.salesDtlAddTables);
//     finalSales.set('SalesReceiptsArray', this.salesReceiptsArray);

//     console.log("in Final Save Cloud"+finalSales);

//     this.sharedserviceService.finalSaveforCloudPOS(finalSales).subscribe((data) => {
//       console.log(data);
//       this.SalesBillDetails=data;
//       console.log(data.salesTransHdr.voucherNumber+"in final submit of POS")
//       this.posFinalBillService.setPosFinalBill(this.SalesBillDetails);
 
//   this.router.navigate(['PosFinalBIll']);
//   // this.sharedserviceService.salesPdfForWeb(data.salesTransHdr.id).subscribe((data:any)=>{
//   //   console.log(data)
//   //   console.log("jjjjjjjj")
//   // })
//     });
// }

// this.clearAll();
// this.clearArraysAfterFinalSave();
// }

finalSubmit(generateBill:any)
{
  if(this.paidBillAmount==undefined){
    this.totalPaidBillAmount=this.cardTotalAmount;
  }
  else{
    this.totalPaidBillAmount=this.paidBillAmount+this.cardTotalAmount;
  }
//  if (this.cardSalesList.length==0)
//  {
//   this.salesReceiptsService.addingSalesReceiptsWithOutCards(this.salesReceiptVoucherNo,this.totalAmount,
//     this.cardSales,this.salesDtlAddTables[0].salesTransHdr);
//  }
if(this.paidBillAmount==undefined&&this.totalPaidBillAmount<this.totalAmount||this.totalPaidBillAmount<this.totalAmount){
  console.log('paid amount field is empty');
  alert('Enter a valid amount');
}
console.log(this.cardTotalAmount+" card total amount in final save")
console.log(this.totalAmount+" total amount in final save")
// if(this.totalPaidBillAmount>this.paidBillAmount){
//   console.log(this.totalPaidBillAmount);
//   console.log(this.paidBillAmount)
//   console.log("Multiple Card entry -mixed mode");
//   alert("Invalid card amount");
//   return
// }

// if(this.totalPaidBillAmount<this.totalAmount){
//   console.log("paid amount less than bill amount");
//   alert("Invalid amount");
//   return;
// }

this.paidAmount=this.totalAmount;
this.dateCov= this.datepipe.transform(this.date, 'dd-MM-yyyy');
console.log("paid amount in final submit= "+this.paidAmount)
this.salesDtlAddTables[0].salesTransHdr.amountTendered=this.paidAmount;
if(environment.clientOrCloud=='CLIENT'){
this.sharedserviceService.updateSalesTransHdr(this.salesDtlAddTables[0].salesTransHdr, this.dateCov).subscribe(
  data=>{this.SalesBillDetails=data;
    console.log(data.salesTransHdr.voucherNumber+"in final submit of POS")
  this.posFinalBillService.setPosFinalBill(this.SalesBillDetails);
 
  this.router.navigate(['PosFinalBIll']);
});
}
else if(environment.clientOrCloud=='CLOUD'){
  this.salesTransHdr = new SalesTransHdr();
   
    this.salesTransHdr.branchCode = environment.myBranch;
    this.salesTransHdr.salesMode = 'POS';
    this.salesTransHdr.voucherType = 'SALESTYPE';
    this.salesTransHdr.creditOrCash = 'CASH';
    this.salesTransHdr.isBranchSales = 'N';
    this.salesTransHdr.invoiceAmount = this.totalAmount;
    this.salesTransHdr.paidAmount=this.paidAmount;
    this.salesTransHdr.userId = this.sharedserviceService.getUserName();
    this.salesTransHdr.voucherDate = this.dateCov;
    this.salesTransHdrArray.push(this.salesTransHdr);

    let finalSales = new Map<string, Object[]>();
    finalSales.set('SalesTransHdr', this.salesTransHdrArray);
    finalSales.set('SalesDtlArray', this.salesDtlAddTables);
    finalSales.set('SalesReceiptsArray', this.salesReceiptsArray);

    console.log("in Final Save Cloud"+finalSales);

    this.sharedserviceService.finalSaveforCloudPOS(finalSales).subscribe((data) => {
      console.log(data);
      this.SalesBillDetails=data;
      if(data.salesTransHdr.voucherNumber.length!=0){
        this.barCodeField.nativeElement.focus();
      }
      console.log(data.salesTransHdr.voucherNumber+"in final submit of POS")
      this.posFinalBillService.setPosFinalBill(this.SalesBillDetails);
 
      this.router.navigate(['PosFinalBIll']);
  // this.sharedserviceService.salesPdfForWeb(data.salesTransHdr.id).subscribe((data:any)=>{
  //   console.log(data)
  //   console.log("jjjjjjjj")
  // })
    });
}

this.clearAll();
this.clearArraysAfterFinalSave();
}

changeTableRowColor(idx: any) { 
  if(this.rowClicked === idx) 
  {this.rowClicked = -1;
  }
  else this.rowClicked = idx;
}

clearAll()
{
  this.cardSalesList.length=0;
  this.totalAmount=0;
  this.changeAmont=0;
  this.cardIndex=0;
  this.cardTotalAmount=0;
  this.holdedPos.length=0;
  this.salesDtlAddTables.length=0;
  this.salesTransHdr=new SalesTransHdr
  this.sharedserviceService.savesalesTransHdrHistory(  this.salesTransHdr);
 
    this.sharedserviceService.PostSalesDtlToPosWindow(
      this.salesDtlAddTables
    );
  console.log("clearAll called")

}

posSalesDetails() {
  this.salesDtlsService.wholeSalesDetails("NULL","POS",'NULL','NULL');
   
}

itemDeletion(salesdtl:SalesDtl)
{ 
  if(environment.clientOrCloud=='CLIENT'){
    this.salesDtlsDeletionService.salesDetailsDeletion(salesdtl);
    this.billDetailTable();
  }
  else if(environment.clientOrCloud=='CLOUD'){
    this.salesDtlAddTables.forEach((value,index)=>{   
      if(value.itemId==salesdtl.itemId) {
        this.totalAmount=this.totalAmount-salesdtl.amount;
        console.log(index+" "+value.itemId+" "+salesdtl.itemId)
        this.salesDtlAddTables.splice(index,1);
      }
  });
  this.sharedserviceService.PostSalesDtlToPosWindow(
    this.salesDtlAddTables
  );
  }
}

itemQtyedit(salesdtl:SalesDtl)
{
  this.salesDtlsDeletionService.ItemQtyEdit(salesdtl);
}

billDetailTable()
{
  if(environment.clientOrCloud=='CLIENT'){
  this.sharedserviceService.salesDtlfromShardService().subscribe(
    data=>{this.salesDtlAddTables=data; 
      this.salesDtlArray=data;
      console.log(this.salesDtlArray)
      this.totalAmount=0.0;
    for (let i=0; i<=this.salesDtlAddTables.length-1;i++)
  {
    this.totalAmount=this.salesDtlAddTables[i].amount+this.totalAmount;
    
  }}
  )
}
else if(environment.clientOrCloud=='CLOUD'){
  this.sharedserviceService.salesDtlfromShardService().subscribe(
    data=>{this.salesDtlAddTables=data; 
      // this.salesDtlArray=data;
      // console.log(this.salesDtlArray)
      this.totalAmount=0.0;
    for (let i=0; i<=this.salesDtlAddTables.length-1;i++)
  {
    this.totalAmount=this.salesDtlAddTables[i].amount+this.totalAmount;
   console.log(this.totalAmount+" sales total")
  }}
  )
}
}

counterFunction(type:string,item:SalesDtl){
  this.totalAmount=this.totalAmount-item.amount;
    console.log(item.itemName)
   if(type==='add'){
    item.qty=item.qty+1;
   }
   else{
     if(item.qty!=1){
      item.qty=item.qty-1;
     }
   }
   item.amount=item.qty*item.standardPrice;
   this.totalAmount=this.totalAmount+item.amount;
  }

  clearArraysAfterFinalSave(){
    this.salesDtlArray=[];
    this.salesTransHdrArray=[];
    this.salesReceiptsArray=[];
  }

  deleteFromSalesReceiptsArray( cardSale:CardSales){
    this.salesReceiptsArray.forEach((value,index)=>{   
      if(value.receiptMode==cardSale.card) {
        console.log(index+" "+value.receiptMode+" "+cardSale.card)
        this.salesReceiptsArray.splice(index,1);
      }
  });
  }

  onKeydown(event) {
    if(typeof this.barcodesearchfiled=="undefined")
    {
     this.barcodesearchfiled='';
    }
    if (event.key === "Enter" || event.key === "Tab") {
      console.log(event);
      if(this.barcodesearchfiled.length!=0){
        //barcode field contains value
        console.log('barcode field contains value')
        // this.qtyVisible=true;
        this.getBarcodeBysearch(event);
      this.quantityField.nativeElement.focus();
      
      }
      else{
      //barcode field doesn't contain value
      // this.qtyVisible=false;
      console.log('barcode field doesnt contain value')
      //focus to paid amount
      this.paidAmtField.nativeElement.focus();
      if(this.paidBillAmount==undefined){
        this.changeAmont=-this.totalAmount;
        }
        else{
          this.calculateBalance();
        }
      // this.changeAmont=-this.totalAmount;
      }
    }
  }

  onKeyDownEventforAddItemInQty(event){
    if (event.key === "Enter" || event.key === "Tab") {
      this.salesDtlByBarcode.qty = this.barcodeqty; 
      this.salesDtlByBarcode.rate = this.salesDtlByBarcode.mrp * this.salesDtlByBarcode.qty;     
      this.salesDtlByBarcode.amount = this.salesDtlByBarcode.mrp * this.salesDtlByBarcode.qty;
    this.sharedserviceService.PostSalesDtlToCloudPosWindow( this.salesDtlByBarcode);
    this.billDetailTable();
    this.clearAfterAddItem();
    this.barCodeField.nativeElement.focus();
      }
  }
  onKeypressEventforItemSearchFromItemBatchDtl()
  {
    console.log("barcode reading"+this.barcodesearchfiled)
    if(typeof this.barcodesearchfiled=="undefined")
       {
        this.barcodesearchfiled='';
       }

    this.sharedserviceService.getStockItemPopupSearchwithBatch(this.dateConversion,this.barcodesearchfiled).subscribe(
     data=>{ 
       this.itemStockDtl=data;
       console.log("barcode reading url"+data)
       this.barcodesearchfiled=data.barcode;
        this.itemName=data.itemName;
    
    
    },  (error: any) => {
       console.log(error), alert("Unable to Fetch Data")
       }
    )
  
    console.log("onKeypressEventforItemSearchFromItemBatchDtl function called")
  }

  getBarcodeBysearch(event:Event) {

    const num = (event.target as HTMLInputElement).value;
    console.log("search barcode "+num)
    const barcodeSearch  = this.itemStockDtl.find((data) => data.barcode.includes(num));
   
      console.log(barcodeSearch)
      if (barcodeSearch==undefined||barcodeSearch==null) {
        // we not found the item with searched barcode
        this.notbarcodeMsg=true;
        this.onKeypressEventForProductSearch(event);
        console.log("we not found barcode")
        return ;
      }
      else{
        this.notbarcodeMsg=false;
      }

      this.itemName=barcodeSearch.itemName;
      this.batch=barcodeSearch.batch;
      this.rate= barcodeSearch.standardPrice;
    
      this.salesDtlByBarcode = new SalesDtl();

      this.salesDtlByBarcode.itemName = barcodeSearch.itemName;      
      this.salesDtlByBarcode.barcode = barcodeSearch.barcode;
      this.salesDtlByBarcode.batch = barcodeSearch.batch;     
           
      this.salesDtlByBarcode.itemId = barcodeSearch.itemId; 
      this.salesDtlByBarcode.mrp = barcodeSearch.standardPrice;
      console.log(this.salesDtlByBarcode.mrp);
      
      this.salesDtlByBarcode.unitName = barcodeSearch.unitName;
      this.salesDtlByBarcode.unitId = barcodeSearch.unitId;
      this.salesDtlByBarcode.taxRate=barcodeSearch.tax;
      this.salesDtlByBarcode.expiryDate=barcodeSearch.expDate;
      this.salesDtlByBarcode.standardPrice=barcodeSearch.standardPrice;
      this.salesDtlByBarcode.store=barcodeSearch.store;
      // this.sharedserviceService.PostSalesDtlToCloudPosWindow( this.salesDtlByBarcode);

    this.barcodesearchfiled= barcodeSearch.barcode;
    console.log("final barcode"+this.barcodesearchfiled)

  }

  dashBoardClick(){
    this.router.navigate(['/loginpage/DashBoard']);
    }
    posClick(){
    this.router.navigate(['/loginpage/PosWindow']);
    }
    
    partyCreateClick(){
    this.router.navigate(['/loginpage/PartyCreation']);
    }
    
    purchaseClick(){
    this.router.navigate(['/loginpage/Purchase']);
    }
    itemClick(){
    this.router.navigate(['/loginpage/ItemCreation']);
    }
    
    @HostListener('window:keydown', ['$event'])
    handleKeyboardEvent(event: KeyboardEvent,) {
      this.keypressed = event.keyCode;
      console.log(" KEY PRESSED {}",this.keypressed)
      if(this.keypressed==13)
      {
        console.log('Enter key is pressed')
        // if(this.searchfiled.length!=0&&this.barcodeqty==1){
        //   console.log("focus to qyuantity field")
        //   this.quantityField.nativeElement.focus();
        //   // return;
        // }
        if(this.barcodesearchfiled.length!=0&&this.barcodeqty.length!=0){
          if(this.barcodeqty<=0){
            console.log('Enter quantity');
            alert("Enter quantity");
          }
          else{
          console.log("Add Item")
          this.onKeyDownEventforAddItemInQty(event);
          }
        }
        if(this.paidBillAmount>0){
          this.onKeyEventforPaidAmount();
        }
        if(this.paidBillAmount==undefined&&this.totalPaidBillAmount==0){
            this.changeAmont=0;
        }
      }
    }

    onKeyEventforPaidAmount(){
      this.salesReceipts= new SalesReceipts;
      this.salesReceipts.receiptMode=environment.myBranch+"-CASH";
      this.salesReceipts.receiptAmount=this.totalAmount;
      this.salesReceipts.userId=this.sharedserviceService.getUserName();
      this.salesReceipts.branchCode=environment.myBranch;
      this.dateCov= this.datepipe.transform(this.date, 'dd-MM-yyyy');
      this.salesReceipts.rereceiptDate=this.dateCov;
      this.salesReceiptsArray.push(this.salesReceipts); 
      this.finalSubmitBtnField.nativeElement.focus();      
    }

    calculateBalance(){this.totalPaidBillAmount=this.paidBillAmount+this.cardTotalAmount;
      this.finalBalance(this.totalPaidBillAmount);
    }
    clearAfterAddItem(){
      this.itemName='';
      this.barcodeqty=1;
      this.batch='';
      this.barcodesearchfiled='';
      this.rate='';

    }
}

export class CardSales{
  card:any;
  cardamount:any;
}
