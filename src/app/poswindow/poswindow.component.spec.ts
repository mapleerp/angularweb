import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PoswindowComponent } from './poswindow.component';

describe('PoswindowComponent', () => {
  let component: PoswindowComponent;
  let fixture: ComponentFixture<PoswindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PoswindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PoswindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
