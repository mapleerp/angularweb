import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CompanyMst } from '../modelclass/company-mst';
import { UserMst } from '../modelclass/user-mst';
import { SharedserviceService } from '../services/sharedservice.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  compName:any;
  compCode:any;
  addr1:any;
  addr2:any;
  email:any;
  mobile:any;
  pswd:any;
  cpswd:any;
  country="INDIA";
  state="KERALA";
  currency="INR";
  gst:any;

  company:CompanyMst=new CompanyMst();
  company1:CompanyMst=new CompanyMst();
  userMst:UserMst=new UserMst();

  countryList:any[]=[];
  stateList:any[]=[];
  currencyList:any[]=[];

  //variables for phone, email validation
mob:string="";
regexp:any;
searchfind:boolean=false;
  passwordchecking: string;
  constructor(private router: Router,private service:SharedserviceService) { }

  ngOnInit(): void {
    this.service.getCountryList().subscribe((data:any)=>{
      this.countryList=data
      console.log(data)
      });
      this.service.getStateByCountry(this.country).subscribe((data:any)=>{
        this.stateList=data
        console.log(data)
        });
      // this.currencyList=this.service.getCurrency();
      console.log(this.countryList);
  }

  saveCompany(){
    const str = this.compName;
    // console.log(str.replace(/[^a-zA-Z ]/g, "")); 
    this.compName=str.replace(/[^A-Z0-9]/ig, "");
    this.compCode=(this.compName.substr(0,5)).toUpperCase();
    console.log(str.replace(/[^A-Z0-9]/ig, ""));
    console.log(this.compName+" company name");
    console.log(this.compCode+" company code");
    //setting values to object
    this.company.id=this.compCode;
    console.log(this.company.id+"id setttttttttttttttttttttttttttt")
    this.company.companyName=this.compName;
    this.company.companyCode=this.compCode;
    this.company.address1=this.addr1;
    this.company.address2=this.addr2;
    this.company.country=this.country;
    this.company.state=this.state;
    this.company.currency=this.currency;
    console.log(this.company.currency+'currencyhhhhhhhhhhhhhhhhhhh')
    this.company.emailId=this.email;
    this.company.mobNo=this.mobile;
    this.company.companyGst=this.gst;

    //null checking
    if(this.compName==null){
      alert("Enter company name");
      return;
    }
    
    if(this.addr1==null){
      alert("Enter address1");
      return;
    }
    // if(this.addr2==null){
    //   alert("Enter address2");
    //   return;
    // }
    if(this.country==null){
      alert("Enter country");
      return;
    }
    if(this.state==null){
      alert("Enter state");
      return;
    }
    if(this.currency==null){
      alert("Enter currency");
      return;
    }
    if(this.email==null){
      alert("Enter Email");
      return;
    }
    if(this.mob==null){
      alert("Enter mobile number");
      return;
    }


    //password checking
    if(this.pswd==this.cpswd){

      this.passwordchecking=String(this.pswd);
     if (this.passwordchecking!=undefined||this.passwordchecking!=null)
      {
      const arrayPswd = Array.from(this.passwordchecking);
      if((arrayPswd.length>=4)){ }
      else{
      alert("your password must be at least 4 characters");
       return;
        }
}
      this.company.password=this.pswd;
    }
    else{
      alert("Password mismatch");
      return;
    }

    //email validation
this.regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
this.searchfind = this.regexp.test(this.email);
console.log(this.searchfind);
if(this.searchfind==false){
    alert("Enter valid email");
return;
}


//phone number validation
this.mob=String(this.mobile);
if (this.mob!=undefined||this.mob!=null)
{
const arrayMob = Array.from(this.mob);
if((arrayMob.length==10)){ }
else{
    alert("Enter a 10 digit number");
return;
}
}
    
    
    // this.service.registerCompany(this.company).subscribe((data:any)=>{this.company1=data})
    // this.service.registerCompany(this.company).subscribe((data:any)=>{
    //   console.log(data);
    //   this.company1=data;
    //   alert("Saved");
    // })

   
   
    
   


    this.service.registerCompany(this.company).subscribe( (data:any) =>
        {
          console.log(data);
          this.company1=data;
          
          // this.service.saveCompanyUserRegistration(this.userMst, this.company1.id);
         
          console.log('Data Send')
          alert("Company saved!!")
          this.clear();
          this.router.navigate(['Login']);
        },
        (       error: any) =>{console.log(error)
          alert("Company cannot be saved!")
          this.clear();
        
        }
       ); 
  }

  getState(c:any)
  {
    // this.stateList=this.service.getStateByCountry(c);
    this.service.getStateByCountry(c).subscribe((data:any)=>{
      this.stateList=data
      console.log(data)
      });
    console.log(this.stateList);
  }

  clear(){
  this.compName='';
  this.compCode=''
  this.addr1=''
  this.addr2=''
  this.email=''
  this.mobile=''
  this.pswd=''
  this.cpswd=''
  this.country=''
  this.state=''
  this.currency=''
  this.gst=''
  }

}
