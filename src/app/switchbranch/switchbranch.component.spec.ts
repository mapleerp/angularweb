import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwitchbranchComponent } from './switchbranch.component';

describe('SwitchbranchComponent', () => {
  let component: SwitchbranchComponent;
  let fixture: ComponentFixture<SwitchbranchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwitchbranchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchbranchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
