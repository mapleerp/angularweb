import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BranchMst } from '../modelclass/branch-mst';
import { UserMst } from '../modelclass/user-mst';
import { UserSubsidiary } from '../modelclass/user-subsidiary';
import { SharedserviceService } from '../services/sharedservice.service';

@Component({
  selector: 'app-switchbranch',
  templateUrl: './switchbranch.component.html',
  styleUrls: ['./switchbranch.component.css']
})
export class SwitchbranchComponent implements OnInit {
  branchId:any;
  branches: BranchMst[] = [];
  users: UserMst[] = [];
  userName:any;
  userMst:UserMst=new UserMst;
  userid:any;
  branchByUser:UserSubsidiary[]=[];
  constructor(private service: SharedserviceService,
    private router: Router) { }

  ngOnInit(): void {
    this.userMst=this.service.getUserMstInLogin();
    this.service.getBranchNames(). subscribe(data => {this.branches=data});
      this.getBranchByUser();
  }

  getBranchNameById(branch:any){
    const branchNameById = this.branches.find((data) => data.id === branch);
    if (!branchNameById) {
      // we not found the parent
      return '';
    }
    return branchNameById.branchCode;
  }

  // getUserIdByUserName(user:any){
  //   const userIdByName = this.users.find((data) => data.userName === user);
  //   if (!userIdByName) {
  //     // we not found the parent
  //     return '';
  //   }
  //   return userIdByName.id; 
  // }

  getBranchByUser(){
    this.service.getAllBranchesByUserId(this.userMst.id).subscribe((data:any)=>{
      this.branchByUser=data;
      console.log(data)
      console.log('branch by user id')
    })
  }

  setBranch(){
    this.service.setmyBranch(this.branchId);
    alert('Switched to branch '+this.branchId);
    this.branchId="";
    this.router.navigate(['/loginpage']);
  }
}
