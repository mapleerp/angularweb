import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleOrderWindowComponent } from './sale-order-window.component';

describe('SaleOrderWindowComponent', () => {
  let component: SaleOrderWindowComponent;
  let fixture: ComponentFixture<SaleOrderWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaleOrderWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleOrderWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
