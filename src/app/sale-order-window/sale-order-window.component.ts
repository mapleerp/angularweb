import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { datas, SearchPopUpComponent } from '../Pop-Up-Windows/search-pop-up/search-pop-up.component';
import { InvoiceBillGerationComponent } from '../reportPages/invoice-bill-geration/invoice-bill-geration.component';
import { SharedserviceService } from '../services/sharedservice.service';
import { FormGroup } from '@angular/forms';
import { CustomerDetailsPopUpComponent } from '../Pop-Up-Windows/customer-details-pop-up/customer-details-pop-up.component';
import { SaleOrderPopupService } from '../services/sale-order-popup.service';
import { CustomerMst } from '../modelclass/customer-mst';
import { BehaviorSubject } from 'rxjs';
import { QtyEditWindowComponent } from '../dialogWindows/qty-edit-window/qty-edit-window.component';
import { WholeSaleBillDtls } from '../modelclass/whole-sale-bill-dtls';
import { DatePipe } from '@angular/common';
import { environment } from 'src/environments/environment.prod';
import { ShippingAddressComponent } from '../dialogWindows/shipping-address/shipping-address.component';
import { ShippingAddress } from '../modelclass/shipping-address';
import { ShippingAdressPopUpComponent } from '../Pop-Up-Windows/shipping-adress-pop-up/shipping-adress-pop-up.component';
import { SalesTypeMst } from '../modelclass/sales-type-mst';
import { CustomerService } from '../services/customer.service';
@Component({
  selector: 'app-sale-order-window',
  templateUrl: './sale-order-window.component.html',
  styleUrls: ['./sale-order-window.component.css']
})
export class SaleOrderWindowComponent implements OnInit {

  constructor(private router: Router, private saleOrderPopupService: SaleOrderPopupService,
    public editQtyAdd: MatDialog, public datepipe: DatePipe, public shippingAddress: MatDialog,
    private sharedserviceService: SharedserviceService, public addDialog1: MatDialog,
    private customerService:CustomerService,
    public addDialog2: MatDialog, public shippingDtlPopUp: MatDialog,) { }

  selectProduct: any
  isCustomerSelected = false
  PosBilling: any = FormGroup;
  branchname = '';
  barCodeRead = '';
  recivdata: datas = new datas;
  tdatas: datas[] = [];
  updateUserInfo: any;
  qty: number | undefined
  itemqty: any
  public addingToTable: datas[] = [];
  //isNotNull:boolean=false;
  totalAmount: number = 0.00;
  paidAmount: number = 0.00;
  changeAmont: number = 0;
  editRowID: any;
  receivecust: any
  cust: CustomerMst = new CustomerMst;
  selectcustName: any;
  editRowqty: any;
  tempnewUser = new BehaviorSubject<any>({});
  dts: CustomerMst = new CustomerMst;
  wholeSaleBillDtls: WholeSaleBillDtls = new WholeSaleBillDtls
  today: any
  VoucherNumberFromServer: any;
  name: any
  pageRefresh: any
  ShippingAdress: ShippingAddress = new ShippingAddress;
  repetedItemFlag: number = 0;
  salestypeMst: SalesTypeMst[] = [];

  ngOnInit(): void {

    this.getAllSalesType();

    this.repetedItemFlag = 0;

    this.sharedserviceService.getNewUserInfo().subscribe(info => {
      this.recivdata = info;
      console.log(this.recivdata.itemName + "load status")
      if (typeof this.recivdata.itemName != 'undefined') {
        this.recivdata.Amount = this.recivdata.qty * this.recivdata.standardPrice;
        if (this.addingToTable.length == 0) {
          this.repetedItemFlag = 2;
          this.addingToTable.push(this.recivdata);
        }

        else {
          for (let i = 0; i <= this.addingToTable.length - 1; i++) {
            if (this.addingToTable[i].itemName === this.recivdata.itemName) {
              this.repetedItemFlag = 1;
              this.addingToTable[i].qty = this.addingToTable[i].qty + this.recivdata.qty;
              this.addingToTable[i].Amount = this.addingToTable[i].Amount + this.recivdata.Amount;
            }

          }
        }
        console.log(this.repetedItemFlag + "flagstatus")
        if (this.repetedItemFlag === 0) {
          this.addingToTable.push(this.recivdata);
        }

        this.repetedItemFlag = 0

        //  this.addingToTable.push(this.recivdata);
      }

      this.removeNull();
      this.totalPrice();
      console.log("recid data length" + this.addingToTable.length)

    })
    this.receiveCustomerDtls();
    const dataSubscribe = this.sharedserviceService.setInfoo
    console.log(dataSubscribe.name + 'printed')
    this.sharedserviceService.tabledatatesting().subscribe
    console.log(this.branchname + 'dddddd')
    this.sharedserviceService.AllDataPopup().subscribe(info => {
      this.tdatas = info;
      this.sharedserviceService.globaldata(this.tdatas);
      localStorage.setItem('testObject', JSON.stringify(this.tdatas));



    })

    this.sharedserviceService.sendItemQty().subscribe((data: any) => { this.itemqty = data });

    // this.AddingToTables();
    this.receiveCustomerDtls();
    this.getCustomerDetailsfromPopup();
    //...................////


  }



  // search popup calling//

  onKeypressEvent(x: any) {
    const dialogConfigAdd1 = new MatDialogConfig();
    dialogConfigAdd1.disableClose = false;
    dialogConfigAdd1.autoFocus = true;
    dialogConfigAdd1.position = { right: `10px`, top: `20px` }
    dialogConfigAdd1.width = "40%";
    dialogConfigAdd1.height = "100%";
    //this.addDialog1.open(SearchPopUpComponent,{ disableClose: true });

    this.addDialog1.open(SearchPopUpComponent, dialogConfigAdd1)
    console.log("searchpopupcalled")


  }
  callBack(name: any) {
    this.name = name;
    this.addingToTable[this.editRowqty].qty = this.name;
    this.addingToTable[this.editRowqty].Amount = this.addingToTable[this.editRowqty].standardPrice * this.addingToTable[this.editRowqty].qty;
    this.totalPrice()
  }
  // onKeypressEventForCustmer(x: any) {
  //   const dialogConfigAdd1 = new MatDialogConfig();
  //   dialogConfigAdd1.disableClose = false;
  //   dialogConfigAdd1.autoFocus = true;
  //   dialogConfigAdd1.position = { right: `10px`, top: `20px` }
  //   dialogConfigAdd1.width = "40%";
  //   dialogConfigAdd1.height = "100%";
  //   dialogConfigAdd1.data = this.cust;
  //   //this.addDialog1.open(SearchPopUpComponent,{ disableClose: true });
  //   this.addDialog1.open(CustomerDetailsPopUpComponent, dialogConfigAdd1)

  //   this.addDialog1.afterAllClosed.subscribe((result: any) => {
  //     this.cust = result;
  //     console.log("pppppooopu called" + this.cust)
  //   });

  //   console.log("customerpopupcalled")

  //   this.isCustomerSelected = true;

  //   this.receiveShippingDtl();



  // }
  onKeypressEventForShippingAdd() {

    const dialogConfigShippingAdd = new MatDialogConfig();
    dialogConfigShippingAdd.disableClose = false;
    dialogConfigShippingAdd.autoFocus = true;
    //dialogConfigShippingAdd.position= { right: `10px`, top: `20px` }
    dialogConfigShippingAdd.width = "50%";
    dialogConfigShippingAdd.height = "80%";
    //dialogConfigShippingAdd.data=this.cust;
    //this.addDialog1.open(SearchPopUpComponent,{ disableClose: true });
    this.shippingAddress.open(ShippingAddressComponent, dialogConfigShippingAdd)



    console.log("ShippingAdd")
    this.receiveShippingDtl();
  }
  onKeypressEventForShippingDtl(shipDtl: any) {

    const dialogConfigShippingDtl = new MatDialogConfig();
    dialogConfigShippingDtl.disableClose = false;
    dialogConfigShippingDtl.autoFocus = true;

    dialogConfigShippingDtl.width = "50%";
    dialogConfigShippingDtl.height = "80%";

    this.shippingDtlPopUp.open(ShippingAdressPopUpComponent, dialogConfigShippingDtl)

  }

  AddingToTables() {
    this.addingToTable.push(this.recivdata);
    this.removeNull();
    console.log("data added to tables")
  }

  deleteTabledata(i: any) {
    delete this.addingToTable[i];
    this.removeNull();
    this.totalPrice()
  }

  removeNull() {

    this.addingToTable = this.addingToTable.filter(Boolean);
  }

  totalPrice() {

    this.totalAmount = 0.00;

    console.log("TotalPrice function called")
    let num: number
    for (let data of this.addingToTable) {
      num = +data.Amount
      this.totalAmount = num + this.totalAmount;
    }

  }
  finalBalance(amount: number) {
    let balance: number;
    balance = amount - this.totalAmount;
    this.changeAmont = balance;
    console.log(balance + "final balance")
  }

  finalSubmit(generateBill: datas[] = []) {

    if (typeof this.ShippingAdress.shippingAddressName === 'undefined' || typeof this.receivecust.customerName === 'undefined') {
      if (!window.confirm('Shipping Details Not Added. Do you Want to continue ?')) {

        return

      }
    }
    let currentDate = new Date();
    this.today = currentDate.setDate(currentDate.getDate());
    this.today = this.datepipe.transform(this.today, 'yyyy-MM-dd');
    this.sharedserviceService.sendSaleOrdreBilldetailsToInvoicePage(generateBill, this.receivecust);

    this.wholeSaleBillDtls.itemDtlInWeb = generateBill;
    this.wholeSaleBillDtls.branch = environment.myBranch;
    this.wholeSaleBillDtls.userName = environment.username;
    this.wholeSaleBillDtls.loginDate = this.today;
    this.wholeSaleBillDtls.invoiceAmount = this.totalAmount;
    this.wholeSaleBillDtls.customer = this.receivecust;
    this.sharedserviceService.postwholeSaleBillDtls(this.wholeSaleBillDtls).subscribe(data => {
      console.log(data);

      if (typeof data == 'undefined') {
        return
      }
      else {

        this.VoucherNumberFromServer = data
      }
      this.sharedserviceService.sentVoucherNumberToInvoiceBill(this.VoucherNumberFromServer);
      console.log("VoucherNumber" + this.VoucherNumberFromServer)
      console.log("sendBilldetailsToInvoicePage");
      const dialogConfigAdd = new MatDialogConfig();
      dialogConfigAdd.disableClose = false;
      dialogConfigAdd.autoFocus = true;
      dialogConfigAdd.width = "80%";
      dialogConfigAdd.height = "80%";
      dialogConfigAdd.data = { callback: this.clearAllAfterBillSaving.bind(this), defaultValue: this.pageRefresh }
      this.addDialog2.open(InvoiceBillGerationComponent, dialogConfigAdd);
      if (typeof this.VoucherNumberFromServer != 'undefined') {
        this.clearAllAfterBillSaving();
      }
      else {
        return
      }
    },
      (error: any) => { console.log(error), alert("Unable to save") }
    );



  }
  billAndTaxPreview(generateBill: datas[] = []) {
    let currentDate = new Date();
    this.today = currentDate.setDate(currentDate.getDate());
    this.today = this.datepipe.transform(this.today, 'yyyy-MM-dd');
    this.VoucherNumberFromServer = 'Bill Preview'
    this.sharedserviceService.sentVoucherNumberToInvoiceBill(this.VoucherNumberFromServer);
    this.sharedserviceService.sendSaleOrdreBilldetailsToInvoicePage(generateBill, this.receivecust);

    this.wholeSaleBillDtls.itemDtlInWeb = generateBill;
    this.wholeSaleBillDtls.branch = environment.myBranch;
    this.wholeSaleBillDtls.userName = environment.username;
    this.wholeSaleBillDtls.loginDate = this.today;

    this.wholeSaleBillDtls.customer = this.receivecust;

    //    this.sharedserviceService.postwholeSaleBillDtls( this.wholeSaleBillDtls).subscribe(data=>{console.log(data); }
    //  );
    console.log("sendBilldetailsToInvoicePage");
    const dialogConfigAdd = new MatDialogConfig();
    dialogConfigAdd.disableClose = true;
    dialogConfigAdd.autoFocus = true;
    dialogConfigAdd.width = "45%";
    dialogConfigAdd.height = "80%";
    this.addDialog2.open(InvoiceBillGerationComponent, dialogConfigAdd);
  }

  qtyEditable(val: any, i: any) {
    this.editRowID = val;
    console.log("editted QTY" + this.editRowID)
  }


  getCustomerDetailsfromPopup() {

    console.log("getCustomerDetailsfromPopup called")
    this.selectProduct = this.saleOrderPopupService.getCustomerDtls()
    return this.selectProduct;
  }



  receiveCustomerDtls() {

    console.log("receiveCustomerDtls")
    this.saleOrderPopupService.getCustomerDtls().subscribe((data: any) => {
      this.receivecust = data;
      console.log(this.receivecust.customerName + "nnnanaaammmeee")
      this.selectcustName = this.receivecust.customerName
    });


  }
  receiveShippingDtl() {
    // console.log("receiveShippingDtl from services");
    // this.sharedserviceService.getShippingDtls().subscribe(
    //   (data: any)=>{this.ShippingAdress=data;
    //     console.log(this.ShippingAdress+"receiveShippingDtl")
    // }
    // );

    this.saleOrderPopupService.getShippingDtls().subscribe(data => {
      this.ShippingAdress = data;

      console.log(this.ShippingAdress + "SSHHHIPPPINGA AA A ")
    });
  }


  editQty(i: any) {
    // this.edittedQtyIndex=qty;
    this.editRowqty = i;
    const dialogConfigAdd = new MatDialogConfig();
    dialogConfigAdd.disableClose = false;
    dialogConfigAdd.autoFocus = true;
    dialogConfigAdd.width = "30%";
    dialogConfigAdd.height = "30%";
    dialogConfigAdd.data = { callback: this.callBack.bind(this), defaultValue: this.name }
    this.editQtyAdd.open(QtyEditWindowComponent, dialogConfigAdd);







  }

  clearAllAfterBillSaving() {


    this.selectcustName = null;
    this.selectProduct = null;
    this.addingToTable = [];
    this.totalAmount = 0;
    this.paidAmount = 0;
    this.ShippingAdress = new ShippingAddress;
    this.receivecust = new CustomerMst;




  }

  ngOnDestroy(): void {

    console.log("page destroyed")
    this.addingToTable = [];
    console.log(this.addingToTable)
    this.recivdata = new datas;
    this.totalAmount = 0.00;
    this.paidAmount = 0.00;
    this.changeAmont = 0;


    const generateBill = '';
    this.receivecust = new CustomerMst;
    this.sharedserviceService.setInfo(this.receivecust);
    this.saleOrderPopupService.addCustomerDtl(this.receivecust);
    this.sharedserviceService.sendSaleOrdreBilldetailsToInvoicePage(generateBill, this.dts);

  }

  getAllSalesType() {
    this.sharedserviceService.getSalesTypes().subscribe(
      data => {
        this.salestypeMst = data;
        console.log(this.salestypeMst)
      }
    )
  }


//................................New Program....//

onKeypressEventForSelectCustmer()
{
   this.customerService.selectCustomer();
}


}
