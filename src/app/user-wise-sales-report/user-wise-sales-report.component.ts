import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from '@angular/forms';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { environment } from 'src/environments/environment.prod';

import * as XLSX from 'xlsx';
import { BranchMst } from '../modelclass/branch-mst';
import { DailySalesReport } from '../modelclass/daily-sales-report';
import { UserMst } from '../modelclass/user-mst';
import { datas } from '../Pop-Up-Windows/search-pop-up/search-pop-up.component';

@Component({
  selector: 'app-user-wise-sales-report',
  templateUrl: './user-wise-sales-report.component.html',
  styleUrls: ['./user-wise-sales-report.component.css'],
})
export class UserWiseSalesReportComponent implements OnInit {
  userWiseSalesReport: any = FormGroup;
  userMst: UserMst[] = [];
  dailySalesReport: datas[] = [];
  fromDate = '';
  toDate = '';
  userid = '';
  dateConvFist: any;
  dateConvSec: any;
  // branchName: string = environment.myBranch;
  salesTransHdr: any;
  salesDetail: any;
  receiptModeReport: any;
  branch: BranchMst[] = [];
  branchname: any;

  constructor(
    private sharedserviceService: SharedserviceService,
    public fb: FormBuilder,
    public datepipe: DatePipe
  ) {}

  ngOnInit(): void {
    this.getBranch();
    this.userWiseSalesReport = this.fb.group({});
    this.sharedserviceService.getUserMst().subscribe((data) => {
      this.userMst = data;
    });
  }

  getBranch() {
    this.sharedserviceService.getBranchNames().subscribe((data: any) => {
      this.branch = data;
    });
  }

  generateReport() {
    console.log(this.fromDate, this.toDate, this.userid, this.branchname);
    this.dateConvFist = this.datepipe.transform(this.fromDate, 'yyyy-MM-dd');
    this.dateConvSec = this.datepipe.transform(this.toDate, 'yyyy-MM-dd');
    this.sharedserviceService
      .generateReport(
        this.userid,
        this.branchname,
        this.dateConvFist,
        this.dateConvSec
      )
      .subscribe((data: any) => {
        console.log(data);
        this.userWiseSalesReport = data;
      });
    this.salesreceipts();
  }
  renderPdf() {
    if(this.userWiseSalesReport.length == 0){
      return;
    }
    let element = document.getElementById('contentToConvert');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'sheet1');
    XLSX.writeFile(
      wb,
      'User-Wise-Sales-Report' +
        '-' +
        this.dateConvFist +
        '-' +
        this.dateConvSec +
        '.xlsx'
    );
  }

  salesDtl(name: DailySalesReport) {
    this.sharedserviceService
      .getSalesTransHdrForUserSale(
        name.voucherNumber,
        name.voucherDate,
        this.branchname
      )
      .subscribe((data: any) => {
        console.log(data);
        this.salesTransHdr = data;
        this.sharedserviceService
          .getSalesDtl(this.salesTransHdr)
          .subscribe((data: any) => {
            console.log(data);
            this.salesDetail = data;
          });
      });
  }

  salesreceipts() {
    console.log(this.fromDate, this.toDate, this.userid, this.branchname);
    this.dateConvFist = this.datepipe.transform(this.fromDate, 'yyyy-MM-dd');
    this.dateConvSec = this.datepipe.transform(this.toDate, 'yyyy-MM-dd');
    this.sharedserviceService
      .getUserWiseSalesReceiptReport(
        this.userid,
        this.branchname,
        this.dateConvFist,
        this.dateConvSec
      )
      .subscribe((data: any) => {
        console.log(data);
        this.receiptModeReport = data;
      });
    // this.printSalesReceiptSummary();
  }
  printSalesReceiptSummary() {
    // this.salesreceipts();

    if(this.receiptModeReport.length == 0){
      return;
    }
    let element = document.getElementById('salesReceipts');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'sheet1');
    XLSX.writeFile(
      wb,
      'User-Wise-Sales-Receipt-Summary--Report' +
        '-' +
        this.dateConvFist +
        '-' +
        this.dateConvSec +
        '.xlsx'
    );
  }
}
