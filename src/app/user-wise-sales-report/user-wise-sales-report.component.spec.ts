import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserWiseSalesReportComponent } from './user-wise-sales-report.component';

describe('UserWiseSalesReportComponent', () => {
  let component: UserWiseSalesReportComponent;
  let fixture: ComponentFixture<UserWiseSalesReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserWiseSalesReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserWiseSalesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
