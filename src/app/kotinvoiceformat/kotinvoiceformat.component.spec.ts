import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KotinvoiceformatComponent } from './kotinvoiceformat.component';

describe('KotinvoiceformatComponent', () => {
  let component: KotinvoiceformatComponent;
  let fixture: ComponentFixture<KotinvoiceformatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KotinvoiceformatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KotinvoiceformatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
