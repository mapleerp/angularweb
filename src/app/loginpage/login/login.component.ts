import { Component, OnInit } from '@angular/core';
import { SharedserviceService } from '../../services/sharedservice.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment.prod';
import { UserMst } from 'src/app/modelclass/user-mst';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  // userMst: UserMst[] = [];
  userMst:UserMst=new UserMst;
  username: any;
  passwords: any;
  companyname = environment.company;
  public loginStatus: boolean = false;
  constructor(
    private router: Router,
    private sharedserviceService: SharedserviceService
  ) {}

  ngOnInit(): void {}

  update() {
    this.sharedserviceService.updateLoginStatus(
      this.loginStatus,
      this.companyname,
      this.username,
      this.userMst
    );
  }

  onSubmit() {
    // if(this.username=='amdc123'&& this.passwords=='amdc123'&&this.companyname=='AMDC')
    //    {
    //   alert("Authenticated..!!")
    //   this.loginStatus=true;
    //   console.log(this.loginStatus)

    //   this.router.navigate(['/home'])
    //   this.update();

    //   }
    //   else if (this.username==environment.username&& this.passwords==environment.password&&this.companyname==environment.company)
    //   {
    //     alert("Authenticated..!!")
    //     this.loginStatus=true;
    //     console.log(this.loginStatus)

    //     this.router.navigate(['/homeHotCakes/DashBoard'])
    //     this.update();
    //   }
    //   else if (this.username=='lakshmi123'&& this.passwords=='lakshmi123'&&this.companyname=='LAKSHMIBAKERS')
    //   {
    //     alert("Authenticated..!!")
    //     this.loginStatus=true;
    //     console.log(this.loginStatus)

    //     this.router.navigate(['/homepageLakshmiBakery']);
    //     this.update();
    //   }

    //   else
    //   {
    //     alert("Incorrect Password or Username")
    //       return;
    //   }
    this.getWebLogin();

    console.log(this.loginStatus);
    this.update();
  }
  getWebLogin() {
    this.sharedserviceService
      .getWebLogin(environment.company, this.passwords, this.username)
      .subscribe((data) => {
        this.userMst = data;
        if (this.userMst == null) {
          if (
            this.username == 'admin' &&
            this.passwords == 'admin' &&
            this.companyname == environment.company
          ) {
            this.loginStatus = true;
            console.log('Login Successful');
            this.update();
            this.router.navigate(['/MainMenuPage/DashBoard']);
          } else {
            alert('Incorrect Password or Username');
            return;
          }
        } else {
          this.loginStatus = true;
          console.log('Login Successful');
          this.update();
          // this.router.navigate(['/homeHotCakes/DashBoard']);

          this.router.navigate(['/loginpage']);
        }
      });
    if (this.username == 'admin' && this.passwords == 'admin') {
      this.loginStatus = true;
      console.log('Login Successful');
      this.update();
      //this.router.navigate(['/mapleSystems'])
      this.router.navigate(['/loginpage']);
    }
  }
}
