import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PointOfSalesWindowComponent } from './point-of-sales-window.component';

describe('PointOfSalesWindowComponent', () => {
  let component: PointOfSalesWindowComponent;
  let fixture: ComponentFixture<PointOfSalesWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PointOfSalesWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PointOfSalesWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
