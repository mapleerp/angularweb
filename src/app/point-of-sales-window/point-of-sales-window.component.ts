import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig ,MatDialogRef} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { Editedqtydetails, QtyEditWindowComponent } from '../dialogWindows/qty-edit-window/qty-edit-window.component';
import { AccountHeads } from '../modelclass/account-heads';
import { SalesDtl } from '../modelclass/sales-dtl';
import { SalesReceipts } from '../modelclass/sales-receipts';
import { SalesTransHdr } from '../modelclass/sales-trans-hdr';
import { Summary } from '../modelclass/summary';
import { ItemStockPopUpComponent } from '../Pop-Up-Windows/item-stock-pop-up/item-stock-pop-up.component';
import { datas, SearchPopUpComponent } from '../Pop-Up-Windows/search-pop-up/search-pop-up.component';
import { InvoiceBillGerationComponent } from '../reportPages/invoice-bill-geration/invoice-bill-geration.component';
import { PosInvoiceBillComponent } from '../reportPages/pos-invoice-bill/pos-invoice-bill.component';
import { SaleOrderPopupService } from '../services/sale-order-popup.service';
import { SharedserviceService } from '../services/sharedservice.service';
import { Network } from '@ngx-pwa/offline';
import { ReceiptModeMst } from '../modelclass/receipt-mode-mst';
import { ProductSearchServiceService } from '../services/product-search-service.service';
import { CardAmountTransactionsService } from '../services/card-amount-transactions.service';
import { SalesReceiptsService } from '../services/sales-receipts.service';
import { SalesBillDetails } from '../modelclass/sales-bill-details';
import { PosFinalBillService } from '../services/pos-final-bill.service';
import { SalesDtlsService } from '../services/sales-dtls.service';
import { SalesDtlsDeletionService } from '../services/sales-dtls-deletion.service';
import { ItemStockPopUp } from '../modelclass/item-stock-pop-up';
import { ItemImageUrlDetail } from '../modelclass/item-image-url-detail';


@Component({
  selector: 'app-point-of-sales-window',
  templateUrl: './point-of-sales-window.component.html',
  styleUrls: ['./point-of-sales-window.component.css']
})
export class PointOfSalesWindowComponent implements OnInit {

  constructor(private router: Router,private sharedserviceService:SharedserviceService,public addDialog1: MatDialog,
    public addDialog2: MatDialog,public editQtyAdd:MatDialog,public formBuilder:FormBuilder,
public productSearchServiceService:ProductSearchServiceService,
public posFinalBillService:PosFinalBillService,  
public salesDtlsService:SalesDtlsService,public salesDtlsDeletionService:SalesDtlsDeletionService,
public salesReceiptsService:SalesReceiptsService,
public cardAmountTransactionsService:CardAmountTransactionsService,
    public saleOrderPopupService:SaleOrderPopupService,public datepipe: DatePipe, protected network: Network,)
     {{ this.networkStatus$ = this.network.onlineChanges; } }

     networkStatus$;
    tempnewUser = new BehaviorSubject<any>({});
  PosBilling:any=FormGroup;
  branchname='';
  barCodeRead='';
  recivdata:datas= new datas;
  tdatas:datas[] =[];
  updateUserInfo: any;
  qty:number | undefined
  itemqty:any
  addingToTable:datas[]=[];
  //isNotNull:boolean=false;
  totalAmount:number=0.00;
  paidAmount:number=0.00;
  changeAmont:number=0;
  editRowqty:any;
  edittedQty:Editedqtydetails= new Editedqtydetails
  edittedQtyIndex:any
  edtQTY:any
  showSalesSummary:Summary=new Summary;
  salesDtl:SalesDtl=new SalesDtl;
  salesDtls:SalesDtl[]=[];
  salesDtlAddTables:SalesDtl[]=[];
  salesdt:SalesDtl[]=[];
  salesReceipts:SalesReceipts=new SalesReceipts;
  cashPaid=0.0;
  cardAmount=0.0;
  cashToPay=0.0;
  sodexoAmount=0.0;
  changeAmount=0.0;
  salesReceiptVoucherNo=null;
  accountHead:AccountHeads=new AccountHeads;
  salesTransHdr: SalesTransHdr=new SalesTransHdr;
  getSalesDtlforDele:SalesDtl=new SalesDtl;
  mobileNumber:any="";
  mob:string="";
  dateCov:any;
  date= new Date();
  holdedPos:SalesTransHdr[]=[]
    temp:SalesDtl[]=[];
    getSalesBillDtl:SalesDtl[]=[];
    summary:Summary=new Summary;
    receiptMOdeMst:ReceiptModeMst[]=[]
    receiptMode="";
    cardamount=0.0;
    cardSales:CardSales= new CardSales;
    cardSalesList:CardSales[]=[];
    cardSalesCount=0;
    deletCardAmount:any;
    cardTotalAmount=0;
    rowClicked:any;
    SalesBillDetails:SalesBillDetails= new SalesBillDetails;

    cardIndex=0;
    showBal=-1;
  
salesReceiptsArray:SalesReceipts[]=[];
salesTransHdrArray:SalesTransHdr[]=[];
salesDtlArray:SalesDtl[]=[];

salesDtlArr:SalesDtl[]=[];
// date= new Date();
dateConversion:any;
productQty:any=1;

itemStockDtl:ItemStockPopUp[]=[];
searchfiled:any;

itemAmount:any=0.0;

itemImageUrl:ItemImageUrlDetail[]=[];
 
  ngOnInit(): void {

    //pos.. Billl Details Collected from Service//
 
      this.billDetailTable();

      this.dateConversion = this.datepipe.transform(this.date, 'y-MM-dd')
      // for(let i=0;i<2;i++){

      //   this.itemStockDtl[i] = new ItemStockPopUp();
      //   this.itemStockDtl[i].itemName="TEST ITEM1";
      //   this.itemStockDtl[i].qty=1;
      //   this.itemStockDtl[i].standardPrice=20;
      // }
    this.itemWithImage();
 
  //Get All receipt mode..//
  this.sharedserviceService.getAllReceiptMode().subscribe(data=>{this.receiptMOdeMst=data});

  //Get All receipt mode..//
    
  }
  itemWithImage()
  {
    if(typeof this.searchfiled=="undefined")
       {
        this.searchfiled='';
       }
    this.sharedserviceService.getStockItemPopupSearchwithBatch(this.dateConversion,this.searchfiled).subscribe(
     data=>{ 
       this.itemStockDtl=data;
       for(let i=0;i<this.itemStockDtl.length;i++)
      { 
        console.log(this.itemStockDtl[i].itemId);
        this.sharedserviceService.getItemImage(this.itemStockDtl[i].itemId).subscribe((itm:any)=>
       {
         this.itemImageUrl=itm;
         for(let j=0;j<this.itemImageUrl.length;j++){
           if(this.itemImageUrl[j].imageName=="PRIMARY"){
            console.log(this.itemImageUrl[j].imageUrl+" url ")
             this.itemStockDtl[i].imageUrl=this.itemImageUrl[j].imageUrl;
           }
         }
       })
      }
       
       console.log(data);
      },  (error: any) => { 
        console.log(error), alert("Unable to Fetch Data") 
      }
    )
    console.log("itemWithImage function called -- for item data")
  }

 removeNull() {
   
    this.salesDtlAddTables = this.salesDtlAddTables.filter(Boolean);
    }

    totalPrice() {

      this.totalAmount=0.00;

      console.log("TotalPrice function called")
      let num:number
      for(let data of this.addingToTable){
        num=+data.Amount
        this.totalAmount= num+this.totalAmount;
      }
    
    }
    finalBalance(amount:number)
    {
      let balance:number;
      balance=amount-this.totalAmount;
      this.changeAmont=balance;
      console.log(balance+"final balance")
    }


    ngOnDestroy(): void
    {

      this.clearAll();
      console.log("page destroyed")
      this.addingToTable=[];
      console.log(this.addingToTable)
      this.recivdata= new datas;
      this.totalAmount=0.00;
      this.paidAmount=0.00;
      this. changeAmont=0;
      this.editRowqty;
      this.edittedQty;
      this.edittedQtyIndex;
      this.edtQTY;
      this.sharedserviceService.setInfo(this.tempnewUser)
     
    }

    onBlurMethod(){
      this.mob=String(this.mobileNumber);
      // console.log(this.mob +"=="+this.mobileNumber);
      if (this.mob!=undefined||this.mob!=null)
      {
      const arrayMob = Array.from(this.mob);
      console.log(arrayMob);
      console.log("Number Length:"+arrayMob.length);
      if((arrayMob.length==10)||arrayMob.length==0){

      }
      else{
        alert("Enter a 10 digit number");
      }
    }

  
  }
  unholdbill(saleshdr:SalesTransHdr)
  {
    console.log("unholdbill")
    this.salesTransHdr=saleshdr;
    this. unHold();
  }
  unHold()
  {

    console.log("unHold")
    if(this.salesTransHdr!=null)
    {
      if (this.salesTransHdr.id!=null)
      {
        this.sharedserviceService.getSalesTransHdr(this.salesTransHdr.id).subscribe(
          data=>{this.salesTransHdr=data;
          this.sharedserviceService.getSalesDtl(this.salesTransHdr).subscribe(
            data=>{this.salesDtlAddTables=data;
            
            }
          )}
        );
        this.sharedserviceService.getSalesBillDtl(this.salesTransHdr).subscribe(
          getSalesBillDt=>{this.getSalesBillDtl=getSalesBillDt;
            this.sharedserviceService.PostSalesDtlToPosWindow(this.getSalesBillDtl);
          
          this.sharedserviceService.getSalesWindowsSummary(this.salesTransHdr.id).subscribe
        (data=>{this.summary=data;
          this.salesTransHdr.invoiceAmount=this.summary.totalAmount;
        this.sharedserviceService.postSalesSummaryToService(this.summary)});
      
      }

        )
      }
    }
  }

  holdedPosDtls()
  {
   this.sharedserviceService.getHoldedPossDtls(environment.myBranch).subscribe(
     data=>{this.holdedPos=data;
      if(this.holdedPos.length==0)
      {
    
       alert("No Holded Bills")
        return;
      }
      for(let i=0;i<=this.holdedPos.length-1;i++)
      {
        this.holdedPos[i].id=data[i][2];
        this.holdedPos[i].userId=data[i][0];
        this.holdedPos[i].invoiceAmount=data[i][1];
      }
    console.log(this.holdedPos)}
   )
   
  }

  refresh(): void {
    window.location.reload();
}
 
addCardAmount()
{
  if(null!=this.salesTransHdr)
  {
    if(''==this.receiptMode || this.cardamount==0.0)
    {
      alert("Please fill card details")
      return
    }
    else 
    {
      let prcardAmount=0;
      this.sharedserviceService.getSumofSalesReceiptBySalestransHdr(this.salesTransHdr.id).subscribe(
        data=>{prcardAmount=data;
          if((prcardAmount+this.cardamount)>this.totalAmount)
          {
            alert("Card Amount should be less than or equal to invoice Amount");
            return
          }

          this.salesReceipts= new SalesReceipts;
            if(null==this.salesReceiptVoucherNo)
            {
              this.sharedserviceService.showItemCodeBarCodeItemIdForm(environment.myBranch).
              subscribe((data: null)=>{this.salesReceiptVoucherNo=data;
              this.salesReceipts.voucherNumber=this.salesReceiptVoucherNo})
            }
            else {
              this.salesReceipts.voucherNumber=this.salesReceiptVoucherNo;
            }

        }
      )
    }
  }
}
//.................................................................New Program .............................//

onKeypressEventForProductSearch(event:any)
{
  this.posSalesDetails();
   this.productSearchServiceService.callItembatchDtlDailogWindow();
   console.log("onKeypressEventForProductSearch function called")
}


addCardAmountEvent()
{
  this.cardIndex++;

  // for showing the balance amount for CASH sale
  if((this.receiptMode==environment.myBranch+"-"+"CASH" )&& this.cardIndex==1){
    this.showBal=0;
    this.finalBalance(this.cardamount);
    if(this.cardamount>this.totalAmount){
      this.cardamount=this.totalAmount;
    }   
    console.log(this.cardamount+" in mybranch-cash")
  }
  else{
    this.showBal=-1;
  }

  if ( this.cardamount==0.0)
  {
    alert("Please add Amount");
    return
  }
  if ( this.receiptMode=="")
  {
    this.cardamount=0.0
    alert("Please add Receipt Mode");
    return
  }
 
  console.log(this.cardamount)
this.cardSales.cardamount= this.cardamount;
this.cardSales.card=this.receiptMode;

if((this.receiptMode!=environment.myBranch+"-"+"CASH" ) && (this.cardTotalAmount+this.cardamount>this.totalAmount)){
  alert("Card Amount should be less than or equal to invoice Amount");
  return;
}
this.cardTotalAmount=this.cardTotalAmount+this.cardamount;

console.log(this.cardTotalAmount+" sum of card sales")
this.cardSalesList.push(this.cardSales);
if(environment.clientOrCloud=='CLIENT'){
//Adding to salesReceipts..///
console.log("In adding salesreceipts url client")
this.salesReceiptsService.addingSalesReceipts(this.salesReceiptVoucherNo,this.totalAmount,
  this.cardSales,this.salesDtlAddTables[0].salesTransHdr);
//Adding to SalesReceipts..//
}
else if(environment.clientOrCloud=='CLOUD'){
  this.salesReceipts= new SalesReceipts;
this.salesReceipts.receiptMode=this.cardSales.card;
this.salesReceipts.receiptAmount=this.cardSales.cardamount;
this.salesReceipts.userId=this.sharedserviceService.getUserName();
this.salesReceipts.branchCode=environment.myBranch;
this.dateCov= this.datepipe.transform(this.date, 'dd-MM-yyyy');
this.salesReceipts.rereceiptDate=this.dateCov;
  this.salesReceiptsArray.push(this.salesReceipts);
}
this.cardamount=0.0;  this.receiptMode="";
this.cardSales=new CardSales;

console.log("addCardAmountEvent function called")



}

cardAmountDel(names:any)
{
  this.deletCardAmount=names;
  console.log("cardAmountDel function called")
}

deleteCardAmountEvent()
{
this.cardIndex--;
 this.cardTotalAmount=this.cardTotalAmount-this.cardSalesList[this.deletCardAmount].cardamount;
 delete this.cardSalesList[this.deletCardAmount];
 this.cardSalesList = this.cardSalesList.filter(Boolean);
 console.log("deleteCardAmountEvent function called")
}

finalSubmit(generateBill:any)
{
//  if (this.cardSalesList.length==0)
//  {
//   this.salesReceiptsService.addingSalesReceiptsWithOutCards(this.salesReceiptVoucherNo,this.totalAmount,
//     this.cardSales,this.salesDtlAddTables[0].salesTransHdr);
//  }
// if(this.cardIndex>1 && this.cardTotalAmount>this.totalAmount){
//   console.log("Multiple Card entry -mixed mode");
//   alert("Invalid card amount");
//   return
// }

// if(this.cardTotalAmount<this.totalAmount){
//   console.log("paid amount less than bill amount");
//   alert("Invalid amount");
//   return
// }

// if(this.cardTotalAmount>this.totalAmount){
//   console.log("paid amount is greater than bill amount");
//   alert("Invalid card Amount")
//   return
// }
this.paidAmount=this.totalAmount;
this.dateCov= this.datepipe.transform(this.date, 'dd-MM-yyyy');
console.log("paid amount in final submit= "+this.paidAmount)
this.salesDtlAddTables[0].salesTransHdr.amountTendered=this.paidAmount;
if(environment.clientOrCloud=='CLIENT'){
this.sharedserviceService.updateSalesTransHdr(this.salesDtlAddTables[0].salesTransHdr, this.dateCov).subscribe(
  data=>{this.SalesBillDetails=data;
    console.log(data.salesTransHdr.voucherNumber+"in final submit of POS")
  this.posFinalBillService.setPosFinalBill(this.SalesBillDetails);
 
  this.router.navigate(['PosFinalBIll']);
});
}
else if(environment.clientOrCloud=='CLOUD'){
  this.salesTransHdr = new SalesTransHdr();
   
    this.salesTransHdr.branchCode = environment.myBranch;
    this.salesTransHdr.salesMode = 'POS';
    this.salesTransHdr.voucherType = 'SALESTYPE';
    this.salesTransHdr.creditOrCash = 'CASH';
    this.salesTransHdr.isBranchSales = 'N';
    this.salesTransHdr.invoiceAmount = this.totalAmount;
    this.salesTransHdr.paidAmount=this.paidAmount;
    this.salesTransHdr.userId = this.sharedserviceService.getUserName();
    this.salesTransHdr.voucherDate = this.dateCov;
    this.sharedserviceService.getAccountHeadByName("POS").subscribe((data:any)=>{
      this.salesTransHdr.customerId=data.id;
      console.log(this.salesTransHdr.customerId);
    })
    
    this.salesTransHdrArray.push(this.salesTransHdr);

    let finalSales = new Map<string, Object[]>();
    finalSales.set('SalesTransHdr', this.salesTransHdrArray);
    finalSales.set('SalesDtlArray', this.salesDtlAddTables);
    finalSales.set('SalesReceiptsArray', this.salesReceiptsArray);

    console.log("in Final Save Cloud"+finalSales);

    this.sharedserviceService.finalSaveforCloudPOS(finalSales).subscribe((data) => {
      console.log(data);
      this.SalesBillDetails=data;
      console.log(data.salesTransHdr.voucherNumber+"in final submit of POS")
      this.posFinalBillService.setPosFinalBill(this.SalesBillDetails);
 
  this.router.navigate(['PosFinalBIll']);
  // this.sharedserviceService.salesPdfForWeb(data.salesTransHdr.id).subscribe((data:any)=>{
  //   console.log(data)
  //   console.log("jjjjjjjj")
  // })
    });
}

this.clearAll();
}


changeTableRowColor(idx: any) { 
  if(this.rowClicked === idx) 
  {this.rowClicked = -1;
  }
  else this.rowClicked = idx;
}

clearAll()
{
  this.cardSalesList.length=0;
  this.totalAmount=0;
  this.cardTotalAmount=0;
  this.holdedPos.length=0;
  this.salesDtlAddTables.length=0;
  this.salesTransHdr=new SalesTransHdr
  this.sharedserviceService.savesalesTransHdrHistory(  this.salesTransHdr);
 
    this.sharedserviceService.PostSalesDtlToPosWindow(
      this.salesDtlAddTables
    );
  console.log("clearAll called")

}

posSalesDetails() {
  this.salesDtlsService.wholeSalesDetails("NULL","POS",'NULL','NULL');
   
}

itemDeletion(salesdtl:SalesDtl)
{  
  this.salesDtlsDeletionService.salesDetailsDeletion(salesdtl);
  this.billDetailTable();
}
itemQtyedit(salesdtl:SalesDtl)
{
  this.salesDtlsDeletionService.ItemQtyEdit(salesdtl);
}

billDetailTable()
{
  if(environment.clientOrCloud=='CLIENT'){
  this.sharedserviceService.salesDtlfromShardService().subscribe(
    data=>{this.salesDtlAddTables=data; 
      this.salesDtlArray=data;
      console.log(this.salesDtlArray)
      this.totalAmount=0.0;
    for (let i=0; i<=this.salesDtlAddTables.length-1;i++)
  {
    this.totalAmount=this.salesDtlAddTables[i].amount+this.totalAmount;
    
  }}
  )
}
else if(environment.clientOrCloud=='CLOUD'){
  this.sharedserviceService.salesDtlfromShardService().subscribe(
    data=>{this.salesDtlAddTables=data; 
      // this.salesDtlArray=data;
      // console.log(this.salesDtlArray)
      this.totalAmount=0.0;
    for (let i=0; i<=this.salesDtlAddTables.length-1;i++)
  {
    this.totalAmount=this.salesDtlAddTables[i].amount+this.totalAmount;
    
  }}
  )
}
}

addItem(itemSelected:ItemStockPopUp){
  this.salesDtl = new SalesDtl();
  if(this.salesDtlAddTables.length!=0){
    for(let i=0;i<this.salesDtlAddTables.length;i++){
        if(this.salesDtlAddTables[i].itemId==itemSelected.itemId){
          alert("Item exists in cart");
          return;     
        }
    }
  }
  this.salesDtl.itemId=itemSelected.itemId;
  this.salesDtl.itemName = itemSelected.itemName;
  // this.salesDtl.itemName = "TEST ITEM";
  this.salesDtl.barcode = itemSelected.barcode;
  this.salesDtl.batch = itemSelected.batch;
  this.salesDtl.qty = this.productQty;
  this.salesDtl.itemId = itemSelected.itemId;
  this.salesDtl.mrp = itemSelected.standardPrice;
  this.salesDtl.rate = this.salesDtl.mrp * this.productQty;
  // this.itemAmount=this.salesDtl.mrp*this.productQty;
  // this.salesDtl.amount =  this.itemAmount;
  this.salesDtl.amount = this.salesDtl.mrp * this.productQty;

  this.salesDtl.unitName = itemSelected.unitName;
  this.salesDtl.unitId = itemSelected.unitId;
  this.salesDtl.taxRate=itemSelected.tax;
  this.salesDtl.expiryDate=itemSelected.expDate;
  this.salesDtl.standardPrice=itemSelected.standardPrice;
  this.salesDtl.store=itemSelected.store;

  this.totalAmount=this.totalAmount+this.salesDtl.amount;
  this.salesDtlAddTables.push(this.salesDtl);
  // this.sharedserviceService.PostSalesDtlToCloudPosWindow( this.salesDtl);
}

deleteItem(item:SalesDtl){
  this.salesDtlAddTables.forEach((value,index)=>{   
        if(value.itemId==item.itemId) {
          console.log(index+" "+value.itemId+" "+item.itemId)
          this.salesDtlAddTables.splice(index,1);
        }
    });
}

// deleteItem(item:SalesDtl,index){
//   for(let i=0;i<this.salesDtlAddTables.length;i++){
//         if(this.salesDtlAddTables[i].itemId==item.itemId) {
//             this.salesDtlAddTables.splice(index,1);
//         }
//   }
// }

counterFunction(type:string,item:SalesDtl){
this.totalAmount=this.totalAmount-item.amount;
  console.log(item.itemName)
 if(type==='add'){
  item.qty=item.qty+1;
 }
 else{
   if(item.qty!=1){
    item.qty=item.qty-1;
   }
 }
 item.amount=item.qty*item.standardPrice;
 this.totalAmount=this.totalAmount+item.amount;
}

counter(item:SalesDtl){
  this.itemAmount=item.standardPrice*this.productQty;
}
}


export class CardSales{

      card:any;
      cardamount:any;
}