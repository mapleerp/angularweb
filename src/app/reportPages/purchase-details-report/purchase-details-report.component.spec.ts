import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseDetailsReportComponent } from './purchase-details-report.component';

describe('PurchaseDetailsReportComponent', () => {
  let component: PurchaseDetailsReportComponent;
  let fixture: ComponentFixture<PurchaseDetailsReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PurchaseDetailsReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseDetailsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
