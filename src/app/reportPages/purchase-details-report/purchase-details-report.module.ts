import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PurchaseDetailsReportRoutingModule } from './purchase-details-report-routing.module';
import { PurchaseDetailsReportComponent } from './purchase-details-report.component';
import { FormsModule } from '@angular/forms';
import {MatToolbarModule} from '@angular/material/toolbar';
import { MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';



@NgModule({
  declarations: [
    PurchaseDetailsReportComponent
  ],
  imports: [
    CommonModule,
    PurchaseDetailsReportRoutingModule,
    FormsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatTableModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule

  ]
})
export class PurchaseDetailsReportModule { }
