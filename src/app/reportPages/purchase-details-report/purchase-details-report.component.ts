import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { PurchaseDetailsReport } from 'src/app/modelclass/purchase-details-report';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-purchase-details-report',
  templateUrl: './purchase-details-report.component.html',
  styleUrls: ['./purchase-details-report.component.css']
})
export class PurchaseDetailsReportComponent implements OnInit {

  purchaseDetailsReport:PurchaseDetailsReport[]=[];
  branchMst:BranchMst[]=[];
  sub: Subscription = new Subscription;
  sub1: Subscription = new Subscription;
   
  fromdate='';
  todate='';
  dateConvFist:any;
  dateConvSec:any;
  branchname='';
  getReportStatus=false;
  getexcelReportStatus=false;

  constructor(private sharedserviceService:SharedserviceService,
    public datepipe: DatePipe) { }

  ngOnInit(): void {
   this.sub1= this.sharedserviceService.getBranchNames().subscribe(data => {this.branchMst=data})
  }

  getPurchaseDetailsreport()
  {
    this.getReportStatus=true;
    this.dateConvFist=this.datepipe.transform(this.fromdate , 'yyyy-MM-dd');
    this.dateConvSec=this.datepipe.transform(this.todate , 'yyyy-MM-dd');

    console.log(this.dateConvFist+'firstdate');
    console.log(this.dateConvSec+'sencondate')
  
this.sub=this.sharedserviceService.getPurchaseDetailsreport(this.dateConvFist,this.dateConvSec,this.branchname).
  subscribe(data => {this.purchaseDetailsReport=data;
    if(this.purchaseDetailsReport!=null)
    {
      this.getReportStatus=false;
      this.getexcelReportStatus=true;
    } })

  }
  renderPdf()
  {
    let element=document.getElementById('contentToConvert');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb:XLSX.WorkBook=XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb,ws,"sheet1");
    XLSX.writeFile(wb,"PurchaseDetailsReport"+"-"+this.dateConvFist+'-'+this.dateConvSec+".xlsx")
  }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
    this.sub1.unsubscribe();
    console.log('service unsubscribed..!!')
    }
  }


