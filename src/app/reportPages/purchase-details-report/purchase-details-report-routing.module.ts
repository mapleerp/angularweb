import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PurchaseDetailsReportComponent } from './purchase-details-report.component';

const routes: Routes = [{ path: '', component: PurchaseDetailsReportComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseDetailsReportRoutingModule { }
