import { DatePipe } from '@angular/common';
import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { SalesDeltedReport } from 'src/app/modelclass/sales-delted-report';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-sales-details-deltd-report',
  templateUrl: './sales-details-deltd-report.component.html',
  styleUrls: ['./sales-details-deltd-report.component.css']
})
export class SalesDetailsDeltdReportComponent implements OnInit ,OnDestroy {
  sub: Subscription = new Subscription;

  salesDeltedReport:SalesDeltedReport[]=[];
  branchMst:BranchMst[]=[];
  fromdate='';
  todate='';
  dateConvFist:any;
  dateConvSec:any;
  branchname='';
  getReportStatus=false;

  constructor(private sharedserviceService:SharedserviceService,
    public datepipe: DatePipe) { }

  ngOnInit(): void {
    this.sub=this.sharedserviceService.getBranchNames().subscribe(data => {this.branchMst=data})
  }

  getSalesDetailDeletedreport()
  {

  }
  renderPdf()
  {
    let element=document.getElementById('contentToConvert');
  const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
  const wb:XLSX.WorkBook=XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb,ws,"sheet1");
  XLSX.writeFile(wb,"SalesBillDetails"+"-"+this.dateConvFist+'-'+this.dateConvSec+".xlsx")

  }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
    console.log('service unsubscribed..!!')
    }

}
