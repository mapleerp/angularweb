import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesDetailsDeltdReportComponent } from './sales-details-deltd-report.component';

describe('SalesDetailsDeltdReportComponent', () => {
  let component: SalesDetailsDeltdReportComponent;
  let fixture: ComponentFixture<SalesDetailsDeltdReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesDetailsDeltdReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesDetailsDeltdReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
