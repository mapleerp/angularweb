import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SalesDetailsDeltdReportComponent } from './sales-details-deltd-report.component';

const routes: Routes = [{ path: '', component: SalesDetailsDeltdReportComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesDetailsDeltdReportRoutingModule { }
