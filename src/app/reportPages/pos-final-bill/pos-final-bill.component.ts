import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { CompanyMst } from 'src/app/modelclass/company-mst';
import { SalesBillDetails } from 'src/app/modelclass/sales-bill-details';
import { SalesDtl } from 'src/app/modelclass/sales-dtl';
import { PosFinalBillService } from 'src/app/services/pos-final-bill.service';

@Component({
  selector: 'app-pos-final-bill',
  templateUrl: './pos-final-bill.component.html',
  styleUrls: ['./pos-final-bill.component.css']
})
export class PosFinalBIllComponent implements OnInit {

  constructor(public posFinalBillService:PosFinalBillService,
    private router: Router) { }

  finalBillStatus:SalesBillDetails= new SalesBillDetails;
  companyMst:CompanyMst=new CompanyMst;
  branchMst: BranchMst=new BranchMst;
  salesDtls:SalesDtl[]=[];

  billInvoiceNumber:any;
  invoiceBillDate:any;

  totalAmount=0;

  ngOnInit(): void {

   this.companyMst= this.posFinalBillService.getPosFinalBillCompanyMSt();
    this.billInvoiceNumber=this.posFinalBillService.getPosFinalBillbillInvoiceNumber();
    this.branchMst=  this.posFinalBillService.getPosFinalBillbranchMst();
    this.invoiceBillDate=  this.posFinalBillService.getPosFinalBillinvoiceBillDate();
    this.salesDtls= this.posFinalBillService.getPosFinalBillsalesDtls();
  
    // this.posFinalBillService.getPosFinalBillsalesDtls().subscribe(
    //   (      data: SalesDtl[])=>{this.salesDtls=data});

    this.totalAmount=this.posFinalBillService.getPosFinalBilltotalAmount();

    
  }

  click()
  {
    window.print();
  }
  close(){
    // this.router.navigate(['PosWindow']);
    history.back();
  }
}
