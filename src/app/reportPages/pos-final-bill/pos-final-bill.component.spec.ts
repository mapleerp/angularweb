import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PosFinalBIllComponent } from './pos-final-bill.component';

describe('PosFinalBIllComponent', () => {
  let component: PosFinalBIllComponent;
  let fixture: ComponentFixture<PosFinalBIllComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PosFinalBIllComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PosFinalBIllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
