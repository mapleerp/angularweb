import { Component, OnInit } from '@angular/core';
import{SharedserviceService} from '../../services/sharedservice.service';
import { StockMovementView } from 'src/app/modelclass/stock-movement-view';
import { DatePipe } from '@angular/common';
import * as XLSX from 'xlsx';
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-stock-movement-view',
  templateUrl: './stock-movement-view.component.html',
  styleUrls: ['./stock-movement-view.component.css']
})
export class StockMovementViewComponent implements OnInit ,OnDestroy {

  stockMovementView:StockMovementView[]=[];
  branchMst:BranchMst[]=[];
  sub: Subscription = new Subscription;
  fromdate='';
  todate='';
  dateConvFist:any;
  dateConvSec:any;
  branchname='';
  getReportStatus=false;
  getexcelReportStatus=false;
 

  constructor(private sharedserviceService:SharedserviceService,
    public datepipe: DatePipe) { }

  ngOnInit(): void {
    this.sharedserviceService.getBranchNames().
    subscribe(data => {this.branchMst=data})
    
  
  }

  getBranchWiseStockreport()
  {
    if (this.fromdate == null) {
      alert('Select the From Date!!!');
      return;
    }
    if (this.todate == null) {
      alert('Select the To Date!!!');
      return;
    }
    if (this.branchname == null) {
      alert('Select the Branch!!!');
      return;
    }
    this.getReportStatus=true;
    this.dateConvFist=this.datepipe.transform(this.fromdate , 'yyyy-MM-dd');
    this.dateConvSec=this.datepipe.transform(this.todate , 'yyyy-MM-dd');

    this.sub=this.sharedserviceService.getBranchWiseStrockReport(this.dateConvFist,this.dateConvSec,this.branchname).
  subscribe(data => {this.stockMovementView=data ;
    if(this.stockMovementView!=null)
    {
      this.getReportStatus=false;
      this.getexcelReportStatus=true;
    }}
    )
 
   
  
  }
  
  renderPdf()
  {
    
 let element=document.getElementById('contentToConvert');
 const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
 const wb:XLSX.WorkBook=XLSX.utils.book_new();
 XLSX.utils.book_append_sheet(wb,ws,"sheet1");
 XLSX.writeFile(wb,"DailySales"+"-"+this.dateConvFist+'-'+this.dateConvSec+".xlsx")
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
    console.log('service unsubscribed..!!')
    }
  
}
