import { DatePipe } from '@angular/common';
import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { CategoryMst } from 'src/app/modelclass/category-mst';
import { StockReport } from 'src/app/modelclass/stock-report';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-stock-report',
  templateUrl: './stock-report.component.html',
  styleUrls: ['./stock-report.component.css']
})
export class StockReportComponent implements OnInit , OnDestroy{

  getReportStatus=false;
  getexcelReportStatus=false;
  branchname:any;
  reportdate:any;
  dateConv:any;
  categoryMst:CategoryMst[]=[]
  stockReport:StockReport[]=[]
  branchMst:BranchMst[]=[];
  category:any;
  sub: Subscription = new Subscription;

  constructor(private sharedserviceService:SharedserviceService,
    public datepipe: DatePipe) { }

  ngOnInit(): void {

    this.sharedserviceService.getCategoryMst().subscribe(data => {this.categoryMst=data })
  this.sharedserviceService.getBranchNames().subscribe(data => {this.branchMst=data})

  }

  getStockReports()
  {
    this.getReportStatus=true;
    this.dateConv=this.datepipe.transform(this.reportdate , 'yyyy-MM-dd');
   
    this.sub= this.sharedserviceService.getStockreport(this.dateConv,this.category,this.branchname).
  subscribe(data => {this.stockReport=data ;
    if(this.stockReport!=null)
    {
      this.getReportStatus=false;
      this.getexcelReportStatus=true;
    }})
 
  }
  renderPdf()
  {
    let element=document.getElementById('contentToConvert');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb:XLSX.WorkBook=XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb,ws,"sheet1");
    XLSX.writeFile(wb,"StockReport"+"-"+this.dateConv+'-'+this.category+'-'+this.branchname+".xlsx")
  }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
    console.log('service unsubscribed..!!')
    }

}
