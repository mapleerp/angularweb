import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { CustomerMst } from 'src/app/modelclass/customer-mst';
import { datas } from 'src/app/Pop-Up-Windows/search-pop-up/search-pop-up.component';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import {jsPDF} from 'jspdf';
import html2canvas from 'html2canvas';  
import { WholeSaleBillDtls } from 'src/app/modelclass/whole-sale-bill-dtls';
import { ShippingAddress } from 'src/app/modelclass/shipping-address';
import { BehaviorSubject } from 'rxjs';
import { TaxCalculations } from 'src/app/modelclass/tax-calculations';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SaleOrderPopupService } from 'src/app/services/sale-order-popup.service';
import { FormControl } from '@angular/forms';
import { SalesDtl } from 'src/app/modelclass/sales-dtl';
export type DialogDataSubmitCallback<T> = (row: T) => void;




@Component({
  selector: 'app-invoice-bill-geration',
  templateUrl: './invoice-bill-geration.component.html',
  styleUrls: ['./invoice-bill-geration.component.css']
})
export class InvoiceBillGerationComponent implements OnInit {

  


  name = 'Angular Html To Pdf ';

  @ViewChild('pdfTable', { static: false })
  pdfTable!: ElementRef;
  
  constructor(private sharedserviceService:SharedserviceService,private saleOrderPopupService:SaleOrderPopupService,
    public invoiceDialog: MatDialogRef<InvoiceBillGerationComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { callback: DialogDataSubmitCallback<any>; defaultValue: any },) { }
 
    input: FormControl = new FormControl('');
  billDetails:datas[]=[];
  taxCalculations:TaxCalculations[]=[]
  
  taxCalculationsObj:TaxCalculations=new TaxCalculations();

  customerDtls: CustomerMst = new CustomerMst;
  totalAmount:any;
  totalAmountWithoutTax:any;
  today:any
  voucherNumber:WholeSaleBillDtls= new WholeSaleBillDtls
  invoiceNumber:any
  ShippingAdress:ShippingAddress=new ShippingAddress
  totalTaxRate:any;
 sgstTotal=0;
 cgstTotal=0;
 invoiceBillreset:any;
 receivecust: CustomerMst = new CustomerMst;
 generateBill:any
dts:any;
newUser = new BehaviorSubject<any>({});
refreshPAge:any;
posBillSummary:SalesDtl[]=[]
  
  ngOnInit(): void {

   this.sharedserviceService.salesDtlfromShardService().subscribe(
     data=>{this.posBillSummary=data}
   )

   this.billDetails= this.sharedserviceService.receiveInvoiceBillDetails();
   this.customerDtls=this.sharedserviceService.getCustomerDtlsforInvoice();
   this.sharedserviceService.getShippingDtls().subscribe(
    (data: any)=>{this.ShippingAdress=data;
      console.log(this.ShippingAdress+"receiveShippingDtl")
  }
  );
    this.totalPrice();
    this. taxSplitUpCalculation();

    this.voucherNumber=this.sharedserviceService.getWholeSaleVoucherNumber();
    this.invoiceNumber=this.voucherNumber.voucherNumber;
    console.log(this.voucherNumber.voucherNumber)

    let currentDate = new Date();
   this.today= currentDate.setDate(currentDate.getDate());

   console.log("billDetailsReceived"+this.billDetails);

   

  }

  totalPrice() {

    this.totalAmount=0.00;
    this.totalAmountWithoutTax=0.00;

    console.log("TotalPrice function called")
    let num:number,befortax:number;
    
    for(let data of this.billDetails){
      num=+data.Amount
      this.totalAmount= num+this.totalAmount;

      befortax=+data.standardPrice*100/(100+data.taxRate)*data.qty;
      this.totalAmountWithoutTax=befortax+this.totalAmountWithoutTax;
    }
  
  }

  taxSplitUpCalculation()
  {
    
    for(let data of this.billDetails)
    {
     this. taxCalculationsObj=new TaxCalculations();
     
        this.taxCalculationsObj.taxRate=data.taxRate;
        this.taxCalculationsObj.sgstPercent=data.taxRate/2;
        this.taxCalculationsObj.cgstPercent=data.taxRate/2;
        this.taxCalculationsObj.rate=data.Amount;
        let value =0.0;
        value=data.standardPrice*100/(100+data.taxRate);
        let amountBfrTax =0;
        amountBfrTax  = value*data.qty;
        let taxAmount=0;
        taxAmount=data.Amount-amountBfrTax;
        this.taxCalculationsObj.sgstAmount=taxAmount/2;
        this.taxCalculationsObj.cgstAmount=taxAmount/2;

        this.taxCalculations.push(this.taxCalculationsObj);
    }
      this. taxArrayGruoping();
   
  }

  taxArrayGruoping()
  {
  
    for (var i=0;i<=this.taxCalculations.length-1;i++){
   
      console.log(this.taxCalculations.length+'i length');
           for (var j=i+1;j<=this.taxCalculations.length-1;j++){
            console.log(this.taxCalculations.length+' length');
            console.log( this.taxCalculations[i].taxRate);
            console.log( this.taxCalculations[j].taxRate);

                     
                         if( this.taxCalculations[i].taxRate===this.taxCalculations[j].taxRate)
                               {

                                this.taxCalculations[i].rate= this.taxCalculations[i].rate+ this.taxCalculations[j].rate;
                                this.taxCalculations[i].sgstAmount= this.taxCalculations[i].sgstAmount+ this.taxCalculations[j].sgstAmount;
                                this.taxCalculations[i].cgstAmount= this.taxCalculations[i].cgstAmount+ this.taxCalculations[j].cgstAmount;
                                 this.taxCalculations[j]= new TaxCalculations;
                                 console.log(this.taxCalculations.length+'i length');

                               
                               }
                       

                      }

     }

     for( let i=0;i<=this.taxCalculations.length-1;i++)
     {
     this. sgstTotal= this.taxCalculations[i].sgstAmount+ this.sgstTotal;
     this. cgstTotal= this.taxCalculations[i].cgstAmount+ this.cgstTotal;

     }

  }

public downloadAsPDF()  
{  
  var data = document.getElementById('pdfTable') as HTMLCanvasElement;;  //Id of the table
  html2canvas(data).then(canvas => {  
    // Few necessary setting options  
    let imgWidth = 208;   
    let pageHeight = 295;    
    let imgHeight = canvas.height * imgWidth / canvas.width;  
    let heightLeft = imgHeight;  

    const contentDataURL = canvas.toDataURL('image/png')  
    const doc: jsPDF = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
    let position = 0;  
    doc.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
    doc.save('InvoiceNumber.pdf'); // Generated PDF   
  });  
}  
public downloadAsPDFv2() {

  window.print();
  // let doc = new jsPDF('p','pt','a4');

  // doc.html(this.pdfTable.nativeElement,{
  //   callback:(doc)=>{
  //     doc.save("Billinvoice.pdf")
  //   }
  // });

}
close()
{
 
  this.refreshPAge="refresh";
  this.saleOrderPopupService.resetInvoiceBillWindow("refresh");
     this.invoiceDialog.close("refreshPage");
}

}
