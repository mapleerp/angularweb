import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceBillGerationComponent } from './invoice-bill-geration.component';

describe('InvoiceBillGerationComponent', () => {
  let component: InvoiceBillGerationComponent;
  let fixture: ComponentFixture<InvoiceBillGerationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvoiceBillGerationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceBillGerationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
