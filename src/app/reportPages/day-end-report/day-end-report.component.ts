
import { DatePipe } from '@angular/common';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChartsModule, Label } from 'ng2-charts';
import { DayEndWebReport } from 'src/app/modelclass/day-end-web-report';
import { SharedserviceService } from '../../services/sharedservice.service';
import * as XLSX from 'xlsx';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-day-end-report',
  templateUrl: './day-end-report.component.html',
  styleUrls: ['./day-end-report.component.css']
})
export class DayEndReportComponent implements OnInit, OnDestroy {

  dayEndWebReport: DayEndWebReport[] = [];
  getReportStatus = false;
  getexcelReportStatus = false;
  sub: Subscription = new Subscription;


  reportdate: any;
  dateConv: any;


  total: any = 0;
  value: any;
  totalCashSales: any = 0;
  totalCreditSales: any = 0;
  totalCardSales: any = 0;
  totalCard2: any = 0;
  totalPhysicalCash: any = 0;
  totalSales: any = 0;
  dataArrayInput: number[] = [];

  constructor(private sharedserviceService: SharedserviceService,
    public datepipe: DatePipe) { }

  public chartType = <const>'bar';
  public chartLabel: string = '';
  public labelArray: string[] = [];
  public dataArray: number[] = this.dataArrayInput;
  /**public chartDatasets: Array<any> = [
    {data: this.dataArray, label: this.chartLabel}
  ];**/

  ngOnInit(): void {

  }

  getDayEndReports() {
    if (this.reportdate == null) {
      alert('Select the  Date!!!');
      return;
    }
   
    this.totalCashSales = 0;
    this.totalCreditSales = 0;
    this.totalCardSales = 0;
    this.totalCard2 = 0;
    this.totalPhysicalCash = 0;
    this.totalSales = 0;

    this.getReportStatus = true;

    this.dateConv = this.datepipe.transform(this.reportdate, 'yyyy-MM-dd');

    this.sub = this.sharedserviceService.getDayEndReports(this.dateConv).
      subscribe(data => {
        this.dayEndWebReport = data; this.findsum(this.dayEndWebReport);
        console.log(this.dayEndWebReport[6]);
        if (this.dayEndWebReport != null) {
          this.getReportStatus = false;
          this.getexcelReportStatus = true;


        }
      })



  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
    console.log('service unsubscribed..!!')
  }

  findsum(data: any) {
    this.dataArrayInput.length = 0;
    this.labelArray.length = 0;
    this.value = data;
    for (let j = 0; j < data.length; j++) {
      this.totalCashSales += this.value[j].cashSales;
      this.totalCreditSales += this.value[j].creditSales;
      this.totalCardSales += this.value[j].cardSales;
      this.totalCard2 += this.value[j].card2;
      this.totalPhysicalCash += this.value[j].physicalCash;
      this.totalSales += this.value[j].totalSales;
      this.dataArrayInput[j] = this.value[j].physicalCash;
      this.labelArray[j] = this.value[j].branchCode;

    }


    console.log('Total Sum Taken')
    console.log('dataArrayInput==' + this.dataArrayInput)
    console.log('labelArray==' + this.labelArray)

  }
  barChartOptions: ChartOptions = {
    responsive: true,
  };
  barChartLabels: Label[] = this.labelArray;

  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];

  barChartData: ChartDataSets[] = [
    { data: this.dataArrayInput, label: 'Day End Summary' }
  ];


  renderPdf() {
    let element = document.getElementById('contentToConvert');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "sheet1");
    XLSX.writeFile(wb, "DayEndSummary" + "-" + this.dateConv + ".xlsx")

  }





}



