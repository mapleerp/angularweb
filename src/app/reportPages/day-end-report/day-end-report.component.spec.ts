import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DayEndReportComponent } from './day-end-report.component';

describe('DayEndReportComponent', () => {
  let component: DayEndReportComponent;
  let fixture: ComponentFixture<DayEndReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DayEndReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DayEndReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
