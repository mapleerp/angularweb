import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrailBalanceReportComponent } from './trail-balance-report.component';

describe('TrailBalanceReportComponent', () => {
  let component: TrailBalanceReportComponent;
  let fixture: ComponentFixture<TrailBalanceReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrailBalanceReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrailBalanceReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
