import { DatePipe } from '@angular/common';
import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { TrailBalanceReport } from 'src/app/modelclass/trail-balance-report';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-trail-balance-report',
  templateUrl: './trail-balance-report.component.html',
  styleUrls: ['./trail-balance-report.component.css']
})
export class TrailBalanceReportComponent implements OnInit ,OnDestroy{

  trailBalanceReport:TrailBalanceReport[]=[];
  branchMst:BranchMst[]=[];
  sub: Subscription = new Subscription;
  fromdate='';
  todate='';
  dateConvFist:any;
  dateConvSec:any;
  branchname='';
  getReportStatus=false;
  getexcelReportStatus=false;

  constructor(private sharedserviceService:SharedserviceService,
    public datepipe: DatePipe) { }

  ngOnInit(): void {

    this.sharedserviceService.getBranchNames().subscribe(data => {this.branchMst=data})
  }

  getTrailBalancereport()
  {

    
    this.getReportStatus=true;
   
    this.dateConvSec=this.datepipe.transform(this.todate , 'yyyy-MM-dd');

  
    console.log(this.dateConvSec+'sencondate')
    this.sub=this.sharedserviceService.getTrailBalancereport(this.dateConvSec,this.branchname).
  subscribe(data => {this.trailBalanceReport=data;
    if(this.trailBalanceReport!=null)
    {
      this.getReportStatus=false;
      this.getexcelReportStatus=true;
    }  })
  }
  renderPdf()
  {
    let element=document.getElementById('contentToConvert');
  const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
  const wb:XLSX.WorkBook=XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb,ws,"sheet1");
  XLSX.writeFile(wb,"TrailBalanceReport"+"-"+this.dateConvSec+".xlsx")
  }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
    console.log('service unsubscribed..!!')
    }

}
