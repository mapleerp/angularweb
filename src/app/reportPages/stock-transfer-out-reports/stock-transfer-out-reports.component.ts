import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StockTransferOutDtl } from 'src/app/modelclass/stock-transfer-out-dtl';
import { StockTransferOutHdr } from 'src/app/modelclass/stock-transfer-out-hdr';
import { StockTransferOutReport } from 'src/app/modelclass/stock-transfer-out-report';
import { UnitMst } from 'src/app/modelclass/unit-mst';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-stock-transfer-out-reports',
  templateUrl: './stock-transfer-out-reports.component.html',
  styleUrls: ['./stock-transfer-out-reports.component.css']
})
export class StockTransferOutReportsComponent implements OnInit {
  fromdate: any;
  todate: any;
  stockTransferOutHdrList: StockTransferOutHdr[] = [];
  stockTransferOutDtlList:StockTransferOutDtl[]=[];
  stockTransferOutHdr:StockTransferOutHdr=new StockTransferOutHdr;
  stockTransferOutReport:StockTransferOutReport[]=[];
  id: any;
  fdate:any;
  tdate:any;
  unitMst: UnitMst[] = [];
  constructor( private sharedserviceService: SharedserviceService,public router: Router) { }

  ngOnInit(): void {
    this.sharedserviceService.getUnitMst().subscribe((data: any) => {
      this.unitMst = data;
    });
  }


  actionShow(){
    if (this.fromdate == null) {
      alert('Select the From Date!!!');
      return;
    }
    if (this.todate == null) {
      alert('Select the To Date!!!');
      return;
    } 
    // this.fdate=this.fromdate;
    // this.tdate=this.todate;
    this.sharedserviceService.setFromDateForReport(this.fromdate);
    this.sharedserviceService.setToDateForReport(this.todate);
    console.log(this.fromdate+" from date");
    console.log(this.todate+" to date")
    this.sharedserviceService
    .getStockTransferBetweenDate(this.fromdate, this.todate, environment.myBranch)
    .subscribe((data: any) => {
      
      this.stockTransferOutHdrList = data;
      console.log(data);
    });

  }


  resendStock(){


    this.sharedserviceService
    .resendStockTransfer(this.id)
    .subscribe((data: any) => {
      
      this.stockTransferOutHdrList = data;
      console.log(data);
    });
  }
 
  getStockTransferHdr(stockTransHdr:StockTransferOutHdr){
    this.stockTransferOutHdr=stockTransHdr;
    this.sharedserviceService.getStockTransferOutDtlByVoucherNo(stockTransHdr.fromBranch,stockTransHdr.voucherNumber).subscribe((data:any)=>{
      this.stockTransferOutDtlList=data;
      console.log(data);
    });
  }
  getUnitName(num: any) {
    const unitNameById = this.unitMst.find((data) => data.id === num);
    if (!unitNameById) {
      // we not found the parent
      return '';
    }
    return unitNameById.unitName;
  }
 
  print(){
    console.log(this.stockTransferOutHdr);
    if(this.stockTransferOutHdr.voucherNumber==null&&this.stockTransferOutHdr.voucherDate==null){
      alert('Select a row from table');
      return;
    }
    // this.sharedserviceService.setVoucherNumberForReport(this.stockTransferOutHdr.voucherNumber);
    // this.sharedserviceService.setVoucherDateForReport(this.stockTransferOutHdr.voucherDate);
    console.log('In print function')
    this.router.navigateByUrl("/stocktransferoutreportprint", { state:  this.stockTransferOutHdr});
  }

  printSummary(){
    if(this.fromdate==null||this.todate==null){
      alert('Select date');
      return;
    }
    console.log('In print summary function');
    this.sharedserviceService.setFromDateForReport(this.fromdate);
    this.sharedserviceService.setToDateForReport(this.todate);
    this.router.navigate(['stocktransferoutreportprintsummary']);
  }
}
