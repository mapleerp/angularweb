import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockTransferOutReportsComponent } from './stock-transfer-out-reports.component';

describe('StockTransferOutReportsComponent', () => {
  let component: StockTransferOutReportsComponent;
  let fixture: ComponentFixture<StockTransferOutReportsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockTransferOutReportsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockTransferOutReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
