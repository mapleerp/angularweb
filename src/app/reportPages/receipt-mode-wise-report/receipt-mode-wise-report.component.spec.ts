import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiptModeWiseReportComponent } from './receipt-mode-wise-report.component';

describe('ReceiptModeWiseReportComponent', () => {
  let component: ReceiptModeWiseReportComponent;
  let fixture: ComponentFixture<ReceiptModeWiseReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReceiptModeWiseReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptModeWiseReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
