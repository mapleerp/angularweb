import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockTransferOutDetailReportComponent } from './stock-transfer-out-detail-report.component';

const routes: Routes = [{ path: '', component: StockTransferOutDetailReportComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockTransferOutDetailReportRoutingModule { }
