import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockTransferOutDetailReportComponent } from './stock-transfer-out-detail-report.component';

describe('StockTransferOutDetailReportComponent', () => {
  let component: StockTransferOutDetailReportComponent;
  let fixture: ComponentFixture<StockTransferOutDetailReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockTransferOutDetailReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockTransferOutDetailReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
