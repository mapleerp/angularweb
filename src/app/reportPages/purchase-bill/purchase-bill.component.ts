import { Component, OnInit } from '@angular/core';
import { PurchaseHdr } from 'src/app/modelclass/purchase-hdr';
import { PurchaseReport } from 'src/app/modelclass/purchase-report';
import { SaleOrderPopupService } from 'src/app/services/sale-order-popup.service';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-purchase-bill',
  templateUrl: './purchase-bill.component.html',
  styleUrls: ['./purchase-bill.component.css']
})
export class PurchaseBillComponent implements OnInit {
  purchaseHdr: PurchaseHdr = new PurchaseHdr;
  purchaseReportArr:PurchaseReport[]=[];
  myCompany=environment.company;

  constructor(private saleOrderPopupService: SaleOrderPopupService,
    private sharedService:SharedserviceService) { }

  ngOnInit(): void {
    this.purchaseHdr = this.saleOrderPopupService.getPurchaseHdrHistory();
    console.log(this.purchaseHdr);
    this.sharedService.getPurchaseReport(this.purchaseHdr.id).subscribe((data:any)=>{
      console.log("On init - purchase bill component");
      this.purchaseReportArr=data;
      console.log(this.purchaseReportArr);
    })
  }
  click()
  {
    window.print();
  }
  close(){
   
    history.back();
  }
}
