import { Component, OnInit } from '@angular/core';
import { ExpenseReport } from 'src/app/modelclass/expense-report';
import { IncomeReport } from 'src/app/modelclass/income-report';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-profit-and-loss-report',
  templateUrl: './profit-and-loss-report.component.html',
  styleUrls: ['./profit-and-loss-report.component.css'],
})
export class ProfitAndLossReportComponent implements OnInit {
  selecteddate: any;
  profitAndLossObject: any[] = [];
  incomeReport: IncomeReport = new IncomeReport();
  expenseReport: ExpenseReport = new ExpenseReport();
  account: any;
  expenseAmount: any;
  incomeAmount: any;
  amount: any;
  incomeReportList: IncomeReport[] = [];
  expenseReportList: ExpenseReport[] = [];
  sumOfExpense: any = 0;
  sumOfIncome: any = 0;
  constructor(private sharedserviceService: SharedserviceService) {}

  ngOnInit(): void {}
  generateReport() {
    if (this.selecteddate == null) {
      alert('Select the  Date!!!');
      return;
    }
    this.sumOfIncome = 0;
    this.sumOfExpense = 0;
    this.incomeReportList.length = 0;
    this.expenseReportList.length = 0;
    this.sharedserviceService
      .getProfitAndLossReport(this.selecteddate)
      .subscribe((data: any) => {
        this.profitAndLossObject = data;

        for (let i = 0; i <= data.length; i++) {
          this.expenseReport = new ExpenseReport();
          this.incomeReport = new IncomeReport();
          this.account = 0;
          this.expenseAmount = 0;
          this.incomeAmount = 0;
          this.account = data[i][0];
          this.expenseAmount = data[i][2];
          this.incomeAmount = data[i][1];

          this.expenseReport.accountName = this.account;
          this.incomeReport.accountName = this.account;
          this.expenseReport.amount = this.expenseAmount;
          this.incomeReport.amount = this.incomeAmount;

          this.sumOfIncome = this.sumOfIncome + this.incomeAmount;
          this.sumOfExpense = this.sumOfExpense + this.expenseAmount;

          this.incomeReportList.push(this.incomeReport);
          this.expenseReportList.push(this.expenseReport);
        }

        console.log(data);
      });
  }
  renderPdf() {
    if (this.profitAndLossObject.length == 0) {
      return;
    }
    let element1 = document.getElementById('contentToConvertOne');
    let element2 = document.getElementById('contentToConvertTwo');

    const ws1: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element1);
    // const wb1: XLSX.WorkBook = XLSX.utils.book_new();
    // XLSX.utils.book_append_sheet(wb1, ws1, 'sheet');

    const ws2: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element2);
    // const wb2: XLSX.WorkBook = XLSX.utils.book_new();
    // XLSX.utils.book_append_sheet(wb2, ws2, 'sheet');

    let a = XLSX.utils.sheet_to_json(ws1, { header: 1 });
    let b = XLSX.utils.sheet_to_json(ws2, { header: 1 });

    a = a.concat(['']).concat(b);
    let worksheet = XLSX.utils.json_to_sheet(a, { skipHeader: true });

    const new_workbook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(new_workbook, worksheet, 'worksheet');
    XLSX.writeFile(
      new_workbook,
      'ProfitAndLossReport' + '-' + this.selecteddate + '.xlsx'
    );

    // XLSX.writeFile(
    //   wb1,
    //   'ProfitAndLossReport' + '-' + this.selecteddate + '.xlsx'
    // );
  }
}
