import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesDetailsReportComponent } from './sales-details-report.component';

describe('SalesDetailsReportComponent', () => {
  let component: SalesDetailsReportComponent;
  let fixture: ComponentFixture<SalesDetailsReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesDetailsReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesDetailsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
