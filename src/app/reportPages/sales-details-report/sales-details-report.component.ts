import { DatePipe } from '@angular/common';
import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { SalesDetailsReport } from 'src/app/modelclass/sales-details-report';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-sales-details-report',
  templateUrl: './sales-details-report.component.html',
  styleUrls: ['./sales-details-report.component.css']
})
export class SalesDetailsReportComponent implements OnInit ,OnDestroy {
  sub: Subscription = new Subscription;

  salesDetailsReport:SalesDetailsReport[]=[];
  branchMst:BranchMst[]=[];
  fromdate='';
  todate='';
  dateConvFist:any;
  dateConvSec:any;
  branchname='';
  getReportStatus=false;
  getexcelReportStatus=false;

  constructor(private sharedserviceService:SharedserviceService,
    public datepipe: DatePipe) { }

  ngOnInit(): void {
    this.sharedserviceService.getBranchNames().subscribe(data => {this.branchMst=data})
  }

  getSalesDetailsreport()
  {
    this.getReportStatus=true;
    this.dateConvFist=this.datepipe.transform(this.fromdate , 'yyyy-MM-dd');
    this.dateConvSec=this.datepipe.transform(this.todate , 'yyyy-MM-dd');

    console.log(this.dateConvFist+'firstdate');
    console.log(this.dateConvSec+'sencondate')
    this.sub=this.sharedserviceService.getDetailsreport(this.dateConvFist,this.dateConvSec,this.branchname).
  subscribe(data => {this.salesDetailsReport=data;
    if(this.salesDetailsReport!=null)
    {
      this.getReportStatus=false;
      this.getexcelReportStatus=true;
    } })

  
  setTimeout(() => {
    this.getReportStatus=false;
  }, 23000);

  }

  renderPdf()
  {
    let element=document.getElementById('contentToConvert');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb:XLSX.WorkBook=XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb,ws,"sheet1");
    XLSX.writeFile(wb,"DailySales"+"-"+this.dateConvFist+'-'+this.dateConvSec+".xlsx")

  }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
    console.log('service unsubscribed..!!')
    }

}
