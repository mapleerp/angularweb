import { Component, OnInit } from '@angular/core';
import { AccountHeads } from 'src/app/modelclass/account-heads';
import { CompanyMst } from 'src/app/modelclass/company-mst';
import { CustomerMst } from 'src/app/modelclass/customer-mst';
import { PurchaseDtl } from 'src/app/modelclass/purchase-dtl';
import { PurchaseHdr } from 'src/app/modelclass/purchase-hdr';
import { PurchaseReport } from 'src/app/modelclass/purchase-report';
import { SalesBillDetails } from 'src/app/modelclass/sales-bill-details';
import { SalesDtl } from 'src/app/modelclass/sales-dtl';
import { SalesTransHdr } from 'src/app/modelclass/sales-trans-hdr';
import { ShippingAddress } from 'src/app/modelclass/shipping-address';
import { TaxSummaryDetails } from 'src/app/modelclass/tax-summary-details';
import { SaleOrderPopupService } from 'src/app/services/sale-order-popup.service';
import { SalesDtlsService } from 'src/app/services/sales-dtls.service';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-purchase-final-bill',
  templateUrl: './purchase-final-bill.component.html',
  styleUrls: ['./purchase-final-bill.component.css']
})
export class PurchaseFinalBillComponent implements OnInit {
  constructor(
    private saleOrderPopupService: SaleOrderPopupService,
    private sharedserviceService: SharedserviceService
  ) {}
  
  purchaseDtlArray: PurchaseDtl[] = [];
  branchCode:any;
  purchaseReport:PurchaseReport[]=[];
  accountHeads:AccountHeads=new AccountHeads;
  companyMst:CompanyMst=new CompanyMst;
 

  //...///
  purchaseHdr:PurchaseHdr=new PurchaseHdr
  ngOnInit(): void {
    this.branchCode=environment.myBranch;
    this.purchaseHdr=this.saleOrderPopupService.getPurchaseHdrHistory()
    this.companyMst=this.purchaseHdr.companyMst;
   this.sharedserviceService.getAccountHeads(this.purchaseHdr.supplierId).subscribe(
       data=>{ this.accountHeads=data}
     )

    this.sharedserviceService.getPurchaseDtl(this.purchaseHdr).subscribe( data =>
      {this.purchaseDtlArray=data})

    this.sharedserviceService.getPruchaseTaxDtl(this.purchaseHdr.voucherNumber,this.purchaseHdr.voucherDate,this.branchCode).subscribe(
      data=>{ this.purchaseReport=data,
      console.log(this.purchaseReport)}
    )
   
  }

  print() {
    window.print();
  }
}
