import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseFinalBillComponent } from './purchase-final-bill.component';

describe('PurchaseFinalBillComponent', () => {
  let component: PurchaseFinalBillComponent;
  let fixture: ComponentFixture<PurchaseFinalBillComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PurchaseFinalBillComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseFinalBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
