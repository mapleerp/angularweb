import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GstinputdetailComponent } from './gstinputdetail.component';

describe('GstinputdetailComponent', () => {
  let component: GstinputdetailComponent;
  let fixture: ComponentFixture<GstinputdetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GstinputdetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GstinputdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
