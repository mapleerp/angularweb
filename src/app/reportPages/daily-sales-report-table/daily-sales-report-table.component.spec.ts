import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailySalesReportTableComponent } from './daily-sales-report-table.component';

describe('DailySalesReportTableComponent', () => {
  let component: DailySalesReportTableComponent;
  let fixture: ComponentFixture<DailySalesReportTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DailySalesReportTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DailySalesReportTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
