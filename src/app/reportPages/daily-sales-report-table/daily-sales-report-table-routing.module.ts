import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DailySalesReportTableComponent } from './daily-sales-report-table.component';

const routes: Routes = [{ path: '', component: DailySalesReportTableComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DailySalesReportTableRoutingModule { }
