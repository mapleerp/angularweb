import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { SalesSummaryReport } from 'src/app/modelclass/sales-summary-report';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import { Subscription } from 'rxjs';
import { VoucherReprintDtl } from 'src/app/modelclass/voucher-reprint-dtl';
@Component({
  selector: 'app-daily-sales-report-table',
  templateUrl: './daily-sales-report-table.component.html',
  styleUrls: ['./daily-sales-report-table.component.css']
})
export class DailySalesReportTableComponent implements OnInit {

  fromdate='';
  todate='';
  dateConvFist:any;
  dateConvSec:any;
  branchname='';
  branchMst:BranchMst[]=[];
  dailySalestables:SalesSummaryReport[]=[];
  voucherReprintDtl:VoucherReprintDtl[]=[]
  sub: Subscription = new Subscription;
  voucher:SalesSummaryReport =new SalesSummaryReport

  constructor(private sharedserviceService:SharedserviceService,
    public datepipe: DatePipe) { }

  ngOnInit(): void {
    this.sharedserviceService.getBranchNames().subscribe(data => {this.branchMst=data})
  }

  showDaliysaleshdr()
  {

    if (this.fromdate == null) {
      alert('Select the From Date!!!');
      return;
    }
    if (this.todate == null) {
      alert('Select the To Date!!!');
      return;
    }
    if (this.branchname == null) {
      alert('Select the Branch!!!');
      return;
    }
    this.voucher =new SalesSummaryReport;
    this.dateConvFist=this.datepipe.transform(this.fromdate , 'yyyy-MM-dd');
    this.dateConvSec=this.datepipe.transform(this.todate , 'yyyy-MM-dd');
    this.sub=this.sharedserviceService.GetshowDaliysaleshdr(this.dateConvFist,this.dateConvSec,this.branchname).
  subscribe(data => {this.dailySalestables=data});
   this.sharedserviceService.getDailyttotalsales(this.dateConvFist,this.dateConvSec)
  

  }

  voucherDetails(voucher: { voucherNumber: any; voucherDate: any; })
  {
   // this.dateConvFist=this.datepipe.transform(voucher.voucherDate , 'yyyy-MM-dd');
    this.sharedserviceService.getVoucherDetails(voucher.voucherNumber,this.branchname,voucher.voucherDate).subscribe
    (data => {this.voucherReprintDtl=data},
     
    )

   
  }

}
