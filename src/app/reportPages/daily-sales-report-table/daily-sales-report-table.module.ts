import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DailySalesReportTableRoutingModule } from './daily-sales-report-table-routing.module';
import { DailySalesReportTableComponent } from './daily-sales-report-table.component';
import { FormsModule } from '@angular/forms';
import {MatToolbarModule} from '@angular/material/toolbar';
import { MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';


@NgModule({
  declarations: [
    DailySalesReportTableComponent
  ],
  imports: [
    CommonModule,
    DailySalesReportTableRoutingModule,
    FormsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatTableModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule
    
  ]
})
export class DailySalesReportTableModule { }
