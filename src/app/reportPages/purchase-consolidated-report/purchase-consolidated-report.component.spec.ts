import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseConsolidatedReportComponent } from './purchase-consolidated-report.component';

describe('PurchaseConsolidatedReportComponent', () => {
  let component: PurchaseConsolidatedReportComponent;
  let fixture: ComponentFixture<PurchaseConsolidatedReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PurchaseConsolidatedReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseConsolidatedReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
