import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { PurchaseReport } from 'src/app/modelclass/purchase-report';
import { datas } from 'src/app/Pop-Up-Windows/search-pop-up/search-pop-up.component';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-purchase-consolidated-report',
  templateUrl: './purchase-consolidated-report.component.html',
  styleUrls: ['./purchase-consolidated-report.component.css'],
})
export class PurchaseConsolidatedReportComponent implements OnInit {
  branch: BranchMst[] = [];
  // category: CategoryMst[] = [];
  groupname: any = [];
  todate: any;
  fromdate: any;
  branchname: any;
  categoryid: String = '';
  temp = '';
  purchaseReport: PurchaseReport[] = [];
  getexcelReportStatus = false;
  constructor(private sharedserviceService: SharedserviceService) {}

  ngOnInit(): void {
    this.getBranch();
  }

  getBranch() {
    this.sharedserviceService.getBranchNames().subscribe((data: any) => {
      this.branch = data;
    });
  }

  submit() {
    this.sharedserviceService
      .getPurchaseConsolidatedReport(
        this.fromdate,
        this.todate,
        this.branchname
      )
      .subscribe((data: any) => {
        // this.groupWiseSalesDtl = data;
        this.purchaseReport = data;
        console.log(data);
      });
  }

  renderPdf() {
    let element = document.getElementById('contentToConvert');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'sheet1');
    XLSX.writeFile(
      wb,
      'Purchase-Consolidated-Report' +
        '-' +
        this.fromdate +
        '-' +
        this.todate +
        '.xlsx'
    );
  }
}
