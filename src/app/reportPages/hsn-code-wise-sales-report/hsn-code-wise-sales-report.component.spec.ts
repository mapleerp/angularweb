import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HsnCodeWiseSalesReportComponent } from './hsn-code-wise-sales-report.component';

describe('HsnCodeWiseSalesReportComponent', () => {
  let component: HsnCodeWiseSalesReportComponent;
  let fixture: ComponentFixture<HsnCodeWiseSalesReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HsnCodeWiseSalesReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HsnCodeWiseSalesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
