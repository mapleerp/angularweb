import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HsnCodeWiseSalesReportRoutingModule } from './hsn-code-wise-sales-report-routing.module';
import { HsnCodeWiseSalesReportComponent } from './hsn-code-wise-sales-report.component';
import { FormsModule } from '@angular/forms';
import {MatToolbarModule} from '@angular/material/toolbar';
import { MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';

@NgModule({
  declarations: [
    HsnCodeWiseSalesReportComponent
  ],
  imports: [
    CommonModule,
    HsnCodeWiseSalesReportRoutingModule,
    FormsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatMenuModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
  ]
})
export class HsnCodeWiseSalesReportModule { }
