import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HsnCodeWiseSalesReportComponent } from './hsn-code-wise-sales-report.component';

const routes: Routes = [{ path: '', component: HsnCodeWiseSalesReportComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HsnCodeWiseSalesReportRoutingModule { }
