import { DatePipe } from '@angular/common';
import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { HsnCodewieseSalesReport } from 'src/app/modelclass/hsn-codewiese-sales-report';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-hsn-code-wise-sales-report',
  templateUrl: './hsn-code-wise-sales-report.component.html',
  styleUrls: ['./hsn-code-wise-sales-report.component.css']
})
export class HsnCodeWiseSalesReportComponent implements OnInit , OnDestroy {
  hsnCodewieseSalesReport:HsnCodewieseSalesReport[]=[];
  sub: Subscription = new Subscription;

  
  fromdate='';
  todate='';
  dateConvFist:any;
  dateConvSec:any;
  branchname='';
  getReportStatus=false;
  getexcelReportStatus=false;
  branchMst:BranchMst[]=[];
  

  constructor(private sharedserviceService:SharedserviceService,
    public datepipe: DatePipe) { }

  ngOnInit(): void {
    this.sharedserviceService.getBranchNames().subscribe(data => {this.branchMst=data})
  }

  getHsnCodeWisereport()
  { if (this.fromdate == null) {
    alert('Select the From Date!!!');
    return;
  }
  if (this.todate == null) {
    alert('Select the To Date!!!');
    return;
  }
  if (this.branchname == null) {
    alert('Select the Branch!!!');
    return;
  }

    this.getReportStatus=true;
    this.dateConvFist=this.datepipe.transform(this.fromdate , 'yyyy-MM-dd');
    this.dateConvSec=this.datepipe.transform(this.todate , 'yyyy-MM-dd');

    console.log(this.dateConvFist+'firstdate');
    console.log(this.dateConvSec+'sencondate')
  
this.sub=this.sharedserviceService.getHsnCodeWisesreport(this.dateConvFist,this.dateConvSec,this.branchname).
  subscribe(data => {this.hsnCodewieseSalesReport=data;
    if(this.hsnCodewieseSalesReport!=null)
    {
      this.getReportStatus=false;
      this.getexcelReportStatus=true;
    } })
  }

  renderPdf()
  {
    let element=document.getElementById('contentToConvert');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb:XLSX.WorkBook=XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb,ws,"sheet1");
    XLSX.writeFile(wb,"HSN-CodeWiseSales"+"-"+this.dateConvFist+'-'+this.dateConvSec+".xlsx")
  }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
    console.log('service unsubscribed..!!')
    }

}
