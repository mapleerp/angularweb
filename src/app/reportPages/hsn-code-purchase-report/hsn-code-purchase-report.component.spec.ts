import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HsnCodePurchaseReportComponent } from './hsn-code-purchase-report.component';

describe('HsnCodePurchaseReportComponent', () => {
  let component: HsnCodePurchaseReportComponent;
  let fixture: ComponentFixture<HsnCodePurchaseReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HsnCodePurchaseReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HsnCodePurchaseReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
