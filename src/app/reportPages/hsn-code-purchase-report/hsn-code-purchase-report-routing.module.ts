import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HsnCodePurchaseReportComponent } from './hsn-code-purchase-report.component';

const routes: Routes = [{ path: '', component: HsnCodePurchaseReportComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HsnCodePurchaseReportRoutingModule { }
