import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AccountHeads } from 'src/app/modelclass/account-heads';
import { CompanyMst } from 'src/app/modelclass/company-mst';
import { CustomerMst } from 'src/app/modelclass/customer-mst';
import { SalesBillDetails } from 'src/app/modelclass/sales-bill-details';
import { SalesDtl } from 'src/app/modelclass/sales-dtl';
import { SalesTransHdr } from 'src/app/modelclass/sales-trans-hdr';
import { ShippingAddress } from 'src/app/modelclass/shipping-address';
import { TaxSummaryDetails } from 'src/app/modelclass/tax-summary-details';
import { SalesDtlsService } from 'src/app/services/sales-dtls.service';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-whole-sales-bill-report',
  templateUrl: './whole-sales-bill-report.component.html',
  styleUrls: ['./whole-sales-bill-report.component.css'],
})
export class WholeSalesBillReportComponent implements OnInit {
  constructor(
    private salesDtlsService: SalesDtlsService,
    private sharedserviceService: SharedserviceService,
    public datepipe: DatePipe,
  ) {}
  salesBillDetails: SalesBillDetails = new SalesBillDetails();
  salesDtl: SalesDtl[] = [];
  salesTransHdr: SalesTransHdr = new SalesTransHdr();
  customerMst: AccountHeads = new AccountHeads();
  companyMst: CompanyMst = new CompanyMst();
  shipping: ShippingAddress = new ShippingAddress();
  taxSummaryDetails: TaxSummaryDetails[] = [];
  date:any;
  ngOnInit(): void {
    this.salesBillDetails = this.salesDtlsService.getWholeSaleReport();
    console.log("Wholesale bill details")
    console.log(this.salesBillDetails)
    this.salesDtl = this.salesBillDetails.arrayOfSalesDtl;
    this.salesTransHdr = this.salesBillDetails.salesTransHdr;
    this.customerMst = this.salesTransHdr.accountHeads;
    this.companyMst = this.salesTransHdr.companyMst;

    this.shipping = this.salesDtlsService.getShippingDtls();
    this.date=this.datepipe.transform(this.salesTransHdr.voucherDate , 'yyyy-MM-dd');
    console.log("voucher Number,branch,voucher date")
    console.log(this.salesTransHdr.voucherNumber,this.salesTransHdr.branchCode,this.date);
    this.sharedserviceService
      .getTaxSummaryDetails(
        this.salesTransHdr.voucherNumber,
        this.salesTransHdr.branchCode,
        this.date
      )
      .subscribe((data) => {
        this.taxSummaryDetails = data;
        console.log(data);
      });
  }

  print() {
    window.print();
  }

  back(){
    history.back();
  }
}
