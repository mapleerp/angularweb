import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WholeSalesBillReportComponent } from './whole-sales-bill-report.component';

describe('WholeSalesBillReportComponent', () => {
  let component: WholeSalesBillReportComponent;
  let fixture: ComponentFixture<WholeSalesBillReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WholeSalesBillReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WholeSalesBillReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
