import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DayEndSalesSummaryComponent } from './day-end-sales-summary.component';

const routes: Routes = [{ path: '', component: DayEndSalesSummaryComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DayEndSalesSummaryRoutingModule { }
