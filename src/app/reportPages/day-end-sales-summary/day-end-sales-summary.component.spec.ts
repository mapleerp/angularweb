import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DayEndSalesSummaryComponent } from './day-end-sales-summary.component';

describe('DayEndSalesSummaryComponent', () => {
  let component: DayEndSalesSummaryComponent;
  let fixture: ComponentFixture<DayEndSalesSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DayEndSalesSummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DayEndSalesSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
