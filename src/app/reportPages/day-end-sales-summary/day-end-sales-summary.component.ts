import { DatePipe } from '@angular/common';
import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DayEndWebReport } from 'src/app/modelclass/day-end-web-report';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-day-end-sales-summary',
  templateUrl: './day-end-sales-summary.component.html',
  styleUrls: ['./day-end-sales-summary.component.css']
})
export class DayEndSalesSummaryComponent implements OnInit,OnDestroy {

  sub: Subscription = new Subscription;

  dayEndWebReport:DayEndWebReport[]=[];
  getReportStatus=false;
  getexcelReportStatus=false;

  reportdate:any;
  dateConv:any;

 total:any=0;
 value:any;
 totalCashSales:any=0;
 totalCreditSales:any=0;
 totalCardSales:any=0;
 totalCard2:any=0;
 totalPhysicalCash:any=0;
  totalSales:any=0;

  constructor(private sharedserviceService:SharedserviceService,
    public datepipe: DatePipe) { }

  ngOnInit(): void {
  }

  getDayEndReports()
  {
   this. totalCashSales=0;
   this.totalCreditSales=0;
   this.totalCardSales=0;
   this.totalCard2=0;
   this.totalPhysicalCash=0;
   this.totalSales=0;
  this.getReportStatus=true;
  this.dateConv=this.datepipe.transform(this.reportdate , 'yyyy-MM-dd');
  
  this.sub=this.sharedserviceService.getDayEndReports(this.dateConv).
  subscribe(data => {this.dayEndWebReport=data ; this.findsum(this.dayEndWebReport); 
  console.log(this.dayEndWebReport[6]);
  if(this.dayEndWebReport!=null)
  {
    this.getReportStatus=false;
    this.getexcelReportStatus=true;
  }})

 
 
  }
  findsum(data: any)
{
  this.value=data;
  for(let j=0;j<data.length;j++){
    this.totalCashSales+= this.value[j].cashSales;
    this.totalCreditSales+=this.value[j].creditSales;
    this.totalCardSales+=this.value[j].cardSales;
    this.totalCard2+=this.value[j].card2;
    this.totalPhysicalCash+=this.value[j].physicalCash;
    this.totalSales+=this.value[j].totalSales;
  }
  
  console.log('Total Sum Taken')
}

renderPdf()
{
  let element=document.getElementById('contentToConvert');
  const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
  const wb:XLSX.WorkBook=XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb,ws,"sheet1");
  XLSX.writeFile(wb,"DayEndSalesSummary"+"-"+this.dateConv+".xlsx")

}

ngOnDestroy(): void {
  this.sub.unsubscribe();
  console.log('service unsubscribed..!!')
  }


}
