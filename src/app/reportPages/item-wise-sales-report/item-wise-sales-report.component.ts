import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { ItemWiseSalesReport } from 'src/app/modelclass/item-wise-sales-report';
import { ItemListPopUpComponent } from 'src/app/Pop-Up-Windows/item-list-pop-up/item-list-pop-up.component';
import {
  datas,
  SearchPopUpComponent,
} from 'src/app/Pop-Up-Windows/search-pop-up/search-pop-up.component';
import { SaleOrderPopupService } from 'src/app/services/sale-order-popup.service';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-item-wise-sales-report',
  templateUrl: './item-wise-sales-report.component.html',
  styleUrls: ['./item-wise-sales-report.component.css'],
})
export class ItemWiseSalesReportComponent implements OnInit {
  // itemWiseSalesReportForm: any = FormGroup;
  branch: BranchMst[] = [];
  // category: CategoryMst[] = [];
  groupname: any = [];
  todate: any;
  fromdate: any;
  branchname: any;
  categoryid: String = '';
  temp = '';
  itm: datas = new datas();
  receiveItm: any;
  itmName: any;
  itmBarCode: any;
  itmCess: number = 0.0;
  itmDiscount: any;
  itmTaxRate: number = 0.0;
  itmMrp: any;
  batch: any;
  itmId: any;
  item: any;
  // groupWiseSalesDtl: GroupWiseSalesDtl[] = [];
  itemWiseSalesReport: ItemWiseSalesReport[] = [];
  getexcelReportStatus = false;
  constructor(
    private sharedserviceService: SharedserviceService,
    public addDialog: MatDialog,
    private saleOrderPopupService: SaleOrderPopupService,
    public fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.getBranch();
    this.saleOrderPopupService.getItemName().subscribe((data) => {
      this.item = data;
    });
  }

  // onKeypressEventItem(event: any) {
  //   const dialogConfigAdd = new MatDialogConfig();
  //   dialogConfigAdd.disableClose = false;
  //   dialogConfigAdd.autoFocus = true;
  //   dialogConfigAdd.width = '80%';
  //   dialogConfigAdd.height = '70%';
  //   this.addDialog.open(ItemListPopUpComponent, dialogConfigAdd);
  //   console.log('hiiiiiiiiiiiiiiiiii ' + this.item);
  // }
  // receiveItemDtls() {
  //   console.log('receiveItemDtls');
  //   this.saleOrderPopupService.getItemDtls().subscribe((data: any) => {
  //     this.receiveItm = data;

  //     this.itmName = this.receiveItm.itemName;
  //     this.itmBarCode = this.receiveItm.barCode;

  //     this.itmCess = this.receiveItm.cess;
  //     this.itmDiscount = this.receiveItm.itemDiscount;
  //     this.itmTaxRate = this.receiveItm.taxRate;
  //     this.itmMrp = this.receiveItm.standardPrice;
  //     this.itmId = this.receiveItm[6];
  //     console.log(this.itmName + 'Selected item name');
  //     console.log(this.itmId + 'Item id');
  //     console.log(this.receiveItm[6]);
  //   });
  // }
  // getCategory() {
  //   this.sharedserviceService.getCategoryMst().subscribe((data: any) => {
  //     this.category = data;
  //   });
  // }

  getBranch() {
    this.sharedserviceService.getBranchNames().subscribe((data: any) => {
      this.branch = data;
    });
  }

  // addCategory() {
  //   this.sharedserviceService
  //     .getCategoryById(this.categoryid)
  //     .subscribe((data: any) => {
  //       if (this.groupname.indexOf(data.categoryName) === -1) {
  //         this.groupname.push(data.categoryName);

  //         this.temp = this.temp.concat(data.categoryName) + ';';
  //       }
  //       console.log(this.temp);
  //     });
  // }

  submit() {
    if (this.fromdate == null) {
      alert('Select the From Date!!!');
      return;
    }
    if (this.todate == null) {
      alert('Select the To Date!!!');
      return;
    }
    if (this.branchname == null) {
      alert('Select the Branch!!!');
      return;
    }
    this.sharedserviceService
      .getItemWiseSalesReport(this.fromdate, this.todate, this.branchname)
      .subscribe((data: any) => {
        // this.groupWiseSalesDtl = data;
        this.itemWiseSalesReport = data;
        console.log(data);
      });
  }

  renderPdf() {
    if(this.itemWiseSalesReport.length == 0){
      return;
    }
    let element = document.getElementById('contentToConvert');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'sheet1');
    XLSX.writeFile(
      wb,
      'ItemWiseSalesReport' + '-' + this.fromdate + '-' + this.todate + '.xlsx'
    );
  }
}
