import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SalesBillDetailsComponent } from './sales-bill-details.component';

const routes: Routes = [{ path: '', component: SalesBillDetailsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesBillDetailsRoutingModule { }
