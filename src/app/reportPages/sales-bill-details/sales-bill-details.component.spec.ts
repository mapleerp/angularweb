import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesBillDetailsComponent } from './sales-bill-details.component';

describe('SalesBillDetailsComponent', () => {
  let component: SalesBillDetailsComponent;
  let fixture: ComponentFixture<SalesBillDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesBillDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesBillDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
