import { DatePipe } from '@angular/common';
import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SalesBillDetails } from 'src/app/modelclass/sales-bill-details';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-sales-bill-details',
  templateUrl: './sales-bill-details.component.html',
  styleUrls: ['./sales-bill-details.component.css']
})
export class SalesBillDetailsComponent implements OnInit,OnDestroy {

  salesBillDetails:SalesBillDetails[]=[];
  sub: Subscription = new Subscription;

  
  todate='';
  dateConv:any;
 
  getReportStatus=false;
  getexcelReportStatus=false;

  constructor(private sharedserviceService:SharedserviceService,
    public datepipe: DatePipe) { }

  ngOnInit(): void {
  }
  getSalesBillDetailsreport()
  {
   
    this.getReportStatus=true;
    this.dateConv=this.datepipe.transform(this.todate , 'yyyy-MM-dd');
    
    this.sub= this.sharedserviceService.getSalesBillDtlReport(this.dateConv).
    subscribe(data => {this.salesBillDetails=data ;
      if(this.salesBillDetails!=null)
      {
        this.getReportStatus=false;
        this.getexcelReportStatus=true;
      }})
  
  
  }
  renderPdf()
  {

  }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
    console.log('service unsubscribed..!!')
    }

}
