import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PosInvoiceBillComponent } from './pos-invoice-bill.component';

describe('PosInvoiceBillComponent', () => {
  let component: PosInvoiceBillComponent;
  let fixture: ComponentFixture<PosInvoiceBillComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PosInvoiceBillComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PosInvoiceBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
