import { Component, OnInit } from '@angular/core';
import { SalesDtl } from 'src/app/modelclass/sales-dtl';
import { SharedserviceService } from 'src/app/services/sharedservice.service';

@Component({
  selector: 'app-pos-invoice-bill',
  templateUrl: './pos-invoice-bill.component.html',
  styleUrls: ['./pos-invoice-bill.component.css']
})
export class PosInvoiceBillComponent implements OnInit {

  constructor(private sharedserviceService:SharedserviceService) { }

  posBillSummary:SalesDtl[]=[]

  ngOnInit(): void {

    this.sharedserviceService.salesDtlfromShardService().subscribe(
      data=>{this.posBillSummary=data}
    )

   
  }
  totalAmount()
  {
    let ta=0;
    for (let entry of this.posBillSummary)
    {
        ta=entry.amount+ta
    }
    return ta;
  }

  printInvoivce()
  {
    window.print();
   
  }
}
