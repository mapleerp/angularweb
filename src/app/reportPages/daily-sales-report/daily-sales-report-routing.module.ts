import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DailySalesReportComponent } from './daily-sales-report.component';

const routes: Routes = [{ path: '', component: DailySalesReportComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DailySalesReportRoutingModule { }
