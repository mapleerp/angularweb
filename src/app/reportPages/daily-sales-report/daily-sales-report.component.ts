import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { DailySalesReport } from 'src/app/modelclass/daily-sales-report';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import * as fs from 'file-saver';
import * as XLSX from 'xlsx';
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-daily-sales-report',
  templateUrl: './daily-sales-report.component.html',
  styleUrls: ['./daily-sales-report.component.css']
})
export class DailySalesReportComponent implements OnInit,OnDestroy {

  sub: Subscription = new Subscription;

  dailySalesReport:DailySalesReport[]=[];
  branchMst:BranchMst[]=[];
  
  fromdate='';
  todate='';
  dateConvFist:any;
  dateConvSec:any;
  branchname='';
  getReportStatus=false;
  getexcelReportStatus=false;
 
  

  constructor(private sharedserviceService:SharedserviceService,
    public datepipe: DatePipe) { }

  ngOnInit(): void {

    this.sharedserviceService.getBranchNames(). subscribe(data => {this.branchMst=data})
    
  }

  getDailySalesreport()
  {

    if (this.fromdate == null) {
      alert('Select the From Date!!!');
      return;
    }
    if (this.todate == null) {
      alert('Select the To Date!!!');
      return;
    }
    if (this.branchname == null) {
      alert('Select the Branch!!!');
      return;
    }
    this.getReportStatus=true;
    
    this.dateConvFist=this.datepipe.transform(this.fromdate , 'yyyy-MM-dd');
    this.dateConvSec=this.datepipe.transform(this.todate , 'yyyy-MM-dd');

    console.log(this.dateConvFist+'firstdate');
    console.log(this.dateConvSec+'sencondate')
    this.sub=this.sharedserviceService.getDailySalesreport(this.dateConvFist,this.dateConvSec,this.branchname).
  subscribe(data => {this.dailySalesReport=data;
    if(this.dailySalesReport!=null)
    {
      this.getReportStatus=false;
      this.getexcelReportStatus=true;
    }  })

   
  }

  renderPdf()
  {
    let element=document.getElementById('contentToConvert');
  const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
  const wb:XLSX.WorkBook=XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb,ws,"sheet1");
  XLSX.writeFile(wb,"DailySales"+"-"+this.dateConvFist+'-'+this.dateConvSec+".xlsx")
  }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
    console.log('service unsubscribed..!!')
    }

  }
 


