import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatementsOfaccountComponent } from './statements-ofaccount.component';

describe('StatementsOfaccountComponent', () => {
  let component: StatementsOfaccountComponent;
  let fixture: ComponentFixture<StatementsOfaccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatementsOfaccountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatementsOfaccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
