import { DatePipe } from '@angular/common';
import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { AccountBalanceReport } from 'src/app/modelclass/account-balance-report';
import { AccountHeads } from 'src/app/modelclass/account-heads';
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { CustomerMst } from 'src/app/modelclass/customer-mst';
import { StatementOfAccount } from 'src/app/modelclass/statement-of-account';
import { CustomerDetailsPopUpComponent } from 'src/app/Pop-Up-Windows/customer-details-pop-up/customer-details-pop-up.component';
import { SaleOrderPopupService } from 'src/app/services/sale-order-popup.service';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-statements-ofaccount',
  templateUrl: './statements-ofaccount.component.html',
  styleUrls: ['./statements-ofaccount.component.css'],
})
export class StatementsOfaccountComponent implements OnInit, OnDestroy {
  // statementOfAccount: StatementOfAccount[] = [];
  statementOfAccount: AccountBalanceReport[] = [];
  branchMst: BranchMst[] = [];
  sub: Subscription = new Subscription();
  fromdate = '';
  todate = '';
  dateConvFist: any;
  dateConvSec: any;
  branchname = '';
  getReportStatus = false;
  getexcelReportStatus = false;
  selectCustomer: any;
  cust: AccountHeads = new AccountHeads();
  receivecust: any;
  selectcustName: any;
  selectCustomerId: any;

  constructor(
    private sharedserviceService: SharedserviceService,
    public addDialog1: MatDialog,
    private saleOrderPopupService: SaleOrderPopupService,
    public datepipe: DatePipe
  ) {}

  ngOnInit(): void {
    this.sharedserviceService.getBranchNames().subscribe((data) => {
      this.branchMst = data;
    });

    this.receiveCustomerDtls();
  }

  getStatementofAccountreport() {
    this.getReportStatus = true;

    this.dateConvFist = this.datepipe.transform(this.fromdate, 'yyyy-MM-dd');
    this.dateConvSec = this.datepipe.transform(this.todate, 'yyyy-MM-dd');

    this.sub = this.sharedserviceService
      .getStatementofAccountReport(
        this.dateConvFist,
        this.dateConvSec,
        this.branchname,
        this.selectCustomerId
      )
      .subscribe((data) => {
        this.statementOfAccount = data;
        if (this.statementOfAccount != null) {
          this.getReportStatus = false;
          this.getexcelReportStatus = true;
        }
      });
  }
  renderPdf() {
    let element = document.getElementById('contentToConvert');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'sheet1');
    XLSX.writeFile(
      wb,
      'DailySales' + '-' + this.dateConvFist + '-' + this.dateConvSec + '.xlsx'
    );
  }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
    console.log('service unsubscribed..!!');
  }

  onKeypressEventForCustmer(x: any) {
    const dialogConfigAdd1 = new MatDialogConfig();
    dialogConfigAdd1.disableClose = false;
    dialogConfigAdd1.autoFocus = true;
    dialogConfigAdd1.position = { right: `10px`, top: `20px` };
    dialogConfigAdd1.width = '40%';
    dialogConfigAdd1.height = '100%';
    dialogConfigAdd1.data = this.cust;
    //this.addDialog1.open(SearchPopUpComponent,{ disableClose: true });
    this.addDialog1.open(CustomerDetailsPopUpComponent, dialogConfigAdd1);

    this.addDialog1.afterAllClosed.subscribe((result: any) => {
      this.cust = result;
      console.log('pppppooopu called' + this.cust);
    });

    console.log('customerpopupcalled');
  }

  receiveCustomerDtls() {
    console.log('receiveCustomerDtls');
    this.saleOrderPopupService.getCustomerDtls().subscribe((data: any) => {
      this.receivecust = data;
      console.log(this.receivecust.accountName + 'nnnanaaammmeee');
      this.selectCustomer = this.receivecust.accountName;
      this.selectCustomerId = this.receivecust.id;
    });
  }
}
