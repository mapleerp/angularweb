import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StatementsOfaccountComponent } from './statements-ofaccount.component';

const routes: Routes = [{ path: '', component: StatementsOfaccountComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StatementsOfaccountRoutingModule { }
