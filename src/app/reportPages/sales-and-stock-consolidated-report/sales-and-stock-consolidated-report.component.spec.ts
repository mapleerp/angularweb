import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesAndStockConsolidatedReportComponent } from './sales-and-stock-consolidated-report.component';

describe('SalesAndStockConsolidatedReportComponent', () => {
  let component: SalesAndStockConsolidatedReportComponent;
  let fixture: ComponentFixture<SalesAndStockConsolidatedReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesAndStockConsolidatedReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesAndStockConsolidatedReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
