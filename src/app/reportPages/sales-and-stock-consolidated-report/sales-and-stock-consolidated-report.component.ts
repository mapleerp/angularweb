import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

import { BranchMst } from 'src/app/modelclass/branch-mst';
import { SalesAndStockConsolidatedReport } from 'src/app/modelclass/sales-and-stock-consolidated-report';
import { ItemListPopUpComponent } from 'src/app/Pop-Up-Windows/item-list-pop-up/item-list-pop-up.component';
import { SaleOrderPopupService } from 'src/app/services/sale-order-popup.service';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-sales-and-stock-consolidated-report',
  templateUrl: './sales-and-stock-consolidated-report.component.html',
  styleUrls: ['./sales-and-stock-consolidated-report.component.css']
})
export class SalesAndStockConsolidatedReportComponent implements OnInit {
  branch: BranchMst[] = [];
  groupname: any = [];
  todate: any;
  fromdate: any;
  frombranchname: any;
  categoryid: String = '';
  temp = '';
  // itm: datas = new datas();
  receiveItm: any;
  itmName: any;
  itmBarCode: any;
  itmCess: number = 0.0;
  itmDiscount: any;
  itmTaxRate: number = 0.0;
  itmMrp: any;
  batch: any;
  itmId: any;
  item: String;
  itemList:any;
  salesAndStockConsolidatedReport : SalesAndStockConsolidatedReport[]=[]; 

  // groupWiseSalesDtl: GroupWiseSalesDtl[] = [];
  // itemWiseSalesReport: ItemWiseSalesReport[] = [];
  getexcelReportStatus = false;
  variable: any;
  constructor( private sharedserviceService: SharedserviceService,
    public addDialog: MatDialog,
    private saleOrderPopupService: SaleOrderPopupService,) { }

  ngOnInit(): void {
    this.getBranch();
    this.saleOrderPopupService.getItemName().subscribe((data) => {
      this.item = data;
    });
  }

  getBranch() {
    this.sharedserviceService.getBranchNames().subscribe((data: any) => {
      this.branch = data;
    });
  }

  onKeypressEventItem(event: any) {
      const dialogConfigAdd = new MatDialogConfig();
      dialogConfigAdd.disableClose = false;
      dialogConfigAdd.autoFocus = true;
      dialogConfigAdd.width = '80%';
      dialogConfigAdd.height = '70%';
      this.addDialog.open(ItemListPopUpComponent, dialogConfigAdd);
      console.log('hiiiiiiiiiiiiiiiiii ' + this.item);
    }
    addItem() {
      console.log('print ' + this.item);

      if(typeof this.item=="undefined")
      {
             return;
      }
      let flag=0;
      if(this.groupname.length ==0)
      {
        this.groupname.push(this.item);
        
     
      console.log(this.temp);
      return;
      }
      for(let i = 0 ; i < this.groupname.length ; i++) {

       
        if(this.groupname[i] === this.item) {
               flag++
         
         
        }
      
      }

      if(flag==0)
      {
        this.groupname.push(this.item);
       
     
      console.log(this.temp);

      }


      
  }
  itemDeletion(name:any){


    for( let j =0; j< this.groupname.length;j++ ){
      if(this.groupname[j]===name){
      
      this.groupname.splice(j,1);
      }
    }


  }
  submit() {
    this.salesAndStockConsolidatedReport.length=0;
    this.temp = '';
    if (this.fromdate == null) {
      alert('Select the From Date!!!');
      return;
    }
    if (this.todate == null) {
      alert('Select the To Date!!!');
      return;
    }
    if (this.frombranchname == null) {
      alert('Select the Branch!!!');
      return;
    }
    for( let i =0; i< this.groupname.length;i++ ){
      this.temp = this.temp.concat(this.groupname[i]) + ';';
    }
    
    this.sharedserviceService
      // .getSalesAndStockConsolidatedReport(this.fromdate, this.todate, this.frombranchname,this.temp)
      // .subscribe((data: any) => {
      
      //   this.salesAndStockConsolidatedReport = data;
      //   console.log(data);
      // });
  }

  renderPdf() {
    if(this.salesAndStockConsolidatedReport.length == 0){
      return;
    }
    let element = document.getElementById('contentToConvert');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'sheet1');
    XLSX.writeFile(
      wb,
      'SalesAndStockConsolidatedReport' + '-' + this.fromdate + '-' + this.todate + '.xlsx'
    );
  }

}
