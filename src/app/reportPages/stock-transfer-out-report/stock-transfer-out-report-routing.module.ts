import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockTransferOutReportComponent } from './stock-transfer-out-report.component';

const routes: Routes = [{ path: '', component: StockTransferOutReportComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockTransferOutReportRoutingModule { }
