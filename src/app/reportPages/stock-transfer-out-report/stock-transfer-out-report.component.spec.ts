import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockTransferOutReportComponent } from './stock-transfer-out-report.component';

describe('StockTransferOutReportComponent', () => {
  let component: StockTransferOutReportComponent;
  let fixture: ComponentFixture<StockTransferOutReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockTransferOutReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockTransferOutReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
