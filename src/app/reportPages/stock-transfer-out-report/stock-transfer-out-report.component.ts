import { DatePipe } from '@angular/common';
import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BranchMst } from 'src/app/modelclass/branch-mst';
import { StockTransferOutReport } from 'src/app/modelclass/stock-transfer-out-report';
import { SharedserviceService } from 'src/app/services/sharedservice.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-stock-transfer-out-report',
  templateUrl: './stock-transfer-out-report.component.html',
  styleUrls: ['./stock-transfer-out-report.component.css']
})
export class StockTransferOutReportComponent implements OnInit, OnDestroy {
  stockTransferOutReport:StockTransferOutReport[]=[];
  branchMst:BranchMst[]=[];
  sub: Subscription = new Subscription;
  
  fromdate='';
  todate='';
  dateConvFist:any;
  dateConvSec:any;
  branchname='';
  getReportStatus=false;
  getexcelReportStatus=false;

  constructor(private sharedserviceService:SharedserviceService,
    public datepipe: DatePipe) { }

  ngOnInit(): void {
    this.sharedserviceService.getBranchNames().subscribe(data => {this.branchMst=data})
  }

  getStockTransferOutDetailreport()
  {

    this.getReportStatus=true;
    this.dateConvFist=this.datepipe.transform(this.fromdate , 'yyyy-MM-dd');
    this.dateConvSec=this.datepipe.transform(this.todate , 'yyyy-MM-dd');

    console.log(this.dateConvFist+'firstdate');
    console.log(this.dateConvSec+'sencondate')
    this.sub=this.sharedserviceService.getStockTransferOutreport(this.dateConvFist,this.dateConvSec,this.branchname).
  subscribe(data => {this.stockTransferOutReport=data ;
    if(this.stockTransferOutReport!=null)
    {
      this.getReportStatus=false;
      this.getexcelReportStatus=true;
    }})
  }

  renderPdf()
  {
    let element=document.getElementById('contentToConvert');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb:XLSX.WorkBook=XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb,ws,"sheet1");
    XLSX.writeFile(wb,"StockTransferOut"+"-"+this.dateConvFist+'-'+this.dateConvSec+".xlsx")
  }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
    console.log('service unsubscribed..!!')
    }

}
